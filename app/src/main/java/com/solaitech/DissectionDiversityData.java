package com.solaitech;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Random;
import java.util.Scanner;

public class DissectionDiversityData {
    static int theNumberOfBlocks;
    static int[] keyColumn;
    static int theNumberOfColumns;
    static int[] keyRow;
    static String theInputString[];
    static int maxSizeArray; //jagged array[][this size]
    static String[][] arrayKeys;
    static int lengthRow;//jagged array[this size][] and for corect table key-value
    static ArrayList<String> DictionaryBlockValue;
    static PrintWriter sw;//For record everything in the file

    //Because values must not be repeated, check there is the element or not
    public static boolean SearchForValuesInAnArray(int[] arr, int value) {
        for (int i = 0; i < arr.length; i++)
            if (value == arr[i])
                return false;
        return true;
    }

    //to fill key columns and rows of keys
    public static void MyRandom(int[] arr, int maxValue) {
        int counter = 1;
        int temp;
        Random rnd = new Random();
        arr = new int[maxValue];
        arr[0] = rnd.nextInt(maxValue + 1);
        while (counter != maxValue) {
            temp = rnd.nextInt(maxValue + 1);
            if (SearchForValuesInAnArray(arr, temp))
                arr[counter++] = temp;
        }
    }

    public static String padRight(String s, int n) {
        return String.format("%1$-" + n + "s", s);
    }

    //filling of the massif = to values of lines and columns
    //and
    //output table key-value
    public static void SetKeysAndPrint() {
        int row = keyRow[0], column = 0, counterlengthString = 0, valueForKeysRows = keyRow[0], counterForPrint = -1, counterKeyRowInTable = -1;

        //indentation from before the column numbers for correct display
        if (keyRow.length < 10) {
            System.out.println(" |");
            sw.print(" |");
        } else if (keyRow.length >= 10 && keyRow.length < 100) {
            System.out.println("  |");
            sw.print("  |");
        } else if (keyRow.length >= 100 && keyRow.length < 1000) {
            System.out.println("   |");
            sw.print("   |");
        }
        //the end of a space before numbers of columns

        //display of values of columns
        for (int count = 0; count < theNumberOfColumns; count++) {
            if (keyColumn.length < 10) {
                System.out.print("{" + keyColumn[count] + "}|");
                sw.print("{" + keyColumn[count] + "}|");
            } else if (keyColumn.length >= 10 && keyColumn.length < 100) {
                if (keyColumn[count] < 10) {
                    System.out.print(" {" + keyColumn[count] + "}|");
                    sw.print(" {" + keyColumn[count] + "}|");
                } else if (keyColumn[count] >= 10) {
                    System.out.print("{" + keyColumn[count] + "}|");
                    sw.print("{" + keyColumn[count] + "}|");
                }
            } else if (keyColumn.length >= 100 && keyColumn.length < 1000) {
                if (keyColumn[count] < 10) {
                    System.out.print("  {" + keyColumn[count] + "}|");
                    sw.print("  {" + keyColumn[count] + "}|");
                } else if (keyColumn[count] >= 10 && keyColumn[count] < 100) {
                    System.out.print(" {" + keyColumn[count] + "}|");
                    sw.print(" {" + keyColumn[count] + "}|");
                } else {
                    System.out.print("{" + keyColumn[count] + "}|");
                    sw.print("{" + keyColumn[count] + "}|");
                }
            } else if (keyColumn.length >= 1000 && keyColumn.length < 10000) {
                if (keyColumn[count] < 10) {
                    System.out.print("   {" + keyColumn[count] + "}|");
                    sw.print("   {" + keyColumn[count] + "}|");
                } else if (keyColumn[count] >= 10 && keyColumn[count] < 100) {
                    System.out.print("  {" + keyColumn[count] + "}|");
                    sw.print("  {" + keyColumn[count] + "}|");
                } else if (keyColumn[count] >= 100 && keyColumn[count] < 1000) {
                    System.out.print(" {" + keyColumn[count] + "}|");
                    sw.print(" {" + keyColumn[count] + "}|");
                } else {
                    System.out.print("{" + keyColumn[count] + "}|");
                    sw.print("{" + keyColumn[count] + "}|");
                }
                //end of display of values of columns
            }
        }

        System.out.println();
        sw.println();

        //approximate filling with a symbol "-" after numbers of columns
        if (keyRow.length < 10) {
            System.out.println(padRight("-", keyColumn.length * 4) + "-");
            sw.println(padRight("-", keyColumn.length * 4) + "-");
        }
        //end filling

        //for corect display table key-value.        The number of rows = number of rows / number of columns
        for (int rows = 0; rows < lengthRow; rows++) {
            //Check for the correct filling of numbers of rows
            if (counterKeyRowInTable < keyRow.length - 1)
                counterKeyRowInTable++;
            else
                counterKeyRowInTable = 0;

            //validate display of value for keys rows
            if (valueForKeysRows > keyRow.length) {
                ++rows;
                valueForKeysRows = keyRow[0];
            }
            valueForKeysRows = keyRow[counterKeyRowInTable];

            //display of values of rows
            if (keyRow.length < 10) {
                System.out.print("{" + valueForKeysRows + "}|");
                sw.print("{" + valueForKeysRows + "}|");
            } else if (keyRow.length >= 10 && keyRow.length < 100) {
                if (valueForKeysRows < 10) {
                    System.out.print(" {" + valueForKeysRows + "}|");
                    sw.print(" {" + valueForKeysRows + "}|");
                } else {
                    System.out.print("{" + valueForKeysRows + "}|");
                    sw.print("{" + valueForKeysRows + "}|");
                }
            } else if (keyRow.length >= 100 && keyRow.length < 1000) {
                if (valueForKeysRows < 10) {
                    System.out.print("  {" + valueForKeysRows + "}|");
                    sw.print("  {" + valueForKeysRows + "}|");
                } else if (valueForKeysRows >= 10 && valueForKeysRows < 100) {
                    System.out.print(" {" + valueForKeysRows + "}|");
                    sw.print(" {" + valueForKeysRows + "}|");
                } else {
                    System.out.print("{" + valueForKeysRows + "}|");
                    sw.print("{" + valueForKeysRows + "}|");
                }
            }
            //end display of values of rows

            //filling of the massif of keys
            for (int j = 0; j < keyColumn.length; j++) {
                row = keyRow[counterKeyRowInTable] - 1;
                column = keyColumn[j] - 1;
                if (counterlengthString == theInputString.length)
                    break;
                arrayKeys[row][column] += theInputString[counterlengthString++];
                //end filling of the massif of keys

                //The output of the input string in the table
                if (keyColumn.length < 10) {
                    System.out.print("{" + theInputString[++counterForPrint] + "}|");
                    sw.print("{" + theInputString[++counterForPrint] + "}|");
                } else if (keyColumn.length >= 10 && keyColumn.length < 100) {
                    System.out.print(" {" + theInputString[++counterForPrint] + "}|");
                    sw.print("{" + theInputString[++counterForPrint] + "}|");
                } else if (keyColumn.length >= 100 && keyColumn.length < 1000) {
                    System.out.print("  {" + theInputString[++counterForPrint] + "}|");
                    sw.print("{" + theInputString[++counterForPrint] + "}|");
                } else if (keyColumn.length >= 1000 && keyColumn.length < 10000) {
                    System.out.print("   {" + theInputString[++counterForPrint] + "}|");
                    sw.print("{" + theInputString[++counterForPrint] + "}|");
                }
                //End output
            }
            System.out.println();
            sw.println();
        }
        //end display corect table key-value.
    }

    //formation of blocks and display
    public static boolean FormingBlocks() {
        //sw = File.CreateText("Block.txt");
        int row = 0, column = 0;
        int k = 0;
        System.out.println("\nFormed blocks:");
        sw.println("\nFormed blocks:");
        System.out.println("Block number - Contents\n");
        sw.println("Block number - Contents\n");

        for (int i = 0; i < keyRow.length; i++) {
            row = keyRow[i] - 1;
            for (int j = 0; j < keyColumn.length; j++) {
                column = keyColumn[j] - 1;
                k = (theNumberOfColumns * (row + 1 - 1) + column + 1);//k =  n*(r(j) - 1) + s
                DictionaryBlockValue.add(k - 1, arrayKeys[row][column]);//record in the dictionary a key - value (formation of blocks)
            }
        }

        //key-value displaying
        for (int counnt = 0; counnt < DictionaryBlockValue.size(); counnt++) {
            System.out.println("{" + counnt + 1 + "} - {" + DictionaryBlockValue.get(counnt) + "}");
            sw.println("{" + counnt + 1 + "} - {" + DictionaryBlockValue.get(counnt) + "}");
        }
        //end key-value displaying
        sw.close();
        return false;
    }

    //input of basic data
    public static boolean InputData() throws FileNotFoundException {
        sw = new PrintWriter(new File("Note.txt"));
        sw.println("hello");
        sw.close();


        System.out.println("The number of blocks has to share totally on number of columns!");
        System.out.println("Enter the number of blocks: ");
        theNumberOfBlocks = sc.nextInt();
        System.out.println("Enter the number of columns: ");
        theNumberOfColumns = sc.nextInt();
        if (theNumberOfBlocks % theNumberOfColumns != 0)
            InputData();
        System.out.println("String length must be greater than the number of blocks!");
        System.out.println("Enter the input string:");
        theInputString[0] = sc.nextLine();
        if (theInputString.length <= theNumberOfBlocks) {
            System.out.println("Incorrect string length!");
            InputData();
        }
        MyRandom(keyColumn, theNumberOfColumns);
        MyRandom(keyRow, theNumberOfBlocks / theNumberOfColumns);
        maxSizeArray = keyColumn.length + 1;
        lengthRow = (int) (Math.ceil((double) (theInputString.length) / (double) (keyColumn.length)));
        arrayKeys = new String[lengthRow][];
        for (int i = 0; i < lengthRow; i++)
            arrayKeys[i] = new String[maxSizeArray];
        DictionaryBlockValue = new ArrayList<>();

        sw.println("the number of blocks = {" + theNumberOfBlocks + "}");
        sw.println("the number of columns = {" + theNumberOfColumns + "}");
        sw.println("the input string:{" + theInputString + "}\n\n\n");

        SetKeysAndPrint();
        FormingBlocks();
        return false;
    }

    //start
    public static void Run() {
        try {
            while (InputData()) ;
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    static Scanner sc;

    public static void main(String[] args) {
        sc = new Scanner(System.in);
        try {
            Run();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}

