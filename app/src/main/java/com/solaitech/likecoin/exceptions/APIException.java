package com.solaitech.likecoin.exceptions;

public class APIException extends Exception {

    private String errorDescr;

    public APIException(String errorDescr) {
        this.errorDescr = errorDescr;
    }

    public String getErrorDescr() {
        return errorDescr;
    }

}