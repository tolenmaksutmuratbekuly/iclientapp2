package com.solaitech.likecoin.interfaces;

/**
 * Created by sultanbek on 11.04.17.
 */

public interface OnSwipeRefreshLayoutListener {

    void onSwipeRefresh();

}