package com.solaitech.likecoin.interfaces;

public interface OnItemClickListener {
    void onItemClick(Object object);
}