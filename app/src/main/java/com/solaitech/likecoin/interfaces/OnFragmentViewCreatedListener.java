package com.solaitech.likecoin.interfaces;

public interface OnFragmentViewCreatedListener {
    void onFragmentViewCreated();
}