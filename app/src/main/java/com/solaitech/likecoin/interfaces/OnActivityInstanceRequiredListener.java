package com.solaitech.likecoin.interfaces;

import android.app.Activity;
import android.view.View;

/**
 * Created by sultanbek on 12.04.17.
 */

public interface OnActivityInstanceRequiredListener {

    Activity getCurrentActivity();

    View getAppbar();

}