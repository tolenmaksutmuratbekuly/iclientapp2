package com.solaitech.likecoin.ui.profile.edit_profile;

import com.solaitech.likecoin.data.models.user.User;
import com.solaitech.likecoin.network.API;
import com.solaitech.likecoin.utils.EmailUtils;
import rx.subscriptions.CompositeSubscription;

class EditProfilePresenter {
    private EditProfileView view;
    private API api;
    private CompositeSubscription compositeSubscription = new CompositeSubscription();

    EditProfilePresenter(EditProfileView view, API api) {
        this.view = view;
        this.api = api;
    }

    public void init() {
        User user = view.getUser();

        if (user != null) {
            if (user.getEmail() != null) {
                view.showEmail(user.getEmail());
            }

            if (user.getName() != null) {
                view.showName(user.getName());
            }

            if (user.getSurname() != null) {
                view.showSurname(user.getSurname());
            }
        }
    }

    void onUpdateProfileButtonClick() {
        boolean isEmailOk = isEmailOk();
        boolean isNameOk = isNameOk();
        boolean isSurnameOk = isSurnameOk();

        if (isEmailOk && isNameOk && isSurnameOk) {
            updateUser();
        } else {
            if (view.getEnteredEmail().isEmpty()) {
                view.showEmailEmptyError();
            } else if (!EmailUtils.isEmailValid(view.getEnteredEmail())) {
                view.showEmailInvalidError();
            }

            if (!isNameOk) {
                view.showNameEmptyError();
            }

            if (!isSurnameOk) {
                view.showSurnameEmptyError();
            }
        }
    }

    private boolean isEmailOk() {
        return (!view.getEnteredEmail().isEmpty()
                && EmailUtils.isEmailValid(view.getEnteredEmail()));
    }

    private boolean isNameOk() {
        return !view.getEnteredName().isEmpty();
    }

    private boolean isSurnameOk() {
        return !view.getEnteredSurname().isEmpty();
    }

    private void updateUser() {
        view.startService(view.getEnteredName(), view.getEnteredSurname(), view.getEnteredEmail());
        view.closeDialog();
//        Subscription subscription = UserRepositoryProvider.provideRepository(api)
//                .updateUserInfo(getUpdateUserInfoParams())
//                .doOnSubscribe(new Action0() {
//                    @Override
//                    public void call() {
//                        view.showProgressDialog();
//                    }
//                })
//                .doOnTerminate(new Action0() {
//                    @Override
//                    public void call() {
//                        view.dismissProgressDialog();
//                    }
//                })
//                .subscribe(new Subscriber<User>() {
//                    @Override
//                    public void onCompleted() {
//                        Logger.e(getClass(), "onCompleted");
//                    }
//
//                    @Override
//                    public void onError(Throwable e) {
//                        Logger.e(getClass(), "onError");
//                        PresenterErrorHandler.handleError2(e, view);
//                    }
//
//                    @Override
//                    public void onNext(User user) {
//                        Logger.e(getClass(), "onNext");
//                        view.showUserUpdateSuccessToastMessage();
//                        view.closeDialog();
//                    }
//                });
//        compositeSubscription.add(subscription);
    }

//    private UpdateUserInfoParams getUpdateUserInfoParams() {
//        return new UpdateUserInfoParams(view.getEnteredFullName(), view.getEnteredEmail());
//    }

    public void onDismiss() {
        unsubscribe();
    }

    private void unsubscribe() {
        if (compositeSubscription != null
                && !compositeSubscription.isUnsubscribed()) {
            compositeSubscription.unsubscribe();
        }
    }

}