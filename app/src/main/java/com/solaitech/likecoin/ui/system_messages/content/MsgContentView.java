package com.solaitech.likecoin.ui.system_messages.content;

import android.content.Context;

import com.solaitech.likecoin.data.models.article.Article;

interface MsgContentView {

    Article getArticleDetail();

    void showTitle(String title);

    void showShortDescription(String description);

    void showContent(String content);

    void showDate(String regDate);

    void hideTitle();

    void hideShortDescription();

    void hideContent();

    void hideDate();

    Context getContextFromPage();
}