package com.solaitech.likecoin.ui.profile.level;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.util.Pair;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;

import com.solaitech.likecoin.ui.menu.MainActivity;
import com.solaitech.likecoin.ui.profile.brix.BrixActivity;
import com.solaitech.likecoin.views.CircularProgressBar;

import butterknife.Bind;
import butterknife.ButterKnife;
import com.solaitech.likecoin.App;
import com.solaitech.likecoin.R;
import com.solaitech.likecoin.data.models.user.User;
import com.solaitech.likecoin.ui.base.BaseActivityFrgm;
import com.solaitech.likecoin.ui.header.ProfileHeaderViewGroup;
import com.solaitech.likecoin.ui.profile.ProfileActivity;
import com.solaitech.likecoin.ui.profile.edit_profile_image.UserInfoUpdate;
import com.solaitech.likecoin.ui.profile.user_statement.UserStatementActivity;
import com.solaitech.likecoin.utils.CustomColorUtils;
import com.solaitech.likecoin.utils.StringUtils;

public class LevelActivity extends BaseActivityFrgm implements LevelView, UserInfoUpdate {
    @Bind(R.id.v_profile) ProfileHeaderViewGroup v_profile;
    @Bind(R.id.tv_level_main) TextView tv_level_main;
    @Bind(R.id.tv_like_count) TextView tv_like_count;
    @Bind(R.id.tv_count) TextView tv_count;
    @Bind(R.id.tv_max_transaction) TextView tv_max_transaction;
    @Bind(R.id.tv_till_next_level) TextView tv_till_next_level;
    @Bind(R.id.cpb_next_level) CircularProgressBar cpb_next_level;
    private LevelPresenter presenter;

    public static Intent getIntent(Context context) {
        return new Intent(context, LevelActivity.class);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_level);
        ButterKnife.bind(this);
        v_profile.setOnUpdateUserInfo(this);
        presenter = new LevelPresenter(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        v_profile.onResume();
    }

    @Override
    protected void onProfileImageUpdated() {
        super.onProfileImageUpdated();
        v_profile.onProfileImageUpdated();
    }

    @Override
    protected void onProfileUpdated() {
        super.onProfileUpdated();
        v_profile.onProfileUpdated();
    }

    ////////////////////////////////////////////////
    /// LevelView                                ///
    ////////////////////////////////////////////////

    @Override
    public User getUser() {
        App app = (App) getApplicationContext();
        return app.getUser();
    }

    @Override
    public void showUserLevel(int userLevel) {
        tv_count.setText(String.valueOf(userLevel * 10));
        tv_level_main.setText(String.valueOf(userLevel));
        cpb_next_level.setColor(CustomColorUtils.getLevelColor(this, userLevel));
        tv_max_transaction.setText(String.valueOf(userLevel));
    }

    @Override
    public void showLikesAmount(String likeCount) {
        tv_like_count.setText(likeCount);
    }

    @Override
    public void showCurrentLevelProgress(int progress) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            cpb_next_level.setProgressWithAnimation(progress, 1500);
        } else {
            cpb_next_level.setProgress(progress);
        }
    }

    @Override
    public void showNextLevelResidueLike(String amount) {
        tv_till_next_level.setText(amount);
    }

    ////////////////////////////////////////////////
    /// UserInfoUpdate                           ///
    ////////////////////////////////////////////////

    @Override
    public void onUpdateUserInfo(User user) {
        presenter.setUserInfo(user);
    }

    @Override
    public void openProfileActivity() {
        intentOpener(ProfileActivity.getIntent(this));
    }

    @Override
    public void openUserStatementActivity() {
        intentOpener(UserStatementActivity.getIntent(this));
    }

    @Override
    public void openLevelActivity() {

    }

    @Override
    public void openBrixActivity() {
        intentOpener(BrixActivity.getIntent(this));
    }

    private void intentOpener(Intent intent) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            Pair<View, String> p1 = Pair.create(findViewById(R.id.v_profile), getString(R.string.tn_profile));
            ActivityOptionsCompat options = ActivityOptionsCompat.makeSceneTransitionAnimation(this, p1);
            startActivity(intent, options.toBundle());
        } else {
            startActivity(intent);
        }
        finish();
    }

    @Override
    public void initToolbar(Toolbar toolbar) {
        toolbar.setTitle(StringUtils.EMPTY_STRING);
        toolbar.setNavigationIcon(R.drawable.ic_chevron_left_white_24dp);
        setSupportActionBar(toolbar);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goBack();
            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        goBack();
    }

    private void goBack() {
        if (isTaskRoot()) {
            startActivity(MainActivity.getIntent(this));
        } else {
            supportFinishAfterTransition();
        }
    }
}