package com.solaitech.likecoin.ui.user_page;

import android.content.Context;
import android.content.Intent;
import android.graphics.Paint;
import android.graphics.drawable.GradientDrawable;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.util.Pair;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.solaitech.likecoin.App;
import com.solaitech.likecoin.R;
import com.solaitech.likecoin.data.models.campaign.Merchant;
import com.solaitech.likecoin.data.models.enums.PartnerTypes;
import com.solaitech.likecoin.data.models.user.User;
import com.solaitech.likecoin.data.preferences.Preferences;
import com.solaitech.likecoin.data.preferences.PreferencesImpl;
import com.solaitech.likecoin.network.API;
import com.solaitech.likecoin.network.RetrofitServiceGenerator;
import com.solaitech.likecoin.ui.base.BaseActivityFrgm;
import com.solaitech.likecoin.ui.contacts.send_like.SendLikeDialog;
import com.solaitech.likecoin.ui.header.ProfileHeaderViewGroup;
import com.solaitech.likecoin.ui.profile.ProfileActivity;
import com.solaitech.likecoin.ui.profile.brix.BrixActivity;
import com.solaitech.likecoin.ui.profile.edit_profile_image.UserInfoUpdate;
import com.solaitech.likecoin.ui.profile.level.LevelActivity;
import com.solaitech.likecoin.ui.profile.user_statement.UserStatementActivity;
import com.solaitech.likecoin.utils.CustomColorUtils;
import com.solaitech.likecoin.utils.ImageUtils;
import com.solaitech.likecoin.utils.ToastUtils;
import com.solaitech.likecoin.utils.animation.AvatarAnimation;
import com.solaitech.likecoin.utils.chrome_customtabs.ChromeCustomTabs;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;

public class UserActivity extends BaseActivityFrgm implements UserView, UserInfoUpdate {
    @Bind(R.id.pb_indicator) ViewGroup pb_indicator;
    @Bind(R.id.v_profile) ProfileHeaderViewGroup v_profile;
    @Bind(R.id.iv_prof_img) CircleImageView iv_prof_img;
    @Bind(R.id.tv_name) TextView tv_name;
    @Bind(R.id.tv_description) TextView tv_description;
    @Bind(R.id.tv_url) TextView tv_url;
    @Bind(R.id.tv_level) TextView tv_level;
    @Bind(R.id.btn_like) Button btn_like;
    @Bind(R.id.ll_ambassador) LinearLayout ll_ambassador;
    @Bind(R.id.img_anim_coin) ImageView img_anim_coin;
    @Bind(R.id.img_expanded) ImageView img_expanded;
    @Bind(R.id.user_photo_container) LinearLayout user_photo_container;

    private UserPresenter presenter;
    private User user;
    private AvatarAnimation avatarAnimation;

    public static Intent getIntent(Context context, String userId, String title, boolean isUser) {
        Bundle bundle = new Bundle();
        bundle.putString("userId", userId);
        bundle.putString("title", title);
        bundle.putBoolean("isUser", isUser);
        Intent intent = new Intent(context, UserActivity.class);
        intent.putExtras(bundle);
        return intent;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user);
        ButterKnife.bind(this);
        v_profile.setOnUpdateUserInfo(this);

        Preferences preferences = new PreferencesImpl(this);
        RetrofitServiceGenerator retrofitServiceGenerator = RetrofitServiceGenerator.getInstance(preferences);
        API api = retrofitServiceGenerator.createService(API.class);

        tv_url.setPaintFlags(tv_url.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
        Intent intent = getIntent();
        if (intent != null && intent.getExtras() != null) {
            Bundle bundle = intent.getExtras();
            String userId = bundle.getString("userId");
            String title = bundle.getString("title");
            boolean isUser = bundle.getBoolean("isUser");

            btn_like.setVisibility(isUser ? View.VISIBLE : View.GONE);
            tv_level.setVisibility(isUser ? View.VISIBLE : View.GONE);
            presenter = new UserPresenter(this, this, api, userId, title, isUser);
        }

        avatarAnimation = new AvatarAnimation(this, user_photo_container, img_expanded, iv_prof_img);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        presenter.onDestroyView();
    }

    @Override
    protected void onResume() {
        super.onResume();
        v_profile.onResume();
    }

    @Override
    public void onBackPressed() {
        if (img_expanded.getVisibility() == View.VISIBLE) {
            avatarAnimation.closeImage();
        } else {
            super.onBackPressed();
        }
    }

    @Override
    protected void onProfileImageUpdated() {
        super.onProfileImageUpdated();
        v_profile.onProfileImageUpdated();
    }

    @Override
    protected void onProfileUpdated() {
        super.onProfileUpdated();
        v_profile.onProfileUpdated();
    }

    @OnClick(R.id.btn_like)
    void onSendLikeClick() {
        User localUser = ((App) getApplication()).getUser();
        if (user != null && localUser != null) {
            SendLikeDialog dialog = SendLikeDialog.newInstance(localUser, user, true, img_anim_coin);
            dialog.show(getSupportFragmentManager(), "");
        }
    }

    @OnClick(R.id.rl_ava_container)
    void onOpenUserPhoto() {
        avatarAnimation.onOpenUserPhoto();
    }

    @OnClick(R.id.tv_url)
    void onUrlClick() {
        new ChromeCustomTabs().loadWebSite(this, tv_url.getText().toString());
    }

    ////////////////////////////////////////////////
    /// UserView                                 ///
    ////////////////////////////////////////////////

    @Override
    public void showLoadingIndicator(boolean show) {
        pb_indicator.setVisibility(show ? View.VISIBLE : View.GONE);
    }

    @Override
    public void setUser(User user) {
        this.user = user;
    }

    @Override
    public void showUserLevel(int userLevel) {
        tv_level.setText(String.valueOf(userLevel));
        GradientDrawable drawable = (GradientDrawable) tv_level.getBackground();
        CustomColorUtils.setLevelColor(this, drawable, userLevel, null);
    }

    @Override
    public void showProfileImage(String avatarUrl) {
        ImageUtils.showAvatar(this, iv_prof_img, avatarUrl);
        ImageUtils.showAvatar(this, img_expanded, avatarUrl);
    }

    @Override
    public void showUserName(String name) {
        tv_name.setText(name);
    }

    @Override
    public void showSurname(String surname) {
        tv_name.append(" " + surname);
    }

    @Override
    public void showShortDescription(String description) {
        tv_description.setText(description);
        tv_description.setVisibility(description.length() > 0 ? View.VISIBLE : View.GONE);
    }

    @Override
    public void showUrl(String url) {
        tv_url.setText(url);
        tv_url.setVisibility(url.length() > 0 ? View.VISIBLE : View.GONE);
    }

    @Override
    public void showError(String error) {
        ToastUtils.show(this, error);
    }

    @Override
    public String getUnknownExceptionMessage() {
        return getString(R.string.server_error);
    }

    @Override
    public String getConnectionTimeOutExceptionMessage() {
        return getString(R.string.connection_error);
    }

    @Override
    public void setAmbassadorRoles(List<Merchant> ambassadorRoles) {
        ll_ambassador.setVisibility(View.VISIBLE);
        LayoutInflater layoutInflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        for (int i = 0; i < ambassadorRoles.size(); i++) {
            if (i < PartnerTypes.ROLES_AMOUNT) {
                // Show only first 3 items
                Merchant merchant = ambassadorRoles.get(i);
                View child = layoutInflater.inflate(R.layout.v_ambassador, null);
                if (merchant.getLogo4AmbassadorUrl() != null) {
                    ImageUtils.showAmbassadorLogo(this, (ImageView) child.findViewById(R.id.iv_logo), merchant.getLogo4AmbassadorUrl());
                }

                if (merchant.getTitle() != null) {
                    ((TextView) child.findViewById(R.id.tv_title)).setText(merchant.getTitle());
                }
                ll_ambassador.addView(child);
            } else {
                break;
            }
        }
    }

    ////////////////////////////////////////////////
    /// UserInfoUpdate                           ///
    ////////////////////////////////////////////////

    @Override
    public void onUpdateUserInfo(User user) {

    }

    @Override
    public void openProfileActivity() {
        intentOpener(ProfileActivity.getIntent(this));
    }

    @Override
    public void openUserStatementActivity() {
        intentOpener(UserStatementActivity.getIntent(this));
    }

    @Override
    public void openLevelActivity() {
        intentOpener(LevelActivity.getIntent(this));
    }

    @Override
    public void openBrixActivity() {
        intentOpener(BrixActivity.getIntent(this));
    }

    @Override
    public void initToolbar(Toolbar toolbar) {
        toolbar.setNavigationIcon(R.drawable.ic_chevron_left_white_24dp);
        setSupportActionBar(toolbar);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                supportFinishAfterTransition();
            }
        });
    }

    private void intentOpener(Intent intent) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            Pair<View, String> p1 = Pair.create(findViewById(R.id.v_profile), getString(R.string.tn_profile));
            ActivityOptionsCompat options = ActivityOptionsCompat.makeSceneTransitionAnimation(this, p1);
            startActivity(intent, options.toBundle());
        } else {
            startActivity(intent);
        }
        finish();
    }
}