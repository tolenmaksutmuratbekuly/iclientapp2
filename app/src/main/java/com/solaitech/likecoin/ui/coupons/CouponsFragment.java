package com.solaitech.likecoin.ui.coupons;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import com.solaitech.likecoin.R;
import com.solaitech.likecoin.ui.base.loading.LoadingBaseFragment;
import com.solaitech.likecoin.ui.base.view_pager.BaseFragmentPagerAdapter;
import com.solaitech.likecoin.ui.coupons.active.ActiveCouponsFragment;
import com.solaitech.likecoin.ui.coupons.used.UsedCouponsFragment;
import com.solaitech.likecoin.ui.menu.BottomNavigationTabFragment;
import com.solaitech.likecoin.ui.menu.Search;

public class CouponsFragment extends LoadingBaseFragment
        implements BottomNavigationTabFragment, Search {
    @Bind(R.id.tab_layout) TabLayout tab_layout;
    @Bind(R.id.view_pager) ViewPager view_pager;

    private List<Fragment> viewPagerPages = new ArrayList<>();

    public static CouponsFragment newInstance() {
        return new CouponsFragment();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = super.onCreateView(inflater, container, savedInstanceState);
        ButterKnife.bind(this, v);
        configureView();
        return v;
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.frgm_coupons;
    }

    private void configureView() {
        viewPagerPages.clear();
        viewPagerPages.add(ActiveCouponsFragment.newInstance());
        viewPagerPages.add(UsedCouponsFragment.newInstance());
        PagerAdapter viewPagerAdapter = new BaseFragmentPagerAdapter(getChildFragmentManager()) {
            @Override
            public CharSequence getPageTitle(int position) {
                switch (position) {
                    case 0:
                        return getString(R.string.active_coupons);
                    case 1:
                        return getString(R.string.used_coupons);
                    default:
                        return "";
                }
            }

            @Override
            public Fragment getItem(int position) {
                return viewPagerPages.get(position);
            }

            @Override
            public int getCount() {
                return viewPagerPages.size();
            }
        };

        view_pager.setAdapter(viewPagerAdapter);
        view_pager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tab_layout));
        tab_layout.setupWithViewPager(view_pager);
    }

    @Override
    protected void tryUploadDataAgain() {
        //do nothing
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    @Override
    public void onBottomNavigationTabReselected() {
        for (Fragment fragment : viewPagerPages) {
            if (fragment instanceof CouponsCallback) {
                ((CouponsCallback) fragment).onBottomNavigationTabReselected();
            }
        }
    }

    @Override
    public void doSearch(String text) {
        for (Fragment fragment : viewPagerPages) {
            if (fragment instanceof Search) {
                ((Search) fragment).doSearch(text);
            }
        }
    }

    @Override
    public void showAllListItems() {
        for (Fragment fragment : viewPagerPages) {
            if (fragment instanceof Search) {
                ((Search) fragment).showAllListItems();
            }
        }
    }
}