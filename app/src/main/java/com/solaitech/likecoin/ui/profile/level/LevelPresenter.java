package com.solaitech.likecoin.ui.profile.level;

import com.solaitech.likecoin.data.models.user.User;
import com.solaitech.likecoin.utils.StringUtils;

class LevelPresenter {
    private LevelView view;

    LevelPresenter(LevelView view) {
        this.view = view;
        if (view.getUser() != null) {
            setUserInfo(view.getUser());
        }
    }

    void setUserInfo(User user) {
        if (user.getUserLevel() != null
                && user.getUserLevel().getLevel() != null) {
            view.showUserLevel(user.getUserLevel().getLevel());
        } else {
            view.showUserLevel(0);
        }

        if (user.getUserLevel() != null
                && user.getUserLevel().getNextLevelProgress() != null) {
            view.showCurrentLevelProgress(user.getUserLevel().getNextLevelProgress());
        } else {
            view.showCurrentLevelProgress(0);
        }

        int likesAmount = 0;
        if (user.getUserLevel() != null
                && user.getUserLevel().getLikesCount() != null) {
            likesAmount = user.getUserLevel().getLikesCount();
            view.showLikesAmount(String.valueOf(likesAmount));
        } else {
            view.showLikesAmount(StringUtils.EMPTY_STRING);
        }


        if (user.getUserLevel() != null
                && user.getUserLevel().getNextLevelLikesCount() != null) {
            view.showNextLevelResidueLike(String.valueOf(user.getUserLevel().getNextLevelLikesCount() - likesAmount));
        } else {
            view.showNextLevelResidueLike(StringUtils.EMPTY_STRING);
        }
    }
}