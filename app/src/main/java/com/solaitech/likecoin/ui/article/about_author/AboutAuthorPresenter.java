package com.solaitech.likecoin.ui.article.about_author;

import com.solaitech.likecoin.data.models.campaign.Merchant;
import com.solaitech.likecoin.utils.HtmlSourceUtils;
import com.solaitech.likecoin.utils.StringUtils;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

class AboutAuthorPresenter {
    private AboutAuthorView view;

    AboutAuthorPresenter(AboutAuthorView view) {
        this.view = view;
    }

    void onMerchantSet() {
        Merchant merchant = view.getMerchant();
        if (merchant != null) {
            if (StringUtils.isStringOk(merchant.getTitle())) {
                view.showTitle(merchant.getTitle());
            } else {
                view.hideTitle();
            }

            if (StringUtils.isStringOk(merchant.getShortDescr())) {
                view.showShortDescription(merchant.getShortDescr());
            } else {
                view.hideShortDescription();
            }

            if (StringUtils.isStringOk(merchant.getFullDescr())) {
                view.showFullDescription(HtmlSourceUtils.styleTextForWebView(merchant.getFullDescr()));
            } else {
                view.hideFullDescription();
            }

            if (StringUtils.isStringOk(merchant.getUrl())) {
                List<String> links = new ArrayList<>();
                links.add(merchant.getUrl());
                view.showLinks(links);
            } else {
                view.hideLinks();
            }

            if (merchant.getContacts() != null) {
                List<String> contacts = new ArrayList<>();
                if (merchant.getContacts().getWhatsApp() != null &&
                        merchant.getContacts().getWhatsApp().size() > 0) {
                    contacts.addAll(merchant.getContacts().getWhatsApp());
                }

                if (merchant.getContacts().getMobile() != null &&
                        merchant.getContacts().getMobile().size() > 0) {
                    contacts.addAll(merchant.getContacts().getMobile());
                }

                if (merchant.getContacts().getLandline() != null &&
                        merchant.getContacts().getLandline().size() > 0) {
                    contacts.addAll(merchant.getContacts().getLandline());
                }

                if (merchant.getContacts().getFax() != null &&
                        merchant.getContacts().getFax().size() > 0) {
                    contacts.addAll(merchant.getContacts().getFax());
                }

                if (contacts.size() > 0) {
                    Set<String> hs = new HashSet<>();
                    hs.addAll(contacts);
                    contacts.clear();
                    contacts.addAll(hs);
                    view.showContacts(contacts);
                } else {
                    view.hideContacts();
                }

                if (merchant.getContacts().getEmail() != null &&
                        merchant.getContacts().getEmail().size() > 0) {
                    contacts.clear();
                    contacts.addAll(merchant.getContacts().getEmail());
                    view.showEmails(contacts);
                } else {
                    view.hideEmails();
                }

            } else {
                view.hideContacts();
                view.hideEmails();
            }
        } else {
            view.hideTitle();
            view.hideShortDescription();
            view.hideFullDescription();
            view.hideLinks();
            view.hideContacts();
            view.hideEmails();
        }
    }
}