package com.solaitech.likecoin.ui.base.interfaces;

public interface MVPBaseView extends MVPBaseErrorView {

    void showProgressTemplateView();

    void showProgressTemplateView(String message);

    void showSuccessTemplateView();

    void showFailTemplateView();

    void showFailTemplateView(String message);

    void showProgressDialog();

    void dismissProgressDialog();

}