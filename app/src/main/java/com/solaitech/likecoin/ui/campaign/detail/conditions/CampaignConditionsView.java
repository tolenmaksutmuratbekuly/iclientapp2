package com.solaitech.likecoin.ui.campaign.detail.conditions;

import android.content.Context;

import java.util.List;

import com.solaitech.likecoin.data.models.campaign.CampaignDetail;
import com.solaitech.likecoin.data.models.campaign.Coordinate;

interface CampaignConditionsView {

    CampaignDetail getCampaignDetail();

    void showTitle(String title);

    void showShortDescription(String description);

    void showFullDescription(String description);

    void showDate(String startDate, String endDate);

    void showContacts(List<String> contacts);

    void showLinks(List<String> links);

    void showCoordinates(List<Coordinate> coordinates);

    void hideTitle();

    void hideShortDescription();

    void hideFullDescription();

    void hideValidUntil();

    void hideContacts();

    void hideLinks();

    void hideCoordinates();

    void setCoordinates(List<Coordinate> coordinates);

    List<Coordinate> getCoordinates();

    void clearMarkers();

    void displayMarkers(List<Coordinate> coordinates);

    //
    boolean isPermissionGrantedToLocation();

    boolean shouldShowRequestPermissionRationaleToLocation();

    void showRequestPermissionRationaleToLocation();

    void requestPermissionToLocation();

    void showRequestPermissioDialogToLocation();

    void openSettingsActivity();

    void setMyLocationEnabled();

    //
    boolean isGoogleApiClientHaveBeenConnected();

    void initLastLocation();

    void moveCamera(double latitude, double longitude);

    //
    void hideAllowAccessToLocationButton();

    void showAllowAccessToLocationButton();

    Context getContextFromPage();
}