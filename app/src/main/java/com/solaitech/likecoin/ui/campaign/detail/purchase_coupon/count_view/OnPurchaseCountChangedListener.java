package com.solaitech.likecoin.ui.campaign.detail.purchase_coupon.count_view;

public interface OnPurchaseCountChangedListener {
    void onPurchaseCountChanged(int count);
}