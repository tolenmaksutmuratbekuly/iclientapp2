package com.solaitech.likecoin.ui.base.recycler_view;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.solaitech.likecoin.interfaces.OnItemClickListener;

public abstract class RecyclerViewBaseAdapter extends RecyclerView.Adapter<RecyclerViewBaseAdapter.MainViewHolder> {

    private Context context;
    private OnItemClickListener onItemClickListener;

    // Provide a suitable constructor (depends on the kind of dataset)
    public RecyclerViewBaseAdapter(Context context) {
        this.context = context;
    }

    public void setOnRecyclerViewItemClickListener(OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }

    // Provide a reference to the views for each data item
// Complex data items may need more than one view per item, and
// you provide access to all the views for a data item in a view holder
    public class MainViewHolder extends RecyclerView.ViewHolder {
        public MainViewHolder(View v) {
            super(v);
        }
    }

    public class FirstElementEmptySpaceViewHolder extends MainViewHolder {
        public FirstElementEmptySpaceViewHolder(View v) {
            super(v);
        }
    }

    protected View inflate(ViewGroup parent, int resource) {
        LayoutInflater mInflater = LayoutInflater.from(parent.getContext());
        return mInflater.inflate(resource, parent, false);
    }

    protected IllegalStateException incorrectOnCreateViewHolder() {
        return new IllegalStateException("Incorrect ViewType found");
    }

    protected IllegalStateException incorrectGetItemViewType() {
        return new IllegalStateException("Incorrect object added");
    }

    protected Context getContext() {
        return context;
    }

    protected void onItemClick(Object object) {
        onItemClickListener.onItemClick(object);
    }

}