package com.solaitech.likecoin.ui.menu;

public interface Search {

    void doSearch(String text);

    void showAllListItems();

}