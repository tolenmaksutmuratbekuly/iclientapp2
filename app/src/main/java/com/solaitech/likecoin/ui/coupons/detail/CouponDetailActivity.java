package com.solaitech.likecoin.ui.coupons.detail;

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import com.solaitech.likecoin.R;
import com.solaitech.likecoin.data.PermissionRequestCodes;
import com.solaitech.likecoin.data.models.coupons.CouponDetail;
import com.solaitech.likecoin.data.models.enums.AppBarLayoutStateEnums;
import com.solaitech.likecoin.data.models.enums.CouponTypeEnums;
import com.solaitech.likecoin.interfaces.OnFragmentViewCreatedListener;
import com.solaitech.likecoin.ui.base.loading.LoadingBaseActivity;
import com.solaitech.likecoin.ui.campaign.detail.conditions.CampaignConditionsFragment;
import com.solaitech.likecoin.ui.coupons.detail.qr_code.ShowQrCodeDialog;
import com.solaitech.likecoin.utils.CustomColorUtils;
import com.solaitech.likecoin.utils.DeviceUtils;
import com.solaitech.likecoin.utils.ImageUtils;
import com.solaitech.likecoin.utils.PermissionUtils;
import com.solaitech.likecoin.utils.ToastUtils;

public class CouponDetailActivity extends LoadingBaseActivity
        implements CouponDetailView, OnFragmentViewCreatedListener {
    private static final int REQUEST_PERMISSION_FOR_STORAGE = 3;
    private static String[] PERMISSIONS_STORAGE = {
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE
    };

    private String id;
    private int couponType;
    private int appBarLayoutState;

    @Bind(R.id.toolbar) Toolbar toolbar;
    @Bind(R.id.app_bar_layout) AppBarLayout app_bar_layout;
    @Bind(R.id.collapsing_toolbar) CollapsingToolbarLayout collapsing_toolbar;
    @Bind(R.id.iv_image_large) ImageView iv_image_large;
    @Bind(R.id.vg_show_coupon) ViewGroup vg_show_coupon;
    @Bind(R.id.fab_share) FloatingActionButton fab_share;

    Bitmap mapSnapshot;
    private CouponDetailPresenter presenter;
    private CouponDetail couponDetail;

    public static Intent getIntent(Context context, String id, int couponType) {
        Bundle bundle = new Bundle();
        bundle.putString("id", id);
        bundle.putInt("couponType", couponType);

        Intent intent = new Intent(context, CouponDetailActivity.class);
        intent.putExtras(bundle);
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_coupon_detail);
        ButterKnife.bind(this);
        handleIntent(getIntent());
        initView();

        if (savedInstanceState == null) {
            replaceFragment(CampaignConditionsFragment.newInstance());
        }

        presenter = new CouponDetailPresenter(this, api);
        presenter.init();
    }

    @Override
    protected void onDestroy() {
        presenter.onDestroyView();
        super.onDestroy();
    }

    private void handleIntent(Intent intent) {
        if (intent != null && intent.getExtras() != null) {
            Bundle bundle = intent.getExtras();
            id = bundle.getString("id");
            couponType = bundle.getInt("couponType");
        }
    }

    private void initView() {
        collapsing_toolbar.setCollapsedTitleTextColor(CustomColorUtils.getColor(this, R.color.white));
        collapsing_toolbar.setExpandedTitleColor(CustomColorUtils.getColor(this, R.color.transparent));
        app_bar_layout.addOnOffsetChangedListener(new AppBarLayout.OnOffsetChangedListener() {
            @Override
            public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
                if (Math.abs(verticalOffset) == appBarLayout.getTotalScrollRange()) {
                    // Collapsed
                    appBarLayoutState = AppBarLayoutStateEnums.COLLAPSED;
                } else if (verticalOffset == 0) {
                    // Expanded
                    appBarLayoutState = AppBarLayoutStateEnums.EXPANDED;
                } else {
                    // Somewhere in between
                    appBarLayoutState = AppBarLayoutStateEnums.IN_BETWEEN;
                }
            }
        });
        app_bar_layout.setExpanded(false, false);
        initToolbar();
        initContainerView(R.id.coordinator_layout);
        initLoadingView();
    }

    private void initToolbar() {
        setSupportActionBar(toolbar);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        if (getSupportActionBar() != null) {
            getSupportActionBar().setTitle("");
        }
        toolbar.setTitle("");
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case REQUEST_PERMISSION_FOR_STORAGE: {
                presenter.onActivityResultToStorage();
                break;
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {

        if (requestCode == PermissionRequestCodes.STORAGE) {
            if (PermissionUtils.verifyPermissions(grantResults)) {
                presenter.onPermissionGrantedToStorage();
            } else {
                presenter.onPermissionNotGrantedToStorage();
            }
        } else {
            super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    @OnClick(R.id.btn_show_coupon)
    void onShowCouponClick() {
        presenter.onShowCouponButtonClick();
    }

    @Override
    public void onFragmentViewCreated() {
        presenter.onFragmentViewCreated();
    }

    @Override
    protected void tryUploadDataAgain() {
        presenter.onTryUploadDataAgainClick();
    }

    @Override
    public String getCouponId() {
        return id;
    }

    @Override
    public int getCouponType() {
        return couponType;
    }

    @Override
    public void showAppBarLayout() {
        app_bar_layout.setExpanded(true, true);
    }

    @Override
    public boolean isAppBarLayoutExpanded() {
        return appBarLayoutState == AppBarLayoutStateEnums.EXPANDED;
    }

    @Override
    public void hideAppBarLayout() {
        app_bar_layout.setExpanded(false, true);
    }

    @Override
    public void showImageLarge(String imgUrl) {
        ImageUtils.showCampaignImgLarge(this, iv_image_large, imgUrl);
    }

    @Override
    public void setCouponDetail(CouponDetail couponDetail) {
        this.couponDetail = couponDetail;

        Fragment fragment = findFragmentById();
        if (fragment instanceof CampaignConditionsFragment) {
            CampaignConditionsFragment campaignDetail = (CampaignConditionsFragment) fragment;
            campaignDetail.setCampaignDetail(couponDetail.getCampaign());
            campaignDetail.setCallback(new CampaignConditionsFragment.Callback() {
                @Override
                public void onSnapshotReady(Bitmap bmp) {
                    if (couponType == CouponTypeEnums.ACTIVE) {
                        fab_share.setVisibility(View.VISIBLE);
                        mapSnapshot = bmp;
                    }
                }
            });
        }
    }

    @Override
    public CouponDetail getCouponDetail() {
        return couponDetail;
    }

    @OnClick(R.id.fab_share)
    void onShareClick() {
        presenter.onShareButtonClick();
    }

    @Override
    public void showShowCouponView() {
        vg_show_coupon.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideShowCouponView() {
        vg_show_coupon.setVisibility(View.GONE);
    }

    @Override
    public void openPromocodeDialog(String key, boolean showQrCode) {
        ShowQrCodeDialog dialog = ShowQrCodeDialog.newInstance(key, showQrCode, getString(R.string.promotional_code));
        dialog.show(getSupportFragmentManager(), "");
    }

    @Override
    public void setToolbarTitle(String title) {
        collapsing_toolbar.setTitle(title);
    }

    @Override
    public Context getAppContext() {
        return this;
    }

    @Override
    public Bitmap getMapSnapshot() {
        return mapSnapshot;
    }

    @Override
    public boolean isPermissionGrantedToStorage() {
        return PermissionUtils.isPermissionGranted(this, Manifest.permission.WRITE_EXTERNAL_STORAGE);
    }

    @Override
    public boolean shouldShowRequestPermissionRationaleToStorage() {
        return ActivityCompat.shouldShowRequestPermissionRationale(this,
                Manifest.permission.WRITE_EXTERNAL_STORAGE);
    }

    @Override
    public void showRequestPermissionDialogToStorage() {
        new AlertDialog.Builder(this)
                .setMessage(getString(R.string.rationale_storage))
                .setPositiveButton(getString(R.string.allow), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                        presenter.onDialogToStorageAllowed();

                    }
                })
                .setNegativeButton(getString(R.string.reject), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                })
                .create()
                .show();
    }

    @Override
    public void showRequestPermissionRationaleToStorage() {
        new AlertDialog.Builder(this)
                .setMessage(getString(R.string.rationale_storage))
                .setPositiveButton(getString(R.string.allow), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                        presenter.onRationaleToStorageAllowed();

                    }
                })
                .setNegativeButton(getString(R.string.reject), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                })
                .create()
                .show();
    }

    @Override
    public void requestPermissionToStorage() {
        ActivityCompat.requestPermissions(
                this,
                PERMISSIONS_STORAGE,
                PermissionRequestCodes.STORAGE);
    }

    @Override
    public void openSettingsActivity() {
        Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
        Uri uri = Uri.fromParts("package", getPackageName(), null);
        intent.setData(uri);
        startActivityForResult(intent, REQUEST_PERMISSION_FOR_STORAGE);

    }

    @Override
    public int getBlackColor() {
        return CustomColorUtils.getColor(this, R.color.black);
    }

    @Override
    public int getWhiteColor() {
        return CustomColorUtils.getColor(this, R.color.white);
    }

    @Override
    public int getDisplayWidth() {
        return DeviceUtils.getDisplayWidth(this);
    }

    @Override
    public int getDisplayHeight() {
        return DeviceUtils.getDisplayHeight(this);
    }

    @Override
    public void qrCodeGeneratedError() {
        ToastUtils.show(this, getString(R.string.error_occurred));
    }

    @Override
    public void shareCoupon(Intent intentShareFile) {
        intentShareFile.putExtra(Intent.EXTRA_SUBJECT, getString(R.string.share));
        intentShareFile.putExtra(Intent.EXTRA_TEXT, getString(R.string.share));
        startActivity(Intent.createChooser(intentShareFile, getString(R.string.share)));
    }
}