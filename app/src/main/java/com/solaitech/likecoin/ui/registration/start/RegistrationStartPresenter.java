package com.solaitech.likecoin.ui.registration.start;

import com.onesignal.OneSignal;

import com.solaitech.likecoin.data.models.registration.RegistrationStart;
import com.solaitech.likecoin.data.params.RegistrationStartParams;
import com.solaitech.likecoin.data.repository.registration.RegistrationRepositoryProvider;
import com.solaitech.likecoin.network.API;
import com.solaitech.likecoin.ui.base.PresenterErrorHandler;
import com.solaitech.likecoin.utils.EmailUtils;
import com.solaitech.likecoin.utils.PhoneUtils;
import rx.Subscriber;
import rx.Subscription;
import rx.functions.Action0;
import rx.subscriptions.CompositeSubscription;

class RegistrationStartPresenter {

    private RegistrationStartView view;
    private API api;

    private CompositeSubscription compositeSubscription = new CompositeSubscription();

    RegistrationStartPresenter(RegistrationStartView view, API api) {
        this.view = view;
        this.api = api;
    }

    public void init() {
        OneSignal.setSubscription(false);
    }

    void onStartRegistrationClick() {
        if (isPhoneNumberOk()) {
            registrationStart();
        } else {
            if (view.getPhoneNumber().isEmpty()) {
                view.showPhoneNumberEmptyError();
            } else if (!PhoneUtils.isPhoneValid(view.getPhoneNumber())) {
                view.showPhoneNumberInvalidError();
            }
        }
    }

    void onSignUpButtonClick() {
        if (isEmailOk() && isNameOk() && isSurnameOk()) {
            view.openRegistrationCompleteDialog(view.getPhoneNumber(), view.getEmail(), view.getName(), view.getSurname(), false);
        } else {
            if (view.getPhoneNumber().isEmpty()) {
                view.showPhoneNumberEmptyError();
            } else if (!PhoneUtils.isPhoneValid(view.getPhoneNumber())) {
                view.showPhoneNumberInvalidError();
            }

            if (!view.getEmail().isEmpty() && !EmailUtils.isEmailValid(view.getEmail())) {
                view.showEmailInvalidError();
            }

            if (!isNameOk()) {
                view.showNameEmptyError();
            }

            if (!isSurnameOk()) {
                view.showSurnameEmptyError();
            }
        }
    }

    private boolean isPhoneNumberOk() {
        return (PhoneUtils.isPhoneValid(view.getPhoneNumber()));
    }

    private boolean isEmailOk() {
        return (view.getEmail().isEmpty() ||
                (!view.getEmail().isEmpty() && EmailUtils.isEmailValid(view.getEmail())));
    }

    private boolean isNameOk() {
        return !view.getName().isEmpty();
    }

    private boolean isSurnameOk() {
        return !view.getSurname().isEmpty();
    }

    private void registrationStart() {
        Subscription subscription = RegistrationRepositoryProvider.provideRepository(api)
                .registrationStart(getRegistrationStartParams())
                .doOnSubscribe(new Action0() {
                    @Override
                    public void call() {
                        view.showProgressDialog();
                    }
                })
                .doOnTerminate(new Action0() {
                    @Override
                    public void call() {
                        view.dismissProgressDialog();
                    }
                })
                .subscribe(new Subscriber<RegistrationStart>() {
                    @Override
                    public void onCompleted() {
                        //do nothing
                    }

                    @Override
                    public void onError(Throwable e) {
                        PresenterErrorHandler.handleError2(e, view);
                    }

                    @Override
                    public void onNext(RegistrationStart registrationStart) {
                        if (registrationStart.isRegistered) {
                            // User already have registered
                            view.openRegistrationCompleteDialog(view.getPhoneNumber(), view.getEmail(), view.getName(), view.getSurname(), registrationStart.isRegistered);
                        } else {
                            // Otherwise show Registration fields
                            view.showRegistrationLayout();
                        }
                    }
                });
        compositeSubscription.add(subscription);
    }

    private RegistrationStartParams getRegistrationStartParams() {
        return new RegistrationStartParams(
                view.getPhoneNumber()
        );
//        view.getEmail(),
//        view.getFullName()
    }

    void onPermissionGrantedToSms() {
        registrationStart();
    }

    void onPermissionNotGrantedToSms() {
        registrationStart();
    }

    public void onDestroyView() {
        unsubscribe();
    }

    private void unsubscribe() {
        if (compositeSubscription != null
                && !compositeSubscription.isUnsubscribed()) {
            compositeSubscription.unsubscribe();
        }
    }
}