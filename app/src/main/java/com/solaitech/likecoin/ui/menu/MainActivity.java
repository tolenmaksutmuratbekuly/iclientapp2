package com.solaitech.likecoin.ui.menu;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.internal.BottomNavigationItemView;
import android.support.design.internal.BottomNavigationMenuView;
import android.support.design.widget.BottomNavigationView;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.app.Fragment;
import android.support.v4.util.Pair;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.LinearInterpolator;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;

import com.onesignal.OSSubscriptionObserver;
import com.onesignal.OSSubscriptionStateChanges;
import com.solaitech.likecoin.R;
import com.solaitech.likecoin.data.models.enums.PushNotificationEnums;
import com.solaitech.likecoin.data.models.user.User;
import com.solaitech.likecoin.exceptions.AppVersionException;
import com.solaitech.likecoin.ui.base.BaseActivityFrgm;
import com.solaitech.likecoin.ui.campaign.DealsFragment;
import com.solaitech.likecoin.ui.contacts.ContactsFragment;
import com.solaitech.likecoin.ui.coupons.CouponsFragment;
import com.solaitech.likecoin.ui.header.ProfileHeaderViewGroup;
import com.solaitech.likecoin.ui.help.HelpFragment;
import com.solaitech.likecoin.ui.news_feed.NewsFeedFragment;
import com.solaitech.likecoin.ui.profile.ProfileActivity;
import com.solaitech.likecoin.ui.profile.brix.BrixActivity;
import com.solaitech.likecoin.ui.profile.edit_profile_image.UserInfoUpdate;
import com.solaitech.likecoin.ui.profile.level.LevelActivity;
import com.solaitech.likecoin.ui.profile.user_statement.UserStatementActivity;
import com.solaitech.likecoin.utils.DeviceUtils;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

public class MainActivity extends BaseActivityFrgm implements MainView, BottomNavigationView.OnNavigationItemSelectedListener,
        BottomNavigationView.OnNavigationItemReselectedListener, OSSubscriptionObserver, UserInfoUpdate {
    private static final int ACTION_HOME = 0;
    private static final int ACTION_PROMOTIONS = 1;
    private static final int ACTION_COUPONS = 2;
    private static final int ACTION_CONTACTS = 3;
    private static final int ACTION_HELP = 4;

    private MainPresenter presenter;
    private List<Fragment> fragments = new ArrayList<>();
    @Bind(R.id.fab_add_contact) FloatingActionButton fab_add_contact;
    @Bind(R.id.pb_header) ProgressBar pb_header;
    @Bind(R.id.v_bottom_navigation) BottomNavigationView v_bottom_navigation;
    @Bind(R.id.v_profile) ProfileHeaderViewGroup v_profile;

    public static Intent getIntent(Context context) {
        return new Intent(context, MainActivity.class);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        initBottomNavigationView();
        initFragments();
        v_profile.setOnUpdateUserInfo(this);

        Intent intent = getIntent();
        if (savedInstanceState == null && !intent.hasExtra("tabSelect")) {
            replaceFragment(getNewsFeedFragment());
        } else if (intent.hasExtra("tabSelect")) {
            Bundle bd = intent.getExtras();
            String tabSelectIndex = bd.getString("tabSelect");
            if (tabSelectIndex != null) {
                if (tabSelectIndex.equals(PushNotificationEnums.PUSH_NOTIFICATION_TYPE_CAMPAIGN_LIST)) {
                    replaceFragment(fragments.get(ACTION_PROMOTIONS), false);
                    v_bottom_navigation.setSelectedItemId(R.id.action_promotions);
                }
            }
        }

        presenter = new MainPresenter(this, api);
        presenter.init();
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        //notification opened handler
        if (v_bottom_navigation != null) {
            v_bottom_navigation.setSelectedItemId(R.id.action_home);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        presenter.onResume();
    }

    private void initBottomNavigationView() {
        v_bottom_navigation.setOnNavigationItemSelectedListener(this);
        v_bottom_navigation.setOnNavigationItemReselectedListener(this);
        disableShiftMode(v_bottom_navigation);
    }

    private void disableShiftMode(BottomNavigationView view) {
        BottomNavigationMenuView menuView = (BottomNavigationMenuView) view.getChildAt(0);
        try {
            Field shiftingMode = menuView.getClass().getDeclaredField("mShiftingMode");
            shiftingMode.setAccessible(true);
            shiftingMode.setBoolean(menuView, false);
            shiftingMode.setAccessible(false);
            for (int i = 0; i < menuView.getChildCount(); i++) {
                BottomNavigationItemView item = (BottomNavigationItemView) menuView.getChildAt(i);
                //noinspection RestrictedApi
                item.setShiftingMode(false);
                // set once again checked value, so view will be updated
                //noinspection RestrictedApi
                item.setChecked(item.getItemData().isChecked());
            }
        } catch (NoSuchFieldException e) {
            Log.e("BNVHelper", "Unable to get shift mode field", e);
        } catch (IllegalAccessException e) {
            Log.e("BNVHelper", "Unable to change value of shift mode", e);
        } catch (Exception e) {
            Log.e("BNVHelper", "Exception", e);
        }
    }

    private void initFragments() {
        fragments.add(ACTION_HOME, getNewsFeedFragment());
        fragments.add(ACTION_PROMOTIONS, getPromotionsFragment());
        fragments.add(ACTION_COUPONS, getCouponsFragment());
        fragments.add(ACTION_CONTACTS, getContactsFragment());
        fragments.add(ACTION_HELP, getHelpFragment());
    }

    private Fragment getNewsFeedFragment() {
        return NewsFeedFragment.newInstance();
    }

    private Fragment getPromotionsFragment() {
        return DealsFragment.newInstance();
    }

    private Fragment getCouponsFragment() {
        return CouponsFragment.newInstance();
    }

    private Fragment getContactsFragment() {
        return ContactsFragment.newInstance();
    }

    private Fragment getHelpFragment() {
        return HelpFragment.newInstance();
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_home:
                replaceFragment(fragments.get(ACTION_HOME), false);
                return true;
            case R.id.action_promotions:
                replaceFragment(fragments.get(ACTION_PROMOTIONS), false);
                return true;
            case R.id.action_coupons:
                replaceFragment(fragments.get(ACTION_COUPONS), false);
                return true;
            case R.id.action_contacts:
                replaceFragment(fragments.get(ACTION_CONTACTS), true);
                return true;
            case R.id.action_help:
                replaceFragment(fragments.get(ACTION_HELP), false);
                return true;
            default:
                return false;
        }
    }

    @Override
    protected void onDestroy() {
        v_profile.onDestroyView();
        presenter.onDestroyView();
        super.onDestroy();
    }

    private void replaceFragment(Fragment fragment, boolean showContactsFab) {
        if (showContactsFab) {
            showContactsFab();
        } else {
            hideContactsFab();
        }

//        if (searchItem != null
//                && searchItem.isActionViewExpanded()) {
//            searchItem.collapseActionView();
//        }

        super.replaceFragment(fragment);
        showProfile();
    }

    ////////////////////////////////////////////////
    /// BottomNavigationView                     ///
    ////////////////////////////////////////////////

    @Override
    public void onNavigationItemReselected(@NonNull MenuItem item) {
//        showProfile();
        Fragment fragment = findFragmentById();
        if (fragment != null
                && fragment instanceof BottomNavigationTabFragment) {
            ((BottomNavigationTabFragment) fragment).onBottomNavigationTabReselected();
        }
    }

    private void showProfile() {
//        app_bar_layout.setExpanded(true, true);
    }

    ////////////////////////////////////////////////
    /// MainView                                 ///
    ////////////////////////////////////////////////

    @Override
    public void showContactsFab() {
        fab_add_contact
                .animate()
                .translationY(0)
                .setInterpolator(new LinearInterpolator())
                .start();
    }

    @Override
    public void hideContactsFab() {
        RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) fab_add_contact.getLayoutParams();
        int fabBottomMargin = layoutParams.bottomMargin;
        fab_add_contact
                .animate()
                .translationY(fab_add_contact.getHeight() + fabBottomMargin)
                .setInterpolator(new LinearInterpolator())
                .start();
    }

    @Override
    public String getAppVersion() {
        return DeviceUtils.getAppVersionName(this);
    }

    @Override
    public void callOnResumeOnProfileView() {
        v_profile.onResume();
    }

    @Override
    public void showInvalidAppVersionDialog(AppVersionException e) {
        InvalidAppVersionDialog dialog = InvalidAppVersionDialog.newInstance();
        dialog.show(getSupportFragmentManager(), "");
    }

    @Override
    protected void onProfileImageUpdated() {
        super.onProfileImageUpdated();
        v_profile.onProfileImageUpdated();
    }

    @Override
    protected void onProfileUpdated() {
        super.onProfileUpdated();
        v_profile.onProfileUpdated();
    }

    public void showHeaderProgress() {
        pb_header.setVisibility(View.VISIBLE);
    }

    public void hideHeaderProgress() {
        pb_header.setVisibility(View.GONE);
    }

    public View getFABBtn() {
        return fab_add_contact;
    }

    private void initSearch(Menu menu) {
//        searchItem = menu.findItem(R.id.action_search);
//        // Associate searchable_account configuration with the SearchView
//        SearchManager searchManager =
//                (SearchManager) getSystemService(Context.SEARCH_SERVICE);
//        SearchView searchView =
//                (SearchView) searchItem.getActionView();
//
//        searchView.setSearchableInfo(
//                searchManager.getSearchableInfo(getComponentName()));
//
//        SearchView.OnQueryTextListener textChangeListener = new SearchView.OnQueryTextListener() {
//            @Override
//            public boolean onQueryTextChange(final String newText) {
//                presenter.onQueryTextChange(newText);
//                return false;
//            }
//
//            @Override
//            public boolean onQueryTextSubmit(String query) {
//                // this is your adapter that will be filtered
//                Log.e("onQueryTextSubmit", "" + query);
//                return false;
//            }
//        };
//
//        searchView.setOnQueryTextListener(textChangeListener);
//
//        //When using the support library, the setOnActionExpandListener() method is
//        //static and accepts the MenuItem object as an argument
//        MenuItemCompat.setOnActionExpandListener(searchItem, new MenuItemCompat.OnActionExpandListener() {
//            @Override
//            public boolean onMenuItemActionCollapse(MenuItem item) {
//                // Do something when collapsed
//                presenter.onMenuItemActionCollapse();
//                return true;  // Return true to collapse action view
//            }
//
//            @Override
//            public boolean onMenuItemActionExpand(MenuItem item) {
//                // Do something when expanded
//                return true;  // Return true to expand action view
//            }
//        });
    }

    @Override
    public void doSearch(final String text) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Fragment fragment = findFragmentById();
                if (fragment != null
                        && fragment instanceof Search) {
                    ((Search) fragment).doSearch(text);
                }
            }
        });
    }

    @Override
    public void showAllListItems() {
        Fragment fragment = findFragmentById();
        if (fragment != null
                && fragment instanceof Search) {
            ((Search) fragment).showAllListItems();
        }
    }

    ////////////////////////////////////////////////
    /// OSSubscriptionObserver                   ///
    ////////////////////////////////////////////////

    @Override
    public void onOSSubscriptionChanged(OSSubscriptionStateChanges stateChanges) {
        presenter.onOSSubscriptionChanged(stateChanges);
    }

    ////////////////////////////////////////////////
    /// UserInfoUpdate                           ///
    ////////////////////////////////////////////////

    @Override
    public void onUpdateUserInfo(User user) {

    }

    @Override
    public void openProfileActivity() {
        intentOpener(ProfileActivity.getIntent(this));
    }

    @Override
    public void openUserStatementActivity() {
        intentOpener(UserStatementActivity.getIntent(this));
    }

    @Override
    public void openLevelActivity() {
        intentOpener(LevelActivity.getIntent(this));
    }

    @Override
    public void openBrixActivity() {
        intentOpener(BrixActivity.getIntent(this));
    }

    @Override
    public void initToolbar(Toolbar toolbar) {
    }

    private void intentOpener(Intent intent) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            Pair<View, String> p1 = Pair.create(findViewById(R.id.v_profile), getString(R.string.tn_profile));
            ActivityOptionsCompat options = ActivityOptionsCompat.makeSceneTransitionAnimation(this, p1);
            startActivity(intent, options.toBundle());
        } else {
            startActivity(intent);
        }
    }
}