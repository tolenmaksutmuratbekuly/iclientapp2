package com.solaitech.likecoin.ui.header;

import android.content.Context;

import com.solaitech.likecoin.data.models.user.User;

interface ProfileHeaderView {

    User getUser();

    void showProgressBar();

    void hideProgressBar();

    void saveUser(User user);

    void showUserLevel(int userLevel);

    void showProfileImage(String avatarUrl);

    void showCoinsAmount(String amount);

    void showBrixAmount(String amount);

    void onDestroyView();

    Context getContext();

    void onResume();

    void showError(String error);

    void hideError();

    String getUnknownExceptionMessage();

    String getConnectionTimeOutExceptionMessage();

    void updateUserInfo(User user);
}




