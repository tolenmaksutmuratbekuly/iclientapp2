package com.solaitech.likecoin.ui.push_notification;

import com.onesignal.OSNotification;
import com.onesignal.OneSignal;

public class NotificationReceivedHandler implements OneSignal.NotificationReceivedHandler {

    @Override
    public void notificationReceived(OSNotification notification) {
        //do nothing
    }
}