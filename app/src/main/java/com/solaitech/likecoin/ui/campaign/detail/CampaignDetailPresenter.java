package com.solaitech.likecoin.ui.campaign.detail;

import com.solaitech.likecoin.data.models.campaign.CampaignDetail;
import com.solaitech.likecoin.data.repository.campaign.CampaignRepositoryProvider;
import com.solaitech.likecoin.network.API;
import com.solaitech.likecoin.ui.base.PresenterErrorHandler;
import com.solaitech.likecoin.utils.StringUtils;

import rx.Subscriber;
import rx.Subscription;
import rx.functions.Action0;
import rx.subscriptions.CompositeSubscription;

class CampaignDetailPresenter {

    private CampaignDetailView view;
    private API api;
    private CompositeSubscription compositeSubscription = new CompositeSubscription();

    CampaignDetailPresenter(CampaignDetailView view, API api) {
        this.view = view;
        this.api = api;
    }

    public void init() {
    }

    private void getCampaignById() {
        Subscription subscription = CampaignRepositoryProvider
                .provideRepository(api)
                .getCampaignById(view.getCampaignId())
                .doOnSubscribe(new Action0() {
                    @Override
                    public void call() {
                        if (view.isAppBarLayoutExpanded()) {
                            view.hideAppBarLayout();
                        }
                        view.hideBuyCouponView();
                        view.showProgressTemplateView();
                    }
                })
                .subscribe(new Subscriber<CampaignDetail>() {
                    @Override
                    public void onCompleted() {
                        view.showAppBarLayout();
                        view.showSuccessTemplateView();
                    }

                    @Override
                    public void onError(Throwable e) {
                        PresenterErrorHandler.handleError(e, view);
                    }

                    @Override
                    public void onNext(CampaignDetail campaignDetail) {
                        view.showImageLarge(campaignDetail.getLargeImgUrl());
                        view.setCampaignDetail(campaignDetail);
                        view.setMerchant(campaignDetail.getMerchant());

                        if (campaignDetail.getMerchant() != null
                                && campaignDetail.getMerchant().getTitle() != null) {
                            view.setToolbarTitle(campaignDetail.getMerchant().getTitle());
                        } else {
                            view.setToolbarTitle(StringUtils.EMPTY_STRING);
                        }

                        if (campaignDetail.isActive()) {
                            view.showBuyCouponView();
                            view.showCouponPrice(String.valueOf(campaignDetail.getCouponPrice()));
                        } else {
                            view.hideBuyCouponView();
                        }
                        view.showCommentEdiText();
                    }
                });
        compositeSubscription.add(subscription);
    }

    void onAllViewPagerPagesInitialized() {
        getCampaignById();
    }

    public void onDestroyView() {
        unsubscribe();
    }

    private void unsubscribe() {
        if (compositeSubscription != null
                && !compositeSubscription.isUnsubscribed()) {
            compositeSubscription.unsubscribe();
        }
    }

    void onShareButtonClick() {

    }

    void onPurchaseCouponButtonClick() {
        if (view.getCampaignDetail() != null) {
            CampaignDetail campaignDetail = view.getCampaignDetail();
            view.openPurchaseDialog(campaignDetail.getId(), campaignDetail.getCouponPrice(), campaignDetail.getMaxCouponCount());
        }
    }

    void onTryUploadDataAgainClick() {
        getCampaignById();
    }
}