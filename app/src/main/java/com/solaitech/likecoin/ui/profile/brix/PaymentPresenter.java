package com.solaitech.likecoin.ui.profile.brix;

import com.solaitech.likecoin.data.Constants;
import com.solaitech.likecoin.data.preferences.Preferences;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import static com.solaitech.likecoin.BuildConfig.BACK_LINK;
import static com.solaitech.likecoin.BuildConfig.FAILURE_BACK_LINK;
import static com.solaitech.likecoin.BuildConfig.POST_LINK;
import static com.solaitech.likecoin.BuildConfig.TEMPLATE;
import static com.solaitech.likecoin.data.Constants.KEY_LANG_EPAY_EN;
import static com.solaitech.likecoin.data.Constants.KEY_LANG_EPAY_RU;
import static com.solaitech.likecoin.utils.StringUtils.length;

class PaymentPresenter {
    private final static String SIGNED_ORDER_B64 = "Signed_Order_B64";
    private final static String EMAIL = "email";
    private final static String TEMPLATE_KEY = "template";
    private final static String LANGUAGE = "Language";
    private final static String BACK_LINK_KEY = "BackLink";
    private final static String POST_LINK_KEY = "PostLink";
    private final static String FAILURE_BACK_LINK_KEY = "FailureBackLink";
    private PaymentView view;
    private Preferences preferences;
    private String hash;

    PaymentPresenter(PaymentView view, Preferences preferences, String hash) {
        this.view = view;
        this.preferences = preferences;
        this.hash = hash;
        buyBrix();
    }

    private void buyBrix() {
        Map<String, String> mapParams = new HashMap<>();
        if (length(hash) > 0) {
            mapParams.put(SIGNED_ORDER_B64, hash);
            if (view.getUser() != null && length(view.getUser().getEmail()) > 0) {
                mapParams.put(EMAIL, view.getUser().getEmail());
            }
            mapParams.put(TEMPLATE_KEY, TEMPLATE);

            String epayLanguage = KEY_LANG_EPAY_RU;
            if (preferences.getLanguage().equals(Constants.KEY_LANG_EN)) {
                epayLanguage = KEY_LANG_EPAY_EN;
            }
            mapParams.put(LANGUAGE, epayLanguage);
            mapParams.put(BACK_LINK_KEY, BACK_LINK);
            mapParams.put(POST_LINK_KEY, POST_LINK);
            mapParams.put(FAILURE_BACK_LINK_KEY, FAILURE_BACK_LINK);

            Collection<Map.Entry<String, String>> postData = mapParams.entrySet();
            view.postDataInWebView(postData);
        }
    }
}