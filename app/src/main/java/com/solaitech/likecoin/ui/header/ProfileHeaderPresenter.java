package com.solaitech.likecoin.ui.header;

import com.solaitech.likecoin.data.models.user.User;
import com.solaitech.likecoin.data.repository.user.UserRepositoryProvider;
import com.solaitech.likecoin.exceptions.APIException;
import com.solaitech.likecoin.exceptions.ConnectionTimeOutException;
import com.solaitech.likecoin.exceptions.NeedReAuthorizeException;
import com.solaitech.likecoin.exceptions.UnknownException;
import com.solaitech.likecoin.network.API;
import com.solaitech.likecoin.network.error.RetrofitErrorHandler;
import com.solaitech.likecoin.utils.Logger;
import com.solaitech.likecoin.utils.StringUtils;

import rx.Subscriber;
import rx.Subscription;
import rx.functions.Action0;
import rx.subscriptions.CompositeSubscription;

class ProfileHeaderPresenter {
    private ProfileHeaderView view;
    private API api;

    private CompositeSubscription compositeSubscription = new CompositeSubscription();

    ProfileHeaderPresenter(ProfileHeaderView view, API api) {
        this.view = view;
        this.api = api;
    }

    public void init() {
        User user = view.getUser();

        if (user != null) {
            showUserInfo(user);
        }

        getUser();
    }

    void onResume() {
        User user = view.getUser();

        if (user != null) {
            showUserInfo(user);
        }

        getUser();
    }

    void onProfileImageUpdated() {
        getUser();
    }

    void onProfileUpdated() {
        getUser();
    }

    public void onDestroyView() {
        unsubscribe();
    }

    private void unsubscribe() {
        if (compositeSubscription != null
                && !compositeSubscription.isUnsubscribed()) {
            compositeSubscription.unsubscribe();
        }
    }

    private void getUser() {
        Subscription subscription = UserRepositoryProvider.provideRepository(api)
                .getUser()
                .doOnSubscribe(new Action0() {
                    @Override
                    public void call() {
                        view.showProgressBar();
                    }
                })
                .doOnTerminate(new Action0() {
                    @Override
                    public void call() {
                        view.hideProgressBar();
                    }
                })
                .subscribe(new Subscriber<User>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        e.printStackTrace();
                        String errorText = "";
                        try {
                            RetrofitErrorHandler.handleException(e);
                        } catch (APIException e1) {
                            errorText = e1.getErrorDescr();
                        } catch (UnknownException e1) {
                            errorText = view.getUnknownExceptionMessage();
                        } catch (ConnectionTimeOutException e1) {
                            errorText = view.getConnectionTimeOutExceptionMessage();
                        } catch (NeedReAuthorizeException e1) {
                            e1.printStackTrace();
                        }

                        view.showError(StringUtils.replaceNull(errorText));
                    }

                    @Override
                    public void onNext(User user) {
                        Logger.e(getClass(), "onNext");
                        view.saveUser(user);
                        showUserInfo(user);
                    }
                });
        compositeSubscription.add(subscription);
    }

    private void showUserInfo(User user) {
        view.updateUserInfo(user);
//        if (user.getUserLevel() != null
//                && user.getUserLevel().getNextLevelProgress() != null) {
//            view.showCurrentLevelProgress(user.getUserLevel().getNextLevelProgress());
//        } else {
//            view.showCurrentLevelProgress(0);
//        }

        if (user.getUserLevel() != null
                && user.getUserLevel().getLevel() != null) {
            view.showUserLevel(user.getUserLevel().getLevel());
        } else {
            view.showUserLevel(1);
        }

//        int likesAmount = 0;
//        if (user.getUserLevel() != null
//                && user.getUserLevel().getLikesCount() != null) {
//            likesAmount = user.getUserLevel().getLikesCount();
//            view.showLikesAmount((likesAmount + "").length() > 4
//                    ? StringUtils.coolFormat((double) likesAmount, 0)
//                    : likesAmount + "");
//        } else {
//            view.showLikesAmount(StringUtils.EMPTY_STRING);
//        }

//        if (user.getUserLevel() != null
//                && user.getUserLevel().getNextLevelLikesCount() != null
//                && user.getUserLevel().getLevel() != null) {
//            view.showResidueLike(
//                    user.getUserLevel().getNextLevelLikesCount() - likesAmount,
//                    user.getUserLevel().getLevel() + 1);
//        } else {
//            view.showResidueLike(0, 0);
//        }

        if (user.getAvatarUrl() != null) {
            view.showProfileImage(user.getAvatarUrl());
        }

//        if (user.getName() != null) {
//            view.showUserName(user.getName());
//        } else {
//            view.showUserName(StringUtils.EMPTY_STRING);
//        }

        if (user.getCoinsBalance() != null) {
            view.showCoinsAmount(user.getCoinsBalance());
        } else {
            view.showCoinsAmount(StringUtils.EMPTY_STRING);
        }

        if (user.getLocalBrixAccount() != null) {
            view.showBrixAmount(String.valueOf(user.getLocalBrixAccount().getBalance()));
        } else {
            view.showBrixAmount(StringUtils.EMPTY_STRING);
        }
    }

    void onRepeatRequestClick() {
        view.hideError();
        getUser();
    }
}