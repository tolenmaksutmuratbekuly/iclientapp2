package com.solaitech.likecoin.ui.news_feed.merchant;

import android.content.Context;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.solaitech.likecoin.R;
import com.solaitech.likecoin.data.models.campaign.Merchant;
import com.solaitech.likecoin.data.models.enums.PartnerTypes;

import java.util.List;

public class MerchantLogosContainerView extends LinearLayout {
    private ViewGroup vgContainer;

    public MerchantLogosContainerView(Context context) {
        super(context);
        inflate();
    }

    public MerchantLogosContainerView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        inflate();
    }

    public MerchantLogosContainerView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        inflate();
    }

    private void inflate() {
        LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflater.inflate(R.layout.v_merchant_logos_container, this);

        initView();
    }

    private void initView() {
        vgContainer = (ViewGroup) findViewById(R.id.vg_container);
    }

    public void showLogos(List<Merchant> merchants) {
        vgContainer.removeAllViews();
        for (int i = 0; i < merchants.size(); i++) {
            if (i < PartnerTypes.ROLES_AMOUNT) {
                vgContainer.addView(getLogoView(merchants.get(i).getLogo4AmbassadorUrl()));
            }
        }
    }

    private View getLogoView(String logoUrl) {
        MerchantLogoView view = new MerchantLogoView(getContext());
        view.showLogo(logoUrl);
        return view;
    }
}