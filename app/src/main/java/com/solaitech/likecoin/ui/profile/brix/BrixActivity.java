package com.solaitech.likecoin.ui.profile.brix;

import android.content.Context;
import android.content.Intent;
import android.graphics.Paint;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.util.Pair;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.solaitech.likecoin.App;
import com.solaitech.likecoin.R;
import com.solaitech.likecoin.data.models.user.User;
import com.solaitech.likecoin.ui.base.loading.LoadingBaseActivity;
import com.solaitech.likecoin.ui.campaign.detail.purchase_coupon.count_view.OnPurchaseCountChangedListener;
import com.solaitech.likecoin.ui.campaign.detail.purchase_coupon.count_view.PurchaseCountView;
import com.solaitech.likecoin.ui.header.ProfileHeaderViewGroup;
import com.solaitech.likecoin.ui.menu.MainActivity;
import com.solaitech.likecoin.ui.profile.ProfileActivity;
import com.solaitech.likecoin.ui.profile.edit_profile_image.UserInfoUpdate;
import com.solaitech.likecoin.ui.profile.level.LevelActivity;
import com.solaitech.likecoin.ui.profile.user_statement.UserStatementActivity;
import com.solaitech.likecoin.utils.StringUtils;
import com.solaitech.likecoin.utils.ToastUtils;
import com.solaitech.likecoin.utils.chrome_customtabs.ChromeCustomTabs;
import com.solaitech.likecoin.views.currencyedittext.Callback;
import com.solaitech.likecoin.views.currencyedittext.CurrencyEditText;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.solaitech.likecoin.data.Constants.LC_LANDING_PAGE_URL;

public class BrixActivity extends LoadingBaseActivity implements BrixView, UserInfoUpdate, OnPurchaseCountChangedListener {
    private static final int DEFAULT_BRIX_SUM = 100;
    @Bind(R.id.v_profile) ProfileHeaderViewGroup v_profile;
    @Bind(R.id.pb_indicator) ViewGroup pb_indicator;
    @Bind(R.id.tv_brix_count) TextView tv_brix_count;
    @Bind(R.id.et_money_amount) CurrencyEditText et_money_amount;
    @Bind(R.id.v_brix_count) PurchaseCountView v_brix_count;
    @Bind(R.id.ll_participate_in_ico) LinearLayout ll_participate_in_ico;
    @Bind(R.id.tv_ico_likecoin) TextView tv_ico_likecoin;
    @Bind(R.id.tv_public_offer) TextView tv_public_offer;
    @Bind(R.id.cb_public_offer) CheckBox cb_public_offer;

    private BrixPresenter presenter;
    private double buyRate;
    private int brixCount;

    public static Intent getIntent(Context context) {
        return new Intent(context, BrixActivity.class);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_brix);
        ButterKnife.bind(this);
        brixCount = DEFAULT_BRIX_SUM;
        v_brix_count.setCount(brixCount);
        v_profile.setOnUpdateUserInfo(this);
        presenter = new BrixPresenter(this, api, preferences);

        initViews();
    }

    @Override
    protected void tryUploadDataAgain() {

    }

    private void initViews() {
        tv_public_offer.setPaintFlags(tv_public_offer.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);
        tv_ico_likecoin.setPaintFlags(tv_ico_likecoin.getPaintFlags() | Paint.UNDERLINE_TEXT_FLAG);

        v_brix_count.setOnPurchaseCountChangedListener(this);
        et_money_amount.setCallback(new Callback() {
            @Override
            public void afterTextChanged(String editable) {
//                if (editable.length() > 0) {
//                    double newCountDecimal = Double.parseDouble(editable);
//                    int newCount = (int) (newCountDecimal / buyRate);
//                    v_brix_count.setCount(newCount);
//                    brixCount = newCount;
//                    updateCost();
//                }
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        v_profile.onResume();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        presenter.unsubscribe();
    }

    @Override
    protected void onProfileImageUpdated() {
        super.onProfileImageUpdated();
        v_profile.onProfileImageUpdated();
    }

    @Override
    protected void onProfileUpdated() {
        super.onProfileUpdated();
        v_profile.onProfileUpdated();
    }

    @OnClick(R.id.btn_buy)
    void onBuyBrixClick() {
        if (cb_public_offer.isChecked()) {
            presenter.purchaseBrix(v_brix_count.getEnteredCount(), et_money_amount.getText().toString(), buyRate);
        } else {
            ToastUtils.show(this, getString(R.string.read_privacy_policy));
        }
    }

    @OnClick(R.id.tv_public_offer)
    void onPublicOfferClick() {
        presenter.onPublicOfferClick();
    }

    @OnClick(R.id.tv_ico_likecoin)
    void onIcoLikeCoin() {
        new ChromeCustomTabs().loadWebSite(this, LC_LANDING_PAGE_URL);
    }

    ////////////////////////////////////////////////
    /// BrixView                                 ///
    ////////////////////////////////////////////////

    @Override
    public User getUser() {
        App app = (App) getApplicationContext();
        return app.getUser();
    }

    @Override
    public void showBrixAmount(String amount) {
        tv_brix_count.setText(getString(R.string.brix_value, amount));
    }

    @Override
    public Context getCtx() {
        return this;
    }

    @Override
    public void showLoadingIndicator(boolean show) {
        pb_indicator.setVisibility(show ? View.VISIBLE : View.GONE);
    }

    @Override
    public void setBrixRate(double buyRate) {
        this.buyRate = buyRate;
        presenter.updateCost(buyRate, brixCount);
    }

    @Override
    public void setMoneyAmount(String amount) {
        et_money_amount.setText(amount);
    }

    @Override
    public void unavailablePurchaseBrix() {
        ToastUtils.show(this, getString(R.string.unavailable_course));
        finish();
    }

    @Override
    public void restartActivity() {
        recreate();
    }

    @Override
    public void openPaymentActivity(String hash) {
        startActivity(PaymentActivity.getIntent(this, hash));
    }

    @Override
    public void openPublicOffer(String link) {
        new ChromeCustomTabs().loadWebSite(this, link);
    }

    @Override
    public void showIcoContainer(boolean show) {
        ll_participate_in_ico.setVisibility(!show ? View.VISIBLE : View.GONE);
    }

    ////////////////////////////////////////////////
    /// UserInfoUpdate                           ///
    ////////////////////////////////////////////////

    @Override
    public void onUpdateUserInfo(User user) {
        presenter.setUserInfo(user);
    }

    @Override
    public void openProfileActivity() {
        intentOpener(ProfileActivity.getIntent(this));
    }

    @Override
    public void openUserStatementActivity() {
        intentOpener(UserStatementActivity.getIntent(this));
    }

    @Override
    public void openLevelActivity() {
        intentOpener(LevelActivity.getIntent(this));
    }

    @Override
    public void openBrixActivity() {

    }

    private void intentOpener(Intent intent) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            Pair<View, String> p1 = Pair.create(findViewById(R.id.v_profile), getString(R.string.tn_profile));
            ActivityOptionsCompat options = ActivityOptionsCompat.makeSceneTransitionAnimation(this, p1);
            startActivity(intent, options.toBundle());
        } else {
            startActivity(intent);
        }
        finish();
    }

    @Override
    public void initToolbar(Toolbar toolbar) {
        toolbar.setTitle(StringUtils.EMPTY_STRING);
        toolbar.setNavigationIcon(R.drawable.ic_chevron_left_white_24dp);
        setSupportActionBar(toolbar);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goBack();
            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        goBack();
    }

    private void goBack() {
        if (isTaskRoot()) {
            startActivity(MainActivity.getIntent(this));
        } else {
            supportFinishAfterTransition();
        }
    }

    ////////////////////////////////////////////////
    /// OnPurchaseCountChangedListener           ///
    ////////////////////////////////////////////////

    @Override
    public void onPurchaseCountChanged(int count) {
        this.brixCount = count;
        presenter.updateCost(buyRate, brixCount);
    }
}