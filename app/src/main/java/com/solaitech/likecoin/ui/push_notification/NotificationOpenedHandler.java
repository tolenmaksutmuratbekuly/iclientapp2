package com.solaitech.likecoin.ui.push_notification;

import android.content.Context;

import com.onesignal.OSNotificationOpenResult;
import com.onesignal.OneSignal;

import org.json.JSONObject;

import com.solaitech.likecoin.data.preferences.Preferences;

public class NotificationOpenedHandler implements OneSignal.NotificationOpenedHandler {

    private Context context;
    private Preferences preferences;
    public NotificationOpenedHandler(Context context, Preferences preferences) {
        this.context = context;
        this.preferences = preferences;
    }

    // This fires when a notification is opened by tapping on it.
    @Override
    public void notificationOpened(OSNotificationOpenResult result) {
        if (preferences.isRegistrationCompleted()) {
            JSONObject data = result.notification.payload.additionalData;
            PushNotificationHandler handler = new PushNotificationHandler(context);
            handler.process(data);
        }
    }
}