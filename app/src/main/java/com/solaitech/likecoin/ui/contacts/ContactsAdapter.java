package com.solaitech.likecoin.ui.contacts;

import android.content.Context;
import android.graphics.drawable.GradientDrawable;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.solaitech.likecoin.R;
import com.solaitech.likecoin.data.models.user.User;
import com.solaitech.likecoin.ui.base.recycler_view.RecyclerViewBaseAdapter;
import com.solaitech.likecoin.ui.news_feed.merchant.MerchantLogosContainerView;
import com.solaitech.likecoin.utils.CustomColorUtils;
import com.solaitech.likecoin.utils.ImageUtils;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;

import static com.solaitech.likecoin.utils.StringUtils.PLUS_SIGN;

class ContactsAdapter extends RecyclerViewBaseAdapter {
    private static final int VIEW_TYPE_CONTACTS = 1;
    static final int VIEW_TYPE_FIRST_ELEMENT_EMPTY_SPACE = 2;
    static final int VIEW_TYPE_NO_ACCESS = 3;
    private List<Object> objects;
    private Callback callback;

    ContactsAdapter(Context context, List<Object> objects) {
        super(context);
        this.objects = objects;
    }

    void setCallback(Callback callback) {
        this.callback = callback;
    }

    @Override
    public MainViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        switch (viewType) {
            case VIEW_TYPE_CONTACTS:
                return new ContactsViewHolder(inflate(parent, R.layout.adapter_contacts));
            case VIEW_TYPE_FIRST_ELEMENT_EMPTY_SPACE:
                return new FirstElementEmptySpaceViewHolder(inflate(parent, R.layout.adapter_first_element_empty_space));
            case VIEW_TYPE_NO_ACCESS:
                return new ContactsAccessViewHolder(inflate(parent, R.layout.adapter_contact_access)
                );
            default:
                throw incorrectOnCreateViewHolder();
        }
    }

    @Override
    public void onBindViewHolder(MainViewHolder holder, int position) {
        switch (holder.getItemViewType()) {
            case VIEW_TYPE_CONTACTS:
                ((ContactsViewHolder) holder).bind((User) objects.get(position), position);
                break;
        }
    }

    @Override
    public int getItemViewType(int position) {
        if (objects.get(position) instanceof User) {
            return VIEW_TYPE_CONTACTS;
        } else if (objects.get(position) instanceof Integer) {
            return (Integer) objects.get(position);
        }
        throw incorrectGetItemViewType();
    }

    @Override
    public int getItemCount() {
        return objects.size();
    }

    class ContactsViewHolder extends MainViewHolder {
        @Bind(R.id.vg_container) ViewGroup vg_container;
        @Bind(R.id.tv_name) TextView tv_name;
        @Bind(R.id.tv_phones) TextView tv_phones;
        @Bind(R.id.tv_level) TextView tv_level;
        @Bind(R.id.iv_avatar) CircleImageView iv_avatar;
        @Bind(R.id.v_merchants) MerchantLogosContainerView v_merchants;

        User bindedUser;
        int bindedPosition;

        ContactsViewHolder(View v) {
            super(v);
            ButterKnife.bind(this, v);
        }

        void bind(User user, int position) {
            if (user == null) {
                return;
            }
            bindedUser = user;
            bindedPosition = position;

            if (user.getUserLevel() != null) {
                if (user.getUserLevel().getLevel() != null) {
                    int level = user.getUserLevel().getLevel();
                    tv_level.setText(String.valueOf(level));
                    GradientDrawable drawable = (GradientDrawable) tv_level.getBackground();
                    CustomColorUtils.setLevelColor(getContext(), drawable, level, null);
                }
            }
//            tv_likes_amount.setText(user.getUserLevel().getLikesCount().toString());
            if (user.getName() != null && !user.getName().isEmpty()) {
                tv_name.setText(user.getName());
            } else {
                tv_name.setText(getContext().getString(R.string.label_user));
            }

            tv_phones.setText(String.valueOf(PLUS_SIGN + user.getCellPhone()));
            ImageUtils.showAvatar(getContext(), iv_avatar, user.getAvatarUrl());

            if (user.getAmbassadorRoles() != null) {
                v_merchants.showLogos(user.getAmbassadorRoles());
                v_merchants.setVisibility(View.VISIBLE);
            } else {
                v_merchants.setVisibility(View.GONE);
            }
        }

        @OnClick(R.id.btn_send_like)
        void onSendLikeClick() {
            onItemClick(objects.get(bindedPosition));
        }

        @OnClick(R.id.rl_ava_container)
        void onAvatarClick() {
            if (callback != null) {
                if (bindedUser.getId() != null) {
                    callback.avatarClick(bindedUser.getId());
                }
            }
        }
    }

    private class ContactsAccessViewHolder extends MainViewHolder implements View.OnClickListener {
        ViewGroup vgContainer;
        View btnAllow;

        ContactsAccessViewHolder(View v) {
            super(v);
            vgContainer = v.findViewById(R.id.vg_container);
            btnAllow = v.findViewById(R.id.btn_allow_access_to_contacts);
            btnAllow.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            int itemPosition = getLayoutPosition();
            switch (v.getId()) {
                case R.id.btn_allow_access_to_contacts:
                    onItemClick(objects.get(itemPosition));
                    break;
            }
        }
    }

    interface Callback {
        void avatarClick(String userId);
    }
}