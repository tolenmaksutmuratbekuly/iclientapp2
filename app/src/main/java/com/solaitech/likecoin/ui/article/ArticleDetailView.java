package com.solaitech.likecoin.ui.article;

import com.solaitech.likecoin.data.models.article.Article;
import com.solaitech.likecoin.data.models.campaign.Merchant;
import com.solaitech.likecoin.ui.base.interfaces.MVPBaseView;

interface ArticleDetailView extends MVPBaseView {

    String getArticleId();

    boolean isAppBarLayoutExpanded();

    void hideAppBarLayout();

    void showAppBarLayout();

    void showImageLarge(String imgUrl);

    void setArticleDetail(Article articleDetail);

    void setMerchant(Merchant merchant);

    void setBuyState(boolean isBought);

    void showBuyArticleView();

    void showArticlePrice(String price);

    void hideBuyArticleView();

    void setToolbarTitle(String title);

    void showCommentEdiText();

    void showSuccessToastMessage();

    void sendBroadcast();
}