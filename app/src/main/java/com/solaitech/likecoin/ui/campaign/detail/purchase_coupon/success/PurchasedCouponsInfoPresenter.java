package com.solaitech.likecoin.ui.campaign.detail.purchase_coupon.success;

class PurchasedCouponsInfoPresenter {
    private PurchasedCouponsInfoView view;
    PurchasedCouponsInfoPresenter(PurchasedCouponsInfoView view) {
        this.view = view;
    }
    public void init() {
        if (view.getCouponsQuantity() != null) {
            view.showCouponsQuantity(view.getCouponsQuantity());
        }

        if (view.getTotalSum() != null) {
            view.showTotalSum(view.getTotalSum());
        }
    }

    void onOkButtonClick() {
        view.closeDialog();
    }
}