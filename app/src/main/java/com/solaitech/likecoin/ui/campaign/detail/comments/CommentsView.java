package com.solaitech.likecoin.ui.campaign.detail.comments;

import com.solaitech.likecoin.data.models.campaign.Comments;
import com.solaitech.likecoin.ui.base.interfaces.MVPBaseView;

import java.util.List;

interface CommentsView extends MVPBaseView {
    /**
     * ADAPTER OBJECTS
     */
    void updateAdapterData(List<? extends Object> comments);

    void addAdapterData(List<Comments> comments);

    void addAdapterData(Comments comment);

    void scrollToPosition(int position);

    void showToastQueryError();

    /**
     * PAGINATE
     */
    void setupPagination();

    void unbindPagination();

    /**
     * PAGINATE VALUES
     */
    //down
    void setIsLoading(boolean loading);

    void setHasLoadedAllItems(boolean hasLoadedAllItems);

    void setBaseId(String baseId);

    String getBaseId();

    void setHasMoreDataToLoad(boolean value);

    // Comment
    void showErrorCommentFieldEmpty();

    void showPbDialog(boolean show);

    // Like4Entity
    void onLiked4EntitySuccess(Comments comment, int position);

    void sendBroadcast();
}