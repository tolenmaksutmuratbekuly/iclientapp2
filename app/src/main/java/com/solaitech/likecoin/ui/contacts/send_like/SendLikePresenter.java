package com.solaitech.likecoin.ui.contacts.send_like;

import com.solaitech.likecoin.data.params.contacts.LikeSendingByIdParams;
import com.solaitech.likecoin.data.params.contacts.LikeSendingByPhoneParams;
import com.solaitech.likecoin.data.repository.like.LikesRepositoryProvider;
import com.solaitech.likecoin.network.API;
import com.solaitech.likecoin.ui.base.PresenterErrorHandler;

import okhttp3.ResponseBody;
import rx.Subscriber;
import rx.Subscription;
import rx.functions.Action0;
import rx.subscriptions.CompositeSubscription;

class SendLikePresenter {

    private SendLikeView view;
    private API api;

    private CompositeSubscription compositeSubscription = new CompositeSubscription();

    SendLikePresenter(SendLikeView view, API api) {
        this.view = view;
        this.api = api;
    }

    public void init() {

    }

    void onSendButtonClick(int likeCount, String comment) {
        if (view.getDstUser() != null) {
            if (view.isUserProfile()) {
                sendLike(new LikeSendingByIdParams(view.getDstUser().getId(), likeCount, comment));
            } else {
                sendLike(new LikeSendingByPhoneParams(view.getDstUser().getCellPhone(), likeCount, comment));
            }
        } else if (view.getOwner() != null) {
            sendLike(new LikeSendingByIdParams(view.getOwner().getId(), likeCount, comment));
        }
    }

    private void sendLike(Object params) {
        Subscription subscription = LikesRepositoryProvider
                .provideRepository(api)
                .sendLike(params instanceof LikeSendingByPhoneParams
                        ? (LikeSendingByPhoneParams) params
                        : (LikeSendingByIdParams) params)
                .doOnSubscribe(new Action0() {
                    @Override
                    public void call() {
                        view.showProgressDialog();
                    }
                })
                .doOnTerminate(new Action0() {
                    @Override
                    public void call() {
                        view.dismissProgressDialog();
                    }
                })
                .subscribe(new Subscriber<ResponseBody>() {
                    @Override
                    public void onCompleted() {
                        view.showSuccessToastMessage();
                        view.closeDialog();
                    }

                    @Override
                    public void onError(Throwable e) {
                        PresenterErrorHandler.handleError2(e, view);
                    }

                    @Override
                    public void onNext(ResponseBody responseBody) {
                        //do nothing
                    }
                });

        compositeSubscription.add(subscription);
    }

    public void onDismiss() {
        unsubscribe();
    }

    private void unsubscribe() {
        if (compositeSubscription != null
                && !compositeSubscription.isUnsubscribed()) {
            compositeSubscription.unsubscribe();
        }
    }
}