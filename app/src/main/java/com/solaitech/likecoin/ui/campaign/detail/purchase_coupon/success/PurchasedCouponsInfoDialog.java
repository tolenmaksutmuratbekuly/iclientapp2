package com.solaitech.likecoin.ui.campaign.detail.purchase_coupon.success;

import android.app.Dialog;
import android.text.SpannableStringBuilder;
import android.text.Spanned;
import android.view.View;
import android.widget.TextView;

import java.util.List;

import com.solaitech.likecoin.R;
import com.solaitech.likecoin.data.models.coupons.Coupon;
import com.solaitech.likecoin.ui.base.BaseBottomSheetDialogFragment;
import com.solaitech.likecoin.utils.CenteredImageSpan;

public class PurchasedCouponsInfoDialog extends BaseBottomSheetDialogFragment
        implements PurchasedCouponsInfoView, View.OnClickListener {

    private String couponsQuantity;
    private String totalSum;
    private List<Coupon> coupons;
    private TextView tvQuantity;
    private TextView tvTotalSum;
    private PurchasedCouponsInfoPresenter presenter;

    public static PurchasedCouponsInfoDialog newInstance(String couponsQuantity, String totalSum, List<Coupon> coupons) {
        PurchasedCouponsInfoDialog d = new PurchasedCouponsInfoDialog();
        d.couponsQuantity = couponsQuantity;
        d.totalSum = totalSum;
        d.coupons = coupons;
        return d;
    }

    private Callback callback;
    public void setCallback(Callback callback) {
        this.callback = callback;
    }


    @Override
    public void setupDialog(Dialog dialog, int style) {
        View v = View.inflate(getContext(), R.layout.dialog_purchased_coupons_info, null);
        dialog.setContentView(v);
        setBottomSheetCallback(v);
        initView(v);

        presenter = new PurchasedCouponsInfoPresenter(this);
        presenter.init();
    }

    private void initView(View v) {
        tvQuantity = (TextView) v.findViewById(R.id.tv_quantity);
        tvTotalSum = (TextView) v.findViewById(R.id.tv_total_sum);
        v.findViewById(R.id.btn_ok).setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_ok:
                presenter.onOkButtonClick();
                break;
        }
    }

    @Override
    public String getCouponsQuantity() {
        return couponsQuantity;
    }

    @Override
    public String getTotalSum() {
        return totalSum;
    }

    @Override
    public List<Coupon> getCoupons() {
        return coupons;
    }

    @Override
    public void showCouponsQuantity(String couponsQuantity) {
        StringBuilder sb = new StringBuilder();
        sb.append(getString(R.string.quantity));
        sb.append(": ");
        sb.append(couponsQuantity);

        tvQuantity.setText(sb.toString());
    }

    @Override
    public void showTotalSum(String totalSum) {
        SpannableStringBuilder spannableStringBuilder = new SpannableStringBuilder();
        spannableStringBuilder.append(
                getString(R.string.total_sum)
        );
        spannableStringBuilder.append(": ");
        spannableStringBuilder.append(totalSum);
        spannableStringBuilder.append("  ");

        CenteredImageSpan centeredImageSpan = new CenteredImageSpan(getContext(), R.drawable.ic_coin);
        spannableStringBuilder.setSpan(centeredImageSpan, spannableStringBuilder.length() - 1, spannableStringBuilder.length(), Spanned.SPAN_INCLUSIVE_EXCLUSIVE);

        tvTotalSum.setText(spannableStringBuilder);
    }

    @Override
    public void closeDialog() {
        dismiss();
        if (callback != null) {
            callback.finishActivity();
        }
    }

    public interface Callback {
        void finishActivity();
    }
}