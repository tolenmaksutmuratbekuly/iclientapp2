package com.solaitech.likecoin.ui.coupons.active;

import java.util.List;

import com.solaitech.likecoin.data.models.coupons.Coupon;
import com.solaitech.likecoin.ui.base.interfaces.MVPBaseView;

/**
 * Created by sultanbek on 27.03.17.
 */

public interface ActiveCouponsView extends MVPBaseView {

    void openCouponDetailActivity(String couponId);

    void updateAdapterData(List<? extends Object> coupons);

    List<Object> getObjects();

    void clearFilterObjects();

    void onSearchObjectFound(Coupon coupon);

}