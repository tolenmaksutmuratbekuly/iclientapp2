package com.solaitech.likecoin.ui.help;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.solaitech.likecoin.R;
import com.solaitech.likecoin.data.models.common_info.CompanyDetails;
import com.solaitech.likecoin.data.repository.company_details.CompanyDetailsRepositoryProvider;
import com.solaitech.likecoin.ui.base.loading.LoadingBaseActivity;
import com.solaitech.likecoin.ui.campaign.detail.conditions.contacts.CampaignInfoListView;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import rx.Subscriber;
import rx.Subscription;
import rx.functions.Action0;
import rx.subscriptions.CompositeSubscription;

public class CompanyDetailActivity extends LoadingBaseActivity {
    private CompositeSubscription aboutSubscription = new CompositeSubscription();
    @Bind(R.id.toolbar) Toolbar toolbar;
    @Bind(R.id.tv_info) TextView tv_info;
    @Bind(R.id.v_emails) CampaignInfoListView v_emails;
    @Bind(R.id.v_addresses) CampaignInfoListView v_addresses;
    @Bind(R.id.v_phones) CampaignInfoListView v_phones;
    @Bind(R.id.pb_indicator) ViewGroup pb_indicator;

    public static Intent getIntent(Context context) {
        return new Intent(context, CompanyDetailActivity.class);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_company_detail);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        configureActionBar(getSupportActionBar());
        getAbout();
    }

    private void configureActionBar(ActionBar actionBar) {
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
        }
    }

    @Override
    protected void tryUploadDataAgain() {

    }

    private void getAbout() {
        Subscription subscription = CompanyDetailsRepositoryProvider
                .provideRepository(api)
                .getCompanyDetails()
                .doOnSubscribe(new Action0() {
                    @Override
                    public void call() {
                        pb_indicator.setVisibility(View.VISIBLE);
                    }
                })
                .doOnTerminate(new Action0() {
                    @Override
                    public void call() {
                        pb_indicator.setVisibility(View.GONE);
                    }
                })
                .subscribe(new Subscriber<CompanyDetails>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {

                    }

                    @Override
                    public void onNext(CompanyDetails companyDetails) {
                        if (companyDetails != null) {
                            tv_info.setText(companyDetails.getCompanyName());
                            // Emails
                            if (companyDetails.getEmails() != null && companyDetails.getEmails().size() > 0) {
                                v_emails.setVisibility(View.VISIBLE);
                                v_emails.showList(companyDetails.getEmails(), CampaignInfoListView.VIEW_EMAILS);
                            } else {
                                v_emails.setVisibility(View.GONE);
                            }

                            // Addresses
                            if (companyDetails.getAddresses() != null && companyDetails.getAddresses().size() > 0) {
                                v_addresses.setVisibility(View.VISIBLE);
                                v_addresses.showList(companyDetails.getAddresses(), CampaignInfoListView.VIEW_ADDRESSES);
                            } else {
                                v_addresses.setVisibility(View.GONE);
                            }

                            // WhatsApp
                            List<String> phoneNumbers = new ArrayList<>();
//                            if (companyDetails.getWhatsAppNumbers() != null && companyDetails.getWhatsAppNumbers().size() > 0) {
//                                phoneNumbers.addAll(companyDetails.getWhatsAppNumbers());
//                            }

                            // Mobile numbers
                            if (companyDetails.getMobileNumbers() != null && companyDetails.getMobileNumbers().size() > 0) {
                                phoneNumbers.addAll(companyDetails.getMobileNumbers());
                            }

                            // Landline numbers
                            if (companyDetails.getLandlineNumbers() != null && companyDetails.getLandlineNumbers().size() > 0) {
                                phoneNumbers.addAll(companyDetails.getLandlineNumbers());
                            }

                            if (phoneNumbers.size() > 0) {
                                v_phones.setVisibility(View.VISIBLE);
                                v_phones.showList(phoneNumbers, CampaignInfoListView.VIEW_CONTACTS);
                            } else {
                                v_phones.setVisibility(View.GONE);
                            }

                        }
                    }
                });
        aboutSubscription.add(subscription);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (aboutSubscription != null
                && !aboutSubscription.isUnsubscribed()) {
            aboutSubscription.unsubscribe();
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}