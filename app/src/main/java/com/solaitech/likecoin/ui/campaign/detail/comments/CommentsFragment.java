package com.solaitech.likecoin.ui.campaign.detail.comments;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.solaitech.likecoin.App;
import com.solaitech.likecoin.R;
import com.solaitech.likecoin.data.models.campaign.Comments;
import com.solaitech.likecoin.data.models.enums.BroadcastEventEnums;
import com.solaitech.likecoin.interfaces.OnFragmentViewCreatedListener;
import com.solaitech.likecoin.paginate.LoadingListItemSpanLookup;
import com.solaitech.likecoin.paginate.Paginate;
import com.solaitech.likecoin.ui.base.recycler_view.RecyclerViewBaseFragment;
import com.solaitech.likecoin.ui.profile.ProfileActivity;
import com.solaitech.likecoin.ui.user_page.UserActivity;
import com.solaitech.likecoin.utils.ToastUtils;
import com.solaitech.likecoin.utils.animation.FlippingAnimationCallback;

import java.util.List;

import static com.solaitech.likecoin.utils.StringUtils.EMPTY_STRING;

public class CommentsFragment extends RecyclerViewBaseFragment implements CommentsView {
    private OnFragmentViewCreatedListener onFragmentViewCreatedListener;
    private CommentsPresenter presenter;
    private ProgressDialog progressDialog;
    private Callback callback;
    private FlippingAnimationCallback coinFlippingCallback;

    // PAGINATION
    private Paginate paginate;

    private boolean loading = false;
    private boolean hasLoadedAllItems = false;
    private String baseId = "";
    private String campaignId;
    private String entityType;
    private String localUserId;

    public static CommentsFragment newInstance(String id, String entityType) {
        CommentsFragment f = new CommentsFragment();
        Bundle args = new Bundle();
        args.putString("id", id);
        args.putString("entityType", entityType);
        f.setArguments(args);
        return f;
    }

    public void setCallback(Callback callback) {
        this.callback = callback;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Bundle args = getArguments();
        campaignId = args.getString("id");
        entityType = args.getString("entityType");
        App app = (App) getContext().getApplicationContext();
        localUserId = app.getUser() != null ? app.getUser().getId() : null;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = super.onCreateView(inflater, container, savedInstanceState);
        initView(v);
        presenter = new CommentsPresenter(this, api, campaignId, entityType);
        presenter.init();
        return v;
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.frgm_campaign_comments;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        onFragmentViewCreatedListener.onFragmentViewCreated();
    }

    @Override
    public void onDestroyView() {
        presenter.unsubscribe();
        super.onDestroyView();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        onFragmentViewCreatedListener = (OnFragmentViewCreatedListener) context;
    }

    @Override
    public void onDetach() {
        onFragmentViewCreatedListener = null;
        super.onDetach();
    }

    private void initView(View v) {
        initRecyclerView(v);
        initSwipeRefreshLayout(v);
    }

    @Override
    protected void tryUploadDataAgain() {
        presenter.onTryToUploadDataAgainClick();
    }

    @Override
    protected void onSwipeRefresh() {
        super.onSwipeRefresh();
        presenter.onSwipeRefresh();
    }

    @Override
    protected RecyclerView.LayoutManager getLayoutManager() {
        return getLinearLayoutManager();
    }

    @Override
    protected RecyclerView.Adapter getAdapter() {
        adapter = new CommentsAdapter(getContext(), objectsFilter, localUserId);
        ((CommentsAdapter) adapter).setCallback(new CommentsAdapter.Callback() {
            @Override
            public void onLike4Entity(String entityId, String entityTypeId, int position, FlippingAnimationCallback listener) {
                coinFlippingCallback = listener;
                presenter.onLike4Entity(entityId, entityTypeId, position);
            }

            @Override
            public void avatarClick(String userId) {
                if (localUserId != null && !userId.equals(localUserId)) {
                    Intent intent = UserActivity.getIntent(getContext(), userId, EMPTY_STRING, true);
                    startActivity(intent);
                } else {
                    Intent intent = ProfileActivity.getIntent(getContext());
                    startActivity(intent);
                }
            }
        });
        ((CommentsAdapter) adapter).setOnRecyclerViewItemClickListener(this);
        return adapter;
    }

    // Adding comment
    public void onClickSendComment(String comment) {
        presenter.onClickSendComment(comment);
    }

    // ADAPTER OBJECTS
    @Override
    public void updateAdapterData(List<? extends Object> newsFeeds) {
        objects.clear();
        objects.addAll(newsFeeds);

        objectsFilter.clear();
        if (!newsFeeds.isEmpty()) {
            objectsFilter.add(CommentsAdapter.VIEW_TYPE_FIRST_ELEMENT_EMPTY_SPACE);
        }
        objectsFilter.addAll(objects);
        adapter.notifyDataSetChanged();
    }

    @Override
    public void addAdapterData(List<Comments> comments) {
        objects.addAll(comments);
        objectsFilter.addAll(comments);
        adapter.notifyDataSetChanged();
    }

    @Override
    public void addAdapterData(Comments comment) {
        objects.add(0, comment);
        objectsFilter.add(0, comment);
        adapter.notifyDataSetChanged();
        scrollToPosition(0);
        if (callback != null) {
            callback.onSentComment();
        }
    }

    @Override
    public void scrollToPosition(int position) {
        ((LinearLayoutManager) recyclerView.getLayoutManager()).scrollToPositionWithOffset(position, 0);
    }

    @Override
    public void showToastQueryError() {
        ToastUtils.show(getContext(), getString(R.string.paginate_error_occured));
    }

    /**
     * PAGINATE
     */
    @Override
    public void setupPagination() {
        if (paginate == null) {
            paginate = Paginate.with(recyclerView, callbacks)
                    .setLoadingTriggerThreshold(0)
                    .addLoadingListItem(false)
                    .setLoadingListItemSpanSizeLookup(new LoadingListItemSpanLookup() {
                        @Override
                        public int getSpanSize() {
                            return 1;
                        }
                    })
                    .build();

            paginate.setHasMoreDataToLoad(!hasLoadedAllItems);
            paginate.setHasMoreDataToLoadUp(false);
        }
    }

    private Paginate.Callbacks callbacks = new Paginate.Callbacks() {
        @Override
        public void onLoadMore() {
            // Load next page of data (e.g. network or database)
            presenter.onLoadMore();
        }

        @Override
        public boolean isLoading() {
            // Indicate whether new page loading is in progress or not
            return loading;
        }

        @Override
        public boolean hasLoadedAllItems() {
            // Indicate whether all data (pages) are loaded or not
            return hasLoadedAllItems;
        }

        @Override
        public void onLoadMoreUp() {
        }

        @Override
        public boolean isLoadingUp() {
            return false;
        }

        @Override
        public boolean hasLoadedAllItemsUp() {
            return true;
        }
    };

    @Override
    public void unbindPagination() {
        if (paginate != null) {
            paginate.unbind();
            paginate = null;
        }
    }

    /**
     * PAGINATE VALUES
     */
    @Override
    public void setIsLoading(boolean loading) {
        this.loading = loading;
    }

    @Override
    public void setHasLoadedAllItems(boolean hasLoadedAllItems) {
        this.hasLoadedAllItems = hasLoadedAllItems;
    }

    @Override
    public void setBaseId(String baseId) {
        this.baseId = baseId;
    }

    @Override
    public String getBaseId() {
        return baseId;
    }

    @Override
    public void setHasMoreDataToLoad(boolean value) {
        if (paginate != null) {
            paginate.setHasMoreDataToLoad(value);
        }
    }

    // Posting comment
    @Override
    public void showErrorCommentFieldEmpty() {
        ToastUtils.show(getContext(), getString(R.string.field_error));
    }

    @Override
    public void showPbDialog(boolean show) {
        if (show) {
            if (progressDialog == null
                    || !progressDialog.isShowing()) {
                progressDialog = ProgressDialog.show(getContext(), "", getString(R.string.loading), true, false);
            }
        } else {
            if (progressDialog != null && progressDialog.isShowing()) {
                progressDialog.dismiss();
            }
        }
    }

    @Override
    public void onLiked4EntitySuccess(Comments comment, int position) {
        Toast.makeText(getContext(), getString(R.string.like_sending_success), Toast.LENGTH_SHORT).show();
        if (coinFlippingCallback != null) {
            coinFlippingCallback.like4EntitySuccess(comment, position);
        }
    }

    @Override
    public void sendBroadcast() {
        Intent broadcastIntent = new Intent(BroadcastEventEnums.PROFILE_UPDATED);
        LocalBroadcastManager.getInstance(getContext()).sendBroadcast(broadcastIntent);
    }

    public interface Callback {
        void onSentComment();
    }
}