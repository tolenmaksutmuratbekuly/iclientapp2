package com.solaitech.likecoin.ui.profile.edit_profile_image;

import android.app.IntentService;
import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;

import com.solaitech.likecoin.BuildConfig;
import com.solaitech.likecoin.R;
import com.solaitech.likecoin.data.models.enums.BroadcastEventEnums;
import com.solaitech.likecoin.data.preferences.Preferences;
import com.solaitech.likecoin.data.preferences.PreferencesImpl;
import com.solaitech.likecoin.data.repository.user.UserRepositoryProvider;
import com.solaitech.likecoin.exceptions.APIException;
import com.solaitech.likecoin.exceptions.ConnectionTimeOutException;
import com.solaitech.likecoin.exceptions.NeedReAuthorizeException;
import com.solaitech.likecoin.exceptions.UnknownException;
import com.solaitech.likecoin.network.API;
import com.solaitech.likecoin.network.RetrofitServiceGenerator;
import com.solaitech.likecoin.network.error.RetrofitErrorHandler;
import com.solaitech.likecoin.utils.Logger;
import com.solaitech.likecoin.utils.StringUtils;
import com.solaitech.likecoin.utils.ToastUtils;
import okhttp3.ResponseBody;
import rx.Subscriber;
import rx.functions.Action0;

public class DeleteProfileImageIntentService extends IntentService {

    public DeleteProfileImageIntentService() {
        super("DeleteProfileImageIntentService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        if (intent != null) {
            deleteUserAvatar();
        }
    }

    private void deleteUserAvatar() {
        UserRepositoryProvider.provideRepository(getAPI())
                .deleteUserAvatar()
                .doOnSubscribe(new Action0() {
                    @Override
                    public void call() {
                        ToastUtils.showOnHandler(getApplicationContext(), getString(R.string.your_request_is_being_processed));
                    }
                })
                .subscribe(new Subscriber<ResponseBody>() {
                    @Override
                    public void onCompleted() {
                        Logger.e(getClass(), "onCompleted");
                        ToastUtils.showOnHandler(getApplicationContext(), getString(R.string.profile_image_deleted_successfully));
                        sendBroadcast();
                    }

                    @Override
                    public void onError(Throwable e) {
                        Logger.e(getClass(), "onError");
                        if (BuildConfig.IS_DEBUG) {
                            e.printStackTrace();
                        }
                        try {
                            RetrofitErrorHandler.handleException(e);
                        } catch (APIException e1) {
                            ToastUtils.showOnHandler(getApplicationContext(), StringUtils.replaceNull(e1.getErrorDescr()));
                        } catch (UnknownException e1) {
                            ToastUtils.showOnHandler(getApplicationContext(), getString(R.string.server_error));
                        } catch (ConnectionTimeOutException e1) {
                            ToastUtils.showOnHandler(getApplicationContext(), getString(R.string.connection_error));
                        } catch (NeedReAuthorizeException e1) {

                        }
                    }

                    @Override
                    public void onNext(ResponseBody responseBody) {
                        Logger.e(getClass(), "onNext");
                    }
                });
    }

    private API getAPI() {
        Preferences preferences = new PreferencesImpl(this);
        RetrofitServiceGenerator retrofitServiceGenerator = RetrofitServiceGenerator.getInstance(preferences);
        return retrofitServiceGenerator.createService(API.class);
    }

    private void sendBroadcast() {
        Intent broadcastIntent = new Intent(BroadcastEventEnums.PROFILE_IMAGE_UPDATED);
        LocalBroadcastManager.getInstance(this).sendBroadcast(broadcastIntent);
    }
}