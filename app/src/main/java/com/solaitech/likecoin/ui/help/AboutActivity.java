package com.solaitech.likecoin.ui.help;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import com.solaitech.likecoin.R;
import com.solaitech.likecoin.ui.base.BaseActivity;

public class AboutActivity extends BaseActivity {
    public static Intent getIntent(Context context) {
        return new Intent(context, AboutActivity.class);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_about;
    }
}