package com.solaitech.likecoin.ui.system_messages;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.solaitech.likecoin.R;
import com.solaitech.likecoin.data.models.article.Article;
import com.solaitech.likecoin.data.models.campaign.Merchant;
import com.solaitech.likecoin.data.models.enums.BroadcastEventEnums;
import com.solaitech.likecoin.interfaces.OnFragmentViewCreatedListener;
import com.solaitech.likecoin.ui.article.about_author.AboutAuthorFragment;
import com.solaitech.likecoin.ui.article.content.ArticleContentFragment;
import com.solaitech.likecoin.ui.base.loading.LoadingBaseActivity;
import com.solaitech.likecoin.ui.base.view_pager.BaseFragmentPagerAdapter;
import com.solaitech.likecoin.ui.campaign.detail.comments.CommentsFragment;
import com.solaitech.likecoin.ui.system_messages.about.MsgOwnerFragment;
import com.solaitech.likecoin.ui.system_messages.content.MsgContentFragment;
import com.solaitech.likecoin.utils.StringUtils;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.solaitech.likecoin.utils.KeyboardUtils.hideSoftKeyboard;

public class MsgDetailActivity extends LoadingBaseActivity implements
        MsgDetailView, OnFragmentViewCreatedListener {

    private String id;
    private String entityType;
    private boolean openComments;
    private List<Fragment> viewPagerPages = new ArrayList<>();

    @Bind(R.id.toolbar) Toolbar toolbar;

    @Bind(R.id.tab_layout) TabLayout tab_layout;
    @Bind(R.id.view_pager) ViewPager view_pager;
    @Bind(R.id.ll_comment_container) LinearLayout ll_comment_container;
    @Bind(R.id.et_comment) EditText et_comment;

    private MsgDetailPresenter presenter;

    public static Intent getIntent(Context context, String id, String entityType, boolean openCommentsTab) {
        Bundle bundle = new Bundle();
        bundle.putString("id", id);
        bundle.putString("type", entityType);
        bundle.putBoolean("openComments", openCommentsTab);

        Intent intent = new Intent(context, MsgDetailActivity.class);
        intent.putExtras(bundle);
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_msg_detail);
        ButterKnife.bind(this);
        handleIntent(getIntent());
        initView();

        presenter = new MsgDetailPresenter(this, api);
        presenter.init();
    }

    private void handleIntent(Intent intent) {
        if (intent != null && intent.getExtras() != null) {
            Bundle bundle = intent.getExtras();
            id = bundle.getString("id");
            entityType = bundle.getString("type");
            openComments = bundle.getBoolean("openComments");
        }
    }

    private void initView() {
        initToolbar();
        initViewPager();

        initContainerView(R.id.coordinator_layout);
        initLoadingView();
    }

    private void initToolbar() {
        setSupportActionBar(toolbar);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        if (getSupportActionBar() != null) getSupportActionBar().setTitle("");
        toolbar.setTitle(StringUtils.EMPTY_STRING);
    }

    private void initViewPager() {
        CommentsFragment commentsFragment = CommentsFragment.newInstance(id, entityType);
        viewPagerPages.clear();
        viewPagerPages.add(MsgContentFragment.newInstance());
        viewPagerPages.add(MsgOwnerFragment.newInstance());

        commentsFragment.setCallback(new CommentsFragment.Callback() {
            @Override
            public void onSentComment() {
                et_comment.setText(StringUtils.EMPTY_STRING);
                hideSoftKeyboard(et_comment);
            }
        });
        viewPagerPages.add(commentsFragment);

        PagerAdapter pagerAdapter = new BaseFragmentPagerAdapter(getSupportFragmentManager()) {
            @Override
            public CharSequence getPageTitle(int position) {
                switch (position) {
                    case 0:
                        return getString(R.string.article);
                    case 1:
                        return getString(R.string.author);
                    case 2:
                        return getString(R.string.comments);
                    default:
                        return "";
                }
            }

            @Override
            public Fragment getItem(int position) {
                return viewPagerPages.get(position);
            }

            @Override
            public int getCount() {
                return viewPagerPages.size();
            }
        };
        view_pager.setOffscreenPageLimit(3);
        view_pager.setAdapter(pagerAdapter);
        view_pager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                if (position == 2) {
                    ll_comment_container.setVisibility(View.VISIBLE);
                } else {
                    ll_comment_container.setVisibility(View.GONE);
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
        tab_layout.setupWithViewPager(view_pager);
    }

    // Adding comment
    @OnClick(R.id.ll_send_comment)
    void onClickSendComment() {
        if (viewPagerPages.get(2) instanceof CommentsFragment) {
            ((CommentsFragment) viewPagerPages.get(2)).onClickSendComment(et_comment.getText().toString());
        }
    }

    @Override
    public void onFragmentViewCreated() {
        for (Fragment f : viewPagerPages) {
            if (f.getView() == null) {
                return;
            }
        }
        presenter.onAllViewPagerPagesInitialized();
    }

    @Override
    protected void tryUploadDataAgain() {
        presenter.onTryUploadDataAgainClick();
    }

    @Override
    public String getArticleId() {
        return id;
    }

    @Override
    public void setArticleDetail(Article articleDetail) {
        if (viewPagerPages.get(0) instanceof ArticleContentFragment) {
            ((ArticleContentFragment) viewPagerPages.get(0)).setArticleDetail(articleDetail);
        }
    }

    @Override
    public void setMerchant(Merchant merchant) {
        if (viewPagerPages.get(1) instanceof AboutAuthorFragment) {
            AboutAuthorFragment fragment = (AboutAuthorFragment) viewPagerPages.get(1);
            fragment.setMerchant(merchant);
        }
    }

    @Override
    public void setToolbarTitle(String title) {
        toolbar.setTitle(title);
    }

    @Override
    public void showCommentEdiText() {
        if (openComments) {
            ll_comment_container.setVisibility(View.VISIBLE);
            view_pager.setCurrentItem(viewPagerPages.size() - 1);
        }
    }

    @Override
    public void showSuccessToastMessage() {
        Toast.makeText(this, getString(R.string.like_sending_success), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void sendBroadcast() {
        Intent broadcastIntent = new Intent(BroadcastEventEnums.PROFILE_UPDATED);
        LocalBroadcastManager.getInstance(this).sendBroadcast(broadcastIntent);
    }

    @Override
    protected void onDestroy() {
        presenter.onDestroyView();
        super.onDestroy();
    }
}