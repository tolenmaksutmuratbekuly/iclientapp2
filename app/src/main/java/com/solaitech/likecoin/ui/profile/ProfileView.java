package com.solaitech.likecoin.ui.profile;

import android.net.Uri;

import com.solaitech.likecoin.data.models.campaign.Merchant;
import com.solaitech.likecoin.data.models.user.User;

import java.util.List;

public interface ProfileView {

    User getUser();

    void openEditProfileDialog(User user);

    void openEditProfileImageDialog();

    void selectProfileImageFromIntent();

    void dispatchTakePictureIntent();

    void startServiceToDeleteProfileImage();

    void startServiceToUploadProfileImage(Uri uri);

    boolean isPermissionGrantedToStorage();

    boolean shouldShowRequestPermissionRationaleToStorage();

    void showRequestPermissionRationaleToStorage();

    void requestPermissionToStorage();

    void showRequestPermissionDialogToStorage();

    void openSettingsActivity();

    void openRegistrationActivity();

    void showLoadingIndicator(boolean show);

    void showPhoneNumber(String cellPhone);

    void showEmail(String email);

    void showUserLevel(int userLevel);

    void showProfileImage(String avatarUrl);

    void showUserName(String name);

    void showSurname(String surname);

    void setLanguage(String language);

    void setAmbassadorRoles(List<Merchant> ambassadorRoles);

    void hideAmbassadorRoles();

    void sendBroadCast();

    void showQrCode(String key);
}