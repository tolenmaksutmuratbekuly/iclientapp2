package com.solaitech.likecoin.ui.profile.user_statement;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.util.Pair;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;

import com.solaitech.likecoin.App;
import com.solaitech.likecoin.R;
import com.solaitech.likecoin.data.models.user.User;
import com.solaitech.likecoin.data.models.user_statement.UserStatement;
import com.solaitech.likecoin.ui.base.recycler_view.RecyclerViewBaseActivity;
import com.solaitech.likecoin.ui.dialogs.CustomDatePickerDialog;
import com.solaitech.likecoin.ui.header.ProfileHeaderViewGroup;
import com.solaitech.likecoin.ui.profile.ProfileActivity;
import com.solaitech.likecoin.ui.profile.brix.BrixActivity;
import com.solaitech.likecoin.ui.profile.edit_profile_image.UserInfoUpdate;
import com.solaitech.likecoin.ui.profile.level.LevelActivity;
import com.solaitech.likecoin.ui.user_page.UserActivity;

import java.util.Date;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.solaitech.likecoin.utils.StringUtils.EMPTY_STRING;

public class UserStatementActivity extends RecyclerViewBaseActivity implements UserStatementView, UserInfoUpdate {
    @Bind(R.id.v_profile) ProfileHeaderViewGroup v_profile;
    @Bind(R.id.btn_start_date) Button btn_start_date;
    @Bind(R.id.btn_end_date) Button btn_end_date;

    private UserStatementPresenter presenter;
    private Date startDate;
    private Date endDate;
    private String localUserId;

    public static Intent getIntent(Context context) {
        return new Intent(context, UserStatementActivity.class);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_statement);
        ButterKnife.bind(this);

        v_profile.setOnUpdateUserInfo(this);
        initContainerView(R.id.vg_content);
        initLoadingView();
        initRecyclerView();
        initSwipeRefreshLayout();

        App app = (App) getApplicationContext();
        localUserId = app.getUser() != null ? app.getUser().getId() : null;

        presenter = new UserStatementPresenter(this, api);
        presenter.init();
    }

    @Override
    protected RecyclerView.LayoutManager getLayoutManager() {
        return getLinearLayoutManager();
    }

    @Override
    protected RecyclerView.Adapter getAdapter() {
        adapter = new UserStatementAdapter(this, objects);
        ((UserStatementAdapter) adapter).setCallback(new UserStatementAdapter.Callback() {
            @Override
            public void userAvatarClick(String userId) {
                if (!userId.equals(localUserId)) {
                    Intent intent = UserActivity.getIntent(UserStatementActivity.this, userId, EMPTY_STRING, true);
                    startActivity(intent);
                } else {
                    Intent intent = ProfileActivity.getIntent(UserStatementActivity.this);
                    startActivity(intent);
                }
            }

            @Override
            public void merchantLogoClick(String merchantId, String title) {
                Intent intent = UserActivity.getIntent(UserStatementActivity.this, merchantId, title, false);
                startActivity(intent);
            }
        });

        ((UserStatementAdapter) adapter).setOnRecyclerViewItemClickListener(this);
        return adapter;
    }

    @Override
    protected void tryUploadDataAgain() {
        presenter.onTryToUploadDataAgainClick();
    }

    @Override
    protected void onSwipeRefresh() {
        super.onSwipeRefresh();
        presenter.onSwipeRefresh();
    }

    @Override
    public void showUserStatement(List<UserStatement> userStatements) {
        objects.clear();
        objects.addAll(userStatements);
        if (!objects.isEmpty()) {
            objects.add(UserStatementAdapter.VIEW_TYPE_EMPTY_SPACE);
        }
        adapter.notifyDataSetChanged();
    }

    @Override
    protected void onResume() {
        super.onResume();
        v_profile.onResume();
    }

    @Override
    protected void onProfileImageUpdated() {
        super.onProfileImageUpdated();
        v_profile.onProfileImageUpdated();
    }

    @Override
    protected void onProfileUpdated() {
        super.onProfileUpdated();
        v_profile.onProfileUpdated();
    }

    @OnClick(R.id.btn_start_date)
    void onStartDateClick() {
        presenter.onChooseStartDateClick();
    }

    @OnClick(R.id.btn_end_date)
    void onEndDateClick() {
        presenter.onChooseEndDateClick();
    }

    ////////////////////////////////////////////////
    /// UserStatementView                        ///
    ////////////////////////////////////////////////

    @Override
    public void chooseStartDate(Date currentDate, Date maxDate) {
        CustomDatePickerDialog dialog = CustomDatePickerDialog.newInstance(currentDate, null, maxDate);
        dialog.setCallback(new CustomDatePickerDialog.Callback() {
            @Override
            public void onDateSet(Bundle bundle) {
                if (bundle != null) {
                    Date dateFrom = (Date) bundle.getSerializable("date");
                    presenter.onStartDateChosen(dateFrom);
                }
            }
        });
        dialog.show(getSupportFragmentManager(), "");
    }

    @Override
    public void chooseEndDate(Date currentDate, Date minDate) {
        CustomDatePickerDialog dialog = CustomDatePickerDialog.newInstance(currentDate, minDate, null);
        dialog.setCallback(new CustomDatePickerDialog.Callback() {
            @Override
            public void onDateSet(Bundle bundle) {
                if (bundle != null) {
                    Date dateTo = (Date) bundle.getSerializable("date");
                    presenter.onEndDateChosen(dateTo);
                }
            }
        });
        dialog.show(getSupportFragmentManager(), "");
    }

    @Override
    public void setStartDate(Date date) {
        this.startDate = date;
    }

    @Override
    public void setEndDate(Date date) {
        this.endDate = date;
    }

    @Override
    public Date getStartDate() {
        return startDate;
    }

    @Override
    public Date getEndDate() {
        return endDate;
    }

    @Override
    public void showStartDate(String date) {
        btn_start_date.setText(date);
    }

    @Override
    public void showEndDate(String date) {
        btn_end_date.setText(date);
    }

    ////////////////////////////////////////////////
    /// UserInfoUpdate                           ///
    ////////////////////////////////////////////////

    @Override
    public void onUpdateUserInfo(User user) {

    }

    @Override
    public void openProfileActivity() {
        intentOpener(ProfileActivity.getIntent(this));
    }

    @Override
    public void openUserStatementActivity() {
    }

    @Override
    public void openLevelActivity() {
        intentOpener(LevelActivity.getIntent(this));
    }

    @Override
    public void openBrixActivity() {
        intentOpener(BrixActivity.getIntent(this));
    }

    @Override
    public void initToolbar(Toolbar toolbar) {
        toolbar.setTitle(EMPTY_STRING);
        toolbar.setNavigationIcon(R.drawable.ic_chevron_left_white_24dp);
        setSupportActionBar(toolbar);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                supportFinishAfterTransition();
            }
        });
    }

    private void intentOpener(Intent intent) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            Pair<View, String> p1 = Pair.create(findViewById(R.id.v_profile), getString(R.string.tn_profile));
            ActivityOptionsCompat options = ActivityOptionsCompat.makeSceneTransitionAnimation(this, p1);
            startActivity(intent, options.toBundle());
        } else {
            startActivity(intent);
        }
        finish();
    }
}