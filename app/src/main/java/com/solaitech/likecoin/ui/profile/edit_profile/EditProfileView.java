package com.solaitech.likecoin.ui.profile.edit_profile;

import com.solaitech.likecoin.data.models.user.User;
import com.solaitech.likecoin.ui.base.interfaces.MVPBaseErrorView;

interface EditProfileView extends MVPBaseErrorView {

    User getUser();

    void showEmail(String email);

    void showName(String name);

    void showSurname(String surname);

    String getEnteredEmail();

    String getEnteredName();

    String getEnteredSurname();

    void showEmailEmptyError();

    void showEmailInvalidError();

    void showNameEmptyError();

    void showSurnameEmptyError();

    void showProgressDialog();

    void dismissProgressDialog();

    void showUserUpdateSuccessToastMessage();

    void closeDialog();

    void startService(String name, String surname, String email);

}