package com.solaitech.likecoin.ui.campaign.detail.purchase_coupon;

import java.util.List;

import com.solaitech.likecoin.data.models.coupons.Coupon;
import com.solaitech.likecoin.data.params.CouponPurchaseParams;
import com.solaitech.likecoin.data.repository.coupons.CouponsRepositoryProvider;
import com.solaitech.likecoin.network.API;
import com.solaitech.likecoin.ui.base.PresenterErrorHandler;
import rx.Subscriber;
import rx.Subscription;
import rx.functions.Action0;
import rx.subscriptions.CompositeSubscription;

class PurchaseCouponPresenter {
    private PurchaseCouponView view;
    private API api;

    private CompositeSubscription compositeSubscription = new CompositeSubscription();

    PurchaseCouponPresenter(PurchaseCouponView view, API api) {
        this.view = view;
        this.api = api;
    }

    public void init() {
        view.showTotalSum(String.valueOf(view.getCouponPrice()));
    }

    void onPurchaseButtonClick() {
        if (view.getCouponsCount() <= view.getMaxCouponsCount()) {
            purchaseCoupon();
        }
//        else {
//            //TODO show smth
//        }
    }

    private void purchaseCoupon() {
        Subscription subscription = CouponsRepositoryProvider
                .provideRepository(api)
                .couponPurchase(getPurchaseParams())
                .doOnSubscribe(new Action0() {
                    @Override
                    public void call() {
                        view.showProgressDialog();
                    }
                })
                .doOnTerminate(new Action0() {
                    @Override
                    public void call() {
                        view.dismissProgressDialog();
                    }
                })
                .subscribe(new Subscriber<List<Coupon>>() {
                    @Override
                    public void onCompleted() {
                        //do nothing
                    }

                    @Override
                    public void onError(Throwable e) {
                        PresenterErrorHandler.handleError2(e, view);
                    }

                    @Override
                    public void onNext(List<Coupon> coupons) {
                        view.closeDialog();
                        view.showCouponsPurchasedDialog(
                                String.valueOf(view.getCouponsCount()), view.getTotalSum(), coupons);
                    }
                });
        compositeSubscription.add(subscription);
    }

    private CouponPurchaseParams getPurchaseParams() {
        return new CouponPurchaseParams(view.getCampaignId(),
                String.valueOf(view.getCouponsCount()));
    }

    public void onDismiss() {
        unsubscribe();
    }

    private void unsubscribe() {
        if (compositeSubscription != null
                && !compositeSubscription.isUnsubscribed()) {
            compositeSubscription.unsubscribe();
        }
    }

    void onPurchaseCountChanged(int count) {
        int totalSum = view.getCouponPrice() * count;
        view.showTotalSum(String.valueOf(totalSum));
    }
}