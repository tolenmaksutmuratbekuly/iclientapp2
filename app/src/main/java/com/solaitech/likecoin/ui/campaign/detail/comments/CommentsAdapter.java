package com.solaitech.likecoin.ui.campaign.detail.comments;

import android.content.Context;
import android.graphics.drawable.GradientDrawable;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.solaitech.likecoin.R;
import com.solaitech.likecoin.data.models.campaign.Comments;
import com.solaitech.likecoin.data.models.campaign.Merchant;
import com.solaitech.likecoin.data.models.enums.NewsSourceEntityRefTypeEnums;
import com.solaitech.likecoin.ui.base.recycler_view.RecyclerViewBaseAdapter;
import com.solaitech.likecoin.ui.news_feed.merchant.MerchantLogosContainerView;
import com.solaitech.likecoin.utils.CustomColorUtils;
import com.solaitech.likecoin.utils.DateTimeUtils;
import com.solaitech.likecoin.utils.ImageUtils;
import com.solaitech.likecoin.utils.StringUtils;
import com.solaitech.likecoin.utils.animation.AnimationCallback;
import com.solaitech.likecoin.utils.animation.CustomAnimationUtils;
import com.solaitech.likecoin.utils.animation.FlippingAnimationCallback;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;

class CommentsAdapter extends RecyclerViewBaseAdapter {
    private static final int VIEW_TYPE_NEWS_SYSTEM_MSG = R.layout.adapter_promo_comment;
    static final int VIEW_TYPE_FIRST_ELEMENT_EMPTY_SPACE = R.layout.adapter_first_element_empty_space;
    private List<Object> objects;
    private String userId;
    private Callback callback;

    CommentsAdapter(Context context, List<Object> objects, String userId) {
        super(context);
        this.objects = objects;
        this.userId = userId;
    }

    void setCallback(Callback callback) {
        this.callback = callback;
    }

    @Override
    public MainViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        switch (viewType) {
            case VIEW_TYPE_NEWS_SYSTEM_MSG:
                return new CommentViewHolder(inflate(parent, VIEW_TYPE_NEWS_SYSTEM_MSG));
            case VIEW_TYPE_FIRST_ELEMENT_EMPTY_SPACE:
                return new FirstElementEmptySpaceViewHolder(inflate(parent, VIEW_TYPE_FIRST_ELEMENT_EMPTY_SPACE));
            default:
                throw incorrectOnCreateViewHolder();
        }
    }

    @Override
    public void onBindViewHolder(MainViewHolder holder, int position) {
        switch (holder.getItemViewType()) {
            case VIEW_TYPE_NEWS_SYSTEM_MSG:
                ((CommentViewHolder) holder).bind((Comments) objects.get(position), position);
                break;
        }
    }

    @Override
    public int getItemViewType(int position) {
        if (objects.get(position) instanceof Comments) {
            return VIEW_TYPE_NEWS_SYSTEM_MSG;
        } else if (objects.get(position) instanceof Integer) {
            return (Integer) objects.get(position);
        }
        throw incorrectGetItemViewType();
    }

    @Override
    public int getItemCount() {
        return objects.size();
    }

    class CommentViewHolder extends MainViewHolder {
        @Bind(R.id.tv_name) TextView tv_name;
        @Bind(R.id.tv_msg) TextView tv_msg;
        @Bind(R.id.tv_level) TextView tv_level;
        @Bind(R.id.iv_avatar) CircleImageView iv_avatar;
        @Bind(R.id.tv_date) TextView tv_date;
        @Bind(R.id.v_merchants) MerchantLogosContainerView v_merchants;
        @Bind(R.id.ll_action) LinearLayout ll_action;
        @Bind(R.id.tv_like_count) TextView tv_like_count;
        @Bind(R.id.img_coin) ImageView img_coin;
        @Bind(R.id.ll_like_count) LinearLayout ll_like_count;

        // Reply owner
        @Bind(R.id.ll_comment_answer) LinearLayout ll_comment_answer;
        @Bind(R.id.img_triangle) ImageView img_triangle;
        @Bind(R.id.tv_name_owner) TextView tv_name_owner;
        @Bind(R.id.tv_name_answer) TextView tv_name_answer;
        @Bind(R.id.tv_msg_answer) TextView tv_msg_answer;
        @Bind(R.id.tv_level_answer) TextView tv_level_answer;
        @Bind(R.id.iv_avatar_answer) CircleImageView iv_avatar_answer;
        @Bind(R.id.tv_date_answer) TextView tv_date_answer;
        @Bind(R.id.v_merchants_answer) MerchantLogosContainerView v_merchants_answer;
        @Bind(R.id.ll_action_answer) LinearLayout ll_action_answer;
        @Bind(R.id.tv_like_count_answer) TextView tv_like_count_answer;
        @Bind(R.id.img_coin_answer) ImageView img_coin_answer;
        @Bind(R.id.ll_like_count_answer) LinearLayout ll_like_count_answer;

        Comments bindedComment;
        int bindedPosition;

        CommentViewHolder(View v) {
            super(v);
            ButterKnife.bind(this, v);
        }

        void bind(Comments comment, int position) {
            bindedComment = comment;
            bindedPosition = position;
            if (comment == null) {
                return;
            }

            // ======= Parent comment =======
            if (comment.getOwner() != null) {
                if (comment.getOwner().isUser()) {
                    setImgUrl(iv_avatar, comment.getOwner().getAvatarUrl());
                    if (comment.getOwner().getCustomData() != null) {
                        if (comment.getOwner().getCustomData().getMerchants() != null) {
                            v_merchants.showLogos(comment.getOwner().getCustomData().getMerchants());
                            v_merchants.setVisibility(View.VISIBLE);
                        }
                    }
                } else {
                    v_merchants.setVisibility(View.GONE);
                    setImgUrl(iv_avatar, comment.getOwner().getLogoUrl());
                }

                if (comment.getOwner().getName() != null) {
                    tv_name.setText(comment.getOwner().getName());
                    tv_name_owner.setText(comment.getOwner().getName());
                } else {
                    tv_name.setText(StringUtils.EMPTY_STRING);
                    tv_name_owner.setText(StringUtils.EMPTY_STRING);
                }

                if (comment.getOwner().getSurname() != null) {
                    tv_name.append(" " + comment.getOwner().getSurname());
                    tv_name_owner.append(" " + comment.getOwner().getSurname());
                } else {
                    tv_name.append(StringUtils.EMPTY_STRING);
                    tv_name_owner.append(StringUtils.EMPTY_STRING);
                }

                // Show user level
                if (comment.getOwner().getCustomData() != null) {
                    if (comment.getOwner().getCustomData().getUserLevel() != null) {
                        tv_level.setVisibility(View.VISIBLE);
                        int level = comment.getOwner().getCustomData().getUserLevel();
                        tv_level.setText(String.valueOf(level));
                        GradientDrawable drawable = (GradientDrawable) tv_level.getBackground();
                        CustomColorUtils.setLevelColor(getContext(), drawable, level, null);
                    } else {
                        tv_level.setVisibility(View.GONE);
                    }
                }

                if (comment.getLikesCounter() != null) {
                    if (comment.getLikesCounter().getTotalLikes() != null &&
                            comment.getLikesCounter().getTotalLikes() > 0) {
                        ll_like_count.setVisibility(View.VISIBLE);
                        tv_like_count.setText(String.valueOf(comment.getLikesCounter().getTotalLikes()));
                    } else {
                        ll_like_count.setVisibility(View.GONE);
                    }

                    if (userId != null && comment.getOwner().getId().equals(userId)) {
                        comment.getLikesCounter().setBeenLiked(true);
                    }

                    img_coin.setImageResource(comment.getLikesCounter().isBeenLiked() ?
                            R.drawable.ic_coin_promotions :
                            R.drawable.ic_header_coin);
                    ll_action.setBackgroundColor(comment.getLikesCounter().isBeenLiked() ?
                            ContextCompat.getColor(getContext(), R.color.btn_like_disabled) :
                            ContextCompat.getColor(getContext(), R.color.actionbar));
                }
            }

            if (comment.getRegDT() != null) {
                tv_date.setText(DateTimeUtils.newsFeedDate(comment.getRegDT(), getContext()));
            }

            if (comment.getMessage() != null) {
                tv_msg.setText(comment.getMessage());
            }

            // ======= Reply comment =======
            if (comment.getReplyMessage() != null) {
                tv_msg_answer.setText(comment.getReplyMessage());
            }

            if (comment.getReplyRegDT() != null) {
                tv_date_answer.setText(DateTimeUtils.newsFeedDate(comment.getReplyRegDT(), getContext()));
            }

            if (comment.getReplyOwner() != null) {
                ll_comment_answer.setVisibility(View.VISIBLE);
                img_triangle.setVisibility(View.VISIBLE);
                if (comment.getReplyOwner().getName() != null) {
                    tv_name_answer.setText(comment.getReplyOwner().getName());
                }

                setImgUrl(iv_avatar_answer, comment.getReplyOwner().getAvatarUrl());
                if (comment.getReplyOwner().getCustomData() != null) {
                    if (comment.getReplyOwner().getCustomData().getEmployerMerchant() != null) {
                        List<Merchant> merchants = new ArrayList<>();
                        merchants.add(comment.getReplyOwner().getCustomData().getEmployerMerchant());
                        v_merchants_answer.showLogos(merchants);
                        v_merchants_answer.setVisibility(View.VISIBLE);
                    } else {
                        v_merchants_answer.setVisibility(View.GONE);
                    }
                }

                if (comment.getReplyLikesCounter() != null) {
                    if (comment.getReplyLikesCounter().getTotalLikes() != null &&
                            comment.getReplyLikesCounter().getTotalLikes() > 0) {
                        ll_like_count_answer.setVisibility(View.VISIBLE);
                        tv_like_count_answer.setText(String.valueOf(comment.getReplyLikesCounter().getTotalLikes()));
                    } else {
                        ll_like_count_answer.setVisibility(View.GONE);
                    }

                    if (userId != null && comment.getReplyOwner().getId().equals(userId)) {
                        comment.getReplyLikesCounter().setBeenLiked(true);
                    }

                    img_coin_answer.setImageResource(comment.getReplyLikesCounter().isBeenLiked() ?
                            R.drawable.ic_coin_promotions :
                            R.drawable.ic_header_coin);
                    ll_action_answer.setBackgroundColor(comment.getReplyLikesCounter().isBeenLiked() ?
                            ContextCompat.getColor(getContext(), R.color.btn_like_disabled) :
                            ContextCompat.getColor(getContext(), R.color.actionbar));
                }
            } else {
                ll_comment_answer.setVisibility(View.GONE);
                img_triangle.setVisibility(View.GONE);
            }
        }

        void setImgUrl(CircleImageView avatar, String avatarUrl) {
            if (avatarUrl != null) {
                ImageUtils.showAvatar(getContext(), avatar, avatarUrl);
            } else {
                iv_avatar.setImageResource(R.drawable.img_placeholder_rounded);
            }
        }

        @OnClick(R.id.ll_action)
        void onLike4Entity() {
            if (bindedComment.getLikesCounter() != null) {
                if (!bindedComment.getLikesCounter().isBeenLiked()) {
                    if (callback != null) {
                        callback.onLike4Entity(bindedComment.getId(), NewsSourceEntityRefTypeEnums.COMMENT, bindedPosition, new FlippingAnimationCallback() {
                            @Override
                            public void show() {
                            }

                            @Override
                            public void like4EntitySuccess(final Object object, final int position) {
                                CustomAnimationUtils.flippingCoinAnimation(getContext(), img_coin, iv_avatar, new AnimationCallback() {
                                    @Override
                                    public void onAnimationFinish() {
                                        objects.set(position, object);
                                        notifyDataSetChanged();
                                    }
                                });
                            }
                        });
                    }
                }
            }
        }

        @OnClick(R.id.ll_action_answer)
        void onLike4EntityReply() {
            if (bindedComment.getReplyLikesCounter() != null) {
                if (!bindedComment.getReplyLikesCounter().isBeenLiked()) {
                    if (callback != null) {
                        callback.onLike4Entity(bindedComment.getId(), NewsSourceEntityRefTypeEnums.COMMENT_REPLY, bindedPosition, new FlippingAnimationCallback() {
                            @Override
                            public void show() {
                            }

                            @Override
                            public void like4EntitySuccess(final Object object, final int position) {
                                CustomAnimationUtils.flippingCoinAnimation(getContext(), img_coin_answer, iv_avatar_answer, new AnimationCallback() {
                                    @Override
                                    public void onAnimationFinish() {
                                        objects.set(position, object);
                                        notifyDataSetChanged();
                                    }
                                });
                            }
                        });
                    }
                }
            }
        }

        @OnClick(R.id.rl_ava_container)
        void onAvatarClick() {
            if (callback != null) {
                if (bindedComment.getOwner() != null && bindedComment.getOwner().getId() != null) {
                    callback.avatarClick(bindedComment.getOwner().getId());
                }
            }
        }

        @OnClick(R.id.rl_ava_container_answer)
        void onAvatarAnswerClick() {
            if (callback != null) {
                if (bindedComment.getReplyOwner() != null && bindedComment.getReplyOwner().getId() != null) {
                    callback.avatarClick(bindedComment.getReplyOwner().getId());
                }
            }
        }
    }

    interface Callback {
        void onLike4Entity(String entityId, String entityTypeId, int position, FlippingAnimationCallback listener);

        void avatarClick(String userId);
    }
}