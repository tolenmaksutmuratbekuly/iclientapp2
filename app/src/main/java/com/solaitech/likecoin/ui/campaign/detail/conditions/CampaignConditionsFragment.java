package com.solaitech.likecoin.ui.campaign.detail.conditions;

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.solaitech.likecoin.R;
import com.solaitech.likecoin.data.PermissionRequestCodes;
import com.solaitech.likecoin.data.models.campaign.CampaignDetail;
import com.solaitech.likecoin.data.models.campaign.Coordinate;
import com.solaitech.likecoin.interfaces.OnFragmentViewCreatedListener;
import com.solaitech.likecoin.ui.base.BaseFragment;
import com.solaitech.likecoin.ui.campaign.detail.conditions.contacts.CampaignInfoListView;
import com.solaitech.likecoin.ui.campaign.detail.conditions.coordinates.CampaignCoordinatesView;
import com.solaitech.likecoin.ui.campaign.detail.conditions.coordinates.OnCampaignCoordinateViewClickListener;
import com.solaitech.likecoin.utils.PermissionUtils;
import com.solaitech.likecoin.utils.StringUtils;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class CampaignConditionsFragment extends BaseFragment
        implements CampaignConditionsView, OnMapReadyCallback, GoogleMap.OnMapLoadedCallback,
        GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener,
        OnCampaignCoordinateViewClickListener, GoogleMap.SnapshotReadyCallback {

    private static String[] PERMISSIONS_LOCATION = {
            Manifest.permission.ACCESS_FINE_LOCATION,
            Manifest.permission.ACCESS_COARSE_LOCATION
    };

    private static final int REQUEST_PERMISSION_FOR_LOCATION = 1;

    private CampaignDetail campaignDetail;

    @Bind(R.id.tv_title) TextView tv_title;
    @Bind(R.id.wv_short_description) WebView wv_short_description;
    @Bind(R.id.wv_full_description) WebView wv_full_description;
    @Bind(R.id.tv_valid_until) TextView tv_valid_until;
    @Bind(R.id.v_contacts) CampaignInfoListView v_contacts;
    @Bind(R.id.v_links) CampaignInfoListView v_links;
    @Bind(R.id.v_coordinates) CampaignCoordinatesView v_coordinates;
    @Bind(R.id.btn_allow_access_to_location) ImageButton btn_allow_access_to_location;
    @Bind(R.id.map_view) MapView map_view;
    @Bind(R.id.rl_map_container) RelativeLayout rl_map_container;

    private OnFragmentViewCreatedListener onFragmentViewCreatedListener;
    private CampaignConditionsPresenter presenter;
    private Callback callback;

    //google map
    private GoogleMap googleMap;
    private GoogleApiClient googleApiClient;

    private Map<Coordinate, Marker> markersMap = new HashMap<>();
    private List<Coordinate> coordinates;

    public static CampaignConditionsFragment newInstance() {
        return new CampaignConditionsFragment();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        onFragmentViewCreatedListener = (OnFragmentViewCreatedListener) context;
    }

    @Override
    public void onDetach() {
        onFragmentViewCreatedListener = null;
        super.onDetach();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.frgm_campaign_conditions, container, false);

        ButterKnife.bind(this, v);
        initView();
        initGoogleMapView(savedInstanceState);

        presenter = new CampaignConditionsPresenter(this);
        presenter.init();
        return v;
    }

    public void setCallback(Callback callback) {
        this.callback = callback;
    }

    private void initView() {
        v_coordinates.setOnCampaignCoordinateViewClickListener(this);

//        Resources res = getResources();
//        float sp16 = res.getDimensionPixelSize(R.dimen.sp_16);
////        int convertedSp16 = Converter.convertSpToPixels(sp16, getContext());
//        int convertedSp16 = (int) sp16;

//        WebSettings shortDescriptionWebSettings = v.getSettings();
//        shortDescriptionWebSettings.setDefaultFontSize(convertedSp16);
//
//        final WebSettings fullDescriptionWebSettings = wvFullDescription.getSettings();
//        fullDescriptionWebSettings.setDefaultFontSize(convertedSp16);

        wv_short_description.setScrollBarStyle(View.SCROLLBARS_INSIDE_OVERLAY);
        wv_full_description.setScrollBarStyle(View.SCROLLBARS_INSIDE_OVERLAY);
    }

    private void initGoogleMapView(Bundle savedInstanceState) {
        // Create an instance of GoogleAPIClient.
        googleApiClient = new GoogleApiClient.Builder(getContext())
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();

        map_view.onCreate(savedInstanceState);
        map_view.getMapAsync(this);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        onFragmentViewCreatedListener.onFragmentViewCreated();
    }

    public void setCampaignDetail(CampaignDetail campaignDetail) {
        this.campaignDetail = campaignDetail;
        presenter.onCampaignDetailSet();
    }

    @Override
    public CampaignDetail getCampaignDetail() {
        return campaignDetail;
    }

    @Override
    public void showTitle(String title) {
        tv_title.setVisibility(View.VISIBLE);
        tv_title.setText(title);
    }

    @Override
    public void showShortDescription(String description) {
        wv_short_description.setVisibility(View.VISIBLE);
        wv_short_description.loadDataWithBaseURL("", description, "text/html; charset=utf-8", "UTF-8", "");
    }

    @Override
    public void showFullDescription(String description) {
        wv_full_description.setVisibility(View.VISIBLE);
        wv_full_description.loadDataWithBaseURL("", description, "text/html; charset=utf-8", "UTF-8", "");
    }

    @Override
    public void showDate(String startDate, String endDate) {
        tv_valid_until.setVisibility(View.VISIBLE);
        tv_valid_until.setText(getString(R.string.valid_until, startDate, endDate));
    }

    @Override
    public void showContacts(List<String> contacts) {
        v_contacts.setVisibility(View.VISIBLE);
        v_contacts.showList(contacts, CampaignInfoListView.VIEW_CONTACTS);
    }

    @Override
    public void showLinks(List<String> links) {
        v_links.setVisibility(View.VISIBLE);
        v_links.showList(links, CampaignInfoListView.VIEW_LINKS);
    }

    @Override
    public void showCoordinates(List<Coordinate> coordinates) {
        v_coordinates.setVisibility(View.VISIBLE);
        v_coordinates.showCoordinates(coordinates);
    }

    @Override
    public void hideTitle() {
        tv_title.setVisibility(View.GONE);
    }

    @Override
    public void hideShortDescription() {
        wv_short_description.setVisibility(View.GONE);
    }

    @Override
    public void hideFullDescription() {
        wv_full_description.setVisibility(View.GONE);
    }

    @Override
    public void hideValidUntil() {
        tv_valid_until.setVisibility(View.GONE);
    }

    @Override
    public void hideContacts() {
        v_contacts.setVisibility(View.GONE);
    }

    @Override
    public void hideLinks() {
        v_links.setVisibility(View.GONE);
    }

    @Override
    public void hideCoordinates() {
        rl_map_container.setVisibility(View.GONE);
        v_coordinates.setVisibility(View.GONE);
    }

    @Override
    public void setCoordinates(List<Coordinate> coordinates) {
        this.coordinates = coordinates;
    }

    @Override
    public List<Coordinate> getCoordinates() {
        return coordinates;
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        this.googleMap = googleMap;
        try {
            configureMap(this.googleMap);
            this.googleMap.setOnMapLoadedCallback(this);
            presenter.onMapReady();
        } catch (GooglePlayServicesNotAvailableException e) {
            e.printStackTrace();
        }
    }

    private void configureMap(final GoogleMap map) throws GooglePlayServicesNotAvailableException {
        if (map == null) {
            return; // Google Maps not available
        }

        MapsInitializer.initialize(getContext());
        map.setInfoWindowAdapter(new GoogleMap.InfoWindowAdapter() {

            // Use default InfoWindow frame
            @Override
            public View getInfoWindow(Marker arg0) {
                return null;
            }

            // Defines the contents of the InfoWindow
            @Override
            public View getInfoContents(Marker arg0) {
                // Getting view from the layout file info_window_layout
                LayoutInflater inflater = LayoutInflater.from(getActivity());
                View v = inflater.inflate(R.layout.v_google_map_info_window, null);

                if (arg0.getTag() != null
                        && arg0.getTag() instanceof Coordinate) {
                    Coordinate coordinate = (Coordinate) arg0.getTag();

                    TextView tv_address = (TextView) v.findViewById(R.id.tv_address);
                    tv_address.setText(StringUtils.replaceNull(coordinate.getAddress()));
                }

                return v;
            }
        });

        map.setOnInfoWindowClickListener(new GoogleMap.OnInfoWindowClickListener() {
            @Override
            public void onInfoWindowClick(Marker arg0) {
            }
        });

        if (isPermissionGrantedToLocation()) {
            map.setMyLocationEnabled(true);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        map_view.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
        map_view.onPause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        map_view.onDestroy();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        map_view.onLowMemory();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        map_view.onSaveInstanceState(outState);
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        presenter.onGoogleApiClientConnected();
    }

    @Override
    public void onConnectionSuspended(int i) {
        //do nothing
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        //do nothing
    }

    @Override
    public void onStart() {
        googleApiClient.connect();
        super.onStart();
    }

    @Override
    public void onStop() {
        googleApiClient.disconnect();
        super.onStop();
    }

    @Override
    public void onCampaignCoordinateViewClick(Coordinate coordinate) {
        CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(
                new LatLng(coordinate.getLatitude(),
                        coordinate.getLongitude()), 16);

        if (googleMap != null) {
            googleMap.animateCamera(cameraUpdate);
        }

        if (markersMap != null) {
            Marker marker = markersMap.get(coordinate);
            if (marker != null) {
                marker.showInfoWindow();
            }
        }
    }

    @Override
    public void clearMarkers() {
        if (googleMap != null) {
            googleMap.clear();
        }

        markersMap.clear();
    }


    @Override
    public void displayMarkers(List<Coordinate> coordinates) {
        if (googleMap == null) {
            return;
        }

        if (coordinates != null
                && !coordinates.isEmpty()) {

            for (Coordinate coordinate : coordinates) {
                addMarker(coordinate);
            }
        }
    }

    private void addMarker(Coordinate coordinate) {
        try {
            Marker marker = googleMap.addMarker(
                            new MarkerOptions().position(coordinate.getLatLng())
                    );
            marker.setTag(coordinate);
            markersMap.put(coordinate, marker);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean isPermissionGrantedToLocation() {
        return (PermissionUtils.isPermissionGranted(getContext(), Manifest.permission.ACCESS_FINE_LOCATION)
                && PermissionUtils.isPermissionGranted(getContext(), Manifest.permission.ACCESS_COARSE_LOCATION));
    }

    @Override
    public boolean shouldShowRequestPermissionRationaleToLocation() {
        return (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION)
                || ActivityCompat.shouldShowRequestPermissionRationale(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION));
    }

    @Override
    public void showRequestPermissionRationaleToLocation() {
        new AlertDialog.Builder(getContext())
                .setMessage(getString(R.string.rationale_location))
                .setPositiveButton(getString(R.string.allow), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                        presenter.onRationaleToLocationAllowed();

                    }
                })
                .setNegativeButton(getString(R.string.reject), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                })
                .create()
                .show();
    }

    @Override
    public void requestPermissionToLocation() {
        requestPermissions(
                PERMISSIONS_LOCATION,
                PermissionRequestCodes.LOCATION);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {

        if (requestCode == PermissionRequestCodes.LOCATION) {
            if (PermissionUtils.verifyPermissions(grantResults)) {
                presenter.onPermissionGrantedToLocation();
            } else {
                presenter.onPermissionNotGrantedToLocation();
            }
        } else {
            super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    @Override
    public void showRequestPermissioDialogToLocation() {
        new AlertDialog.Builder(getContext())
                .setMessage(getString(R.string.rationale_location))
                .setPositiveButton(getString(R.string.allow), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                        presenter.onDialogToLocationAllowed();

                    }
                })
                .setNegativeButton(getString(R.string.reject), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                })
                .create()
                .show();
    }

    @Override
    public void openSettingsActivity() {
        Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
        Uri uri = Uri.fromParts("package", getActivity().getPackageName(), null);
        intent.setData(uri);
        startActivityForResult(intent, REQUEST_PERMISSION_FOR_LOCATION);
    }

    @Override
    public void setMyLocationEnabled() {
        if (googleMap != null
                && isPermissionGrantedToLocation()) {
            googleMap.setMyLocationEnabled(true);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case REQUEST_PERMISSION_FOR_LOCATION: {
                presenter.onActivityResultToLocation();
                break;
            }
        }
    }

    @Override
    public boolean isGoogleApiClientHaveBeenConnected() {
        return googleApiClient != null && googleApiClient.isConnected();
    }

    @Override
    public void initLastLocation() {
        if (googleApiClient != null
                && isPermissionGrantedToLocation()) {
//            Location lastLocation =
            LocationServices.FusedLocationApi.getLastLocation(googleApiClient);
        }
    }

    @Override
    public void moveCamera(double latitude, double longitude) {
        if (googleMap != null) {
            CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(new LatLng(latitude, longitude), 16);
            googleMap.animateCamera(cameraUpdate);
        }
    }

    @Override
    public void hideAllowAccessToLocationButton() {
        btn_allow_access_to_location.setVisibility(View.GONE);
    }

    @Override
    public void showAllowAccessToLocationButton() {
        btn_allow_access_to_location.setVisibility(View.VISIBLE);
    }

    @Override
    public Context getContextFromPage() {
        return getContext();
    }

    @OnClick(R.id.btn_allow_access_to_location)
    void allowAccessToLocationClick() {
        presenter.onAllowAccessToLocationButtonClick();
    }

    @Override
    public void onSnapshotReady(Bitmap bitmap) {
        if (callback != null) {
            callback.onSnapshotReady(bitmap);
        }
    }

    @Override
    public void onMapLoaded() {
        if (googleMap != null) {
            googleMap.snapshot(this);
        }
    }

    public interface Callback {
        void onSnapshotReady(Bitmap bmp);
    }
}