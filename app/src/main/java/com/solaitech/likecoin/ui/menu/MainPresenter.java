package com.solaitech.likecoin.ui.menu;

import com.onesignal.OSPermissionSubscriptionState;
import com.onesignal.OSSubscriptionStateChanges;
import com.onesignal.OneSignal;

import java.util.Timer;
import java.util.TimerTask;

import com.solaitech.likecoin.data.models.app_version_check.AppVersionChecking;
import com.solaitech.likecoin.data.params.PushNotificationAddDeviceParams;
import com.solaitech.likecoin.data.repository.app_version_check.AppVersionCheckRepositoryProvider;
import com.solaitech.likecoin.data.repository.user.UserRepositoryProvider;
import com.solaitech.likecoin.exceptions.AppVersionException;
import com.solaitech.likecoin.network.API;
import okhttp3.ResponseBody;
import rx.Subscriber;
import rx.Subscription;
import rx.subscriptions.CompositeSubscription;

class MainPresenter {

    private static final long TEXT_WATCHER_QUERY_DELAY = 300; // in ms

    private static final String OS = "android";
    private static final String APP_TYPE = "client";

    private MainView view;
    private API api;

    private Timer timer;
    private CompositeSubscription compositeSubscription = new CompositeSubscription();

    MainPresenter(MainView view, API api) {
        this.view = view;
        this.api = api;
    }

    public void init() {
        view.hideContactsFab();

        OneSignal.setSubscription(true);
        addDevice();
    }

    private void addDevice() {
        OSPermissionSubscriptionState status = OneSignal.getPermissionSubscriptionState();

        if (status != null
                && status.getSubscriptionStatus() != null
                && status.getSubscriptionStatus().getUserId() != null) {
            addDevice(status.getSubscriptionStatus().getUserId());
        } else {
            OneSignal.idsAvailable(new OneSignal.IdsAvailableHandler() {
                @Override
                public void idsAvailable(String userId, String registrationId) {
                    if (userId != null) {
                        addDevice(userId);
                    }
                }
            });
        }
    }

    private void addDevice(String userId) {
        Subscription subscription = UserRepositoryProvider
                .provideRepository(api)
                .addDevice(new PushNotificationAddDeviceParams(userId))
                .subscribe(new Subscriber<ResponseBody>() {
                    @Override
                    public void onCompleted() {
                        //do nothing
                    }

                    @Override
                    public void onError(Throwable e) {
                        //do nothing
                    }

                    @Override
                    public void onNext(ResponseBody responseBody) {
                        //do nothing
                    }
                });
        compositeSubscription.add(subscription);
    }

    void onResume() {
        checkAppVersion();
        view.callOnResumeOnProfileView();
    }

    private void checkAppVersion() {
        Subscription subscription = AppVersionCheckRepositoryProvider
                .provideRepository(api)
                .appVersionChecking(OS, view.getAppVersion(), APP_TYPE)
                .subscribe(new Subscriber<AppVersionChecking>() {
                    @Override
                    public void onCompleted() {
                        //do nothing
                    }

                    @Override
                    public void onError(Throwable e) {
                        //do nothing
                    }

                    @Override
                    public void onNext(AppVersionChecking appVersionChecking) {
                        if (!appVersionChecking.isOk()) {
                            view.showInvalidAppVersionDialog(new AppVersionException());
                        }
                    }
                });
        compositeSubscription.add(subscription);
    }

    void onQueryTextChange(final String newText) {
        if (newText.isEmpty()) {
            return;
        }

        if (timer != null)
            timer.cancel();

        timer = new Timer();
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                view.doSearch(newText);
            }
        }, TEXT_WATCHER_QUERY_DELAY);
    }

    void onMenuItemActionCollapse() {
        view.showAllListItems();
    }

    public void onDestroyView() {
        unsubscribe();
    }

    private void unsubscribe() {
        if (compositeSubscription != null
                && !compositeSubscription.isUnsubscribed()) {
            compositeSubscription.unsubscribe();
        }
    }

    void onOSSubscriptionChanged(OSSubscriptionStateChanges stateChanges) {
        if (stateChanges != null
                && stateChanges.getTo() != null
                && stateChanges.getTo().getUserId() != null) {
            addDevice(stateChanges.getTo().getUserId());
        }
    }
}