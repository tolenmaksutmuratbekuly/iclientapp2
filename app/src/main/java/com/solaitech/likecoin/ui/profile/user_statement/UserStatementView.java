package com.solaitech.likecoin.ui.profile.user_statement;

import java.util.Date;
import java.util.List;

import com.solaitech.likecoin.data.models.user_statement.UserStatement;
import com.solaitech.likecoin.ui.base.interfaces.MVPBaseView;

interface UserStatementView extends MVPBaseView {

    void showUserStatement(List<UserStatement> userStatements);

    void setStartDate(Date date);

    void setEndDate(Date date);

    Date getStartDate();

    Date getEndDate();

    void chooseStartDate(Date date, Date maxDate);

    void chooseEndDate(Date date, Date minDate);

    void showStartDate(String date);

    void showEndDate(String date);
}