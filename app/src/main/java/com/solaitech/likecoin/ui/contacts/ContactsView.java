package com.solaitech.likecoin.ui.contacts;

import android.content.ContentResolver;
import android.view.View;

import java.util.List;

import com.solaitech.likecoin.data.models.user.User;
import com.solaitech.likecoin.ui.base.interfaces.MVPBaseView;

interface ContactsView extends MVPBaseView {

    boolean isPermissionGrantedToReadContacts();

    void requestContactsPermissions();

    ContentResolver getContentResolver();

    void updateAdapterData(List<User> registeredUsers);

    View getFABBtn();

    void showHeaderProgress();

    void hideHeaderProgress();


    void saveRegisteredUsers(List<User> registeredUsers);

    List<User> getRegisteredUsers();

    void shareApp(String text);

    void sendLike(User distUser);

    void setContactsUpdated(boolean isUpdated);

    boolean isContactsUpdated();

    void showAccessDisclaimer();
}