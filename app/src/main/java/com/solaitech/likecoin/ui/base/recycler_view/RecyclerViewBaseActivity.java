package com.solaitech.likecoin.ui.base.recycler_view;

import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import java.util.ArrayList;
import java.util.List;

import com.solaitech.likecoin.R;
import com.solaitech.likecoin.interfaces.OnItemClickListener;
import com.solaitech.likecoin.ui.base.loading.LoadingBaseActivity;
import com.solaitech.likecoin.utils.SwipeRefreshLayoutUtils;
import com.solaitech.likecoin.utils.recycler_view.MarginDecoration;
import com.solaitech.likecoin.utils.recycler_view.SimpleDividerItemDecoration;

public abstract class RecyclerViewBaseActivity extends LoadingBaseActivity implements OnItemClickListener {

    protected SwipeRefreshLayout swipeRefreshLayout;
    protected RecyclerView recyclerView;
    protected View emptyView;

    protected RecyclerView.Adapter adapter;
    protected List<Object> objects = new ArrayList<>();
    protected List<Object> objectsFilter = new ArrayList<>();

    protected void initRecyclerView() {
        initEmptyView();

        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        recyclerView.setHasFixedSize(false);

        RecyclerView.LayoutManager rvLayoutManager = getLayoutManager();
        recyclerView.setLayoutManager(rvLayoutManager);

        RecyclerView.Adapter adapter = getAdapter();
        adapter.registerAdapterDataObserver(adapterDataObserver);

        recyclerView.setAdapter(adapter);
    }

    protected void initEmptyView() {
        emptyView = findViewById(R.id.v_empty_view);

        if (!objects.isEmpty()
                && emptyView != null) {
            emptyView.setVisibility(View.GONE);
        }
    }

    protected abstract RecyclerView.LayoutManager getLayoutManager();

    protected abstract RecyclerView.Adapter getAdapter();

    protected void addDividerItemDecoration() {
        recyclerView.addItemDecoration(new SimpleDividerItemDecoration(this));
    }

    protected void addMarginDecoration() {
        recyclerView.addItemDecoration(new MarginDecoration(this));
    }

    protected RecyclerView.LayoutManager getLinearLayoutManager() {
        return new LinearLayoutManager(this);
    }

    protected RecyclerView.LayoutManager getHorizontalLinearLayoutManager() {
        return new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
    }

    private RecyclerView.AdapterDataObserver adapterDataObserver = new RecyclerView.AdapterDataObserver() {
        @Override
        public void onItemRangeMoved(int fromPosition, int toPosition, int itemCount) {
            super.onItemRangeMoved(fromPosition, toPosition, itemCount);
            onAdapterItemCountChanged();
            checkAdapterIsEmpty();
        }

        @Override
        public void onItemRangeRemoved(int positionStart, int itemCount) {
            super.onItemRangeRemoved(positionStart, itemCount);
            onAdapterItemCountChanged();
            checkAdapterIsEmpty();
        }

        @Override
        public void onItemRangeInserted(int positionStart, int itemCount) {
            super.onItemRangeInserted(positionStart, itemCount);
            onAdapterItemCountChanged();
            checkAdapterIsEmpty();
        }

        @Override
        public void onItemRangeChanged(int positionStart, int itemCount, Object payload) {
            super.onItemRangeChanged(positionStart, itemCount, payload);
            onAdapterItemCountChanged();
            checkAdapterIsEmpty();
        }

        @Override
        public void onItemRangeChanged(int positionStart, int itemCount) {
            super.onItemRangeChanged(positionStart, itemCount);
            onAdapterItemCountChanged();
            checkAdapterIsEmpty();
        }

        @Override
        public void onChanged() {
            super.onChanged();
            onAdapterItemCountChanged();
            checkAdapterIsEmpty();
        }
    };

    protected void onAdapterItemCountChanged() {
        //do nothing
    }

    private void checkAdapterIsEmpty() {
        if (emptyView != null) {
            if (isAdapterEmpty()) {
                emptyView.setVisibility(View.VISIBLE);
            } else {
                emptyView.setVisibility(View.GONE);
            }
        }
    }

    protected boolean isAdapterEmpty() {
        return adapter != null && adapter.getItemCount() <= getAdapterEmptyCount();
    }

    protected int getAdapterEmptyCount() {
        return 0;
    }

    protected void initSwipeRefreshLayout() {
        swipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipe_refresh_layout);
        if (swipeRefreshLayout != null) {
            SwipeRefreshLayoutUtils.setColorSchemeColors(this, swipeRefreshLayout);

            swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
                @Override
                public void onRefresh() {
                    onSwipeRefresh();
                }
            });

        }
    }

    protected void onSwipeRefresh() {
        hideSwipeRefreshLayout();
    }

    protected void showSwipeRefreshLayout() {
        SwipeRefreshLayoutUtils.showSwipeRefreshLayout(swipeRefreshLayout);
    }

    protected void hideSwipeRefreshLayout() {
        SwipeRefreshLayoutUtils.hideSwipeRefreshLayout(swipeRefreshLayout);
    }

    @Override
    public void onItemClick(Object object) {
        //override in subclasses
    }
}