package com.solaitech.likecoin.ui.intro;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import com.solaitech.likecoin.R;
import com.solaitech.likecoin.data.preferences.Preferences;
import com.solaitech.likecoin.data.preferences.PreferencesImpl;
import com.solaitech.likecoin.ui.menu.MainActivity;

import butterknife.ButterKnife;

public class SplashActivity extends AppCompatActivity {
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        ButterKnife.bind(this);
        Thread t = new Thread() {
            @Override
            public void run() {
                try {
                    sleep(1500);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                } finally {
                    openNextActivity();

                }
            }
        };
        t.start();
    }

    private void openNextActivity() {
        Preferences preferences = new PreferencesImpl(this);
        Intent intent;
        if (preferences.isRegistrationCompleted()) {
            intent = MainActivity.getIntent(this);
        } else {
            intent = TutorialActivity.getIntent(this);
        }
        startActivity(intent);
        finish();
    }
}