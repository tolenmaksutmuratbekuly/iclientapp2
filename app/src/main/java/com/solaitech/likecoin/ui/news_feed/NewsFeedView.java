package com.solaitech.likecoin.ui.news_feed;

import com.solaitech.likecoin.data.models.news_feed.NewsFeed;
import com.solaitech.likecoin.ui.base.interfaces.MVPBaseView;

import java.util.List;

interface NewsFeedView extends MVPBaseView {

    /**
     * QUERY PARAMS
     */
    String getNewsFeedLastVisibleItemId();

    void setNewsFeedLastVisibleItemId(String lastNewsId);

    /**
     * ADAPTER OBJECTS
     */
    void updateAdapterData(List<? extends Object> newsFeeds);

    void addAdapterData(List<NewsFeed> newsFeeds);

    void addAdapterDataUp(List<NewsFeed> newsFeeds);

    void scrollToPosition(int position);

    void showToastQueryError();

    /**
     * PAGINATE
     */
    void setupPagination();

    void unbindPagination();

    /**
     * PAGINATE VALUES
     */
    //down
    void setIsLoading(boolean loading);

    void setHasLoadedAllItems(boolean hasLoadedAllItems);

    boolean hasLoadedAllItems();

    void setBaseId(String baseId);

    String getBaseId();

    void setHasMoreDataToLoad(boolean value);

    //up
    void setIsLoadingUp(boolean loading);

    void setHasLoadedAllItemsUp(boolean hasLoadedAllItems);

    boolean hasLoadedAllItemsUp();

    void setBaseIdUp(String baseIdUp);

    String getBaseIdUp();

    void setHasMoreDataToLoadUp(boolean value);

    /**
     * SEARCH
     */
    void disableSwipeToRefresh();

    void enableSwipeToRefresh();

    List<Object> getObjects();

    void clearFilterObjects();

    void onSearchObjectFound(NewsFeed newsFeed);

    /**
     * ON RECYCLER VIEW ITEM CLICK
     */
    void openCampaignDetailActivity(String id, boolean openCommentsTab);

    void openUserStatementActivity();

    void openArticleDetailActivity(String id, boolean openCommentsTab);

    void openMsgDetailActivity(String id, String type, boolean openCommentsTab);

    /**
     * SAVE LAST VISIBLE NEWS FEED ID
     */
    int getFirstVisibleItemPosition();

    List<Object> getFilterObjects();

    void saveNewsFeedLastVisibleItemId(String newsFeedId);

    // Send Like
    void showSuccessToastMessage();

    void sendBroadcast();

    void showPbDialog();

    void dismissPbDialog();

    // Like4Entity
    void onLiked4EntitySuccess(NewsFeed newsFeed, int position);

    void onFavoriteState(NewsFeed newsFeed);

    void onArticleBought();

    String getLocalUserId();

    void openUserActivity(String userId, boolean isUser);

    // IcoCondition
    void showIcoContainer(boolean show);
}