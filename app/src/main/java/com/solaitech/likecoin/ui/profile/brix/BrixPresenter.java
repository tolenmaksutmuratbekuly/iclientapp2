package com.solaitech.likecoin.ui.profile.brix;

import com.solaitech.likecoin.BuildConfig;
import com.solaitech.likecoin.R;
import com.solaitech.likecoin.data.models.brix.BrixRate;
import com.solaitech.likecoin.data.models.common_info.IcoCondition;
import com.solaitech.likecoin.data.models.user.User;
import com.solaitech.likecoin.data.params.brix.BuyBrixParams;
import com.solaitech.likecoin.data.preferences.Preferences;
import com.solaitech.likecoin.data.repository.brix.BrixRepositoryProvider;
import com.solaitech.likecoin.data.repository.company_details.CompanyDetailsRepositoryProvider;
import com.solaitech.likecoin.exceptions.APIException;
import com.solaitech.likecoin.exceptions.ConnectionTimeOutException;
import com.solaitech.likecoin.exceptions.NeedReAuthorizeException;
import com.solaitech.likecoin.exceptions.UnknownException;
import com.solaitech.likecoin.network.API;
import com.solaitech.likecoin.network.error.RetrofitErrorHandler;
import com.solaitech.likecoin.utils.Logger;
import com.solaitech.likecoin.utils.StringUtils;
import com.solaitech.likecoin.utils.ToastUtils;

import rx.Subscriber;
import rx.Subscription;
import rx.functions.Action0;
import rx.subscriptions.CompositeSubscription;

import static com.solaitech.likecoin.data.Constants.BRIX_PUBLIC_OFFER_EN;
import static com.solaitech.likecoin.data.Constants.BRIX_PUBLIC_OFFER_RU;
import static com.solaitech.likecoin.data.Constants.KEY_LANG_EN;
import static com.solaitech.likecoin.data.Constants.KEY_LANG_RU;
import static com.solaitech.likecoin.utils.StringUtils.isStringOk;

class BrixPresenter {
    private BrixView view;
    private API api;
    private Preferences preferences;

    private CompositeSubscription compositeSubscription = new CompositeSubscription();

    BrixPresenter(BrixView view, API api, Preferences preferences) {
        this.view = view;
        this.api = api;
        this.preferences = preferences;
        if (view.getUser() != null) {
            setUserInfo(view.getUser());
        }
        localBrixRate();
        icoCondition();
    }

    void setUserInfo(User user) {
        if (user.getLocalBrixAccount() != null) {
            view.showBrixAmount(String.valueOf(user.getLocalBrixAccount().getBalance()));
        } else {
            view.showBrixAmount(StringUtils.EMPTY_STRING);
        }
    }

    void updateCost(double buyRate, int brixCount) {
        if (buyRate > 0 && brixCount > 0) {
            view.setMoneyAmount(String.valueOf((int) (buyRate * brixCount)));
        }
    }

    void purchaseBrix(int brixAmount, String money, double buyRate) {
        double epayAmount = isStringOk(money) ? Double.parseDouble(money) : 0;
        if (buyRate == 0) {
            localBrixRate();
        }
        if (brixAmount > 0 && epayAmount > 0 && buyRate > 0) {
            Subscription subscription = BrixRepositoryProvider
                    .provideRepository(api)
                    .purchaseBrix(new BuyBrixParams(brixAmount, epayAmount, buyRate))
                    .doOnSubscribe(new Action0() {
                        @Override
                        public void call() {
                            view.showLoadingIndicator(true);
                        }
                    })
                    .doOnTerminate(new Action0() {
                        @Override
                        public void call() {
                            view.showLoadingIndicator(false);
                        }
                    })
                    .subscribe(new Subscriber<String>() {
                        @Override
                        public void onCompleted() {
                        }

                        @Override
                        public void onError(Throwable e) {
                            Logger.e(getClass(), "onError=" + e.getMessage());
                            if (BuildConfig.IS_DEBUG) {
                                e.printStackTrace();
                            }
                            try {
                                RetrofitErrorHandler.handleException(e);
                            } catch (APIException e1) {
                                view.restartActivity();
                                ToastUtils.showOnHandler(view.getCtx(), StringUtils.replaceNull(e1.getErrorDescr()));
                            } catch (UnknownException e1) {
                                ToastUtils.showOnHandler(view.getCtx(), view.getCtx().getString(R.string.server_error));
                            } catch (ConnectionTimeOutException e1) {
                                ToastUtils.showOnHandler(view.getCtx(), view.getCtx().getString(R.string.connection_error));
                            } catch (NeedReAuthorizeException e1) {

                            }
                        }

                        @Override
                        public void onNext(String hash) {
                            if (isStringOk(hash)) {
                                view.openPaymentActivity(hash);
                            }
                        }
                    });
            compositeSubscription.add(subscription);
        } else {
            view.unavailablePurchaseBrix();
        }
    }

    private void localBrixRate() {
        Subscription subscription = BrixRepositoryProvider
                .provideRepository(api)
                .brixRate()
                .doOnSubscribe(new Action0() {
                    @Override
                    public void call() {
                        view.showLoadingIndicator(true);
                    }
                })
                .doOnTerminate(new Action0() {
                    @Override
                    public void call() {
                        view.showLoadingIndicator(false);
                    }
                })
                .subscribe(new Subscriber<BrixRate>() {
                    @Override
                    public void onCompleted() {
                        //do nothing
                    }

                    @Override
                    public void onError(Throwable e) {
                        Logger.e(getClass(), "onError=" + e.getMessage());
                        if (BuildConfig.IS_DEBUG) {
                            e.printStackTrace();
                        }
                        try {
                            RetrofitErrorHandler.handleException(e);
                        } catch (APIException e1) {
                            ToastUtils.showOnHandler(view.getCtx(), StringUtils.replaceNull(e1.getErrorDescr()));
                        } catch (UnknownException e1) {
                            ToastUtils.showOnHandler(view.getCtx(), view.getCtx().getString(R.string.server_error));
                        } catch (ConnectionTimeOutException e1) {
                            ToastUtils.showOnHandler(view.getCtx(), view.getCtx().getString(R.string.connection_error));
                        } catch (NeedReAuthorizeException e1) {

                        }
                    }

                    @Override
                    public void onNext(BrixRate response) {
                        if (response != null && response.getBuyRate() != null) {
                            view.setBrixRate(response.getBuyRate());
                        }
                    }
                });
        compositeSubscription.add(subscription);
    }

    ////////////////////////////////////////////////
    /// ICO Condition
    ////////////////////////////////////////////////

    private void icoCondition() {
        Subscription subscription = CompanyDetailsRepositoryProvider
                .provideRepository(api)
                .getIcoCondition()
                .subscribe(new Subscriber<IcoCondition>() {
                    @Override
                    public void onCompleted() {
                    }

                    @Override
                    public void onError(Throwable e) {
                        try {
                            RetrofitErrorHandler.handleException(e);
                        } catch (APIException e1) {
                            ToastUtils.showOnHandler(view.getCtx(), StringUtils.replaceNull(e1.getErrorDescr()));
                        } catch (UnknownException e1) {
                            ToastUtils.showOnHandler(view.getCtx(), view.getCtx().getString(R.string.server_error));
                        } catch (ConnectionTimeOutException e1) {
                            ToastUtils.showOnHandler(view.getCtx(), view.getCtx().getString(R.string.connection_error));
                        } catch (NeedReAuthorizeException e1) {

                        }
                    }

                    @Override
                    public void onNext(IcoCondition icoCondition) {
                        view.showIcoContainer(icoCondition.isIco());
                    }
                });

        compositeSubscription.add(subscription);
    }

    void unsubscribe() {
        if (compositeSubscription != null
                && !compositeSubscription.isUnsubscribed()) {
            compositeSubscription.unsubscribe();
        }
    }

    void onPublicOfferClick() {
        String savedLang = preferences.getLanguage();
        if (savedLang.length() > 0) {
            if (savedLang.equals(KEY_LANG_RU)) {
                view.openPublicOffer(BRIX_PUBLIC_OFFER_RU);
            } else if (savedLang.equals(KEY_LANG_EN)) {
                view.openPublicOffer(BRIX_PUBLIC_OFFER_EN);
            }
        } else {
            view.openPublicOffer(BRIX_PUBLIC_OFFER_RU);
        }
    }
}