package com.solaitech.likecoin.ui.help;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.StringRes;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import com.solaitech.likecoin.R;
import com.solaitech.likecoin.ui.base.BaseActivity;
import com.solaitech.likecoin.utils.ViewUtils;

public class FAQActivity extends BaseActivity {
    public static Intent getIntent(Context context) {
        return new Intent(context, FAQActivity.class);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        new RowViewHolder(findViewById(R.id.inc_faq1)).bindHolder(R.string.faq_title1, R.string.faq_desc1);
        new RowViewHolder(findViewById(R.id.inc_faq2)).bindHolder(R.string.faq_title2, R.string.faq_desc2);
        new RowViewHolder(findViewById(R.id.inc_faq3)).bindHolder(R.string.faq_title3, R.string.faq_desc3);
        new RowViewHolder(findViewById(R.id.inc_faq4)).bindHolder(R.string.faq_title4, R.string.faq_desc4);
        new RowViewHolder(findViewById(R.id.inc_faq5)).bindHolder(R.string.faq_title5, R.string.faq_desc5);
        new RowViewHolder(findViewById(R.id.inc_faq6)).bindHolder(R.string.faq_title6, R.string.faq_desc6);
        new RowViewHolder(findViewById(R.id.inc_faq7)).bindHolder(R.string.faq_title7, R.string.faq_desc7);
        new RowViewHolder(findViewById(R.id.inc_faq8)).bindHolder(R.string.faq_title8, R.string.faq_desc8);
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.activity_faq;
    }

    class RowViewHolder {
        ViewGroup rowView;
        @Bind(R.id.tv_title) TextView tv_title;
        @Bind(R.id.tv_description) TextView tv_description;
        @Bind(R.id.img_arrow_down) ImageView img_arrow_down;
        @Bind(R.id.ll_expandable) LinearLayout ll_expandable;

        RowViewHolder(View view) {
            ButterKnife.bind(this, view);
            rowView = (ViewGroup) view;
        }

        void bindHolder(@StringRes int titleRes, @StringRes int description) {
            if (titleRes == 0 || description == 0) {
                rowView.setVisibility(View.GONE);
                tv_description.setText(null);
                tv_title.setText(null);
            } else {
                rowView.setVisibility(View.VISIBLE);
                tv_title.setText(titleRes);
                tv_description.setText(description);
            }
        }

        @OnClick(R.id.ll_main_prize)
        void onClickExpandCollapseLayout() {
            if (ll_expandable.getTag().toString().equals("collapse")) {
                ViewUtils.expand(ll_expandable);
                ll_expandable.setTag("expand");
                img_arrow_down.setImageResource(R.drawable.ic_arrow_up_blue);
            } else if (ll_expandable.getTag().toString().equals("expand")) {
                ViewUtils.collapse(ll_expandable);
                ll_expandable.setTag("collapse");
                img_arrow_down.setImageResource(R.drawable.ic_arrow_down_blue);
            }
        }
    }
}