package com.solaitech.likecoin.ui.header;

import android.content.Context;
import android.content.res.TypedArray;
import android.support.annotation.Nullable;
import android.support.v7.widget.Toolbar;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.solaitech.likecoin.App;
import com.solaitech.likecoin.R;
import com.solaitech.likecoin.data.models.user.User;
import com.solaitech.likecoin.data.preferences.Preferences;
import com.solaitech.likecoin.data.preferences.PreferencesImpl;
import com.solaitech.likecoin.network.API;
import com.solaitech.likecoin.network.RetrofitServiceGenerator;
import com.solaitech.likecoin.ui.profile.edit_profile_image.UserInfoUpdate;
import com.solaitech.likecoin.utils.ImageUtils;
import com.solaitech.likecoin.utils.StringUtils;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;

public class ProfileHeaderViewGroup extends LinearLayout implements ProfileHeaderView {
    @Bind(R.id.tv_error) TextView tv_error;
    @Bind(R.id.progress_bar) ProgressBar progress_bar;
    @Bind(R.id.vg_error) ViewGroup vg_error;

    // Toolbar implementation
    @Bind(R.id.v_margin) View v_margin;
    @Bind(R.id.toolbar) Toolbar toolbar;
    @Bind(R.id.tv_user_level) TextView tv_user_level;
    @Bind(R.id.tv_coins_amount) TextView tv_coins_amount;
    @Bind(R.id.tv_brix_amount) TextView tv_brix_amount;
    @Bind(R.id.iv_avatar) CircleImageView iv_avatar;

    private ProfileHeaderPresenter presenter;
    private UserInfoUpdate userInfo;
    private boolean isMainBranch;

    public ProfileHeaderViewGroup(Context context) {
        super(context);
        inflate();
    }

    public ProfileHeaderViewGroup(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        parseAttrs(attrs);
    }

    public ProfileHeaderViewGroup(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        parseAttrs(attrs);
    }

    private void parseAttrs(AttributeSet attrs) {
        TypedArray typedArray = getContext().obtainStyledAttributes(attrs, R.styleable.ProfileView);
        isMainBranch = typedArray.getBoolean(R.styleable.ProfileView_pv_is_main_branch, false);
        typedArray.recycle();
        inflate();
    }

    public void setOnUpdateUserInfo(UserInfoUpdate userInfo) {
        this.userInfo = userInfo;
        userInfo.initToolbar(toolbar);
    }

    private void inflate() {
        LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflater.inflate(R.layout.v_profile_header, this);
        ButterKnife.bind(this);
        vg_error.setVisibility(GONE);

        if (!isMainBranch) {
            v_margin.setVisibility(GONE);
        } else {
            v_margin.setVisibility(VISIBLE);
        }

        Preferences preferences = new PreferencesImpl(getContext());
        RetrofitServiceGenerator retrofitServiceGenerator = RetrofitServiceGenerator.getInstance(preferences);
        API api = retrofitServiceGenerator.createService(API.class);

        presenter = new ProfileHeaderPresenter(this, api);
//        presenter.init();
    }

    @OnClick(R.id.tv_error_repeat_request)
    void onRepeatRequest() {
        presenter.onRepeatRequestClick();
    }

    @OnClick(R.id.iv_avatar)
    void onProfileViewClick() {
        if (userInfo != null) {
            userInfo.openProfileActivity();
        }
    }

    @OnClick(R.id.ll_coins_container)
    void onLikesContainerClick() {
        if (userInfo != null) {
            userInfo.openUserStatementActivity();
        }
    }

    @OnClick(R.id.ll_brix_container)
    void onBrixContainerClick() {
        if (userInfo != null) {
            userInfo.openBrixActivity();
        }
    }

    @OnClick(R.id.rl_progress)
    void onLevelProgressClick() {
        if (userInfo != null) {
            userInfo.openLevelActivity();
        }
    }

    @Override
    public User getUser() {
        App app = (App) getContext().getApplicationContext();
        return app.getUser();
    }

    @Override
    public void showProgressBar() {
        progress_bar.setVisibility(VISIBLE);
    }

    @Override
    public void hideProgressBar() {
        progress_bar.setVisibility(GONE);
    }

    @Override
    public void saveUser(User user) {
        App app = (App) getContext().getApplicationContext();
        app.setUser(user);
    }

    @Override
    public void showUserLevel(int userLevel) {
        tv_user_level.setText(String.valueOf(userLevel));
    }

    @Override
    public void showProfileImage(String avatarUrl) {
        ImageUtils.showAvatar(getContext(), iv_avatar, avatarUrl);
    }

    @Override
    public void showCoinsAmount(String amount) {
        tv_coins_amount.setText(amount);
    }

    @Override
    public void showBrixAmount(String amount) {
        tv_brix_amount.setText(amount);
    }

    @Override
    public void onDestroyView() {
        presenter.onDestroyView();
    }

    @Override
    public void onResume() {
        presenter.onResume();
    }

    @Override
    public void showError(String error) {
        vg_error.setVisibility(VISIBLE);
        tv_error.setText(error);
    }

    @Override
    public void hideError() {
        vg_error.setVisibility(GONE);
    }

    @Override
    public String getUnknownExceptionMessage() {
        return getContext().getString(R.string.server_error);
    }

    @Override
    public String getConnectionTimeOutExceptionMessage() {
        return getContext().getString(R.string.connection_error);
    }

    @Override
    public void updateUserInfo(User user) {
        if (userInfo != null) {
            userInfo.onUpdateUserInfo(user);
        }
    }

    public void onProfileImageUpdated() {
        presenter.onProfileImageUpdated();
    }

    public void onProfileUpdated() {
        presenter.onProfileUpdated();
    }
}