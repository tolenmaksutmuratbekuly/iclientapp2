package com.solaitech.likecoin.ui.campaign.detail.company;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.TextView;

import com.solaitech.likecoin.R;
import com.solaitech.likecoin.data.models.campaign.Merchant;
import com.solaitech.likecoin.interfaces.OnFragmentViewCreatedListener;
import com.solaitech.likecoin.ui.base.BaseFragment;
import com.solaitech.likecoin.ui.campaign.detail.conditions.contacts.CampaignInfoListView;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

public class CampaignCompanyFragment extends BaseFragment implements CampaignCompanyView {
    @Bind(R.id.tv_title) TextView tv_title;
    @Bind(R.id.tv_short_description) TextView tv_short_description;
    @Bind(R.id.wv_full_description) WebView wv_full_description;
    //    @Bind(R.id.tv_full_description) TextView tv_full_description;
    @Bind(R.id.v_links) CampaignInfoListView v_links;
    @Bind(R.id.v_contacts) CampaignInfoListView v_contacts;
    @Bind(R.id.v_emails) CampaignInfoListView v_emails;

    private OnFragmentViewCreatedListener onFragmentViewCreatedListener;

    private CampaignCompanyPresenter presenter;

    private Merchant merchant;

    public static CampaignCompanyFragment newInstance() {
        return new CampaignCompanyFragment();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        onFragmentViewCreatedListener = (OnFragmentViewCreatedListener) context;
    }

    @Override
    public void onDetach() {
        onFragmentViewCreatedListener = null;
        super.onDetach();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.frgm_campaign_company, container, false);
        ButterKnife.bind(this, v);
        presenter = new CampaignCompanyPresenter(this);
        return v;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        onFragmentViewCreatedListener.onFragmentViewCreated();
    }

    public void setMerchant(Merchant merchant) {
        this.merchant = merchant;
        presenter.onMerchantSet();
    }

    ///////////////////////////////////////////////////
    /// CampaignCompanyView
    /////////////////////////////////////////////////

    @Override
    public Merchant getMerchant() {
        return merchant;
    }

    @Override
    public void showTitle(String title) {
        tv_title.setVisibility(View.VISIBLE);
        tv_title.setText(title);
    }

    @Override
    public void showShortDescription(String description) {
        tv_short_description.setVisibility(View.VISIBLE);
        tv_short_description.setText(description);
    }

    @Override
    public void showFullDescription(String description) {
        wv_full_description.setVisibility(View.VISIBLE);
        wv_full_description.loadDataWithBaseURL("", description, "text/html; charset=utf-8", "UTF-8", "");
    }

    @Override
    public void showLinks(List<String> links) {
        v_links.setVisibility(View.VISIBLE);
        v_links.showList(links, CampaignInfoListView.VIEW_LINKS);
    }

    @Override
    public void hideTitle() {
        tv_title.setVisibility(View.GONE);
    }

    @Override
    public void hideShortDescription() {
        tv_short_description.setVisibility(View.GONE);
    }

    @Override
    public void hideFullDescription() {
        wv_full_description.setVisibility(View.GONE);
    }

    @Override
    public void hideLinks() {
        v_links.setVisibility(View.GONE);
    }

    @Override
    public void showContacts(List<String> contacts) {
        v_contacts.setVisibility(View.VISIBLE);
        v_contacts.showList(contacts, CampaignInfoListView.VIEW_CONTACTS);
    }

    @Override
    public void hideContacts() {
        v_contacts.setVisibility(View.GONE);
    }

    @Override
    public void showEmails(List<String> emails) {
        v_emails.setVisibility(View.VISIBLE);
        v_emails.showList(emails, CampaignInfoListView.VIEW_EMAILS);
    }

    @Override
    public void hideEmails() {
        v_emails.setVisibility(View.GONE);
    }
}