package com.solaitech.likecoin.ui.article.content;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.TextView;

import com.solaitech.likecoin.R;
import com.solaitech.likecoin.data.models.article.Article;
import com.solaitech.likecoin.interfaces.OnFragmentViewCreatedListener;
import com.solaitech.likecoin.ui.base.BaseFragment;

import butterknife.Bind;
import butterknife.ButterKnife;

public class ArticleContentFragment extends BaseFragment implements ArticleContentView {
    private Article articleDetail;

    @Bind(R.id.tv_title) TextView tv_title;
    @Bind(R.id.wv_short_description) WebView wv_short_description;
    @Bind(R.id.wv_full_description) WebView wv_full_description;
    @Bind(R.id.tv_valid_until) TextView tv_valid_until;

    private OnFragmentViewCreatedListener onFragmentViewCreatedListener;
    private ArticleContentPresenter presenter;

    public static ArticleContentFragment newInstance() {
        return new ArticleContentFragment();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        onFragmentViewCreatedListener = (OnFragmentViewCreatedListener) context;
    }

    @Override
    public void onDetach() {
        onFragmentViewCreatedListener = null;
        super.onDetach();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.frgm_article_content, container, false);

        ButterKnife.bind(this, v);
        initView();
        presenter = new ArticleContentPresenter(this);
        presenter.init();
        return v;
    }

    private void initView() {
        wv_short_description.setScrollBarStyle(View.SCROLLBARS_INSIDE_OVERLAY);
        wv_full_description.setScrollBarStyle(View.SCROLLBARS_INSIDE_OVERLAY);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        onFragmentViewCreatedListener.onFragmentViewCreated();
    }

    public void setArticleDetail(Article articleDetail) {
        this.articleDetail = articleDetail;
        presenter.onArticleDetailSet();
    }

    @Override
    public Article getArticleDetail() {
        return articleDetail;
    }

    @Override
    public void showTitle(String title) {
        tv_title.setVisibility(View.VISIBLE);
        tv_title.setText(title);
    }

    @Override
    public void showShortDescription(String description) {
        wv_short_description.setVisibility(View.VISIBLE);
        wv_short_description.loadDataWithBaseURL("", description, "text/html; charset=utf-8", "UTF-8", "");
    }

    @Override
    public void showContent(String content) {
        wv_full_description.setVisibility(View.VISIBLE);
        wv_full_description.loadDataWithBaseURL("", content, "text/html; charset=utf-8", "UTF-8", "");
    }

    @Override
    public void showDate(String regDate) {
        tv_valid_until.setVisibility(View.VISIBLE);
        tv_valid_until.setText(regDate);
    }

    @Override
    public void hideTitle() {
        tv_title.setVisibility(View.GONE);
    }

    @Override
    public void hideShortDescription() {
        wv_short_description.setVisibility(View.GONE);
    }

    @Override
    public void hideContent() {
        wv_full_description.setVisibility(View.GONE);
    }

    @Override
    public void hideDate() {
        tv_valid_until.setVisibility(View.GONE);
    }

    @Override
    public Context getContextFromPage() {
        return getContext();
    }

}