package com.solaitech.likecoin.ui.contacts.send_like;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.GradientDrawable;
import android.support.v4.app.DialogFragment;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.solaitech.likecoin.R;
import com.solaitech.likecoin.data.models.enums.BroadcastEventEnums;
import com.solaitech.likecoin.data.models.news_feed.NewsOwner;
import com.solaitech.likecoin.data.models.user.User;
import com.solaitech.likecoin.data.preferences.PreferencesImpl;
import com.solaitech.likecoin.exceptions.APIException;
import com.solaitech.likecoin.exceptions.ConnectionTimeOutException;
import com.solaitech.likecoin.exceptions.NeedReAuthorizeException;
import com.solaitech.likecoin.exceptions.UnknownException;
import com.solaitech.likecoin.network.API;
import com.solaitech.likecoin.network.RetrofitServiceGenerator;
import com.solaitech.likecoin.ui.campaign.detail.purchase_coupon.count_view.OnPurchaseCountChangedListener;
import com.solaitech.likecoin.ui.campaign.detail.purchase_coupon.count_view.PurchaseCountView;
import com.solaitech.likecoin.utils.CustomColorUtils;
import com.solaitech.likecoin.utils.ImageUtils;
import com.solaitech.likecoin.utils.animation.AnimationCallback;
import com.solaitech.likecoin.utils.animation.CustomAnimationUtils;
import com.solaitech.likecoin.utils.animation.SendLikeAnimation;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class SendLikeDialog extends DialogFragment
        implements SendLikeView, OnPurchaseCountChangedListener {

    private User dstUser;
    private User user;
    private NewsOwner owner;
    private boolean isUserProfile;
    @Bind(R.id.tv_user_name) TextView tv_user_name;
    @Bind(R.id.iv_avatar) ImageView iv_avatar;
    @Bind(R.id.tv_level) TextView tv_level;
    @Bind(R.id.v_like_count) PurchaseCountView v_like_count;
    @Bind(R.id.et_comment) EditText et_comment;

    private SendLikePresenter presenter;
    private ProgressDialog progressDialog;
    private ImageView img_anim_coin;

    public static SendLikeDialog newInstance(User user, User dstUser, boolean isUserProfile, ImageView img_anim_coin) {
        SendLikeDialog d = new SendLikeDialog();
        d.user = user;
        d.dstUser = dstUser;
        d.isUserProfile = isUserProfile;
        d.img_anim_coin = img_anim_coin;
        return d;
    }

    public static SendLikeDialog newInstance(User user, NewsOwner owner, ImageView img_anim_coin) {
        SendLikeDialog d = new SendLikeDialog();
        d.user = user;
        d.owner = owner;
        d.img_anim_coin = img_anim_coin;
        return d;
    }

    @Override
    public void setupDialog(Dialog dialog, int style) {
        View v = View.inflate(getContext(), R.layout.dialog_send_like, null);
        ButterKnife.bind(this, v);
        dialog.setContentView(v);
        if (getDialog() != null) {
            if (getDialog().getWindow() != null) {
                getDialog().getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            }
        }

        initView(v);
        RetrofitServiceGenerator serviceGenerator = RetrofitServiceGenerator.getInstance(new PreferencesImpl(getContext()));
        API api = serviceGenerator.createService(API.class);
        presenter = new SendLikePresenter(this, api);
        presenter.init();
    }

    private void initView(View v) {
        ViewGroup vgContainer = v.findViewById(R.id.vg_container);
        CustomAnimationUtils.enableTransition(vgContainer);
        initDistUserData();
        if (user != null && user.getUserLevel() != null && user.getUserLevel().getLevel() != null) {
            v_like_count.setEachDayLikeAmount(user.getUserLevel().getLevel());
        }
        v_like_count.setOnPurchaseCountChangedListener(this);
    }

    private void initDistUserData() {
        if (dstUser != null) {
            if (dstUser.getName() != null) {
                tv_user_name.setText(dstUser.getName());
            }

            ImageUtils.showAvatar(getContext(), iv_avatar, dstUser.getAvatarUrl());
            if (dstUser.getUserLevel() != null && dstUser.getUserLevel().getLevel() != null) {
                showUserLevel(dstUser.getUserLevel().getLevel());
            }
        } else if (owner != null) {
            tv_user_name.setText(owner.getName());
            ImageUtils.showAvatar(getContext(), iv_avatar, owner.getAvatarUrl());
            if (owner.getCustomData() != null && owner.getCustomData().getUserLevel() != null) {
                showUserLevel(owner.getCustomData().getUserLevel());
            }
        }
    }

    private void showUserLevel(Integer userLevel) {
        if (userLevel != null) {
            tv_level.setVisibility(View.VISIBLE);
            tv_level.setText(String.valueOf(userLevel));
            GradientDrawable drawable = (GradientDrawable) tv_level.getBackground();
            CustomColorUtils.setLevelColor(getContext(), drawable, userLevel, null);
        } else {
            tv_level.setVisibility(View.GONE);
        }
    }

    @OnClick(R.id.btn_send_like)
    void onSendLikeClick() {
        presenter.onSendButtonClick(v_like_count.getEnteredCount(), et_comment.getText().toString());
    }

    @Override
    public User getDstUser() {
        return dstUser;
    }

    @Override
    public NewsOwner getOwner() {
        return owner;
    }

    @Override
    public boolean isUserProfile() {
        return isUserProfile;
    }

    @Override
    public void showProgressDialog() {
        if (progressDialog == null
                || !progressDialog.isShowing()) {
            progressDialog = ProgressDialog.show(getContext(), "", getString(R.string.loading), true, false);
        }
    }

    @Override
    public void dismissProgressDialog() {
        if (progressDialog != null && progressDialog.isShowing()) {
            progressDialog.dismiss();
        }
    }

    @Override
    public void showSuccessToastMessage() {
        Toast.makeText(getContext(), getString(R.string.like_sending_success), Toast.LENGTH_SHORT).show();
        if (img_anim_coin != null) {
            new SendLikeAnimation(new AnimationCallback() {
                @Override
                public void onAnimationFinish() {
                    sendBroadcast();
                }
            }).likeAnimation(img_anim_coin);
        }
    }

    public void sendBroadcast() {
        Intent broadcastIntent = new Intent(BroadcastEventEnums.PROFILE_UPDATED);
        LocalBroadcastManager.getInstance(getContext()).sendBroadcast(broadcastIntent);
    }

    @Override
    public void closeDialog() {
        if (!isUserProfile)
            getTargetFragment().onActivityResult(getTargetRequestCode(), Activity.RESULT_OK, getActivity().getIntent());
        dismiss();
    }

    @Override
    public void onDismiss(DialogInterface dialog) {
        presenter.onDismiss();
        super.onDismiss(dialog);
    }

    @Override
    public void handleAPIException(APIException e) {
        //do nothing
    }

    @Override
    public void handleConnectionTimeOutException(ConnectionTimeOutException e) {
        //do nothing
    }

    @Override
    public void handleUnknownException(UnknownException e) {
        //do nothing
    }

    @Override
    public void showErrorDialog(String message) {
        new AlertDialog.Builder(getContext())
                .setMessage(message)
                .setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                })
                .create()
                .show();
    }

    @Override
    public String getUnknownExceptionMessage() {
        return getString(R.string.server_error);
    }

    @Override
    public String getConnectionTimeOutExceptionMessage() {
        return getString(R.string.connection_error);
    }

    @Override
    public void handleNeedReAuthorizeException(NeedReAuthorizeException e) {

    }

    @Override
    public void onPurchaseCountChanged(int count) {

    }
}