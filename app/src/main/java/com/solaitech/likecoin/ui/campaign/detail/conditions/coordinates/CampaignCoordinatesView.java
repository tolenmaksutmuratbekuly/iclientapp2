package com.solaitech.likecoin.ui.campaign.detail.conditions.coordinates;

import android.content.Context;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import java.util.List;

import com.solaitech.likecoin.R;
import com.solaitech.likecoin.data.models.campaign.Coordinate;

public class CampaignCoordinatesView extends LinearLayout {

    private ViewGroup vgContainer;
    private OnCampaignCoordinateViewClickListener onCampaignCoordinateViewClickListener;

    public CampaignCoordinatesView(Context context) {
        super(context);
        inflate();
    }

    public CampaignCoordinatesView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        inflate();
    }

    public CampaignCoordinatesView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        inflate();
    }

    public void setOnCampaignCoordinateViewClickListener(OnCampaignCoordinateViewClickListener onCampaignCoordinateViewClickListener) {
        this.onCampaignCoordinateViewClickListener = onCampaignCoordinateViewClickListener;
    }

    private void inflate() {
        LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflater.inflate(R.layout.v_campaign_coordinates, this);

        initView();
    }

    private void initView() {
        vgContainer = (ViewGroup) findViewById(R.id.vg_container);
    }

    public void showCoordinates(List<Coordinate> coordinates) {
        vgContainer.removeAllViews();

        for (int i = 0; i < coordinates.size(); i++) {
            Coordinate coordinate = coordinates.get(i);
            if (coordinate.getLatitude() != 0 && coordinate.getLongitude() != 0) {
                vgContainer.addView(getCoordinateView(coordinate, i));
            }
        }
    }

    private View getCoordinateView(Coordinate coordinate, int position) {
        CampaignCoordinateView view = new CampaignCoordinateView(getContext());
        view.setOnCampaignCoordinateViewClickListener(onCampaignCoordinateViewClickListener);
        view.showCoordinate(coordinate);

        if (position == 0) {
            view.hideLine();
        }
        return view;
    }

}