package com.solaitech.likecoin.ui.registration.complete;

import com.solaitech.likecoin.data.models.registration.RegistrationComplete;
import com.solaitech.likecoin.data.models.registration.RegistrationStart;
import com.solaitech.likecoin.data.params.RegistrationCompleteParams;
import com.solaitech.likecoin.data.params.RegistrationStartParams;
import com.solaitech.likecoin.data.repository.registration.RegistrationRepositoryProvider;
import com.solaitech.likecoin.network.API;
import com.solaitech.likecoin.ui.base.PresenterErrorHandler;
import com.solaitech.likecoin.utils.DateTimeUtils;
import com.solaitech.likecoin.utils.Logger;
import com.solaitech.likecoin.utils.StringUtils;

import java.util.concurrent.TimeUnit;

import rx.Observable;
import rx.Subscriber;
import rx.Subscription;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action0;
import rx.functions.Func1;
import rx.schedulers.Schedulers;
import rx.subscriptions.CompositeSubscription;

class RegistrationCompletePresenter {

    private static final long TIME_INTERVAL_IN_MILLIS = 1000L;
    private static final long TIME_TOTAL_IN_MILLIS = 60 * 1000L;
    private static final int MAX_CONFIRMATION_CODE_LENGTH = 5;

    private RegistrationCompleteView view;
    private API api;

    private CompositeSubscription compositeSubscription = new CompositeSubscription();

    RegistrationCompletePresenter(RegistrationCompleteView view, API api) {
        this.view = view;
        this.api = api;
    }

    public void init() {
        view.showSmsInfoText(view.getPhoneNumber());
        startTimer();
    }

    void onConfirmButtonClick(boolean isRegistered) {
        boolean isPhoneNumberOk = isPhoneNumberOk();
        boolean isNameOk = isFullNameOk();
        boolean isConfirmationCodeOk = isConfirmationCodeOk();

        if (isRegistered && isPhoneNumberOk && isConfirmationCodeOk) {
            registrationComplete();
        } else if (!isRegistered && isPhoneNumberOk && isConfirmationCodeOk && isNameOk) {
            registrationComplete();
        } else {
            if (!isConfirmationCodeOk) {
                view.showConfirmationCodeFieldError();
            }
        }
    }

    private boolean isPhoneNumberOk() {
        return !view.getPhoneNumber().isEmpty();
    }

    private boolean isConfirmationCodeOk() {
        return StringUtils.isStringOk(view.getConfirmationCode())
                && StringUtils.length(view.getConfirmationCode()) == MAX_CONFIRMATION_CODE_LENGTH;
    }

    private void registrationComplete() {
        Subscription subscription = RegistrationRepositoryProvider.provideRepository(api)
                .registrationComplete(
                        view.getConfirmationCode(),
                        new RegistrationCompleteParams(view.getPhoneNumber(), view.getEmail(), view.getName(), view.getSurname(), view.getLanguage())
                )
                .doOnSubscribe(new Action0() {
                    @Override
                    public void call() {
                        view.showProgressDialog();
                    }
                })
                .doOnTerminate(new Action0() {
                    @Override
                    public void call() {
                        view.dismissProgressDialog();
                    }
                })
                .subscribe(new Subscriber<RegistrationComplete>() {
                    @Override
                    public void onCompleted() {
                        //do nothing
                        Logger.e(getClass(), "onCompleted");
                    }

                    @Override
                    public void onError(Throwable e) {
                        PresenterErrorHandler.handleError2(e, view);
                    }

                    @Override
                    public void onNext(RegistrationComplete registrationComplete) {
                        Logger.e(getClass(), "onNext");
                        view.saveRegistrationCompleteData(registrationComplete);
                        view.setRegistrationCompleted();
                        view.closeDialog();
                        view.openMainActivity();
                    }
                });
        compositeSubscription.add(subscription);
    }

    void onResendSmsClick() {
        boolean isPhoneNumberOk = isPhoneNumberOk();

        if (isPhoneNumberOk) {
            registrationStart();
        }
    }

    private boolean isFullNameOk() {
        return !view.getName().isEmpty() && !view.getSurname().isEmpty();
    }

    private void registrationStart() {
        Subscription subscription = RegistrationRepositoryProvider.provideRepository(api)
                .registrationStart(getRegistrationStartParams())
                .doOnSubscribe(new Action0() {
                    @Override
                    public void call() {
                        view.showProgressDialog();
                    }
                })
                .doOnTerminate(new Action0() {
                    @Override
                    public void call() {
                        view.dismissProgressDialog();
                    }
                })
                .subscribe(new Subscriber<RegistrationStart>() {
                    @Override
                    public void onCompleted() {
                        //do nothing
                    }

                    @Override
                    public void onError(Throwable e) {
                        PresenterErrorHandler.handleError2(e, view);
                    }

                    @Override
                    public void onNext(RegistrationStart registrationStart) {
                        startTimer();
                    }
                });
        compositeSubscription.add(subscription);
    }

    private RegistrationStartParams getRegistrationStartParams() {
        return new RegistrationStartParams(
                view.getPhoneNumber()
        );
    }

    private void startTimer() {
        Subscription subscription = Observable.interval(TIME_INTERVAL_IN_MILLIS, TimeUnit.MILLISECONDS)
                .takeWhile(new Func1<Long, Boolean>() {
                    @Override
                    public Boolean call(Long aLong) {
                        return aLong <= TIME_TOTAL_IN_MILLIS / 1000;
                    }
                })
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe(new Action0() {
                    @Override
                    public void call() {
                        view.hideResendSmsButton();
                        view.showTimeLeftView();
                    }
                })
                .subscribe(new Subscriber<Long>() {
                    @Override
                    public void onCompleted() {
                        Logger.e(getClass(), "timer onCompleted");
                        view.showResendSmsButton();
                        view.hideTimeLeftView();
                    }

                    @Override
                    public void onError(Throwable e) {
                        e.printStackTrace();
                        Logger.e(getClass(), "timer onError");
                        view.showResendSmsButton();
                        view.hideTimeLeftView();
                    }

                    @Override
                    public void onNext(Long aLong) {
                        Logger.e(getClass(), "timer onNext");
                        int timeLeftInSeconds = (int) (TIME_TOTAL_IN_MILLIS / 1000 - aLong);
                        try {
                            view.updateTimeLeftToEnableResendSmsButton(
                                    DateTimeUtils.convertMillisToTimer(timeLeftInSeconds)
                            );
                        } catch (Exception e) {
                            view.updateTimeLeftToEnableResendSmsButton("-");
                        }
                    }
                });
        compositeSubscription.add(subscription);
    }

    public void onDismiss() {
        unsubscribe();
    }

    private void unsubscribe() {
        if (compositeSubscription != null
                && !compositeSubscription.isUnsubscribed()) {
            compositeSubscription.unsubscribe();
        }
    }
}