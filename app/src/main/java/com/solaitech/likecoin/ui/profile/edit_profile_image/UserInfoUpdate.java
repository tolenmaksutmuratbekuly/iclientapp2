package com.solaitech.likecoin.ui.profile.edit_profile_image;

import android.support.v7.widget.Toolbar;

import com.solaitech.likecoin.data.models.user.User;

public interface UserInfoUpdate {
    void onUpdateUserInfo(User user);

    void openProfileActivity();

    void openUserStatementActivity();

    void openLevelActivity();

    void initToolbar(Toolbar toolbar);

    void openBrixActivity();
}