package com.solaitech.likecoin.ui.campaign.detail.purchase_coupon.success;

import java.util.List;

import com.solaitech.likecoin.data.models.coupons.Coupon;

interface PurchasedCouponsInfoView {

    String getCouponsQuantity();

    String getTotalSum();

    List<Coupon> getCoupons();

    void showCouponsQuantity(String couponsQuantity);

    void showTotalSum(String totalSum);

    void closeDialog();

}