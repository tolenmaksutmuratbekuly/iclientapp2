package com.solaitech.likecoin.ui.profile.user_statement;

import android.content.Context;
import android.graphics.drawable.GradientDrawable;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.solaitech.likecoin.R;
import com.solaitech.likecoin.data.models.user_statement.UserStatement;
import com.solaitech.likecoin.ui.base.recycler_view.RecyclerViewBaseAdapter;
import com.solaitech.likecoin.utils.CustomColorUtils;
import com.solaitech.likecoin.utils.DateTimeUtils;
import com.solaitech.likecoin.utils.ImageUtils;
import com.solaitech.likecoin.utils.StringUtils;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;

import static com.solaitech.likecoin.data.models.enums.PartnerTypes.MERCHANT;
import static com.solaitech.likecoin.data.models.enums.PartnerTypes.USER;

class UserStatementAdapter extends RecyclerViewBaseAdapter {
    private static final int VIEW_TYPE_STATEMENT = 1;
    static final int VIEW_TYPE_EMPTY_SPACE = 2;
    private List<Object> objects;
    private Callback callback;

    UserStatementAdapter(Context context, List<Object> objects) {
        super(context);
        this.objects = objects;
    }

    void setCallback(Callback callback) {
        this.callback = callback;
    }

    @Override
    public MainViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        switch (viewType) {
            case VIEW_TYPE_STATEMENT:
                return new StatementViewHolder(inflate(parent, R.layout.adapter_user_statement));
            case VIEW_TYPE_EMPTY_SPACE:
                return new EmptySpaceViewHolder(inflate(parent, R.layout.adapter_user_statement_empty_space));
            default:
                throw incorrectOnCreateViewHolder();
        }
    }

    @Override
    public void onBindViewHolder(MainViewHolder holder, int position) {
        switch (holder.getItemViewType()) {
            case VIEW_TYPE_STATEMENT:
                ((StatementViewHolder) holder).bind((UserStatement) objects.get(position));
                break;
        }
    }

    class StatementViewHolder extends MainViewHolder {
        @Bind(R.id.tv_date) TextView tv_date;
        @Bind(R.id.tv_time) TextView tv_time;
        @Bind(R.id.tv_operation_type) TextView tv_operation_type;
        @Bind(R.id.iv_profile_image) CircleImageView iv_profile_image;
        @Bind(R.id.tv_name) TextView tv_name;
        @Bind(R.id.tv_level) TextView tv_level;
        @Bind(R.id.img_operation_type) ImageView img_operation_type;
        @Bind(R.id.v_operation_type) View v_operation_type;
        UserStatement bindedUserStatement;

        StatementViewHolder(View v) {
            super(v);
            ButterKnife.bind(this, v);
        }

        void bind(UserStatement userStatement) {
            bindedUserStatement = userStatement;
            if (userStatement == null) {
                return;
            }
            if (userStatement.getRegDT() != null) {
                tv_date.setText(DateTimeUtils.convertUserStatementDateToString(userStatement.getRegDT()));
                tv_time.setText(DateTimeUtils.convertUserStatementTimeToString(userStatement.getRegDT()));
            } else {
                tv_date.setText(StringUtils.EMPTY_STRING);
                tv_time.setText(StringUtils.EMPTY_STRING);
            }

            if (userStatement.getAmount() != null) {
                int operationType = UserStatementUtils.getOperationType(userStatement.getAmount());
                tv_operation_type.setText(
                        UserStatementUtils.getOperationText(
                                operationType,
                                getContext(),
                                userStatement.getAmount(),
                                userStatement.getDescr())
                );
                v_operation_type.setBackgroundResource(UserStatementUtils.getOperationViewBgResId(operationType));
                img_operation_type.setImageResource(UserStatementUtils.getOperationImgResId(operationType));
            }

            if (userStatement.getPartner() != null) {
                tv_name.setText(UserStatementUtils.getFullName(userStatement.getPartner()));
                ImageUtils.showUserStatementProfileImage(getContext(), iv_profile_image, userStatement.getPartner());
                if (userStatement.getPartner().getUserLevel() != null) {
                    tv_level.setVisibility(View.VISIBLE);
                    int level = userStatement.getPartner().getUserLevel();
                    tv_level.setText(String.valueOf(level));
                    GradientDrawable drawable = (GradientDrawable) tv_level.getBackground();
                    CustomColorUtils.setLevelColor(getContext(), drawable, level, null);
                } else {
                    tv_level.setVisibility(View.GONE);
                }
            }
        }

        @OnClick(R.id.rl_ava_container)
        void onAvatarClick() {
            if (callback != null) {
                if (bindedUserStatement.getPartner() != null &&
                        bindedUserStatement.getPartner().getType() != null) {
                    if (bindedUserStatement.getPartner().getType().equals(MERCHANT)) {
                        callback.merchantLogoClick(bindedUserStatement.getPartner().getId(),
                                UserStatementUtils.getFullName(bindedUserStatement.getPartner()));
                    } else if (bindedUserStatement.getPartner().getType().equals(USER)) {
                        callback.userAvatarClick(bindedUserStatement.getPartner().getId());
                    }
                }
            }
        }
    }

    private class EmptySpaceViewHolder extends MainViewHolder {
        EmptySpaceViewHolder(View v) {
            super(v);
        }
    }

    @Override
    public int getItemViewType(int position) {
        if (objects.get(position) instanceof UserStatement) {
            return VIEW_TYPE_STATEMENT;
        } else if (objects.get(position) instanceof Integer) {
            return (Integer) objects.get(position);
        }

        throw incorrectGetItemViewType();
    }

    @Override
    public int getItemCount() {
        return objects.size();
    }

    interface Callback {
        void userAvatarClick(String userId);

        void merchantLogoClick(String merchantId, String title);
    }
}