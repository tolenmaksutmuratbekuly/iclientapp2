package com.solaitech.likecoin.ui.profile.edit_profile;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import com.solaitech.likecoin.R;
import com.solaitech.likecoin.data.models.user.User;
import com.solaitech.likecoin.exceptions.APIException;
import com.solaitech.likecoin.exceptions.ConnectionTimeOutException;
import com.solaitech.likecoin.exceptions.NeedReAuthorizeException;
import com.solaitech.likecoin.exceptions.UnknownException;
import com.solaitech.likecoin.ui.base.BaseBottomSheetDialogFragment;
import com.solaitech.likecoin.utils.EditTextWatcher;
import com.solaitech.likecoin.utils.ToastUtils;
import com.solaitech.likecoin.utils.animation.CustomAnimationUtils;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class EditProfileDialog extends BaseBottomSheetDialogFragment implements EditProfileView {

    private User user;

    @Bind(R.id.et_email) EditText et_email;
    @Bind(R.id.et_name) EditText et_name;
    @Bind(R.id.et_surname) EditText et_surname;

    @Bind(R.id.til_email) TextInputLayout til_email;
    @Bind(R.id.til_name) TextInputLayout til_name;
    @Bind(R.id.til_surname) TextInputLayout til_surname;

    private EditTextWatcher watcherEmail;
    private EditTextWatcher watcherName;
    private EditTextWatcher watcherSurname;

    private EditProfilePresenter presenter;
    private ProgressDialog progressDialog;

    public static EditProfileDialog newInstance(User user) {
        EditProfileDialog d = new EditProfileDialog();
        d.user = user;
        return d;
    }

    @Override
    public void setupDialog(Dialog dialog, int style) {
        View v = View.inflate(getContext(), R.layout.dialog_edit_profile, null);
        dialog.setContentView(v);
        setBottomSheetCallback(v);
        initView(v);

        presenter = new EditProfilePresenter(this, api);
        presenter.init();
    }

    private void initView(View v) {
        ViewGroup vgContainer = (ViewGroup) v.findViewById(R.id.vg_container);
        CustomAnimationUtils.enableTransition(vgContainer);
        ButterKnife.bind(this, v);


        watcherEmail = new EditTextWatcher(getContext(), til_email);
        watcherName = new EditTextWatcher(getContext(), til_name);
        watcherSurname = new EditTextWatcher(getContext(), til_surname);

        et_email.addTextChangedListener(watcherEmail);
        et_name.addTextChangedListener(watcherName);
        et_surname.addTextChangedListener(watcherSurname);
    }

    @OnClick(R.id.btn_update)
    void onClickUpdate() {
        presenter.onUpdateProfileButtonClick();
    }

    @Override
    public User getUser() {
        return user;
    }

    @Override
    public void showEmail(String email) {
        et_email.getText().clear();
        et_email.append(email);
    }

    @Override
    public void showName(String name) {
        et_name.getText().clear();
        et_name.append(name);
    }

    @Override
    public void showSurname(String surname) {
        et_surname.getText().clear();
        et_surname.append(surname);
    }

    @Override
    public String getEnteredEmail() {
        return et_email.getText().toString();
    }

    @Override
    public String getEnteredName() {
        return et_name.getText().toString();
    }

    @Override
    public String getEnteredSurname() {
        return et_surname.getText().toString();
    }

    @Override
    public void showEmailEmptyError() {
        watcherEmail.showFieldError(getString(R.string.field_error));
    }

    @Override
    public void showEmailInvalidError() {
        watcherEmail.showFieldError(getString(R.string.email_invalid_error));
    }

    @Override
    public void showNameEmptyError() {
        watcherName.showFieldError(getString(R.string.field_error));
    }

    @Override
    public void showSurnameEmptyError() {
        watcherSurname.showFieldError(getString(R.string.field_error));
    }

    @Override
    public void showProgressDialog() {
        if (progressDialog == null
                || !progressDialog.isShowing()) {
            progressDialog = ProgressDialog.show(getContext(), "", getString(R.string.loading), true, false);
        }
    }

    @Override
    public void dismissProgressDialog() {
        if (progressDialog != null && progressDialog.isShowing()) {
            progressDialog.dismiss();
        }
    }

    @Override
    public void showUserUpdateSuccessToastMessage() {
        ToastUtils.show(getContext(), getString(R.string.update_profile_success_message));
    }

    @Override
    public void closeDialog() {
        dismiss();
    }

    @Override
    public void startService(String name, String surname, String email) {
        Bundle bundle = new Bundle();
        bundle.putString(UpdateProfileIntentService.NAME, name);
        bundle.putString(UpdateProfileIntentService.SURNAME, surname);
        bundle.putString(UpdateProfileIntentService.EMAIL, email);

        Intent intent = new Intent(getContext(), UpdateProfileIntentService.class);
        intent.putExtras(bundle);
        getContext().startService(intent);
    }

    @Override
    public void onDismiss(DialogInterface dialog) {
        presenter.onDismiss();
        super.onDismiss(dialog);
    }

    @Override
    public void handleAPIException(APIException e) {
        //do nothing
    }

    @Override
    public void handleConnectionTimeOutException(ConnectionTimeOutException e) {
        //do nothing
    }

    @Override
    public void handleUnknownException(UnknownException e) {
        //do nothing
    }

    @Override
    public void handleNeedReAuthorizeException(NeedReAuthorizeException e) {
        //do nothing
    }

    @Override
    public void showErrorDialog(String message) {
        new AlertDialog.Builder(getContext())
                .setMessage(message)
                .setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                })
                .create()
                .show();
    }

    @Override
    public String getUnknownExceptionMessage() {
        return getString(R.string.server_error);
    }

    @Override
    public String getConnectionTimeOutExceptionMessage() {
        return getString(R.string.connection_error);
    }

}