package com.solaitech.likecoin.ui.base.interfaces;

import com.solaitech.likecoin.exceptions.APIException;
import com.solaitech.likecoin.exceptions.ConnectionTimeOutException;
import com.solaitech.likecoin.exceptions.NeedReAuthorizeException;
import com.solaitech.likecoin.exceptions.UnknownException;

public interface MVPBaseErrorView {

    void handleAPIException(APIException e);

    void handleConnectionTimeOutException(ConnectionTimeOutException e);

    void handleUnknownException(UnknownException e);

    void handleNeedReAuthorizeException(NeedReAuthorizeException e);

    void showErrorDialog(String message);

    String getUnknownExceptionMessage();

    String getConnectionTimeOutExceptionMessage();
}