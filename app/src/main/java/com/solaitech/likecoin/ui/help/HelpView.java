package com.solaitech.likecoin.ui.help;

import com.solaitech.likecoin.ui.base.interfaces.MVPBaseView;

interface HelpView extends MVPBaseView {
    boolean isPermissionGrantedContacts();

    void requestContactsPermissions();
}