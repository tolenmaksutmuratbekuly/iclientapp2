package com.solaitech.likecoin.ui.campaign.detail;

import com.solaitech.likecoin.data.models.campaign.CampaignDetail;
import com.solaitech.likecoin.data.models.campaign.Merchant;
import com.solaitech.likecoin.ui.base.interfaces.MVPBaseView;

interface CampaignDetailView extends MVPBaseView {

    String getCampaignId();

    boolean isAppBarLayoutExpanded();

    void hideAppBarLayout();

    void showAppBarLayout();

    void showImageLarge(String imgUrl);

    void setCampaignDetail(CampaignDetail campaignDetail);

    CampaignDetail getCampaignDetail();

    void setMerchant(Merchant merchant);

    void showBuyCouponView();

    void showCouponPrice(String price);

    void hideBuyCouponView();

    void openPurchaseDialog(String campaignId, int couponPrice, int maxCouponsCount);

    void setToolbarTitle(String title);

    void showCommentEdiText();
}