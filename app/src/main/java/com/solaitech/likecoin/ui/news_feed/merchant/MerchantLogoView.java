package com.solaitech.likecoin.ui.news_feed.merchant;

import android.content.Context;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.solaitech.likecoin.R;
import com.solaitech.likecoin.utils.ImageUtils;

public class MerchantLogoView extends LinearLayout {
    private ImageView ivLogo;
    public MerchantLogoView(Context context) {
        super(context);
        inflate();
    }

    public MerchantLogoView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        inflate();
    }

    public MerchantLogoView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        inflate();
    }

    private void inflate() {
        LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflater.inflate(R.layout.v_merchant_logo, this);
        initView();
    }

    private void initView() {
        ivLogo = (ImageView) findViewById(R.id.iv_logo);
    }

    public void showLogo(String logoUrl) {
        ImageUtils.showMerchantLogo(getContext(), ivLogo, logoUrl);
    }
}