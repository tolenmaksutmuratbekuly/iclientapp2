package com.solaitech.likecoin.ui.profile.brix;

import android.content.Context;

import com.solaitech.likecoin.data.models.user.User;

interface BrixView {

    User getUser();

    void showBrixAmount(String amount);

    Context getCtx();

    void showLoadingIndicator(boolean show);

    void setBrixRate(double buyRate);

    void setMoneyAmount(String amount);

    void unavailablePurchaseBrix();

    void restartActivity();

    void openPaymentActivity(String hash);

    void openPublicOffer(String link);

    void showIcoContainer(boolean show);
}