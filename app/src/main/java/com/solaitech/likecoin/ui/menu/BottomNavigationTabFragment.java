package com.solaitech.likecoin.ui.menu;

public interface BottomNavigationTabFragment {
    void onBottomNavigationTabReselected();
}