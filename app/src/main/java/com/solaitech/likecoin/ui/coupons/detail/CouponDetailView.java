package com.solaitech.likecoin.ui.coupons.detail;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;

import com.solaitech.likecoin.data.models.coupons.CouponDetail;
import com.solaitech.likecoin.ui.base.interfaces.MVPBaseView;

interface CouponDetailView extends MVPBaseView {

    String getCouponId();

    int getCouponType();

    boolean isAppBarLayoutExpanded();

    void hideAppBarLayout();

    void showAppBarLayout();

    void showImageLarge(String imgUrl);

    void setCouponDetail(CouponDetail couponDetail);

    CouponDetail getCouponDetail();

    void showShowCouponView();

    void hideShowCouponView();

    void openPromocodeDialog(String key, boolean showQrCode);

    void setToolbarTitle(String title);

    Context getAppContext();

    Bitmap getMapSnapshot();

    boolean isPermissionGrantedToStorage();

    boolean shouldShowRequestPermissionRationaleToStorage();

    void showRequestPermissionDialogToStorage();

    void showRequestPermissionRationaleToStorage();

    void requestPermissionToStorage();

    void openSettingsActivity();

    int getBlackColor();

    int getWhiteColor();

    int getDisplayWidth();

    int getDisplayHeight();

    void qrCodeGeneratedError();

    void shareCoupon(Intent intentShareFile);
}