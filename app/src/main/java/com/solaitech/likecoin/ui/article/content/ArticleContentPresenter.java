package com.solaitech.likecoin.ui.article.content;

import com.solaitech.likecoin.data.models.article.Article;
import com.solaitech.likecoin.utils.DateTimeUtils;
import com.solaitech.likecoin.utils.HtmlSourceUtils;
import com.solaitech.likecoin.utils.StringUtils;

class ArticleContentPresenter {
    private ArticleContentView view;
    ArticleContentPresenter(ArticleContentView view) {
        this.view = view;
    }

    public void init() {

    }

    void onArticleDetailSet() {
        Article article = view.getArticleDetail();
        if (article != null) {
            if (StringUtils.isStringOk(article.getTitle())) {
                view.showTitle(article.getTitle());
            } else {
                view.hideTitle();
            }

            if (StringUtils.isStringOk(article.getShortDescr())) {
                view.showShortDescription(HtmlSourceUtils.styleTextForWebView(article.getShortDescr()));
            } else {
                view.hideShortDescription();
            }

            if (StringUtils.isStringOk(article.getContent())) {
                view.showContent(HtmlSourceUtils.styleTextForWebView(article.getContent()));
            } else {
                view.hideContent();
            }

            if (article.getRegDT() != null) {
                view.showDate(DateTimeUtils.getCampaignConditionsDate(view.getContextFromPage(), article.getRegDT()));
            } else {
                view.hideDate();
            }
        } else {
            view.hideTitle();
            view.hideShortDescription();
            view.hideContent();
            view.hideDate();
        }
    }
}