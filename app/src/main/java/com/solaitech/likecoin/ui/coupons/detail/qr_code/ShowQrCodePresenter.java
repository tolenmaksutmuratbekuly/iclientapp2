package com.solaitech.likecoin.ui.coupons.detail.qr_code;

import android.graphics.Bitmap;

import com.solaitech.likecoin.data.params.QrBitmapParams;
import com.solaitech.likecoin.data.repository.qr_code.QrCodeRepositoryProvider;
import com.solaitech.likecoin.utils.StringUtils;
import rx.Subscriber;
import rx.Subscription;
import rx.functions.Action0;
import rx.subscriptions.CompositeSubscription;

class ShowQrCodePresenter {
    private ShowQrCodeView view;

    private CompositeSubscription compositeSubscription = new CompositeSubscription();

    ShowQrCodePresenter(ShowQrCodeView view) {
        this.view = view;
    }

    public void init(boolean showQrCode) {
        if (StringUtils.isStringOk(view.getKey())) {
            view.showPromotionalCode(view.getKey());
            if (showQrCode) {
                getQrCode();
            }
        }
    }

    private void getQrCode() {
        Subscription subscription = QrCodeRepositoryProvider
                .provideRepository()
                .getQrBitmap(getParams())
                .doOnSubscribe(new Action0() {
                    @Override
                    public void call() {
                        view.showProgressBar();
                        view.hideErrorView();
                    }
                })
                .doOnTerminate(new Action0() {
                    @Override
                    public void call() {
                        view.hideProgressBar();
                    }
                })
                .subscribe(new Subscriber<Bitmap>() {
                    @Override
                    public void onCompleted() {
                        //do nothing
                    }

                    @Override
                    public void onError(Throwable e) {
                        e.printStackTrace();
                        view.showErrorView();
                    }

                    @Override
                    public void onNext(Bitmap bitmap) {
                        view.showQrCode(bitmap);
                    }
                });
        compositeSubscription.add(subscription);
    }

    private QrBitmapParams getParams() {
        return new QrBitmapParams(
                view.getKey(), view.getBlackColor(), view.getWhiteColor(), view.getDisplayWidth(), view.getDisplayHeight()
        );
    }

    public void onDismiss() {
        unsubscribe();
    }

    private void unsubscribe() {
        if (compositeSubscription != null
                && !compositeSubscription.isUnsubscribed()) {
            compositeSubscription.unsubscribe();
        }
    }
}