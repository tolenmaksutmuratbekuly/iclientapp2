package com.solaitech.likecoin.ui.profile.edit_profile;

import android.app.IntentService;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.v4.content.LocalBroadcastManager;
import android.widget.Toast;

import com.solaitech.likecoin.BuildConfig;
import com.solaitech.likecoin.R;
import com.solaitech.likecoin.data.models.enums.BroadcastEventEnums;
import com.solaitech.likecoin.data.models.user.User;
import com.solaitech.likecoin.data.params.UpdateUserInfoParams;
import com.solaitech.likecoin.data.preferences.Preferences;
import com.solaitech.likecoin.data.preferences.PreferencesImpl;
import com.solaitech.likecoin.data.repository.user.UserRepositoryProvider;
import com.solaitech.likecoin.exceptions.APIException;
import com.solaitech.likecoin.exceptions.ConnectionTimeOutException;
import com.solaitech.likecoin.exceptions.NeedReAuthorizeException;
import com.solaitech.likecoin.exceptions.UnknownException;
import com.solaitech.likecoin.network.API;
import com.solaitech.likecoin.network.RetrofitServiceGenerator;
import com.solaitech.likecoin.network.error.RetrofitErrorHandler;
import com.solaitech.likecoin.utils.Logger;
import com.solaitech.likecoin.utils.StringUtils;

import rx.Subscriber;
import rx.functions.Action0;

public class UpdateProfileIntentService extends IntentService {
    public static final String NAME = "name";
    public static final String SURNAME = "surname";
    public static final String EMAIL = "email";
    public static final String LANGUAGE = "language";

    public UpdateProfileIntentService() {
        super("UpdateProfileIntentService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        if (intent != null
                && intent.getExtras() != null) {
            Bundle bundle = intent.getExtras();
            String name = bundle.getString(NAME);
            String surname = bundle.getString(SURNAME);
            String email = bundle.getString(EMAIL);
            String language = bundle.getString(LANGUAGE);

            UpdateUserInfoParams params = new UpdateUserInfoParams(name, surname, email, language);
            updateUserInfo(params);
        }
    }

    private void updateUserInfo(UpdateUserInfoParams params) {
        UserRepositoryProvider.provideRepository(getAPI())
                .updateUserInfo(params)
                .doOnSubscribe(new Action0() {
                    @Override
                    public void call() {
                        showToastMessage(getString(R.string.your_request_is_being_processed));
                    }
                })
                .subscribe(new Subscriber<User>() {
                    @Override
                    public void onCompleted() {
                        Logger.e(getClass(), "onCompleted");
                        showToastMessage(getString(R.string.profile_data_updated_successfully));
                        sendBroadcast();
                    }

                    @Override
                    public void onError(Throwable e) {
                        Logger.e(getClass(), "onError");
                        if (BuildConfig.IS_DEBUG) {
                            e.printStackTrace();
                        }
                        try {
                            RetrofitErrorHandler.handleException(e);
                        } catch (APIException e1) {
                            showToastMessage(StringUtils.replaceNull(e1.getErrorDescr()));
                        } catch (UnknownException e1) {
                            showToastMessage(getString(R.string.server_error));
                        } catch (ConnectionTimeOutException e1) {
                            showToastMessage(getString(R.string.connection_error));
                        } catch (NeedReAuthorizeException e1) {

                        }
                    }

                    @Override
                    public void onNext(User user) {
                        Logger.e(getClass(), "onNext");
                    }
                });
    }

    private API getAPI() {
        Preferences preferences = new PreferencesImpl(this);
        RetrofitServiceGenerator retrofitServiceGenerator = RetrofitServiceGenerator.getInstance(preferences);
        return retrofitServiceGenerator.createService(API.class);
    }

    private void showToastMessage(final String text) {
        new Handler(Looper.getMainLooper()).post(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(getApplicationContext(), text, Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void sendBroadcast() {
        Intent broadcastIntent = new Intent(BroadcastEventEnums.PROFILE_UPDATED);
        LocalBroadcastManager.getInstance(this).sendBroadcast(broadcastIntent);
    }

}