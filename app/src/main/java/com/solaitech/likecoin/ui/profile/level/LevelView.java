package com.solaitech.likecoin.ui.profile.level;

import com.solaitech.likecoin.data.models.user.User;

interface LevelView {

    User getUser();

    void showUserLevel(int userLevel);

    void showLikesAmount(String likeCount);

    void showCurrentLevelProgress(int progress);

    void showNextLevelResidueLike(String amount);
}