package com.solaitech.likecoin.ui.push_notification;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.LocalBroadcastManager;

import com.onesignal.OSNotificationReceivedResult;

import com.solaitech.likecoin.R;
import com.solaitech.likecoin.data.models.enums.BroadcastEventEnums;
import com.solaitech.likecoin.data.models.enums.PushNotificationEnums;
import com.solaitech.likecoin.data.models.push_notification.PushNotification;
import com.solaitech.likecoin.data.models.push_notification.PushNotificationData;
import com.solaitech.likecoin.data.preferences.Preferences;
import com.solaitech.likecoin.data.preferences.PreferencesImpl;
import com.solaitech.likecoin.network.GsonProvider;
import com.solaitech.likecoin.utils.Logger;

public class NotificationExtenderService extends com.onesignal.NotificationExtenderService {

    /**
     * @param receivedResult
     * @return true to stop the notification from displaying.
     */
    @Override
    protected boolean onNotificationProcessing(OSNotificationReceivedResult receivedResult) {
        Preferences preferences = new PreferencesImpl(this);

        boolean notShowPush = true;
        if (!preferences.isRegistrationCompleted()) {
            Logger.e(getClass(), "Registration not completed");
            notShowPush = true;
        }

        if (receivedResult != null && receivedResult.payload != null &&
                receivedResult.payload.additionalData != null) {
            PushNotificationData pushNotificationData = GsonProvider.gson.fromJson(receivedResult.payload.additionalData.toString(), PushNotificationData.class);
            if (pushNotificationData != null) {
                if (!pushNotificationData.isDisplay() && pushNotificationData.getTypeId().equals(PushNotificationEnums.PUSH_NOTIFICATION_P2P_INPUT)) {
                    if (receivedResult.isAppInFocus) {
                        Intent broadcastIntent = new Intent(BroadcastEventEnums.PROFILE_UPDATED);
                        LocalBroadcastManager.getInstance(this).sendBroadcast(broadcastIntent);
                    }
                    notShowPush = true;
                } else {
                    if (receivedResult.isAppInFocus) {
                        // If application opened now
                        PushNotification pushNotification = new PushNotification();
                        pushNotification.setTitle(receivedResult.payload.title);
                        pushNotification.setBody(receivedResult.payload.body);
                        pushNotification.setData(receivedResult.payload.additionalData.toString());
                        sendBroadcast(pushNotification);

                        // Used coupons (Reload coupon)
                        if (pushNotificationData.getTypeId().equals(PushNotificationEnums.PUSH_NOTIFICATION_TYPE_USED_COUPON)) {
                                
                        }

                        notShowPush = true;
                    } else {
                        OverrideSettings overrideSettings = new OverrideSettings();
                        overrideSettings.extender = new NotificationCompat.Extender() {
                            @Override
                            public NotificationCompat.Builder extend(NotificationCompat.Builder builder) {
                                // Sets the background notification color to Green on Android 5.0+ devices.
                                return builder.setSmallIcon(R.drawable.ic_push);
                            }
                        };
                        displayNotification(overrideSettings);
                        notShowPush = false;
                    }
                }
            } else {
                notShowPush = true;
            }
        }
        return notShowPush;
    }

    private void sendBroadcast(PushNotification pushNotification) {
        Bundle bundle = new Bundle();
        bundle.putParcelable("pushNotification", pushNotification);

        Intent broadcastIntent = new Intent(BroadcastEventEnums.PUSH_NOTIFICATION);
        broadcastIntent.putExtras(bundle);
        LocalBroadcastManager.getInstance(this).sendBroadcast(broadcastIntent);
    }
}