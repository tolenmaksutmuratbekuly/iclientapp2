package com.solaitech.likecoin.ui.campaign;

import com.solaitech.likecoin.data.models.campaign.Campaign;
import com.solaitech.likecoin.data.models.campaign.Tag;
import com.solaitech.likecoin.ui.base.interfaces.MVPBaseView;

import java.util.List;

interface DealsView extends MVPBaseView {

    void addAllPromotions(List<Campaign> campaigns);

    void addPromotionByTag(Tag tag);

    void notifyAdapter();
}
