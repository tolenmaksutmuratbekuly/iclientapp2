package com.solaitech.likecoin.ui.campaign.detail.comments;

import com.solaitech.likecoin.data.models.campaign.Comments;
import com.solaitech.likecoin.data.models.enums.ListDirectionEnums;
import com.solaitech.likecoin.data.params.CommentParams;
import com.solaitech.likecoin.data.params.Like4EntityParams;
import com.solaitech.likecoin.data.repository.campaign.comments.CommentsRepositoryProvider;
import com.solaitech.likecoin.data.repository.like.LikesRepositoryProvider;
import com.solaitech.likecoin.network.API;
import com.solaitech.likecoin.ui.base.PresenterErrorHandler;
import com.solaitech.likecoin.utils.Logger;
import com.solaitech.likecoin.utils.StringUtils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import rx.Subscriber;
import rx.Subscription;
import rx.functions.Action0;
import rx.subscriptions.CompositeSubscription;

class CommentsPresenter {
    private static final int NEWS_FEED_ROW_COUNT = 30;
    private CommentsView view;
    private API api;
    private String campaignId;
    private String entityType;
    private CompositeSubscription compositeSubscription = new CompositeSubscription();
    private CompositeSubscription sendLikeSubscription = new CompositeSubscription();

    CommentsPresenter(CommentsView view, API api, String campaignId, String entityType) {
        this.view = view;
        this.api = api;
        this.campaignId = campaignId;
        this.entityType = entityType;
    }

    public void init() {
        resetPaginateValues();
        //при swipeRefresh препятствовать вызову методов onLoadMore
        view.setIsLoading(true);

        Subscription subscription = CommentsRepositoryProvider
                .provideRepository(api)
                .getComments(campaignId, entityType, StringUtils.EMPTY_STRING, NEWS_FEED_ROW_COUNT, ListDirectionEnums.DESCENDING)
                .doOnSubscribe(new Action0() {
                    @Override
                    public void call() {
                        view.showProgressTemplateView();
                    }
                })
                .doOnTerminate(new Action0() {
                    @Override
                    public void call() {
                        view.showSuccessTemplateView();
                    }
                })
                .subscribe(new Subscriber<List<Comments>>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        PresenterErrorHandler.handleError(e, view);
                    }

                    @Override
                    public void onNext(List<Comments> comments) {
                        if (comments == null) {
                            comments = new ArrayList<>();
                        }

                        Collections.sort(comments);
                        if (!comments.isEmpty()) {
                            view.setBaseId(comments.get(comments.size() - 1).getId());
                        }
                        view.updateAdapterData(comments);

                        if (!comments.isEmpty()) {
                            view.setIsLoading(false);
                            view.setupPagination();
                        }
                    }
                });

        compositeSubscription.add(subscription);
    }

    private void resetPaginateValues() {
        view.setBaseId("");
        view.setHasLoadedAllItems(false);
        view.setIsLoading(false);
    }

    public void onTryToUploadDataAgainClick() {
        init();
    }

    public void onSwipeRefresh() {
        init();
    }

    public void onBottomNavigationTabReselected() {
        //do nothing
    }

    void onLoadMore() {
        Logger.e(getClass(), "onLoadMore");
        Subscription subscription = CommentsRepositoryProvider
                .provideRepository(api)
                .getComments(campaignId, entityType, view.getBaseId(), NEWS_FEED_ROW_COUNT, ListDirectionEnums.DESCENDING)
                .doOnSubscribe(new Action0() {
                    @Override
                    public void call() {
                        view.setIsLoading(true);
                    }
                })
                .doOnTerminate(new Action0() {
                    @Override
                    public void call() {
                        view.setIsLoading(false);
                    }
                })
                .subscribe(new Subscriber<List<Comments>>() {
                    @Override
                    public void onCompleted() {
                        //do nothing
                        Logger.e(getClass(), "onCompleted");
                    }

                    @Override
                    public void onError(Throwable e) {
                        e.printStackTrace();
                        view.showToastQueryError();
                        view.setHasMoreDataToLoad(false);
                    }

                    @Override
                    public void onNext(List<Comments> comments) {
                        if (comments == null) {
                            comments = new ArrayList<>();
                        }

                        Collections.sort(comments);
                        if (!comments.isEmpty()) {
                            view.setBaseId(comments.get(comments.size() - 1).getId());
                        }

                        if (comments.size() < NEWS_FEED_ROW_COUNT) {
                            view.setHasLoadedAllItems(true);
                            view.setHasMoreDataToLoad(false);
                        }
                        view.addAdapterData(comments);
                    }
                });
        compositeSubscription.add(subscription);
    }

    void unsubscribe() {
        if (compositeSubscription != null
                && !compositeSubscription.isUnsubscribed()) {
            compositeSubscription.unsubscribe();
        }
    }

    // ======= Send comment =======
    void onClickSendComment(String comment) {
        if (comment.length() > 0) {
            postComment(comment);
        } else {
            view.showErrorCommentFieldEmpty();
        }
    }

    private void postComment(String comment) {
        Subscription subscription = CommentsRepositoryProvider
                .provideRepository(api)
                .postComment(new CommentParams(campaignId, entityType, comment))
                .doOnSubscribe(new Action0() {
                    @Override
                    public void call() {
                        view.showPbDialog(true);
                    }
                })
                .doOnTerminate(new Action0() {
                    @Override
                    public void call() {
                        view.showPbDialog(false);
                    }
                })
                .subscribe(new Subscriber<Comments>() {
                    @Override
                    public void onCompleted() {
                        //do nothing
                    }

                    @Override
                    public void onError(Throwable e) {
                        //do nothing
                    }

                    @Override
                    public void onNext(Comments comments) {
                        view.addAdapterData(comments);
                    }
                });
        compositeSubscription.add(subscription);
    }

    ///////////////////////////////////////////////////////////////////////////////////////
    /// Like action for: System msg, comment, comment reply, merchant msg (like4Entity) ///
    /////////////////////////////////////////////////////////////////////////////////////
    void onLike4Entity(String entityId, String entityTypeId, int position) {
        if (entityId != null && entityTypeId != null) {
            like4Entity(new Like4EntityParams(entityId, entityTypeId), position);
        }
    }

    private void like4Entity(Like4EntityParams params, final int position) {
        Subscription subscription = LikesRepositoryProvider
                .provideRepository(api)
                .like4EntityComment(params)
                .doOnSubscribe(new Action0() {
                    @Override
                    public void call() {
                        view.showPbDialog(true);
                    }
                })
                .doOnTerminate(new Action0() {
                    @Override
                    public void call() {
                        view.showPbDialog(false);
                    }
                })
                .subscribe(new Subscriber<Comments>() {
                    @Override
                    public void onCompleted() {
                    }

                    @Override
                    public void onError(Throwable e) {
                        PresenterErrorHandler.handleError2(e, view);
                    }

                    @Override
                    public void onNext(Comments comments) {
                        view.onLiked4EntitySuccess(comments, position);
                        view.sendBroadcast();
                    }
                });

        sendLikeSubscription.add(subscription);
    }
}