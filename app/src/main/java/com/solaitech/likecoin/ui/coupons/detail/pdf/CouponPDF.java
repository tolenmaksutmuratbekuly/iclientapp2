package com.solaitech.likecoin.ui.coupons.detail.pdf;

import android.content.Context;
import android.graphics.Bitmap;
import android.support.v4.content.ContextCompat;

import com.itextpdf.text.BadElementException;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Image;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.Rectangle;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.solaitech.likecoin.R;
import com.solaitech.likecoin.data.models.campaign.CampaignDetail;
import com.solaitech.likecoin.data.models.campaign.Coordinate;
import com.solaitech.likecoin.utils.BitmapUtils;
import com.solaitech.likecoin.utils.DateTimeUtils;
import com.solaitech.likecoin.utils.HtmlSourceUtils;
import com.solaitech.likecoin.utils.StringUtils;
import com.solaitech.likecoin.utils.pdf.PDFonts;

import java.io.ByteArrayOutputStream;
import java.io.IOException;

public class CouponPDF {
    private Context context;
    private String key;
    private Document document;
    private CampaignDetail campaign;
    private Bitmap mapSnapshot;
    private Bitmap qrCodeBitmap;

    public CouponPDF(Context context, String key, CampaignDetail campaignDetail, Document document, Bitmap mapSnapshot,
                     Bitmap qrCodeBitmap) {
        this.context = context;
        this.key = key;
        this.campaign = campaignDetail;
        this.document = document;
        this.mapSnapshot = mapSnapshot;
        this.qrCodeBitmap = qrCodeBitmap;
    }

    public void addPage() throws DocumentException {
        if (campaign != null) {
            Paragraph preface = new Paragraph();

            PdfPTable headerTable = new PdfPTable(2);
            headerTable.setWidthPercentage(100);
            headerTable.setWidths(new float[]{6f, 2f});
            if (campaign.getMerchant() != null && campaign.getMerchant().getTitle() != null) {
                headerTable.addCell(tableCellStyle(new Phrase("\n" + campaign.getMerchant().getTitle(), PDFonts.fontBlackBold19)));
            } else {
                headerTable.addCell(tableCellStyle(new Phrase(StringUtils.EMPTY_STRING, PDFonts.fontBlackBold16)));
            }

            Bitmap lcLogo = BitmapUtils.drawableToBitmap(ContextCompat.getDrawable(context, R.drawable.pdf_logo));
            if (lcLogo != null) {
                ByteArrayOutputStream stream = new ByteArrayOutputStream();
                lcLogo.compress(Bitmap.CompressFormat.JPEG, 100, stream);
                try {
                    Image imgDraft = Image.getInstance(stream.toByteArray());
                    imgDraft.setAlignment(Image.RIGHT);
                    imgDraft.scaleAbsolute(80f, 65f);
                    headerTable.addCell(tableCellStyle(imgDraft));
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            preface.add(headerTable);

            if (StringUtils.isStringOk(campaign.getTitle())) {
                Paragraph titlePgf = new Paragraph("\n" + campaign.getTitle(), PDFonts.fontBlackBold16);
                preface.add(titlePgf);
            }

            if (StringUtils.isStringOk(campaign.getShortDescr())) {
                String descr = campaign.getShortDescr();
                if (descr.length() > 500) {
                    descr = descr.substring(0, 500) + "...";
                }
                preface.add(new Chunk("" + HtmlSourceUtils.fromHtml(descr), PDFonts.fontBlack14));
            }

            if (StringUtils.isStringOk(campaign.getFullDescr())) {
                String descr = campaign.getFullDescr();
                if (descr.length() > 800) {
                    descr = descr.substring(0, 800) + "...";
                }
                preface.add(new Chunk("\n" + HtmlSourceUtils.fromHtml(descr), PDFonts.fontBlack14));
            }

            PdfPTable table = new PdfPTable(3);
            table.setWidthPercentage(100);
            table.setWidths(new float[]{0.4f, 3.6f, 4f});

            if (campaign.getCouponStartDt() != null && campaign.getExpiryDate() != null) {
                table.addCell(tableCellStyle(getIcon(R.drawable.pdf_date, 15f, 15f)));
                table.addCell(tableCellStyle(new Phrase("   " + context.getString(R.string.valid_until,
                        DateTimeUtils.getCampaignConditionsDate(context, campaign.getCouponStartDt()),
                        DateTimeUtils.getCampaignConditionsDate(context, campaign.getExpiryDate())), PDFonts.fontBlackBold12)));
            }

            // Promo code
            table.addCell(tableCellStyle(new Paragraph(context.getString(R.string.promotional_code) + ": " + key, PDFonts.fontBlackBold16)));

            preface.add(new Chunk("\n", PDFonts.fontBlack12));
            if (campaign.getContacts() != null
                    && !campaign.getContacts().isEmpty()) {
                table.addCell(tableCellStyle(getIcon(R.drawable.pdf_call, 14f, 17f)));

                Paragraph phoneNumbers = new Paragraph();
                for (String phoneNumber : campaign.getContacts()) {
                    if (StringUtils.isStringOk(phoneNumber)) {
                        phoneNumbers.add(new Chunk(" " + phoneNumber + "\n", PDFonts.fontBlack12));
                    }
                }
                table.addCell(tableCellStyle(phoneNumbers));
            }

            if (qrCodeBitmap != null) {
                ByteArrayOutputStream stream = new ByteArrayOutputStream();
                qrCodeBitmap.compress(Bitmap.CompressFormat.JPEG, 100, stream);
                try {
                    Image img = Image.getInstance(stream.toByteArray());
                    img.scaleAbsolute(222f, 222f); // Neymar JR sold to PSG for 222 bln EURO, that's why here 222f

                    PdfPCell cell = new PdfPCell(img);
                    cell.setBorder(Rectangle.NO_BORDER);
                    cell.setPadding(6);
                    cell.setColspan(3);
                    cell.setRowspan(3);
                    table.addCell(cell);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            if (campaign.getCoordinates() != null
                    && !campaign.getCoordinates().isEmpty()) {
                table.addCell(tableCellStyle(getIcon(R.drawable.pdf_location, 13f, 18f)));
                Paragraph locations = new Paragraph();
                for (Coordinate coordinate : campaign.getCoordinates()) {
                    if (StringUtils.isStringOk(coordinate.getAddress())) {
                        locations.add(new Chunk(" " + coordinate.getAddress() + "\n", PDFonts.fontBlack12));
                    }
                }
                table.addCell(tableCellStyle(locations));
                table.addCell(tableCellStyle(new Phrase(StringUtils.EMPTY_STRING, PDFonts.fontBlack12)));
                if (mapSnapshot != null) {
                    ByteArrayOutputStream stream = new ByteArrayOutputStream();
                    mapSnapshot.compress(Bitmap.CompressFormat.JPEG, 100, stream);
                    try {
                        Image imgDraft = Image.getInstance(stream.toByteArray());
                        imgDraft.setAlignment(Image.MIDDLE);
                        table.addCell(tableCellStyle(imgDraft));
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }

            preface.add(table);
            document.add(preface);
        }
    }

    private PdfPCell tableCellStyle(Object object) {
        PdfPCell cell;
        if (object instanceof Image) {
            cell = new PdfPCell((Image) object);
        } else if (object instanceof Phrase) {
            cell = new PdfPCell((Phrase) object);
        } else if (object instanceof Paragraph) {
            cell = new PdfPCell((Paragraph) object);
        } else {
            cell = new PdfPCell();
        }
        cell.setBorder(Rectangle.NO_BORDER);
        cell.setPadding(6);
        return cell;
    }

    private Image getIcon(int resource, float width, float height) {
        Bitmap icon = BitmapUtils.drawableToBitmap(ContextCompat.getDrawable(context, resource));
        Image img = null;
        if (icon != null) {
            ByteArrayOutputStream stream = new ByteArrayOutputStream();
            icon.compress(Bitmap.CompressFormat.JPEG, 100, stream);
            try {
                img = Image.getInstance(stream.toByteArray());
                img.scaleAbsolute(width, height);
                img.setAlignment(Image.LEFT);
            } catch (IOException | BadElementException e) {
                e.printStackTrace();
            }
        }
        return img;
    }
}
