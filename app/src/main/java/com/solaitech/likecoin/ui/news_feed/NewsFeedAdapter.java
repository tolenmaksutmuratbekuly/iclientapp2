package com.solaitech.likecoin.ui.news_feed;

import android.content.Context;
import android.graphics.drawable.GradientDrawable;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.solaitech.likecoin.R;
import com.solaitech.likecoin.data.models.enums.FavoriteEnums;
import com.solaitech.likecoin.data.models.enums.NewsSourceEntityRefTypeEnums;
import com.solaitech.likecoin.data.models.news_feed.NewsFeed;
import com.solaitech.likecoin.data.models.news_feed.NewsOwner;
import com.solaitech.likecoin.ui.base.recycler_view.RecyclerViewBaseAdapter;
import com.solaitech.likecoin.ui.news_feed.merchant.MerchantLogosContainerView;
import com.solaitech.likecoin.utils.CustomColorUtils;
import com.solaitech.likecoin.utils.DateTimeUtils;
import com.solaitech.likecoin.utils.HtmlSourceUtils;
import com.solaitech.likecoin.utils.ImageUtils;
import com.solaitech.likecoin.utils.StringUtils;
import com.solaitech.likecoin.utils.animation.AnimationCallback;
import com.solaitech.likecoin.utils.animation.CustomAnimationUtils;
import com.solaitech.likecoin.utils.animation.FlippingAnimationCallback;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;

class NewsFeedAdapter extends RecyclerViewBaseAdapter {
    private static final int VIEW_TYPE_NEWS_SYSTEM_MSG = R.layout.adapter_news_system_msg;
    private static final int VIEW_TYPE_TRANSACTION = R.layout.adapter_news_transaction;
    private static final int VIEW_TYPE_CAMPAIGN = R.layout.adapter_news_campaign;
    private static final int VIEW_TYPE_COMMENT = R.layout.adapter_news_comment;
    private static final int VIEW_TYPE_CONGRATS = R.layout.adapter_news_congrats;
    private static final int VIEW_TYPE_MERCHANT_MSG = R.layout.adapter_news_merchant_msg;
    private static final int VIEW_TYPE_ARTICLE = R.layout.adapter_news_article;
    static final int VIEW_TYPE_FIRST_ELEMENT_EMPTY_SPACE = R.layout.adapter_first_element_empty_space;
    private List<Object> objects;
    private boolean hasLoadedAllItemsUp;
    private Callback callback;

    void setCallback(Callback callback) {
        this.callback = callback;
    }

    void setHasLoadedAllItemsUp(boolean hasLoadedAllItems) {
        this.hasLoadedAllItemsUp = hasLoadedAllItems;
    }

    NewsFeedAdapter(Context context, List<Object> objects) {
        super(context);
        this.objects = objects;
    }

    @Override
    public MainViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        switch (viewType) {
            case VIEW_TYPE_NEWS_SYSTEM_MSG:
                return new SystemMsgViewHolder(inflate(parent, VIEW_TYPE_NEWS_SYSTEM_MSG));
            case VIEW_TYPE_TRANSACTION:
                return new TransactionViewHolder(inflate(parent, VIEW_TYPE_TRANSACTION));
            case VIEW_TYPE_CAMPAIGN:
                return new CampaignViewHolder(inflate(parent, VIEW_TYPE_CAMPAIGN));
            case VIEW_TYPE_COMMENT:
                return new CommentViewHolder(inflate(parent, VIEW_TYPE_COMMENT));
            case VIEW_TYPE_CONGRATS:
                return new CongratsViewHolder(inflate(parent, VIEW_TYPE_CONGRATS));
            case VIEW_TYPE_MERCHANT_MSG:
                return new MerchantMsgViewHolder(inflate(parent, VIEW_TYPE_MERCHANT_MSG));
            case VIEW_TYPE_ARTICLE:
                return new ArticleViewHolder(inflate(parent, VIEW_TYPE_ARTICLE));
            case VIEW_TYPE_FIRST_ELEMENT_EMPTY_SPACE:
                return new FirstElementEmptySpaceViewHolder(inflate(parent, VIEW_TYPE_FIRST_ELEMENT_EMPTY_SPACE));
            default:
                throw incorrectOnCreateViewHolder();
        }
    }

    @Override
    public void onBindViewHolder(MainViewHolder holder, int position) {
        switch (holder.getItemViewType()) {
            case VIEW_TYPE_NEWS_SYSTEM_MSG:
                ((SystemMsgViewHolder) holder).bind((NewsFeed) objects.get(position), position);
                break;
            case VIEW_TYPE_TRANSACTION:
                ((TransactionViewHolder) holder).bind((NewsFeed) objects.get(position));
                break;
            case VIEW_TYPE_CAMPAIGN:
                ((CampaignViewHolder) holder).bind((NewsFeed) objects.get(position), position);
                break;
            case VIEW_TYPE_COMMENT:
                ((CommentViewHolder) holder).bind((NewsFeed) objects.get(position), position);
                break;
            case VIEW_TYPE_CONGRATS:
                ((CongratsViewHolder) holder).bind((NewsFeed) objects.get(position));
                break;
            case VIEW_TYPE_MERCHANT_MSG:
                ((MerchantMsgViewHolder) holder).bind((NewsFeed) objects.get(position), position);
                break;
            case VIEW_TYPE_ARTICLE:
                ((ArticleViewHolder) holder).bind((NewsFeed) objects.get(position), position);
                break;
        }
    }

    @Override
    public int getItemViewType(int position) {
        if (objects.get(position) instanceof NewsFeed) {
            NewsFeed newsFeed = (NewsFeed) objects.get(position);
            if (newsFeed.getTypeId() != null) {
                switch (newsFeed.getTypeId()) {
                    case NewsSourceEntityRefTypeEnums.NEWSFEEDTYPEID_SYSMESSAGE: // LIKE functionality
                        return VIEW_TYPE_NEWS_SYSTEM_MSG;
                    case NewsSourceEntityRefTypeEnums.NEWSFEEDTYPEID_TRANSACTION_P2P:
                        return VIEW_TYPE_TRANSACTION;
                    case NewsSourceEntityRefTypeEnums.NEWSFEEDTYPEID_CAMPAIGN:
                        return VIEW_TYPE_CAMPAIGN;
                    case NewsSourceEntityRefTypeEnums.NEWSFEEDTYPEID_COMMENT:
                        return VIEW_TYPE_COMMENT;
                    case NewsSourceEntityRefTypeEnums.NEWSFEEDTYPEID_NEWLEVEL:
                        return VIEW_TYPE_CONGRATS;
                    case NewsSourceEntityRefTypeEnums.NEWSFEEDTYPEID_ANSWER_COMMENT:
                        return VIEW_TYPE_COMMENT;
                    case NewsSourceEntityRefTypeEnums.NEWSFEEDTYPEID_MERCHANT_MESSAGE:
                        return VIEW_TYPE_MERCHANT_MSG;
                    case NewsSourceEntityRefTypeEnums.NEWSFEEDTYPEID_ARTICLE:
                        return VIEW_TYPE_ARTICLE;

                    case NewsSourceEntityRefTypeEnums.NEWSFEEDTYPEID_CAMPAIGNLIST:
                        return VIEW_TYPE_NEWS_SYSTEM_MSG;
                    default:
                        return VIEW_TYPE_NEWS_SYSTEM_MSG;//VIEW_TYPE_FIRST_ELEMENT_EMPTY_SPACE;
                }
            }
        } else if (objects.get(position) instanceof Integer) {
            return (Integer) objects.get(position);
        }
        throw incorrectGetItemViewType();
    }

    @Override
    public int getItemCount() {
        return objects.size();
    }

    class SystemMsgViewHolder extends MainViewHolder {
        @Bind(R.id.tv_title) TextView tv_title;
        @Bind(R.id.tv_name) TextView tv_name;
        @Bind(R.id.tv_date) TextView tv_date;
        @Bind(R.id.tv_description) TextView tv_description;
        @Bind(R.id.iv_avatar) CircleImageView iv_avatar;
        @Bind(R.id.tv_action_title) TextView tv_action_title;
        @Bind(R.id.tv_like_count) TextView tv_like_count;
        @Bind(R.id.ll_like_count) LinearLayout ll_like_count;
        @Bind(R.id.ll_action) LinearLayout ll_action;
        @Bind(R.id.img_coin) ImageView img_coin;
        NewsFeed bindedNewsFeed;
        int bindedPosition;

        SystemMsgViewHolder(View v) {
            super(v);
            ButterKnife.bind(this, v);
        }

        void bind(NewsFeed newsFeed, int position) {
            bindedNewsFeed = newsFeed;
            bindedPosition = position;
            if (newsFeed == null) {
                return;
            }

            tv_action_title.setText(getContext().getText(R.string.action_like));
            if (newsFeed.getTitle() != null) {
                tv_title.setText(newsFeed.getTitle());
            }

            if (newsFeed.getOwner() != null && newsFeed.getOwner().getName() != null) {
                tv_name.setText(newsFeed.getOwner().getName());
            }

            if (newsFeed.getDescr() != null) {
                tv_description.setVisibility(View.VISIBLE);
                HtmlSourceUtils.htmlHyperlinkClickable(getContext(), tv_description, newsFeed.getDescr());
            } else {
                tv_description.setVisibility(View.GONE);
                tv_description.setText(StringUtils.EMPTY_STRING);
            }

            if (newsFeed.getCreateDT() != null) {
                tv_date.setText(DateTimeUtils.newsFeedDate(newsFeed.getCreateDT(), getContext()));
            }

            String userId = null;
            if (newsFeed.getOwner() != null) {
                if (newsFeed.getOwner().getId() != null) {
                    userId = newsFeed.getOwner().getId();
                }
                setImgUrl(newsFeed.getOwner().getAvatarUrl());
                if (newsFeed.getOwner().isUser()) {
                    setImgUrl(newsFeed.getOwner().getAvatarUrl());
                } else {
                    setImgUrl(newsFeed.getOwner().getLogoUrl());
                }
            }

            if (newsFeed.getLikesCounter() != null) {
                if (newsFeed.getLikesCounter().getTotalLikes() != null &&
                        newsFeed.getLikesCounter().getTotalLikes() > 0) {
                    ll_like_count.setVisibility(View.VISIBLE);
                    tv_like_count.setText(String.valueOf(newsFeed.getLikesCounter().getTotalLikes()));
                } else {
                    ll_like_count.setVisibility(View.GONE);
                }

                if (callback != null && callback.localUserId() != null
                        && userId != null && userId.equals(callback.localUserId())) {
                    newsFeed.getLikesCounter().setBeenLiked(true);
                }

                img_coin.setImageResource(newsFeed.getLikesCounter().isBeenLiked() ?
                        R.drawable.ic_coin_promotions :
                        R.drawable.ic_header_coin);
                ll_action.setBackgroundColor(newsFeed.getLikesCounter().isBeenLiked() ?
                        ContextCompat.getColor(getContext(), R.color.btn_like_disabled) :
                        ContextCompat.getColor(getContext(), R.color.actionbar));
            }
        }

        void setImgUrl(String avatarUrl) {
            if (avatarUrl != null) {
                ImageUtils.showAvatar(getContext(), iv_avatar, avatarUrl);
            } else {
                iv_avatar.setImageResource(R.drawable.img_placeholder_rounded);
            }
        }

        @OnClick(R.id.ll_action)
        void onLike4Entity() {
            if (bindedNewsFeed.getLikesCounter() != null) {
                if (!bindedNewsFeed.getLikesCounter().isBeenLiked()) {
                    if (callback != null) {
                        callback.onLike4Entity(bindedNewsFeed.getId(), NewsSourceEntityRefTypeEnums.NEWSFEED, bindedPosition, new FlippingAnimationCallback() {
                            @Override
                            public void show() {
                            }

                            @Override
                            public void like4EntitySuccess(final Object object, final int position) {
                                CustomAnimationUtils.flippingCoinAnimation(getContext(), img_coin, iv_avatar, new AnimationCallback() {
                                    @Override
                                    public void onAnimationFinish() {
                                        objects.set(position, object);
                                        notifyDataSetChanged();
                                    }
                                });
                            }
                        });
                    }
                }
            }
        }

        @OnClick(R.id.vg_container)
        void onItemViewClick() {
//            onItemClick(bindedNewsFeed);
        }
    }

    class TransactionViewHolder extends MainViewHolder {
        @Bind(R.id.tv_title) TextView tv_title;
        @Bind(R.id.tv_u_got) TextView tv_u_got;
        @Bind(R.id.tv_date) TextView tv_date;
        @Bind(R.id.tv_description) TextView tv_description;
        @Bind(R.id.tv_action_title) TextView tv_action_title;
        @Bind(R.id.tv_level) TextView tv_level;
        @Bind(R.id.iv_avatar) CircleImageView iv_avatar;
        @Bind(R.id.ll_action) LinearLayout ll_action;
        @Bind(R.id.img_coin) ImageView img_coin;
        @Bind(R.id.v_merchants) MerchantLogosContainerView v_merchants;
        NewsFeed bindedNewsFeed;

        TransactionViewHolder(View v) {
            super(v);
            ButterKnife.bind(this, v);
        }

        void bind(NewsFeed newsFeed) {
            bindedNewsFeed = newsFeed;
            if (newsFeed == null) {
                return;
            }

            tv_action_title.setText(getContext().getText(R.string.action_like));
            if (newsFeed.getOwner() != null) {
                if (newsFeed.getOwner().getName() != null) {
                    tv_title.setText(newsFeed.getOwner().getName());
                } else {
                    tv_title.setText(StringUtils.EMPTY_STRING);
                }

                if (newsFeed.getOwner().isUser()) {
                    ll_action.setVisibility(View.VISIBLE);
                    setImgUrl(newsFeed.getOwner().getAvatarUrl());

                    // Show merchant's logos
                    if (newsFeed.getOwner().getCustomData() != null) {
                        v_merchants.setVisibility(View.VISIBLE);
                        v_merchants.showLogos(newsFeed.getOwner().getCustomData().getMerchants());
                    } else {
                        v_merchants.setVisibility(View.GONE);
                    }
                } else {
                    ll_action.setVisibility(View.GONE);
                    setImgUrl(newsFeed.getOwner().getLogoUrl());

                    // Show merchant's logos
                    v_merchants.setVisibility(View.GONE);
                }

                // Show user level
                if (newsFeed.getOwner().getCustomData() != null) {
                    if (newsFeed.getOwner().getCustomData().getUserLevel() != null) {
                        tv_level.setVisibility(View.VISIBLE);
                        int level = newsFeed.getOwner().getCustomData().getUserLevel();
                        tv_level.setText(String.valueOf(level));
                        GradientDrawable drawable = (GradientDrawable) tv_level.getBackground();
                        CustomColorUtils.setLevelColor(getContext(), drawable, level, null);
                    } else {
                        tv_level.setVisibility(View.GONE);
                    }
                }
            } else {
                ll_action.setVisibility(View.GONE);
                v_merchants.setVisibility(View.GONE);
            }

            if (newsFeed.getDescr() != null) {
                tv_description.setVisibility(View.VISIBLE);
                tv_description.setText(HtmlSourceUtils.fromHtml(newsFeed.getDescr()));
            } else {
                tv_description.setVisibility(View.GONE);
                tv_description.setText(StringUtils.EMPTY_STRING);
            }

            if (newsFeed.getCreateDT() != null) {
                tv_date.setText(DateTimeUtils.newsFeedDate(newsFeed.getCreateDT(), getContext()));
            }

            if (newsFeed.getCustomData() != null) {
                if (newsFeed.getCustomData().getLikes() != null) {
                    tv_u_got.setText(String.valueOf(newsFeed.getCustomData().getLikes()));
                }
            }
        }

        void setImgUrl(String avatarUrl) {
            if (avatarUrl != null) {
                ImageUtils.showAvatar(getContext(), iv_avatar, avatarUrl);
            } else {
                iv_avatar.setImageResource(R.drawable.img_placeholder_rounded);
            }
        }

        @OnClick(R.id.vg_container)
        void onItemViewClick() {
            onItemClick(bindedNewsFeed);
        }

        @OnClick(R.id.ll_comment)
        void onSendLikeWithComment() {
            if (bindedNewsFeed.getOwner() != null) {
                if (bindedNewsFeed.getOwner().isUser()) {
                    if (callback != null) {
                        callback.onSendLikeClick(bindedNewsFeed.getOwner(), false, new FlippingAnimationCallback() {
                            @Override
                            public void show() {
                                CustomAnimationUtils.flippingCoinAnimation(getContext(), img_coin, iv_avatar, null);
                            }

                            @Override
                            public void like4EntitySuccess(Object object, int position) {

                            }
                        });
                    }
                }
            }
        }

        @OnClick(R.id.ll_action)
        void onSendLike() {
            if (bindedNewsFeed.getOwner() != null) {
                if (bindedNewsFeed.getOwner().isUser()) {
                    if (callback != null) {
                        callback.onSendLikeClick(bindedNewsFeed.getOwner(), true, new FlippingAnimationCallback() {
                            @Override
                            public void show() {
                                CustomAnimationUtils.flippingCoinAnimation(getContext(), img_coin, iv_avatar, null);
                            }

                            @Override
                            public void like4EntitySuccess(Object object, int position) {
                            }
                        });
                    }
                }
            }
        }

        @OnClick(R.id.rl_ava_container)
        void onAvatarClick() {
            if (callback != null) {
                if (bindedNewsFeed.getOwner() != null) {
                    if (bindedNewsFeed.getOwner().getId() != null) {
                        callback.avatarClick(bindedNewsFeed.getOwner().getId());
                    }
                }
            }
        }
    }

    class CampaignViewHolder extends MainViewHolder {
        @Bind(R.id.tv_title) TextView tv_title;
        @Bind(R.id.tv_date) TextView tv_date;
        @Bind(R.id.tv_action_title) TextView tv_action_title;
        @Bind(R.id.tv_count) TextView tv_count;
        @Bind(R.id.tv_campaign_name) TextView tv_campaign_name;
        @Bind(R.id.tv_description) TextView tv_description;
        @Bind(R.id.iv_avatar) CircleImageView iv_avatar;
        @Bind(R.id.iv_small_image) ImageView iv_small_image;
        @Bind(R.id.iv_favorite) ImageView iv_favorite;
        NewsFeed bindedNewsFeed;
        int bindedPosition;

        CampaignViewHolder(View v) {
            super(v);
            ButterKnife.bind(this, v);
        }

        void bind(NewsFeed newsFeed, int position) {
            bindedNewsFeed = newsFeed;
            bindedPosition = position;
            if (newsFeed == null) {
                return;
            }
            tv_action_title.setText(getContext().getText(R.string.action_buy));
            if (newsFeed.getOwner() != null) {
                if (newsFeed.getOwner().isUser()) {
                    setImgUrl(newsFeed.getOwner().getAvatarUrl());
                } else {
                    setImgUrl(newsFeed.getOwner().getLogoUrl());
                }

                if (newsFeed.getOwner().getName() != null) {
                    tv_campaign_name.setText(newsFeed.getOwner().getName());
                }
            }

            if (newsFeed.getTitle() != null) {
                tv_title.setText(newsFeed.getTitle());
            } else {
                tv_title.setText(StringUtils.EMPTY_STRING);
            }

            if (newsFeed.getDescr() != null) {
                tv_description.setVisibility(View.VISIBLE);
                HtmlSourceUtils.htmlHyperlinkClickable(getContext(), tv_description, newsFeed.getDescr());
            } else {
                tv_description.setVisibility(View.GONE);
                tv_description.setText(StringUtils.EMPTY_STRING);
            }

            if (newsFeed.getImgUrl() != null) {
                ImageUtils.showNewsFeedImg(getContext(), iv_small_image, newsFeed.getImgUrl(), R.drawable.news_system_bg);
            } else {
                iv_small_image.setImageResource(R.drawable.news_system_bg);
            }

            if (newsFeed.getCreateDT() != null) {
                tv_date.setText(DateTimeUtils.newsFeedDate(newsFeed.getCreateDT(), getContext()));
            }

            if (newsFeed.getCustomData() != null) {
                if (newsFeed.getCustomData().getCouponPrice() != null) {
                    tv_count.setText(String.valueOf(newsFeed.getCustomData().getCouponPrice()));
                }
            }

            iv_favorite.setImageResource(newsFeed.getRelatedFavorite() != null
                    && newsFeed.getRelatedFavorite().getIsFavorite() != null
                    && newsFeed.getRelatedFavorite().getIsFavorite() == FavoriteEnums.ADD_TO_FAVORITES ?
                    R.drawable.news_favorite_active :
                    R.drawable.news_favorite_default);
        }

        void setImgUrl(String avatarUrl) {
            if (avatarUrl != null) {
                ImageUtils.showAvatar(getContext(), iv_avatar, avatarUrl);
            } else {
                iv_avatar.setImageResource(R.drawable.img_placeholder_rounded);
            }
        }

        @OnClick(R.id.vg_container)
        void onItemViewClick() {
            onItemClick(bindedNewsFeed);
        }

        @OnClick(R.id.ll_comment)
        void onCommentClick() {
            if (callback != null) {
                callback.onCommentsClick(bindedNewsFeed);
            }
        }

        @OnClick(R.id.tv_remove_promotion)
        void onRemovePromotionClick() {
            favoriteResult(FavoriteEnums.REMOVE_FOREVER);
        }

        @OnClick(R.id.iv_favorite)
        void onFavoriteClick() {
            if (callback != null) {
                favoriteResult(
                        (bindedNewsFeed.getRelatedFavorite() != null
                                && bindedNewsFeed.getRelatedFavorite().getIsFavorite() != null
                                && bindedNewsFeed.getRelatedFavorite().getIsFavorite() == FavoriteEnums.ADD_TO_FAVORITES) ?
                                FavoriteEnums.REMOVE_FROM_FAVORITES :
                                FavoriteEnums.ADD_TO_FAVORITES
                );
            }
        }

        private void favoriteResult(final int state) {
            callback.onFavoriteClick(bindedNewsFeed.getId(), state, new FavoriteCallback() {
                @Override
                public void onFavoriteState(NewsFeed newsFeed) {
                    objects.set(bindedPosition, newsFeed);
                    if (state == FavoriteEnums.REMOVE_FOREVER) {
                        objects.remove(bindedPosition);
                    }
                    notifyDataSetChanged();
                }
            });
        }

        @OnClick(R.id.ll_action)
        void onBuyClick() {
            if (callback != null) {
                if (bindedNewsFeed.getCustomData() != null && bindedNewsFeed.getSourceEntityRef() != null) {
                    if (bindedNewsFeed.getSourceEntityRef().getId() != null &&
                            bindedNewsFeed.getCustomData().getCouponPrice() != null &&
                            bindedNewsFeed.getCustomData().getMaxCouponsCount() != null) {
                        callback.onBuyClick(bindedNewsFeed.getSourceEntityRef().getId(),
                                bindedNewsFeed.getCustomData().getCouponPrice(), bindedNewsFeed.getCustomData().getMaxCouponsCount());
                    }
                }
            }
        }
    }

    class CommentViewHolder extends MainViewHolder {
        @Bind(R.id.tv_comment_action) TextView tv_comment_action;
        @Bind(R.id.tv_promotion_title) TextView tv_promotion_title;
        @Bind(R.id.tv_title) TextView tv_title;
        @Bind(R.id.tv_date) TextView tv_date;
        @Bind(R.id.tv_description) TextView tv_description;
        @Bind(R.id.tv_action_title) TextView tv_action_title;
        @Bind(R.id.ll_action) LinearLayout ll_action;
        @Bind(R.id.ll_like_count) LinearLayout ll_like_count;
        @Bind(R.id.tv_like_count) TextView tv_like_count;
        @Bind(R.id.tv_level) TextView tv_level;
        @Bind(R.id.img_coin) ImageView img_coin;
        @Bind(R.id.v_merchants) MerchantLogosContainerView v_merchants;
        @Bind(R.id.iv_avatar) CircleImageView iv_avatar;
        NewsFeed bindedNewsFeed;
        int bindedPosition;

        CommentViewHolder(View v) {
            super(v);
            ButterKnife.bind(this, v);
        }

        void bind(NewsFeed newsFeed, int position) {
            bindedNewsFeed = newsFeed;
            bindedPosition = position;
            if (newsFeed == null) {
                return;
            }

            if (newsFeed.getTypeId() != null && newsFeed.getTypeId() == NewsSourceEntityRefTypeEnums.NEWSFEEDTYPEID_COMMENT) {
                tv_comment_action.setText(getContext().getString(R.string.your_friend_commented));
            } else {
                tv_comment_action.setText(getContext().getString(R.string.your_friend_answered));
            }

            tv_action_title.setText(getContext().getText(R.string.action_like));
            String userId = null;
            if (newsFeed.getOwner() != null) {
                if (newsFeed.getOwner().getId() != null) {
                    userId = newsFeed.getOwner().getId();
                }

                setImgUrl(newsFeed.getOwner().getAvatarUrl());
                if (newsFeed.getOwner().isUser()) {
                    ll_action.setVisibility(View.VISIBLE);
                    setImgUrl(newsFeed.getOwner().getAvatarUrl());

                    if (newsFeed.getOwner().getCustomData() != null) {
                        v_merchants.setVisibility(View.VISIBLE);
                        v_merchants.showLogos(newsFeed.getOwner().getCustomData().getMerchants());
                    }
                } else {
                    ll_action.setVisibility(View.GONE);
                    v_merchants.setVisibility(View.INVISIBLE);
                    setImgUrl(newsFeed.getOwner().getLogoUrl());
                }

                if (newsFeed.getOwner().getName() != null) {
                    tv_title.setText(newsFeed.getOwner().getName());
                } else {
                    tv_title.setText(StringUtils.EMPTY_STRING);
                }

                // Show user level
                if (newsFeed.getOwner().getCustomData() != null) {
                    if (newsFeed.getOwner().getCustomData().getUserLevel() != null) {
                        tv_level.setVisibility(View.VISIBLE);
                        int level = newsFeed.getOwner().getCustomData().getUserLevel();
                        tv_level.setText(String.valueOf(level));
                        GradientDrawable drawable = (GradientDrawable) tv_level.getBackground();
                        CustomColorUtils.setLevelColor(getContext(), drawable, level, null);
                    } else {
                        tv_level.setVisibility(View.GONE);
                    }
                }
            } else {
                v_merchants.setVisibility(View.INVISIBLE);
                ll_action.setVisibility(View.GONE);
            }

            if (newsFeed.getTitle() != null) {
                tv_promotion_title.setText(newsFeed.getTitle());
            } else {
                tv_promotion_title.setText(StringUtils.EMPTY_STRING);
            }

            if (newsFeed.getDescr() != null) {
                tv_description.setVisibility(View.VISIBLE);
                HtmlSourceUtils.htmlHyperlinkClickable(getContext(), tv_description, newsFeed.getDescr());
            } else {
                tv_description.setVisibility(View.GONE);
                tv_description.setText(StringUtils.EMPTY_STRING);
            }

            if (newsFeed.getCreateDT() != null) {
                tv_date.setText(DateTimeUtils.newsFeedDate(newsFeed.getCreateDT(), getContext()));
            }

            if (newsFeed.getLikesCounter() != null) {
                if (newsFeed.getLikesCounter().getTotalLikes() != null &&
                        newsFeed.getLikesCounter().getTotalLikes() > 0) {
                    ll_like_count.setVisibility(View.VISIBLE);
                    tv_like_count.setText(String.valueOf(newsFeed.getLikesCounter().getTotalLikes()));
                } else {
                    ll_like_count.setVisibility(View.GONE);
                }

                if (callback != null && callback.localUserId() != null
                        && userId != null && userId.equals(callback.localUserId())) {
                    newsFeed.getLikesCounter().setBeenLiked(true);
                }

                img_coin.setImageResource(newsFeed.getLikesCounter().isBeenLiked() ?
                        R.drawable.ic_coin_promotions :
                        R.drawable.ic_header_coin);
                ll_action.setBackgroundColor(newsFeed.getLikesCounter().isBeenLiked() ?
                        ContextCompat.getColor(getContext(), R.color.btn_like_disabled) :
                        ContextCompat.getColor(getContext(), R.color.actionbar));
            }
        }

        void setImgUrl(String avatarUrl) {
            if (avatarUrl != null) {
                ImageUtils.showAvatar(getContext(), iv_avatar, avatarUrl);
            } else {
                iv_avatar.setImageResource(R.drawable.img_placeholder_rounded);
            }

        }

        @OnClick(R.id.ll_action)
        void onLike4Entity() {
            if (bindedNewsFeed.getLikesCounter() != null) {
                if (!bindedNewsFeed.getLikesCounter().isBeenLiked()) {
                    if (callback != null) {
                        callback.onLike4Entity(bindedNewsFeed.getId(), NewsSourceEntityRefTypeEnums.NEWSFEED, bindedPosition, new FlippingAnimationCallback() {
                            @Override
                            public void show() {
                            }

                            @Override
                            public void like4EntitySuccess(final Object object, final int position) {
                                CustomAnimationUtils.flippingCoinAnimation(getContext(), img_coin, iv_avatar, new AnimationCallback() {
                                    @Override
                                    public void onAnimationFinish() {
                                        objects.set(position, object);
                                        notifyDataSetChanged();
                                    }
                                });
                            }
                        });
                    }
                }
            }
        }

        @OnClick(R.id.vg_container)
        void onItemViewClick() {
            onItemClick(bindedNewsFeed);
        }

        @OnClick(R.id.ll_comment)
        void onCommentClick() {
            if (callback != null) {
                callback.onCommentsClick(bindedNewsFeed);
            }
        }

        @OnClick(R.id.rl_ava_container)
        void onAvatarClick() {
            if (callback != null) {
                if (bindedNewsFeed.getOwner() != null) {
                    if (bindedNewsFeed.getOwner().getId() != null) {
                        callback.avatarClick(bindedNewsFeed.getOwner().getId());
                    }
                }
            }
        }
    }

    class CongratsViewHolder extends MainViewHolder {
        @Bind(R.id.tv_user_level) TextView tv_user_level;
        @Bind(R.id.tv_title) TextView tv_title;
        NewsFeed bindedNewsFeed;

        CongratsViewHolder(View v) {
            super(v);
            ButterKnife.bind(this, v);
        }

        void bind(NewsFeed newsFeed) {
            bindedNewsFeed = newsFeed;
            if (newsFeed == null) {
                return;
            }

            if (newsFeed.getCustomData() != null) {
                if (newsFeed.getCustomData().getLevel() != null) {
                    tv_user_level.setText(String.valueOf(newsFeed.getCustomData().getLevel()));
                    tv_title.setText(getContext().getString(R.string.u_reached_next_lvl, newsFeed.getCustomData().getLevel()));
                }
            }
        }
    }

    class MerchantMsgViewHolder extends MainViewHolder {
        @Bind(R.id.tv_title) TextView tv_title;
        @Bind(R.id.tv_name) TextView tv_name;
        @Bind(R.id.tv_date) TextView tv_date;
        @Bind(R.id.tv_description) TextView tv_description;
        @Bind(R.id.iv_avatar) CircleImageView iv_avatar;
        @Bind(R.id.ll_like_count) LinearLayout ll_like_count;
        @Bind(R.id.tv_like_count) TextView tv_like_count;
        @Bind(R.id.ll_action) LinearLayout ll_action;
        @Bind(R.id.img_coin) ImageView img_coin;
        NewsFeed bindedNewsFeed;
        int bindedPosition;

        MerchantMsgViewHolder(View v) {
            super(v);
            ButterKnife.bind(this, v);
        }

        void bind(NewsFeed newsFeed, int position) {
            bindedNewsFeed = newsFeed;
            bindedPosition = position;
            if (newsFeed == null) {
                return;
            }

            if (newsFeed.getTitle() != null) {
                tv_title.setText(newsFeed.getTitle());
            }

            if (newsFeed.getOwner() != null && newsFeed.getOwner().getName() != null) {
                tv_name.setText(newsFeed.getOwner().getName());
            }


            if (newsFeed.getDescr() != null) {
                tv_description.setVisibility(View.VISIBLE);
                HtmlSourceUtils.htmlHyperlinkClickable(getContext(), tv_description, newsFeed.getDescr());
            } else {
                tv_description.setVisibility(View.GONE);
                tv_description.setText(StringUtils.EMPTY_STRING);
            }

            if (newsFeed.getCreateDT() != null) {
                tv_date.setText(DateTimeUtils.newsFeedDate(newsFeed.getCreateDT(), getContext()));
            }

            String userId = null;
            if (newsFeed.getOwner() != null) {
                if (newsFeed.getOwner().getId() != null) {
                    userId = newsFeed.getOwner().getId();
                }

                setImgUrl(newsFeed.getOwner().getAvatarUrl());
                if (newsFeed.getOwner().isUser()) {
                    setImgUrl(newsFeed.getOwner().getAvatarUrl());
                } else {
                    setImgUrl(newsFeed.getOwner().getLogoUrl());
                }
            }

            if (newsFeed.getLikesCounter() != null) {
                if (newsFeed.getLikesCounter().getTotalLikes() != null &&
                        newsFeed.getLikesCounter().getTotalLikes() > 0) {
                    ll_like_count.setVisibility(View.VISIBLE);
                    tv_like_count.setText(String.valueOf(newsFeed.getLikesCounter().getTotalLikes()));
                } else {
                    ll_like_count.setVisibility(View.GONE);
                }

                if (callback != null && callback.localUserId() != null
                        && userId != null && userId.equals(callback.localUserId())) {
                    newsFeed.getLikesCounter().setBeenLiked(true);
                }

                img_coin.setImageResource(newsFeed.getLikesCounter().isBeenLiked() ?
                        R.drawable.ic_coin_promotions :
                        R.drawable.ic_header_coin);
                ll_action.setBackgroundColor(newsFeed.getLikesCounter().isBeenLiked() ?
                        ContextCompat.getColor(getContext(), R.color.btn_like_disabled) :
                        ContextCompat.getColor(getContext(), R.color.actionbar));
            }
        }

        void setImgUrl(String avatarUrl) {
            if (avatarUrl != null) {
                ImageUtils.showAvatar(getContext(), iv_avatar, avatarUrl);
            } else {
                iv_avatar.setImageResource(R.drawable.img_placeholder_rounded);
            }
        }

        @OnClick(R.id.ll_action)
        void onLike4Entity() {
            if (bindedNewsFeed.getLikesCounter() != null) {
                if (!bindedNewsFeed.getLikesCounter().isBeenLiked()) {
                    if (callback != null) {
                        callback.onLike4Entity(bindedNewsFeed.getId(), NewsSourceEntityRefTypeEnums.NEWSFEED, bindedPosition, new FlippingAnimationCallback() {
                            @Override
                            public void show() {
                            }

                            @Override
                            public void like4EntitySuccess(final Object object, final int position) {
                                CustomAnimationUtils.flippingCoinAnimation(getContext(), img_coin, iv_avatar, new AnimationCallback() {
                                    @Override
                                    public void onAnimationFinish() {
                                        objects.set(position, object);
                                        notifyDataSetChanged();
                                    }
                                });
                            }
                        });
                    }
                }
            }
        }

        @OnClick(R.id.vg_container)
        void onItemViewClick() {
//            onItemClick(bindedNewsFeed);
        }
    }

    class ArticleViewHolder extends MainViewHolder {
        @Bind(R.id.tv_title) TextView tv_title;
        @Bind(R.id.tv_name) TextView tv_name;
        @Bind(R.id.tv_date) TextView tv_date;
        @Bind(R.id.tv_action_title) TextView tv_action_title;
        @Bind(R.id.tv_count) TextView tv_count;
        @Bind(R.id.tv_description) TextView tv_description;
        @Bind(R.id.iv_avatar) CircleImageView iv_avatar;
        @Bind(R.id.iv_small_image) ImageView iv_small_image;
        @Bind(R.id.ll_action) LinearLayout ll_action;
        @Bind(R.id.img_coin) ImageView img_coin;

        NewsFeed bindedNewsFeed;
        int bindedPosition;

        ArticleViewHolder(View v) {
            super(v);
            ButterKnife.bind(this, v);
        }

        void bind(NewsFeed newsFeed, int position) {
            bindedNewsFeed = newsFeed;
            bindedPosition = position;
            if (newsFeed == null) {
                return;
            }

            tv_action_title.setText(getContext().getText(R.string.action_buy));
            if (newsFeed.getTitle() != null) {
                tv_title.setText(newsFeed.getTitle());
            }

            if (newsFeed.getOwner() != null && newsFeed.getOwner().getName() != null) {
                tv_name.setText(newsFeed.getOwner().getName());
            }

            if (newsFeed.getDescr() != null) {
                tv_description.setVisibility(View.VISIBLE);
                HtmlSourceUtils.htmlHyperlinkClickable(getContext(), tv_description, newsFeed.getDescr());
            } else {
                tv_description.setVisibility(View.GONE);
                tv_description.setText(StringUtils.EMPTY_STRING);
            }

            if (newsFeed.getCreateDT() != null) {
                tv_date.setText(DateTimeUtils.newsFeedDate(newsFeed.getCreateDT(), getContext()));
            }

            String userId = null;
            if (newsFeed.getOwner() != null) {
                if (newsFeed.getOwner().getId() != null) {
                    userId = newsFeed.getOwner().getId();
                }

                setImgUrl(newsFeed.getOwner().getAvatarUrl());
                if (newsFeed.getOwner().isUser()) {
                    setImgUrl(newsFeed.getOwner().getAvatarUrl());
                } else {
                    setImgUrl(newsFeed.getOwner().getLogoUrl());
                }
            }

            if (newsFeed.getOwned() != null) {
                if (callback != null && callback.localUserId() != null
                        && userId != null && userId.equals(callback.localUserId())) {
                    newsFeed.setOwned(true);
                }

                img_coin.setImageResource(newsFeed.getOwned() ?
                        R.drawable.ic_coin_promotions :
                        R.drawable.ic_header_coin);
                ll_action.setBackgroundColor(newsFeed.getOwned() ?
                        ContextCompat.getColor(getContext(), R.color.btn_like_disabled) :
                        ContextCompat.getColor(getContext(), R.color.actionbar));
            }

            if (bindedNewsFeed.getCustomData() != null && bindedNewsFeed.getCustomData().getArticlePrice() != null) {
                tv_count.setText(String.valueOf(bindedNewsFeed.getCustomData().getArticlePrice()));
            }

            if (newsFeed.getImgUrl() != null) {
                ImageUtils.showNewsFeedImg(getContext(), iv_small_image, newsFeed.getImgUrl(), R.drawable.news_system_bg);
            } else {
                iv_small_image.setImageResource(R.drawable.news_system_bg);
            }
        }

        void setImgUrl(String avatarUrl) {
            if (avatarUrl != null) {
                ImageUtils.showAvatar(getContext(), iv_avatar, avatarUrl);
            } else {
                iv_avatar.setImageResource(R.drawable.img_placeholder_rounded);
            }
        }

        @OnClick(R.id.ll_comment)
        void onCommentClick() {
            if (callback != null) {
                callback.onCommentsClick(bindedNewsFeed);
            }
        }

        @OnClick(R.id.vg_container)
        void onItemViewClick() {
            onItemClick(bindedNewsFeed);
        }

        @OnClick(R.id.ll_action)
        void onLike4Entity() {
            if (bindedNewsFeed.getOwned() != null) {
                if (!bindedNewsFeed.getOwned()) {
                    if (callback != null) {
                        callback.onArticleBuyClick(bindedNewsFeed.getSourceEntityRef().getId(),
                                new ArticleCallback() {
                                    @Override
                                    public void onArticleBoughtState() {
                                        bindedNewsFeed.setOwned(true);
                                        notifyDataSetChanged();
                                    }
                                });
                    }
                }
            }
        }
    }

    interface Callback {
        void onSendLikeClick(NewsOwner owner, boolean isOneLike, FlippingAnimationCallback listener);

        void onLike4Entity(String entityId, String entityTypeId, int position, FlippingAnimationCallback listener);

        void onBuyClick(String campaignId, int couponPrice, int maxCouponsCount);

        void onArticleBuyClick(String articleId, ArticleCallback callback);

        void onCommentsClick(NewsFeed newsFeed);

        void onFavoriteClick(String newsFeedId, int isFavorite, FavoriteCallback callback);

        void avatarClick(String userId);

        String localUserId();
    }

    interface FavoriteCallback {
        void onFavoriteState(NewsFeed newsFeed);
    }

    interface ArticleCallback {
        void onArticleBoughtState();
    }
}