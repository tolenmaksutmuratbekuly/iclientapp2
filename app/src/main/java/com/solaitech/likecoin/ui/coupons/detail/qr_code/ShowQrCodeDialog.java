package com.solaitech.likecoin.ui.coupons.detail.qr_code;

import android.app.Dialog;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.solaitech.likecoin.R;
import com.solaitech.likecoin.ui.base.BaseBottomSheetDialogFragment;
import com.solaitech.likecoin.utils.CustomColorUtils;
import com.solaitech.likecoin.utils.DeviceUtils;

import butterknife.Bind;
import butterknife.ButterKnife;

public class ShowQrCodeDialog extends BaseBottomSheetDialogFragment implements ShowQrCodeView {

    private String key;
    private boolean showQrCode;
    private String title;

    @Bind(R.id.iv_qr_code) ImageView iv_qr_code;
    @Bind(R.id.tv_promotional_code) TextView tv_promotional_code;
    @Bind(R.id.progress_bar) ProgressBar progress_bar;
    @Bind(R.id.tv_error) TextView tv_error;
    @Bind(R.id.tv_title) TextView tv_title;
    private ShowQrCodePresenter presenter;

    public static ShowQrCodeDialog newInstance(String key, boolean showQrCode, String title) {
        ShowQrCodeDialog d = new ShowQrCodeDialog();
        d.key = key;
        d.showQrCode = showQrCode;
        d.title = title;
        return d;
    }

    @Override
    public void setupDialog(Dialog dialog, int style) {
        View v = View.inflate(getContext(), R.layout.dialog_show_qr_code, null);
        dialog.setContentView(v);
        ButterKnife.bind(this, v);
        setBottomSheetCallback(v);
        tv_title.setText(title);
        iv_qr_code.setVisibility(showQrCode ? View.VISIBLE : View.GONE);
        tv_error.setVisibility(View.GONE);

        presenter = new ShowQrCodePresenter(this);
        presenter.init(showQrCode);
    }

    @Override
    public void showPromotionalCode(String code) {
        tv_promotional_code.setText(code);
    }

    @Override
    public void showProgressBar() {
        progress_bar.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgressBar() {
        progress_bar.setVisibility(View.GONE);
    }

    @Override
    public void showErrorView() {
        tv_error.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideErrorView() {
        tv_error.setVisibility(View.GONE);
    }

    @Override
    public String getKey() {
        return key;
    }

    @Override
    public int getBlackColor() {
        return CustomColorUtils.getColor(getContext(), R.color.black);
    }

    @Override
    public int getWhiteColor() {
        return CustomColorUtils.getColor(getContext(), R.color.white);
    }

    @Override
    public void showQrCode(Bitmap bitmap) {
        iv_qr_code.setImageBitmap(bitmap);
    }

    @Override
    public int getDisplayWidth() {
        return DeviceUtils.getDisplayWidth(getContext());
    }

    @Override
    public int getDisplayHeight() {
        return DeviceUtils.getDisplayHeight(getContext());
    }

    @Override
    public void onDismiss(DialogInterface dialog) {
        presenter.onDismiss();
        super.onDismiss(dialog);
    }
}