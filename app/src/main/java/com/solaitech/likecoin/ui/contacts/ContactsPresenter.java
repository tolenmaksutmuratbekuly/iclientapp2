package com.solaitech.likecoin.ui.contacts;

import android.view.View;

import java.util.ArrayList;
import java.util.List;

import com.solaitech.likecoin.data.models.contacts.InvitationText;
import com.solaitech.likecoin.data.models.contacts.UserContact;
import com.solaitech.likecoin.data.models.contacts.UserContactLocal;
import com.solaitech.likecoin.data.models.user.User;
import com.solaitech.likecoin.data.params.contacts.ContactAddingParams;
import com.solaitech.likecoin.data.repository.contacts.ContactsRepositoryProvider;
import com.solaitech.likecoin.network.API;
import com.solaitech.likecoin.ui.base.PresenterErrorHandler;
import com.solaitech.likecoin.utils.DateTimeUtils;
import okhttp3.ResponseBody;
import rx.Observable;
import rx.Subscriber;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action0;
import rx.functions.Func2;
import rx.schedulers.Schedulers;

class ContactsPresenter {
    private ContactsView view;
    private API api;

    ContactsPresenter(ContactsView view, API api) {
        this.view = view;
        this.api = api;
    }

    public void init() {
        if (view.getFABBtn() != null) {
            view.getFABBtn().setOnClickListener(fabClickListener);
        }

        if (view.isPermissionGrantedToReadContacts()) {
            resetAndUpdateData();
        } else {
            view.showAccessDisclaimer();
        }

    }

    View.OnClickListener fabClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            inviteFriend();
        }
    };

    private void inviteFriend() {
        getInvitationTextObservable()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnTerminate(new Action0() {
                    @Override
                    public void call() {
                        view.dismissProgressDialog();
                    }
                })
                .doOnSubscribe(new Action0() {
                    @Override
                    public void call() {
                        view.showProgressDialog();
                    }
                })
                .subscribe(new Subscriber<InvitationText>() {
                    @Override
                    public void onCompleted() {
                        view.dismissProgressDialog();
                    }

                    @Override
                    public void onError(Throwable e) {
                        view.dismissProgressDialog();
                    }

                    @Override
                    public void onNext(InvitationText text) {
                        view.shareApp(text.getInviteText());
                    }
                });
    }

    public void onSwipeRefresh() {
        if (view.isPermissionGrantedToReadContacts()) {
            view.setContactsUpdated(false);
            resetAndUpdateData();
        }
    }

    public void onTryToUploadDataAgainClick() {
        if (view.isPermissionGrantedToReadContacts()) {
            loadRegisteredUsers();
        }
    }

    private void loadRegisteredUsers() {
        getContactsObservable(true)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe(new Action0() {
                    @Override
                    public void call() {
                        view.showProgressTemplateView();
                    }
                })
                .subscribe(new Subscriber<List<UserContact>>() {
                    @Override
                    public void onCompleted() {
                        view.showSuccessTemplateView();
                    }

                    @Override
                    public void onError(Throwable e) {
                        PresenterErrorHandler.handleError(e, view);
                    }

                    @Override
                    public void onNext(List<UserContact> userContacts) {
                        view.saveRegisteredUsers(getRegisteredUsers(userContacts));
                        view.updateAdapterData(view.getRegisteredUsers());
                    }
                });
    }

    private void resetAndUpdateData() {
        loadRegisteredUsers();
        if (!view.isContactsUpdated()) {
            checkContactsForAddingAndGetRegisteredObservable()
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .doOnTerminate(new Action0() {
                        @Override
                        public void call() {
                            view.hideHeaderProgress();
                        }
                    })
                    .doOnSubscribe(new Action0() {
                        @Override
                        public void call() {
                            view.showHeaderProgress();
                        }
                    })
                    .subscribe(new Subscriber<List<User>>() {
                        @Override
                        public void onCompleted() {
                            view.hideHeaderProgress();
                        }

                        @Override
                        public void onError(Throwable e) {
                            view.hideHeaderProgress();
                        }

                        @Override
                        public void onNext(List<User> registeredUsers) {
                            view.setContactsUpdated(true);
                        }
                    });
        }
    }

    private Observable<List<User>> checkContactsForAddingAndGetRegisteredObservable() {
        return Observable.zip(getContactsObservable(false), getLocalContactsObservable(), new Func2<List<UserContact>, List<UserContactLocal>, List<User>>() {
            @Override
            public List<User> call(List<UserContact> userContacts, List<UserContactLocal> userContactLocals) {
                checkContactsForUpdate(userContacts, userContactLocals);
                return null;
            }
        });
    }

    private void checkContactsForUpdate(List<UserContact> userContacts, List<UserContactLocal> userContactLocals) {
        List<UserContact> listForDeleting = new ArrayList<>();
        List<UserContactLocal> listForAdding = new ArrayList<>();

        for (UserContact u : userContacts) {
            boolean isFound = false;
            for (UserContactLocal ul : userContactLocals) {
                if (u.getExtId().equals(ul.getId())) {
                    isFound = true;
                    if (DateTimeUtils.compareContactDates(ul.getLastUpdatedDate(), u.getUpdateDT()) == 0) {
                        ul.setChecked(true);
                    }
                    break;
                }
            }
            if (!isFound)
                listForDeleting.add(u);
        }


        for (UserContactLocal ul : userContactLocals) {
            if (!ul.isChecked()) {
                listForAdding.add(ul);
            }
        }

        if (listForAdding.size() > 0) {
            addUserContacts(getFilteredContactsToAdd(listForAdding));
        }

        // Deleting contacts
        if (listForDeleting.size() > 0) {
            ArrayList<String> contactsForDeleting = new ArrayList<>();
            for (UserContact userContact : listForDeleting) {
                contactsForDeleting.add(userContact.getExtId());
            }
            deleteContactsObservable(contactsForDeleting);
        }
    }

    private void addUserContacts(List<ContactAddingParams> params) {
        if (params.size() == 0)
            return;

        Subscriber subscriber = new Subscriber<Object>() {
            @Override
            public void onCompleted() {
            }

            @Override
            public void onError(Throwable e) {
            }

            @Override
            public void onNext(Object object) {
            }
        };
        addUserContactsObservable(params).subscribe(subscriber);
    }

    private List<ContactAddingParams> getFilteredContactsToAdd(List<UserContactLocal> users) {
        List<ContactAddingParams> userContacts = new ArrayList<>();
        for (UserContactLocal u : users) {
            if (!u.isChecked()) {
                userContacts.add(
                        new ContactAddingParams(
                                u.getId(),
                                u.getName(),
                                u.getLastUpdatedDateString(),
                                u.getPhoneNumberList()
                        )
                );
            }
        }

        return userContacts;
    }

    private Observable<ResponseBody> addUserContactsObservable(List<ContactAddingParams> userContacts) {
        return ContactsRepositoryProvider
                .provideRepository(api)
                .addContacts(userContacts);
    }

    private Observable<ResponseBody> deleteContactsObservable(List<String> contacts) {
        return ContactsRepositoryProvider
                .provideRepository(api)
                .deleteUserContacts(contacts);
    }

    /**
     * @param userContacts - all received contacts
     * @return only registered Users
     */
    private List<User> getRegisteredUsers(List<UserContact> userContacts) {
        List<User> users = new ArrayList<>();
        for (UserContact userContact : userContacts) {
            if (userContact.getValues() != null) {
                for (int i = 0; i < userContact.getValues().size(); i++) {
                    if (userContact.getValues().get(i).getRefUser() != null) {
                        userContact.getValues().get(i).getRefUser().setName(userContact.getName());
                        users.add(userContact.getValues().get(i).getRefUser());
                    }
                }
            }
        }

        return users;

    }

    private Observable<InvitationText> getInvitationTextObservable() {
        return ContactsRepositoryProvider
                .provideRepository(api)
                .getInvitationText();
    }

    private Observable<List<UserContact>> getContactsObservable(boolean isOnlyRegistered) {
        return ContactsRepositoryProvider
                .provideRepository(api)
                .getContacts(isOnlyRegistered);
    }

    private Observable<List<UserContactLocal>> getLocalContactsObservable() {
        return ContactsRepositoryProvider
                .provideRepository(view.getContentResolver())
                .getLocalContacts();
    }

    public void onItemClick(Object object) {

        if (object instanceof User) {
            view.sendLike((User) object);
        } else if (object instanceof Integer) {
            if (((int) object) == ContactsAdapter.VIEW_TYPE_NO_ACCESS) {
                view.requestContactsPermissions();
            }
        }
    }

    void onPermissionGrantedToReadContacts() {
        resetAndUpdateData();
    }

    public void onBottomNavigationTabReselected() {

    }

}