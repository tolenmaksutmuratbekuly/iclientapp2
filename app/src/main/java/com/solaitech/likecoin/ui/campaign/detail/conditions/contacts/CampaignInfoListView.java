package com.solaitech.likecoin.ui.campaign.detail.conditions.contacts;

import android.content.Context;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.solaitech.likecoin.R;

import java.util.List;

public class CampaignInfoListView extends LinearLayout {
    public static final int VIEW_CONTACTS = 1;
    public static final int VIEW_LINKS = 2;
    public static final int VIEW_EMAILS = 3;
    public static final int VIEW_ADDRESSES = 4;

    private ViewGroup vgContainer;
    private ImageView img_icon;

    public CampaignInfoListView(Context context) {
        super(context);
        inflate();
    }

    public CampaignInfoListView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        inflate();
    }

    public CampaignInfoListView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        inflate();
    }

    private void inflate() {
        LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflater.inflate(R.layout.v_campaign_info_list, this);

        initView();
    }

    private void initView() {
        vgContainer = findViewById(R.id.vg_container);
        img_icon = findViewById(R.id.img_icon);
    }

    public void showList(List<String> list, int viewType) {
        if (viewType == VIEW_CONTACTS) {
            img_icon.setImageResource(R.drawable.ic_call);
        } else if (viewType == VIEW_LINKS) {
            img_icon.setImageResource(R.drawable.ic_clink);
        } else if (viewType == VIEW_EMAILS) {
            img_icon.setImageResource(R.drawable.ic_profile_email);
        } else if (viewType == VIEW_ADDRESSES) {
            img_icon.setImageResource(R.drawable.ic_location);
        }

        vgContainer.removeAllViews();
        for (int i = 0; i < list.size(); i++) {
            String item = list.get(i);
            vgContainer.addView(getInfoView(item, i, viewType));
        }
    }

    private View getInfoView(String info, int position, int viewType) {
        CampaignInfoView view = new CampaignInfoView(getContext());
        view.showInfoView(info, viewType);
        if (position == 0) {
            view.hideLine();
        }
        return view;
    }
}