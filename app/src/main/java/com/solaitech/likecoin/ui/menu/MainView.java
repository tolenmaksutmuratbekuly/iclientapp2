package com.solaitech.likecoin.ui.menu;

import com.solaitech.likecoin.exceptions.AppVersionException;

interface MainView {

    void showContactsFab();

    void hideContactsFab();

    String getAppVersion();

    void callOnResumeOnProfileView();

    void showInvalidAppVersionDialog(AppVersionException e);

    void doSearch(String text);

    void showAllListItems();
}