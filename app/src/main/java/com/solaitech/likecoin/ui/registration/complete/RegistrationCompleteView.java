package com.solaitech.likecoin.ui.registration.complete;

import com.solaitech.likecoin.data.models.registration.RegistrationComplete;
import com.solaitech.likecoin.ui.base.interfaces.MVPBaseErrorView;

interface RegistrationCompleteView extends MVPBaseErrorView {

    void showSmsInfoText(String phoneNumber);

    String getPhoneNumber();

    String getEmail();

    String getName();

    String getSurname();

    String getConfirmationCode();

    void showConfirmationCodeFieldError();

    void showProgressDialog();

    void dismissProgressDialog();

    void saveRegistrationCompleteData(RegistrationComplete registrationComplete);

    void setRegistrationCompleted();

    void closeDialog();

    void openMainActivity();

    void hideResendSmsButton();

    void showResendSmsButton();

    void hideTimeLeftView();

    void showTimeLeftView();

    void updateTimeLeftToEnableResendSmsButton(String time);

    String getLanguage();
}