package com.solaitech.likecoin.ui.registration.complete;

import android.animation.LayoutTransition;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomSheetDialog;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AlertDialog;
import android.telephony.SmsMessage;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import com.solaitech.likecoin.R;
import com.solaitech.likecoin.data.Constants;
import com.solaitech.likecoin.data.models.registration.RegistrationComplete;
import com.solaitech.likecoin.exceptions.APIException;
import com.solaitech.likecoin.exceptions.ConnectionTimeOutException;
import com.solaitech.likecoin.exceptions.NeedReAuthorizeException;
import com.solaitech.likecoin.exceptions.UnknownException;
import com.solaitech.likecoin.ui.base.BaseBottomSheetDialogFragment;
import com.solaitech.likecoin.ui.intro.TutorialActivity;
import com.solaitech.likecoin.ui.menu.MainActivity;
import com.solaitech.likecoin.utils.EditTextWatcher;
import com.solaitech.likecoin.utils.FabricAnalytics;
import com.solaitech.likecoin.utils.HtmlSourceUtils;
import com.solaitech.likecoin.utils.ToastUtils;
import com.solaitech.likecoin.utils.animation.CustomAnimationUtils;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class RegistrationCompleteDialog extends BaseBottomSheetDialogFragment
        implements RegistrationCompleteView {
    private static final String KEY_PHONE_NUMBER = "phoneNumber";
    private static final String KEY_EMAIL = "email";
    private static final String KEY_NAME = "name";
    private static final String KEY_SURNAME = "surname";
    private static final String KEY_IS_REGISTERED = "isRegistered";

    private String phoneNumber;
    private String email;
    private String name;
    private String surname;
    private boolean isRegistered;

    @Bind(R.id.vg_container) ViewGroup vg_container;
    @Bind(R.id.tv_sms_info) TextView tv_sms_info;
    @Bind(R.id.tv_time_left_to_show_resend_sms) TextView tv_time_left_to_show_resend_sms;
    @Bind(R.id.tv_resend_sms) TextView tv_resend_sms;
    @Bind(R.id.til_confirmation_code) TextInputLayout til_confirmation_code;
    @Bind(R.id.et_confirmation_code) EditText et_confirmation_code;

    private EditTextWatcher watcherConfirmationCode;
    private RegistrationCompletePresenter presenter;
    private ProgressDialog progressDialog;
//    private SmsBroadcastReceiver smsBroadcastReceiver;

    public static RegistrationCompleteDialog newInstance(String phoneNumber, String email, String name, String surname, boolean isRegistered) {
        RegistrationCompleteDialog fragment = new RegistrationCompleteDialog();
        Bundle data = new Bundle();
        data.putString(KEY_PHONE_NUMBER, phoneNumber);
        data.putString(KEY_EMAIL, email);
        data.putString(KEY_NAME, name);
        data.putString(KEY_SURNAME, surname);
        data.putBoolean(KEY_IS_REGISTERED, isRegistered);
        fragment.setArguments(data);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        if (PermissionUtils.isPermissionGranted(getContext(), Manifest.permission.RECEIVE_SMS)) {
//            smsBroadcastReceiver = new SmsBroadcastReceiver();
//        }

        handleBundle();
    }

    @Override
    public void onResume() {
        super.onResume();
//        if (smsBroadcastReceiver != null) {
//            getContext().registerReceiver(smsBroadcastReceiver, new IntentFilter(SmsBroadcastReceiver.INTENT_ACTION_SMS_RECEIVED));
//        }
    }

    @Override
    public void onPause() {
//        if (smsBroadcastReceiver != null) {
//            getContext().unregisterReceiver(smsBroadcastReceiver);
//        }
        super.onPause();
    }

    @Override
    public void setupDialog(Dialog dialog, int style) {
        View v = View.inflate(getContext(), R.layout.dialog_registration_complete, null);
        ButterKnife.bind(this, v);
        dialog.setContentView(v);
        setBottomSheetCallback(v);
        configureViews();

        presenter = new RegistrationCompletePresenter(this, api);
        presenter.init();
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        return new BottomSheetDialog(getActivity(), getTheme()) {
            @Override
            public void onBackPressed() {
                ToastUtils.show(getContext(), getString(R.string.please_press_home_btn));
                FabricAnalytics.eventBackButtonPressed();
            }
        };
    }

    @Override
    public void onStop() {
        super.onStop();
        FabricAnalytics.eventHomeButtonPressed();
    }

    private void handleBundle() {
        Bundle bundle = getArguments();
        if (bundle != null) {
            phoneNumber = bundle.getString(KEY_PHONE_NUMBER);
            email = bundle.getString(KEY_EMAIL);
            name = bundle.getString(KEY_NAME);
            surname = bundle.getString(KEY_SURNAME);
            isRegistered = bundle.getBoolean(KEY_IS_REGISTERED);
        }
    }

    private void configureViews() {
        LayoutTransition transition = new LayoutTransition();
        transition.setAnimateParentHierarchy(false);
        vg_container.setLayoutTransition(transition);
        CustomAnimationUtils.enableTransition(vg_container);

        watcherConfirmationCode = new EditTextWatcher(getContext(), til_confirmation_code);
        et_confirmation_code.addTextChangedListener(watcherConfirmationCode);
//        KeyboardUtils.showKeyBoard(getContext(), etConfirmationCode);
    }

    @OnClick(R.id.btn_confirm)
    void onConfirmClick() {
        presenter.onConfirmButtonClick(isRegistered);
    }

    @OnClick(R.id.tv_resend_sms)
    void onResendSmsClick() {
        presenter.onResendSmsClick();
    }

    @Override
    public void onDismiss(DialogInterface dialog) {
        presenter.onDismiss();
        super.onDismiss(dialog);
    }

    @Override
    public void showSmsInfoText(String phoneNumber) {
        tv_sms_info.setText(HtmlSourceUtils.fromHtml(getString(R.string.sms_with_confirmation_code_was_sent_to, phoneNumber)));
    }

    @Override
    public String getPhoneNumber() {
        return phoneNumber;
    }

    @Override
    public String getEmail() {
        return email;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public String getSurname() {
        return surname;
    }

    @Override
    public String getConfirmationCode() {
        return et_confirmation_code.getText().toString();
    }

    @Override
    public void showConfirmationCodeFieldError() {
        watcherConfirmationCode.showFieldError(getString(R.string.field_error));
    }

    @Override
    public void showProgressDialog() {
        if (progressDialog == null
                || !progressDialog.isShowing()) {
            progressDialog = ProgressDialog.show(getContext(), "", getString(R.string.loading), true, false);
        }
    }

    @Override
    public void dismissProgressDialog() {
        if (progressDialog != null && progressDialog.isShowing()) {
            progressDialog.dismiss();
        }
    }

    @Override
    public void saveRegistrationCompleteData(RegistrationComplete registrationComplete) {
        preferences.setUserDeviceToken(registrationComplete.getKey());
        preferences.setRegistrationCompleteId(registrationComplete.getId());
        preferences.setRegistrationCompleteType(registrationComplete.getType());
        preferences.setRegistrationCompleteUserId(registrationComplete.getUserId());
        preferences.setRegistrationCompleteRegDT(registrationComplete.getRegDT());
        if (!isRegistered) {
            FabricAnalytics.signUpEvent();
        }
    }

    @Override
    public void setRegistrationCompleted() {
        preferences.setRegistrationCompleted(true);
    }

    @Override
    public void closeDialog() {
        dismiss();
    }

    @Override
    public void openMainActivity() {
        Intent intent = MainActivity.getIntent(getContext());
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
    }

    @Override
    public void hideResendSmsButton() {
        tv_resend_sms.setVisibility(View.GONE);
    }

    @Override
    public void showResendSmsButton() {
        tv_resend_sms.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideTimeLeftView() {
        tv_time_left_to_show_resend_sms.setVisibility(View.GONE);
    }

    @Override
    public void showTimeLeftView() {
        tv_time_left_to_show_resend_sms.setVisibility(View.VISIBLE);
    }

    @Override
    public void updateTimeLeftToEnableResendSmsButton(String time) {
        tv_time_left_to_show_resend_sms.setText(
                getString(R.string.resend_in, time));
    }

    @Override
    public String getLanguage() {
        return preferences.getLanguage();
    }

    @Override
    public void handleAPIException(APIException e) {
        //do nothing
    }

    @Override
    public void handleConnectionTimeOutException(ConnectionTimeOutException e) {
        //do nothing
    }

    @Override
    public void handleUnknownException(UnknownException e) {
        //do nothing
    }

    @Override
    public void showErrorDialog(String message) {
        new AlertDialog.Builder(getContext())
                .setMessage(message)
                .setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                })
                .create()
                .show();
    }

    @Override
    public String getUnknownExceptionMessage() {
        return getString(R.string.server_error);
    }

    @Override
    public String getConnectionTimeOutExceptionMessage() {
        return getString(R.string.connection_error);
    }

    @Override
    public void handleNeedReAuthorizeException(NeedReAuthorizeException e) {

    }

    public class SmsBroadcastReceiver extends BroadcastReceiver {

        public static final String INTENT_ACTION_SMS_RECEIVED = "android.provider.Telephony.SMS_RECEIVED";

        @Override
        public void onReceive(Context context, Intent intent) {
            // Get the SMS message received
            if (intent != null
                    && intent.getAction().equals(INTENT_ACTION_SMS_RECEIVED)) {

                final Bundle bundle = intent.getExtras();
                try {
                    if (bundle != null) {
                        // A PDU is a "protocol data unit". This is the industrial standard for SMS message
                        final Object[] pdusObj = (Object[]) bundle.get("pdus");
                        if (pdusObj != null) {
                            for (Object item : pdusObj) {
                                // This will create an SmsMessage object from the received pdu
                                SmsMessage sms = SmsMessage.createFromPdu((byte[]) item);
                                String phoneNumber = sms.getDisplayOriginatingAddress();
                                String message = sms.getDisplayMessageBody();
                                Log.d(Constants.TAG, phoneNumber + ", " + message);
                            }
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }
}