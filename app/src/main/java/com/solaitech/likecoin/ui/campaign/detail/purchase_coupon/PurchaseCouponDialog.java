package com.solaitech.likecoin.ui.campaign.detail.purchase_coupon;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.util.List;

import com.solaitech.likecoin.R;
import com.solaitech.likecoin.data.models.coupons.Coupon;
import com.solaitech.likecoin.exceptions.APIException;
import com.solaitech.likecoin.exceptions.ConnectionTimeOutException;
import com.solaitech.likecoin.exceptions.NeedReAuthorizeException;
import com.solaitech.likecoin.exceptions.UnknownException;
import com.solaitech.likecoin.ui.base.BaseBottomSheetDialogFragment;
import com.solaitech.likecoin.ui.campaign.detail.purchase_coupon.count_view.OnPurchaseCountChangedListener;
import com.solaitech.likecoin.ui.campaign.detail.purchase_coupon.count_view.PurchaseCountView;

public class PurchaseCouponDialog extends BaseBottomSheetDialogFragment
        implements PurchaseCouponView, View.OnClickListener, OnPurchaseCountChangedListener {

    private String campaignId;
    private int couponPrice;
    private int maxCouponsCount;

    private TextView tvTotalPrice;
    private PurchaseCountView purchaseCountView;
    private Button btnPurchase;

    private PurchaseCouponPresenter presenter;

    private ProgressDialog progressDialog;

    private OnCouponsPurchasedListener onCouponsPurchasedListener;

    public static PurchaseCouponDialog newInstance(String campaignId, int couponPrice, int maxCouponsCount) {
        PurchaseCouponDialog d = new PurchaseCouponDialog();
        d.campaignId = campaignId;
        d.couponPrice = couponPrice;
        d.maxCouponsCount = maxCouponsCount;
        return d;
    }

    public void setOnCouponsPurchasedListener(OnCouponsPurchasedListener onCouponsPurchasedListener) {
        this.onCouponsPurchasedListener = onCouponsPurchasedListener;
    }

    @Override
    public void setupDialog(Dialog dialog, int style) {
        View v = View.inflate(getContext(), R.layout.dialog_purchase_coupon, null);
        dialog.setContentView(v);
        setBottomSheetCallback(v);
        initView(v);

        presenter = new PurchaseCouponPresenter(this, api);
        presenter.init();
    }

    private void initView(View v) {
        tvTotalPrice = (TextView) v.findViewById(R.id.tv_total_price);

        purchaseCountView = (PurchaseCountView) v.findViewById(R.id.v_purchase_count);
        purchaseCountView.setMaxCouponCount(maxCouponsCount);
        purchaseCountView.setOnPurchaseCountChangedListener(this);

        btnPurchase = (Button) v.findViewById(R.id.btn_purchase);
        btnPurchase.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_purchase:
                presenter.onPurchaseButtonClick();
                break;
        }
    }

    @Override
    public String getCampaignId() {
        return campaignId;
    }

    @Override
    public int getCouponsCount() {
        return purchaseCountView.getEnteredCount();
    }

    @Override
    public int getCouponPrice() {
        return couponPrice;
    }

    @Override
    public int getMaxCouponsCount() {
        return maxCouponsCount;
    }

    @Override
    public void showProgressDialog() {
        if (progressDialog == null
                || !progressDialog.isShowing()) {
            progressDialog = ProgressDialog.show(getContext(), "", getString(R.string.loading), true, false);
        }
    }

    @Override
    public void dismissProgressDialog() {
        if (progressDialog != null && progressDialog.isShowing()) {
            progressDialog.dismiss();
        }
    }

    @Override
    public void closeDialog() {
        dismiss();
    }

    @Override
    public void showTotalSum(String totalSum) {
        tvTotalPrice.setText(totalSum);
    }

    @Override
    public String getTotalSum() {
        return tvTotalPrice.getText().toString();
    }

    @Override
    public void showCouponsPurchasedDialog(String couponsQuantity, String totalSum, List<Coupon> coupons) {
        onCouponsPurchasedListener.onCouponsPurchased(couponsQuantity, totalSum, coupons);
    }

    @Override
    public void handleAPIException(APIException e) {
        //do nothing
    }

    @Override
    public void handleConnectionTimeOutException(ConnectionTimeOutException e) {
        //do nothing
    }

    @Override
    public void handleUnknownException(UnknownException e) {
        //do nothing
    }

    @Override
    public void showErrorDialog(String message) {
        new AlertDialog.Builder(getContext())
                .setMessage(message)
                .setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                })
                .create()
                .show();
    }

    @Override
    public String getUnknownExceptionMessage() {
        return getString(R.string.server_error);
    }

    @Override
    public String getConnectionTimeOutExceptionMessage() {
        return getString(R.string.connection_error);
    }

    @Override
    public void handleNeedReAuthorizeException(NeedReAuthorizeException e) {

    }

    @Override
    public void onDismiss(DialogInterface dialog) {
        presenter.onDismiss();
        super.onDismiss(dialog);
    }

    @Override
    public void onPurchaseCountChanged(int count) {
        presenter.onPurchaseCountChanged(count);
    }

}