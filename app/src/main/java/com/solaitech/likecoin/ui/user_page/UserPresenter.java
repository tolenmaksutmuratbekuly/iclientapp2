package com.solaitech.likecoin.ui.user_page;

import android.content.Context;

import com.solaitech.likecoin.data.models.campaign.Merchant;
import com.solaitech.likecoin.data.models.user.User;
import com.solaitech.likecoin.data.repository.user.UserRepositoryProvider;
import com.solaitech.likecoin.exceptions.APIException;
import com.solaitech.likecoin.exceptions.ConnectionTimeOutException;
import com.solaitech.likecoin.exceptions.NeedReAuthorizeException;
import com.solaitech.likecoin.exceptions.UnknownException;
import com.solaitech.likecoin.network.API;
import com.solaitech.likecoin.network.error.RetrofitErrorHandler;
import com.solaitech.likecoin.utils.StringUtils;

import rx.Subscriber;
import rx.Subscription;
import rx.functions.Action0;
import rx.subscriptions.CompositeSubscription;

class UserPresenter {
    private Context context;
    private UserView view;
    private API api;
    private CompositeSubscription compositeSubscription = new CompositeSubscription();
    private String title;

    UserPresenter(Context context, UserView view, API api, String userId, String title, boolean isUser) {
        this.context = context;
        this.view = view;
        this.api = api;
        this.title = title;

        if (isUser) getUserInfo(userId);
        else getMerchantInfo(userId);
    }

    private void getUserInfo(String userId) {
        Subscription subscription = UserRepositoryProvider.provideRepository(api)
                .getUser(userId)
                .doOnSubscribe(new Action0() {
                    @Override
                    public void call() {
                        view.showLoadingIndicator(true);
                    }
                })
                .doOnTerminate(new Action0() {
                    @Override
                    public void call() {
                        view.showLoadingIndicator(false);
                    }
                })
                .subscribe(new Subscriber<User>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        e.printStackTrace();
                        String errorText = "";
                        try {
                            RetrofitErrorHandler.handleException(e);
                        } catch (APIException e1) {
                            errorText = e1.getErrorDescr();
                        } catch (UnknownException e1) {
                            errorText = view.getUnknownExceptionMessage();
                        } catch (ConnectionTimeOutException e1) {
                            errorText = view.getConnectionTimeOutExceptionMessage();
                        } catch (NeedReAuthorizeException e1) {
                            e1.printStackTrace();
                        }

                        view.showError(StringUtils.replaceNull(errorText));
                    }

                    @Override
                    public void onNext(User user) {
                        setUserInfo(user);
                    }
                });
        compositeSubscription.add(subscription);
    }

    private void getMerchantInfo(String merchantId) {
        Subscription subscription = UserRepositoryProvider.provideRepository(api)
                .getMerchant(merchantId)
                .doOnSubscribe(new Action0() {
                    @Override
                    public void call() {
                        view.showLoadingIndicator(true);
                    }
                })
                .doOnTerminate(new Action0() {
                    @Override
                    public void call() {
                        view.showLoadingIndicator(false);
                    }
                })
                .subscribe(new Subscriber<Merchant>() {
                    @Override
                    public void onCompleted() {

                    }

                    @Override
                    public void onError(Throwable e) {
                        e.printStackTrace();
                        String errorText = "";
                        try {
                            RetrofitErrorHandler.handleException(e);
                        } catch (APIException e1) {
                            errorText = e1.getErrorDescr();
                        } catch (UnknownException e1) {
                            errorText = view.getUnknownExceptionMessage();
                        } catch (ConnectionTimeOutException e1) {
                            errorText = view.getConnectionTimeOutExceptionMessage();
                        } catch (NeedReAuthorizeException e1) {
                            e1.printStackTrace();
                        }

                        view.showError(StringUtils.replaceNull(errorText));
                    }

                    @Override
                    public void onNext(Merchant merchant) {
                        setMerchantInfo(merchant);
                    }
                });
        compositeSubscription.add(subscription);
    }

    public void onDestroyView() {
        unsubscribe();
    }

    private void unsubscribe() {
        if (compositeSubscription != null
                && !compositeSubscription.isUnsubscribed()) {
            compositeSubscription.unsubscribe();
        }
    }

    private void setUserInfo(User user) {
        if (user == null)
            return;

        view.setUser(user);
        if (user.getAmbassadorRoles() != null && user.getAmbassadorRoles().size() > 0) {
            view.setAmbassadorRoles(user.getAmbassadorRoles());
        }

        if (user.getUserLevel() != null
                && user.getUserLevel().getLevel() != null) {
            view.showUserLevel(user.getUserLevel().getLevel());
        } else {
            view.showUserLevel(0);
        }

        if (user.getAvatarUrl() != null) {
            view.showProfileImage(user.getAvatarUrl());
        }

        if (user.getName() != null) {
            view.showUserName(user.getName());
        } else {
            view.showUserName(StringUtils.EMPTY_STRING);
        }

        if (user.getSurname() != null) {
            view.showSurname(user.getSurname());
        } else {
            view.showSurname(StringUtils.EMPTY_STRING);
        }
    }

    private void setMerchantInfo(Merchant merchant) {
        if (merchant == null)
            return;

        if (merchant.getLogoUrl() != null) {
            view.showProfileImage(merchant.getLogoUrl());
        }

        if (title != null) {
            view.showUserName(title);
        } else {
            view.showUserName(StringUtils.EMPTY_STRING);
        }

        if (StringUtils.length(merchant.getShortDescr()) > 0) {
            view.showShortDescription(merchant.getShortDescr());
        } else {
            view.showShortDescription(StringUtils.EMPTY_STRING);
        }

        if (StringUtils.length(merchant.getUrl()) > 0) {
            view.showUrl(merchant.getUrl());
        } else {
            view.showUrl(StringUtils.EMPTY_STRING);
        }
    }
}