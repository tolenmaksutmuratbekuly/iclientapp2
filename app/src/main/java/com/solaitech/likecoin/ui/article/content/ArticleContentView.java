package com.solaitech.likecoin.ui.article.content;

import android.content.Context;

import com.solaitech.likecoin.data.models.article.Article;

import java.util.List;

interface ArticleContentView {

    Article getArticleDetail();

    void showTitle(String title);

    void showShortDescription(String description);

    void showContent(String content);

    void showDate(String regDate);

    void hideTitle();

    void hideShortDescription();

    void hideContent();

    void hideDate();

    Context getContextFromPage();
}