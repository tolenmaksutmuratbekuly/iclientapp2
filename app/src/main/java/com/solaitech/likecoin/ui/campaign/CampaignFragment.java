package com.solaitech.likecoin.ui.campaign;

import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.solaitech.likecoin.R;
import com.solaitech.likecoin.data.models.campaign.Campaign;
import com.solaitech.likecoin.ui.base.recycler_view.RecyclerViewBaseFragment;
import com.solaitech.likecoin.ui.campaign.detail.CampaignDetailActivity;
import com.solaitech.likecoin.ui.menu.BottomNavigationTabFragment;
import com.solaitech.likecoin.ui.menu.Search;

import java.util.ArrayList;
import java.util.List;

import static com.solaitech.likecoin.utils.StringUtils.length;

public class CampaignFragment extends RecyclerViewBaseFragment implements CampaignView, BottomNavigationTabFragment, Search {
    private CampaignPresenter presenter;
    private Callback callback;
    private String tagId;

    public static CampaignFragment newInstance(String tagId, List<Campaign> campaigns) {
        CampaignFragment fragment = new CampaignFragment();
        Bundle data = new Bundle();
        data.putString("tagId", tagId);
        data.putParcelableArrayList("campaigns", (ArrayList<? extends Parcelable>) campaigns);
        fragment.setArguments(data);
        return fragment;
    }

    public void setCallback(Callback callback) {
        this.callback = callback;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        handleBundle();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = super.onCreateView(inflater, container, savedInstanceState);
        initView(v);
        presenter = new CampaignPresenter(this, api);
        if (length(tagId) == 0) {
            presenter.onSwipeRefresh(tagId);
        }
        return v;
    }

    private void handleBundle() {
        Bundle bundle = getArguments();
        if (bundle != null) {
            ArrayList<? extends Campaign> campaigns = bundle.getParcelableArrayList("campaigns");
            tagId = bundle.getString("tagId");
            if (campaigns != null) {
                updateData(campaigns);
            }
        }
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.template_recycler_view_with_swipe_refresh_layout;
    }

    private void initView(View v) {
        initRecyclerView(v);
        initSwipeRefreshLayout(v);
    }

    private void updateData(List<?> campaigns) {
        objects.clear();
        objects.addAll(campaigns);

        objectsFilter.clear();
        if (!campaigns.isEmpty()) {
            objectsFilter.add(CampaignAdapter.VIEW_TYPE_FIRST_ELEMENT_EMPTY_SPACE);
        }
        objectsFilter.addAll(objects);
    }

    @Override
    protected void tryUploadDataAgain() {
        presenter.onTryToUploadDataAgainClick(tagId);
    }

    @Override
    protected void onSwipeRefresh() {
        super.onSwipeRefresh();
        presenter.onSwipeRefresh(tagId);
    }

    @Override
    public void openCampaignDetailActivity(String id, boolean openCommentsTab) {
        startActivity(CampaignDetailActivity.getIntent(getContext(), id, openCommentsTab));
    }

    @Override
    public void updateAdapterData(List<? extends Object> campaigns) {
        updateData(campaigns);
        if (callback != null) {
            callback.setNewCampaignList(objectsFilter);
        }
        adapter.notifyDataSetChanged();
    }

    @Override
    protected RecyclerView.LayoutManager getLayoutManager() {
        return getLinearLayoutManager();
    }

    @Override
    protected RecyclerView.Adapter getAdapter() {
        adapter = new CampaignAdapter(getContext(), objectsFilter);
        ((CampaignAdapter) adapter).setOnRecyclerViewItemClickListener(this);
        return adapter;
    }

    @Override
    public void onItemClick(Object object) {
        super.onItemClick(object);
        presenter.onItemClick(object);
    }

    public void scrollToPosition(int position) {
        if (objectsFilter.size() > 0) {
            ((LinearLayoutManager) recyclerView.getLayoutManager()).scrollToPositionWithOffset(position, 0);
        }
    }

    @Override
    public void onBottomNavigationTabReselected() {
        scrollToPosition(0);
    }

    @Override
    public void doSearch(String text) {
        presenter.onSearch(text);
    }

    @Override
    public void showAllListItems() {
        presenter.onSearchShowAllListItems();
    }

    @Override
    public List<Object> getObjects() {
        return objects;
    }

    @Override
    public void clearFilterObjects() {
        objectsFilter.clear();
        adapter.notifyDataSetChanged();
    }

    @Override
    public void onSearchObjectFound(Campaign campaign) {
        if (objectsFilter.isEmpty()) {
            objectsFilter.add(CampaignAdapter.VIEW_TYPE_FIRST_ELEMENT_EMPTY_SPACE);
        }
        objectsFilter.add(campaign);
        adapter.notifyItemInserted(objectsFilter.size());
    }

    interface Callback {
        void setNewCampaignList(List<Object> campaignList);
    }
}