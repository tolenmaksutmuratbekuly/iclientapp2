package com.solaitech.likecoin.ui.base.loading;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.solaitech.likecoin.R;
import com.solaitech.likecoin.data.preferences.PreferencesImpl;
import com.solaitech.likecoin.exceptions.APIException;
import com.solaitech.likecoin.exceptions.ConnectionTimeOutException;
import com.solaitech.likecoin.exceptions.NeedReAuthorizeException;
import com.solaitech.likecoin.exceptions.UnknownException;
import com.solaitech.likecoin.ui.base.BaseFragment;
import com.solaitech.likecoin.ui.base.interfaces.MVPBaseView;
import com.solaitech.likecoin.ui.base.loading.views.TemplateFailView;
import com.solaitech.likecoin.ui.base.loading.views.TemplateProgressView;
import com.solaitech.likecoin.ui.registration.start.RegistrationStartActivity;
import com.solaitech.likecoin.utils.animation.CustomAnimationUtils;

public abstract class LoadingBaseFragment extends BaseFragment
        implements MVPBaseView, TemplateFailView.OnTryAgainClickListener {

    private ViewGroup vgSuccess;
    private LoadingBase loadingBase;
    private PreferencesImpl preferencesLogOut;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        preferencesLogOut = new PreferencesImpl(getContext());
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.frgm_loading_base, container, false);
        initView(v);
        inflate();
        return v;
    }

    private void initView(View v) {
        ViewGroup vgContent = (ViewGroup) v.findViewById(R.id.vg_content);
        CustomAnimationUtils.enableTransition(vgContent);

        vgSuccess = (ViewGroup) v.findViewById(R.id.vg_success);

        TemplateProgressView vProgress = (TemplateProgressView) v.findViewById(R.id.v_template_progress);

        TemplateFailView vFail = (TemplateFailView) v.findViewById(R.id.v_template_fail);
        vFail.setOnTryAgainClickListener(this);

        loadingBase = new LoadingBase(getContext(), this, vgSuccess, vFail, vProgress);

        showSuccessTemplateView();
    }

    private View inflate() {
        LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        return inflater.inflate(getLayoutResourceId(), vgSuccess);
    }

    protected abstract int getLayoutResourceId();

    @Override
    public void showProgressTemplateView() {
        loadingBase.showProgressTemplateView();
    }

    @Override
    public void showProgressTemplateView(String title) {
        loadingBase.showProgressTemplateView(title);
    }

    @Override
    public void showSuccessTemplateView() {
        loadingBase.showSuccessTemplateView();
    }

    @Override
    public void showFailTemplateView() {
        loadingBase.showFailTemplateView();
    }

    @Override
    public void showFailTemplateView(String message) {
        loadingBase.showFailTemplateView(message);
    }

    @Override
    public void onTryAgainClick() {
        tryUploadDataAgain();
    }

    protected abstract void tryUploadDataAgain();

    @Override
    public void showProgressDialog() {
        loadingBase.showProgressDialog();
    }

    @Override
    public void dismissProgressDialog() {
        loadingBase.dismissProgressDialog();
    }

    @Override
    public void handleAPIException(APIException e) {
        loadingBase.handleAPIException(e);
    }

    @Override
    public void handleConnectionTimeOutException(ConnectionTimeOutException e) {
        loadingBase.handleConnectionTimeOutException(e);
    }

    @Override
    public void handleUnknownException(UnknownException e) {
        loadingBase.handleUnknownException(e);
    }

    @Override
    public void showErrorDialog(String message) {
        loadingBase.showErrorDialog(message);
    }

    @Override
    public String getUnknownExceptionMessage() {
        return loadingBase.getUnknownExceptionMessage();
    }

    @Override
    public String getConnectionTimeOutExceptionMessage() {
        return loadingBase.getConnectionTimeOutExceptionMessage();
    }

    @Override
    public void handleNeedReAuthorizeException(NeedReAuthorizeException e) {
        preferencesLogOut.clearAllPreferences();
        startActivity(RegistrationStartActivity.newLogoutIntent(getContext()));
    }
}