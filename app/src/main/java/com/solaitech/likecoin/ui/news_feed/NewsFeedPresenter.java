package com.solaitech.likecoin.ui.news_feed;

import com.solaitech.likecoin.BuildConfig;
import com.solaitech.likecoin.data.models.common_info.IcoCondition;
import com.solaitech.likecoin.data.models.enums.ListDirectionEnums;
import com.solaitech.likecoin.data.models.enums.NewsSourceEntityRefTypeEnums;
import com.solaitech.likecoin.data.models.news_feed.NewsFeed;
import com.solaitech.likecoin.data.models.news_feed.NewsOwner;
import com.solaitech.likecoin.data.params.BuyArticleParams;
import com.solaitech.likecoin.data.params.FavoriteParams;
import com.solaitech.likecoin.data.params.Like4EntityParams;
import com.solaitech.likecoin.data.params.contacts.LikeSendingByIdParams;
import com.solaitech.likecoin.data.repository.company_details.CompanyDetailsRepositoryProvider;
import com.solaitech.likecoin.data.repository.like.LikesRepositoryProvider;
import com.solaitech.likecoin.data.repository.news_feed.NewsFeedRepositoryProvider;
import com.solaitech.likecoin.network.API;
import com.solaitech.likecoin.ui.base.PresenterErrorHandler;
import com.solaitech.likecoin.utils.Logger;
import com.solaitech.likecoin.utils.StringUtils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import okhttp3.ResponseBody;
import rx.Subscriber;
import rx.Subscription;
import rx.functions.Action0;
import rx.subscriptions.CompositeSubscription;

class NewsFeedPresenter {
    private static final int NEWS_FEED_ROW_COUNT = 30;
    private NewsFeedView view;
    private API api;

    private CompositeSubscription compositeSubscription = new CompositeSubscription();
    private CompositeSubscription sendLikeSubscription = new CompositeSubscription();
    private Subscription searchSubscription;

    NewsFeedPresenter(NewsFeedView view, API api) {
        this.view = view;
        this.api = api;

        icoCondition();
    }

    public void init() {
        view.disableSwipeToRefresh();
        resetPaginateValues();

        //при swipeRefresh препятствовать вызову методов onLoadMore и onLoadMoreUp
        view.setIsLoading(true);
        view.setIsLoadingUp(true);

        Subscription subscription = NewsFeedRepositoryProvider
                .provideRepository(api)
                .getNewsFeed(view.getNewsFeedLastVisibleItemId(), NEWS_FEED_ROW_COUNT, ListDirectionEnums.ASCENDING_DESCENDING)
                .doOnSubscribe(new Action0() {
                    @Override
                    public void call() {
                        view.showProgressTemplateView();
                    }
                })
                .subscribe(new Subscriber<List<NewsFeed>>() {
                    @Override
                    public void onCompleted() {
                        view.showSuccessTemplateView();
                    }

                    @Override
                    public void onError(Throwable e) {
                        PresenterErrorHandler.handleError(e, view);
                    }

                    @Override
                    public void onNext(List<NewsFeed> newsFeeds) {
                        if (newsFeeds == null) {
                            newsFeeds = new ArrayList<>();
                        }

                        Collections.sort(newsFeeds);

                        if (!newsFeeds.isEmpty()) {
                            view.setBaseIdUp(newsFeeds.get(0).getId());
                            view.setBaseId(newsFeeds.get(newsFeeds.size() - 1).getId());
                        }

                        view.updateAdapterData(newsFeeds);

                        int position = 0;

                        for (int i = 0; i < newsFeeds.size(); i++) {
                            NewsFeed newsFeed = newsFeeds.get(i);
                            if (newsFeed.getId().equalsIgnoreCase(view.getNewsFeedLastVisibleItemId())) {
                                position = i;
                                break;
                            }
                        }

                        position = position + 1; //the first element is VIEW_TYPE_FIRST_ELEMENT_EMPTY_SPACE
                        if (!newsFeeds.isEmpty()) {
                            view.scrollToPosition(position);
                        }

                        if (!newsFeeds.isEmpty()) {
                            view.setIsLoading(false);
                            view.setIsLoadingUp(false);
                            view.setupPagination();
                        }
                    }
                });

        compositeSubscription.add(subscription);
    }

    private void resetPaginateValues() {
        view.setBaseId("");
        view.setHasLoadedAllItems(false);
        view.setIsLoading(false);

        view.setBaseIdUp("");
        view.setHasLoadedAllItemsUp(false);
        view.setIsLoadingUp(false);
    }

    public void onTryToUploadDataAgainClick() {
        init();
    }

    public void onSwipeRefresh() {
        view.setNewsFeedLastVisibleItemId("");
        init();
    }

    public void onBottomNavigationTabReselected() {
        //do nothing
    }

    void onLoadMore() {
        Logger.e(getClass(), "onLoadMore");
        Subscription subscription = NewsFeedRepositoryProvider
                .provideRepository(api)
                .getNewsFeed(view.getBaseId(), NEWS_FEED_ROW_COUNT, ListDirectionEnums.DESCENDING)
                .doOnSubscribe(new Action0() {
                    @Override
                    public void call() {
                        view.setIsLoading(true);
                    }
                })
                .doOnTerminate(new Action0() {
                    @Override
                    public void call() {
                        view.setIsLoading(false);
                    }
                })
                .subscribe(new Subscriber<List<NewsFeed>>() {
                    @Override
                    public void onCompleted() {
                        //do nothing
                        Logger.e(getClass(), "onCompleted");
                    }

                    @Override
                    public void onError(Throwable e) {
                        e.printStackTrace();
                        view.showToastQueryError();
                        view.setHasMoreDataToLoad(false);
                    }

                    @Override
                    public void onNext(List<NewsFeed> newsFeeds) {
                        if (newsFeeds == null) {
                            newsFeeds = new ArrayList<>();
                        }

                        Collections.sort(newsFeeds);

                        if (!newsFeeds.isEmpty()) {
                            view.setBaseId(newsFeeds.get(newsFeeds.size() - 1).getId());
                        }

                        if (newsFeeds.size() < NEWS_FEED_ROW_COUNT) {
                            view.setHasLoadedAllItems(true);
                            view.setHasMoreDataToLoad(false);
                        }

                        view.addAdapterData(newsFeeds);
                    }
                });
        compositeSubscription.add(subscription);
    }

    void onLoadMoreUp() {
        Logger.e(getClass(), "onLoadMoreUp");
        Subscription subscription = NewsFeedRepositoryProvider
                .provideRepository(api)
                .getNewsFeed(view.getBaseIdUp(), NEWS_FEED_ROW_COUNT, ListDirectionEnums.ASCENDING)
                .doOnSubscribe(new Action0() {
                    @Override
                    public void call() {
                        view.setIsLoadingUp(true);
                    }
                })
                .doOnTerminate(new Action0() {
                    @Override
                    public void call() {
                        view.setIsLoadingUp(false);
                    }
                })
                .subscribe(new Subscriber<List<NewsFeed>>() {
                    @Override
                    public void onCompleted() {
                        Logger.e(getClass(), "onCompleted");
                    }

                    @Override
                    public void onError(Throwable e) {
                        e.printStackTrace();
                        view.showToastQueryError();
                        view.setHasMoreDataToLoadUp(false);
                    }

                    @Override
                    public void onNext(List<NewsFeed> newsFeeds) {
                        if (newsFeeds == null) {
                            newsFeeds = new ArrayList<>();
                        }

                        Collections.sort(newsFeeds);

                        if (!newsFeeds.isEmpty()) {
                            view.setBaseIdUp(newsFeeds.get(0).getId());
                        }

                        if (newsFeeds.size() < NEWS_FEED_ROW_COUNT) {
                            view.setHasLoadedAllItemsUp(true);
                            view.setHasMoreDataToLoadUp(false);
                            view.enableSwipeToRefresh();
                        }

                        view.addAdapterDataUp(newsFeeds);

                        view.scrollToPosition(newsFeeds.size());
                    }
                });
        compositeSubscription.add(subscription);
    }

    void onSearch(final String query) {
        if (searchSubscription != null
                && !searchSubscription.isUnsubscribed()) {
            searchSubscription.unsubscribe();
        }

        searchSubscription =
                NewsFeedRepositoryProvider
                        .provideRepository(api)
                        .search(view.getObjects(), query)
                        .doOnSubscribe(new Action0() {
                            @Override
                            public void call() {
                                view.unbindPagination();
                                view.clearFilterObjects();
                                view.disableSwipeToRefresh();
                            }
                        })
                        .subscribe(new Subscriber<NewsFeed>() {
                            @Override
                            public void onCompleted() {
                                //do nothing
                            }

                            @Override
                            public void onError(Throwable e) {
                                if (BuildConfig.IS_DEBUG) {
                                    e.printStackTrace();
                                }
                            }

                            @Override
                            public void onNext(NewsFeed newsFeed) {
                                view.onSearchObjectFound(newsFeed);
                            }
                        });
        compositeSubscription.add(searchSubscription);
    }

    void onSearchShowAllListItems() {
        List<Object> objects = new ArrayList<>(view.getObjects());
        view.updateAdapterData(objects);

        if (!objects.isEmpty()
                && (!view.hasLoadedAllItems()
                || !view.hasLoadedAllItemsUp())) {
            view.setupPagination();

            if (view.hasLoadedAllItemsUp()) {
                view.enableSwipeToRefresh();
            }
        }
    }

    public void onItemClick(Object object) {
        if (object instanceof NewsFeed) {
            NewsFeed newsFeed = (NewsFeed) object;
            Logger.e(getClass(), "id == " + newsFeed.getId());
            openDetailPage(newsFeed, false);
        }
    }

    void openDetailPage(NewsFeed newsFeed, boolean openCommentsTab) {
        String type = null;
        if (newsFeed.getSourceEntityRef() != null && newsFeed.getSourceEntityRef().getType() != null) {
            type = newsFeed.getSourceEntityRef().getType().getId();
        }

        if (type != null) {
            switch (type) {
                case NewsSourceEntityRefTypeEnums.CAMPAIGN:
                    if (newsFeed.getSourceEntityRef().getId() == null) {
                        return;
                    }
                    view.openCampaignDetailActivity(newsFeed.getSourceEntityRef().getId(), openCommentsTab);
                    break;
                case NewsSourceEntityRefTypeEnums.TRANSACTION:
                    view.openUserStatementActivity();
                    break;
                case NewsSourceEntityRefTypeEnums.ARTICLE:
                    if (newsFeed.getSourceEntityRef().getId() == null) {
                        return;
                    }
                    view.openArticleDetailActivity(newsFeed.getSourceEntityRef().getId(), openCommentsTab);
                    break;
                case NewsSourceEntityRefTypeEnums.SYS_MESSAGE:
                    if (newsFeed.getSourceEntityRef().getId() == null) {
                        return;
                    }
                    view.openMsgDetailActivity(newsFeed.getSourceEntityRef().getId(), type, openCommentsTab);
                    break;
                case NewsSourceEntityRefTypeEnums.MERCHANT_MESSAGE:
                    if (newsFeed.getSourceEntityRef().getId() == null) {
                        return;
                    }
                    view.openMsgDetailActivity(newsFeed.getSourceEntityRef().getId(), type, openCommentsTab);
                    break;

            }
        }
    }

    public void onDestroyView() {
        int firstVisibleItemPosition = view.getFirstVisibleItemPosition();
        List<Object> objects = view.getFilterObjects();

        if (firstVisibleItemPosition != -1
                && objects.size() > firstVisibleItemPosition) {

            String newsFeedId = null;

            if (objects.get(firstVisibleItemPosition) instanceof NewsFeed) {
                NewsFeed newsFeed = (NewsFeed) objects.get(firstVisibleItemPosition);
                newsFeedId = newsFeed.getId();

            } else {
                for (int i = firstVisibleItemPosition - 1; i >= 0; i--) {

                    if (objects.get(i) instanceof NewsFeed) {
                        NewsFeed newsFeed = (NewsFeed) objects.get(i);
                        newsFeedId = newsFeed.getId();
                        break;
                    }
                }
            }
            view.saveNewsFeedLastVisibleItemId(newsFeedId);
        }

        unsubscribe();
        view.unbindPagination();
    }


    private void unsubscribe() {
        if (compositeSubscription != null
                && !compositeSubscription.isUnsubscribed()) {
            compositeSubscription.unsubscribe();
        }
    }

    ////////////////////////////////////////////////
    /// ICO Condition
    ////////////////////////////////////////////////

    private void icoCondition() {
        Subscription subscription = CompanyDetailsRepositoryProvider
                .provideRepository(api)
                .getIcoCondition()
                .subscribe(new Subscriber<IcoCondition>() {
                    @Override
                    public void onCompleted() {
                    }

                    @Override
                    public void onError(Throwable e) {
                        PresenterErrorHandler.handleError2(e, view);
                    }

                    @Override
                    public void onNext(IcoCondition icoCondition) {
                        view.showIcoContainer(icoCondition.isIco());
                    }
                });

        compositeSubscription.add(subscription);
    }

    ////////////////////////////////////////////////
    /// Send Like                                ///
    ////////////////////////////////////////////////
    void onSendButtonClick(NewsOwner owner) {
        if (owner != null) {
            sendLike(new LikeSendingByIdParams(owner.getId(), 1, StringUtils.EMPTY_STRING));
        }
    }

    private void sendLike(LikeSendingByIdParams params) {
        Subscription subscription = LikesRepositoryProvider
                .provideRepository(api)
                .sendLike(params)
                .doOnSubscribe(new Action0() {
                    @Override
                    public void call() {
                        view.showPbDialog();
                    }
                })
                .doOnTerminate(new Action0() {
                    @Override
                    public void call() {
                        view.dismissPbDialog();
                    }
                })
                .subscribe(new Subscriber<ResponseBody>() {
                    @Override
                    public void onCompleted() {
                        view.showSuccessToastMessage();
                        view.sendBroadcast();
                    }

                    @Override
                    public void onError(Throwable e) {
                        PresenterErrorHandler.handleError2(e, view);
                    }

                    @Override
                    public void onNext(ResponseBody responseBody) {
                        //do nothing
                    }
                });

        sendLikeSubscription.add(subscription);
    }

    void onArticleBuyClick(String articleId) {
        if (articleId != null) {
            buyArticle(new BuyArticleParams(articleId));
        }
    }

    private void buyArticle(BuyArticleParams params) {
        Subscription subscription = LikesRepositoryProvider
                .provideRepository(api)
                .buyArticle(params)
                .doOnSubscribe(new Action0() {
                    @Override
                    public void call() {
                        view.showPbDialog();
                    }
                })
                .doOnTerminate(new Action0() {
                    @Override
                    public void call() {
                        view.dismissPbDialog();
                    }
                })
                .subscribe(new Subscriber<ResponseBody>() {
                    @Override
                    public void onCompleted() {
                        view.showSuccessToastMessage();
                        view.sendBroadcast();
                    }

                    @Override
                    public void onError(Throwable e) {
                        PresenterErrorHandler.handleError2(e, view);
                    }

                    @Override
                    public void onNext(ResponseBody responseBody) {
                        view.onArticleBought();
                    }
                });

        sendLikeSubscription.add(subscription);
    }

    ///////////////////////////////////////////////////////////////////////////////////////
    /// Like action for: System msg, comment, comment reply, merchant msg (like4Entity) ///
    /////////////////////////////////////////////////////////////////////////////////////
    void onLike4Entity(String entityId, String entityTypeId, int position) {
        if (entityId != null && entityTypeId != null) {
            like4Entity(new Like4EntityParams(entityId, entityTypeId), position);
        }
    }

    private void like4Entity(Like4EntityParams params, final int position) {
        Subscription subscription = LikesRepositoryProvider
                .provideRepository(api)
                .like4EntityNews(params)
                .doOnSubscribe(new Action0() {
                    @Override
                    public void call() {
                        view.showPbDialog();
                    }
                })
                .doOnTerminate(new Action0() {
                    @Override
                    public void call() {
                        view.dismissPbDialog();
                    }
                })
                .subscribe(new Subscriber<NewsFeed>() {
                    @Override
                    public void onCompleted() {
                    }

                    @Override
                    public void onError(Throwable e) {
                        PresenterErrorHandler.handleError2(e, view);
                    }

                    @Override
                    public void onNext(NewsFeed newsFeed) {
                        view.onLiked4EntitySuccess(newsFeed, position);
                        view.sendBroadcast();
                    }
                });

        sendLikeSubscription.add(subscription);
    }

    ////////////////////////////////////////////////
    /// Favorites                                ///
    ////////////////////////////////////////////////
    void onFavoritesClick(String newsFeedId, int isFavorite) {
        favoriteState(newsFeedId, isFavorite);
    }

    private void favoriteState(String newsFeedId, int isFavorite) {
        Subscription subscription = LikesRepositoryProvider
                .provideRepository(api)
                .favoriteState(newsFeedId, new FavoriteParams(isFavorite))
                .doOnSubscribe(new Action0() {
                    @Override
                    public void call() {
                        view.showPbDialog();
                    }
                })
                .doOnTerminate(new Action0() {
                    @Override
                    public void call() {
                        view.dismissPbDialog();
                    }
                })
                .subscribe(new Subscriber<NewsFeed>() {
                    @Override
                    public void onCompleted() {
                    }

                    @Override
                    public void onError(Throwable e) {
                        PresenterErrorHandler.handleError2(e, view);
                    }

                    @Override
                    public void onNext(NewsFeed newsFeed) {
                        view.onFavoriteState(newsFeed);
                    }
                });

        sendLikeSubscription.add(subscription);
    }

    public void onAvatarClick(String userId) {
        String localUserId = view.getLocalUserId();
        view.openUserActivity(userId,
                localUserId != null && !userId.equals(localUserId));
    }
}