package com.solaitech.likecoin.ui.campaign.detail.purchase_coupon;

import java.util.List;

import com.solaitech.likecoin.data.models.coupons.Coupon;
import com.solaitech.likecoin.ui.base.interfaces.MVPBaseErrorView;

interface PurchaseCouponView extends MVPBaseErrorView {

    void showProgressDialog();

    void dismissProgressDialog();

    String getCampaignId();

    int getCouponsCount();

    int getCouponPrice();

    int getMaxCouponsCount();

    void showTotalSum(String totalSum);

    String getTotalSum();

    void showCouponsPurchasedDialog(String couponsQuantity, String totalSum, List<Coupon> coupons);

    void closeDialog();

}