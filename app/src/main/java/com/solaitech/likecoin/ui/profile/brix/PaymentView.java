package com.solaitech.likecoin.ui.profile.brix;

import com.solaitech.likecoin.data.models.user.User;

import java.util.Collection;
import java.util.Map;

interface PaymentView {

    User getUser();

    void showLoadingIndicator(boolean show);

    void postDataInWebView(Collection<Map.Entry<String, String>> postData);
}