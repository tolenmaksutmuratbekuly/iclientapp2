package com.solaitech.likecoin.ui.article;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.solaitech.likecoin.R;
import com.solaitech.likecoin.data.models.article.Article;
import com.solaitech.likecoin.data.models.campaign.Merchant;
import com.solaitech.likecoin.data.models.enums.AppBarLayoutStateEnums;
import com.solaitech.likecoin.data.models.enums.BroadcastEventEnums;
import com.solaitech.likecoin.data.models.enums.CommentsEntityTypeEnums;
import com.solaitech.likecoin.interfaces.OnFragmentViewCreatedListener;
import com.solaitech.likecoin.ui.article.about_author.AboutAuthorFragment;
import com.solaitech.likecoin.ui.article.content.ArticleContentFragment;
import com.solaitech.likecoin.ui.base.loading.LoadingBaseActivity;
import com.solaitech.likecoin.ui.base.view_pager.BaseFragmentPagerAdapter;
import com.solaitech.likecoin.ui.campaign.detail.comments.CommentsFragment;
import com.solaitech.likecoin.utils.CustomColorUtils;
import com.solaitech.likecoin.utils.ImageUtils;
import com.solaitech.likecoin.utils.StringUtils;

import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.solaitech.likecoin.utils.KeyboardUtils.hideSoftKeyboard;

public class ArticleDetailActivity extends LoadingBaseActivity implements
        ArticleDetailView, OnFragmentViewCreatedListener {

    private String id;
    private boolean openComments;
    private int appBarLayoutState;
    private List<Fragment> viewPagerPages = new ArrayList<>();
    private boolean isBought;

    @Bind(R.id.app_bar_layout) AppBarLayout app_bar_layout;
    @Bind(R.id.collapsing_toolbar) CollapsingToolbarLayout collapsing_toolbar;
    @Bind(R.id.iv_image_large) ImageView iv_image_large;
    @Bind(R.id.toolbar) Toolbar toolbar;

    @Bind(R.id.tab_layout) TabLayout tab_layout;
    @Bind(R.id.view_pager) ViewPager view_pager;
    @Bind(R.id.vg_buy_coupon) ViewGroup vg_buy_coupon;
    @Bind(R.id.btn_buy_coupon) Button btn_buy_coupon;
    @Bind(R.id.ll_comment_container) LinearLayout ll_comment_container;
    @Bind(R.id.et_comment) EditText et_comment;

    private ArticleDetailPresenter presenter;

    public static Intent getIntent(Context context, String id, boolean openCommentsTab) {
        Bundle bundle = new Bundle();
        bundle.putString("id", id);
        bundle.putBoolean("openComments", openCommentsTab);

        Intent intent = new Intent(context, ArticleDetailActivity.class);
        intent.putExtras(bundle);
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_article_detail);
        ButterKnife.bind(this);
        handleIntent(getIntent());
        initView();

        presenter = new ArticleDetailPresenter(this, api);
        presenter.init();
    }

    private void handleIntent(Intent intent) {
        if (intent != null && intent.getExtras() != null) {
            Bundle bundle = intent.getExtras();
            id = bundle.getString("id");
            openComments = bundle.getBoolean("openComments");
        }
    }

    private void initView() {
        collapsing_toolbar.setCollapsedTitleTextColor(CustomColorUtils.getColor(this, R.color.white));
        collapsing_toolbar.setExpandedTitleColor(CustomColorUtils.getColor(this, R.color.transparent));
        app_bar_layout.addOnOffsetChangedListener(new AppBarLayout.OnOffsetChangedListener() {
            @Override
            public void onOffsetChanged(AppBarLayout appBarLayout, int verticalOffset) {
                if (Math.abs(verticalOffset) == appBarLayout.getTotalScrollRange()) {
                    // Collapsed
                    appBarLayoutState = AppBarLayoutStateEnums.COLLAPSED;
                } else if (verticalOffset == 0) {
                    // Expanded
                    appBarLayoutState = AppBarLayoutStateEnums.EXPANDED;
                } else {
                    // Somewhere in between
                    appBarLayoutState = AppBarLayoutStateEnums.IN_BETWEEN;
                }
            }
        });

        app_bar_layout.setExpanded(false, false);
        initToolbar();
        initViewPager();

        initContainerView(R.id.coordinator_layout);
        initLoadingView();
    }

    private void initToolbar() {
        setSupportActionBar(toolbar);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        if (getSupportActionBar() != null) getSupportActionBar().setTitle("");
        toolbar.setTitle(StringUtils.EMPTY_STRING);
    }

    private void initViewPager() {
        CommentsFragment commentsFragment = CommentsFragment.newInstance(id, CommentsEntityTypeEnums.ARTICLE);
        viewPagerPages.clear();
        viewPagerPages.add(ArticleContentFragment.newInstance());
        viewPagerPages.add(AboutAuthorFragment.newInstance());

        commentsFragment.setCallback(new CommentsFragment.Callback() {
            @Override
            public void onSentComment() {
                et_comment.setText(StringUtils.EMPTY_STRING);
                hideSoftKeyboard(et_comment);
            }
        });
        viewPagerPages.add(commentsFragment);

        PagerAdapter pagerAdapter = new BaseFragmentPagerAdapter(getSupportFragmentManager()) {
            @Override
            public CharSequence getPageTitle(int position) {
                switch (position) {
                    case 0:
                        return getString(R.string.article);
                    case 1:
                        return getString(R.string.author);
                    case 2:
                        return getString(R.string.comments);
                    default:
                        return "";
                }
            }

            @Override
            public Fragment getItem(int position) {
                return viewPagerPages.get(position);
            }

            @Override
            public int getCount() {
                return viewPagerPages.size();
            }
        };
        view_pager.setOffscreenPageLimit(3);
        view_pager.setAdapter(pagerAdapter);
        view_pager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                if (position == 2) {
                    ll_comment_container.setVisibility(View.VISIBLE);
                    hideBuyArticleView();
                } else {
                    showBuyArticleView();
                    ll_comment_container.setVisibility(View.GONE);
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
        tab_layout.setupWithViewPager(view_pager);
    }

    @OnClick(R.id.btn_buy_coupon)
    void onBuyArticleClick() {
        presenter.onBuyArticleClick();
    }

    // Adding comment
    @OnClick(R.id.ll_send_comment)
    void onClickSendComment() {
        if (viewPagerPages.get(2) instanceof CommentsFragment) {
            ((CommentsFragment) viewPagerPages.get(2)).onClickSendComment(et_comment.getText().toString());
        }
    }

    @Override
    public void onFragmentViewCreated() {
        for (Fragment f : viewPagerPages) {
            if (f.getView() == null) {
                return;
            }
        }
        presenter.onAllViewPagerPagesInitialized();
    }

    @Override
    protected void tryUploadDataAgain() {
        presenter.onTryUploadDataAgainClick();
    }

    @Override
    public String getArticleId() {
        return id;
    }

    @Override
    public boolean isAppBarLayoutExpanded() {
        return appBarLayoutState == AppBarLayoutStateEnums.EXPANDED;
    }

    @Override
    public void hideAppBarLayout() {
        app_bar_layout.setExpanded(false, true);
    }

    @Override
    public void showAppBarLayout() {
        app_bar_layout.setExpanded(true, true);
    }

    @Override
    public void showImageLarge(String imgUrl) {
        ImageUtils.showCampaignImgLarge(this, iv_image_large, imgUrl);
    }

    @Override
    public void setArticleDetail(Article articleDetail) {
        if (viewPagerPages.get(0) instanceof ArticleContentFragment) {
            ((ArticleContentFragment) viewPagerPages.get(0)).setArticleDetail(articleDetail);
        }
    }

    @Override
    public void setMerchant(Merchant merchant) {
        if (viewPagerPages.get(1) instanceof AboutAuthorFragment) {
            AboutAuthorFragment fragment = (AboutAuthorFragment) viewPagerPages.get(1);
            fragment.setMerchant(merchant);
        }
    }

    @Override
    public void showArticlePrice(String price) {
        btn_buy_coupon.setText(getString(R.string.buy_a_article_for, price));
    }

    @Override
    public void setBuyState(boolean isBought) {
        this.isBought = isBought;
    }

    @Override
    public void showBuyArticleView() {
        vg_buy_coupon.setVisibility(!isBought ? View.VISIBLE : View.GONE);
    }

    @Override
    public void hideBuyArticleView() {
        vg_buy_coupon.setVisibility(View.GONE);
    }

    @Override
    public void setToolbarTitle(String title) {
        collapsing_toolbar.setTitle(title);
    }

    @Override
    public void showCommentEdiText() {
        if (openComments) {
            hideBuyArticleView();
            ll_comment_container.setVisibility(View.VISIBLE);
            view_pager.setCurrentItem(viewPagerPages.size() - 1);
        }
    }

    @Override
    public void showSuccessToastMessage() {
        Toast.makeText(this, getString(R.string.like_sending_success), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void sendBroadcast() {
        Intent broadcastIntent = new Intent(BroadcastEventEnums.PROFILE_UPDATED);
        LocalBroadcastManager.getInstance(this).sendBroadcast(broadcastIntent);
    }

    @Override
    protected void onDestroy() {
        presenter.onDestroyView();
        super.onDestroy();
    }
}