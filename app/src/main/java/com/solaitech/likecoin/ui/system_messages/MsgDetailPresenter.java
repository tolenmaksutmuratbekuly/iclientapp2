package com.solaitech.likecoin.ui.system_messages;

import com.solaitech.likecoin.data.models.article.Article;
import com.solaitech.likecoin.data.repository.article.ArticleRepositoryProvider;
import com.solaitech.likecoin.network.API;
import com.solaitech.likecoin.ui.base.PresenterErrorHandler;
import com.solaitech.likecoin.utils.StringUtils;

import rx.Subscriber;
import rx.Subscription;
import rx.functions.Action0;
import rx.subscriptions.CompositeSubscription;

class MsgDetailPresenter {
    private MsgDetailView view;
    private API api;
    private CompositeSubscription compositeSubscription = new CompositeSubscription();

    MsgDetailPresenter(MsgDetailView view, API api) {
        this.view = view;
        this.api = api;
    }

    public void init() {
    }

    private void getMessageDetail() {
        Subscription subscription = ArticleRepositoryProvider
                .provideRepository(api)
                .getArticleById(view.getArticleId())
                .doOnSubscribe(new Action0() {
                    @Override
                    public void call() {
                        view.showProgressTemplateView();
                    }
                })
                .subscribe(new Subscriber<Article>() {
                    @Override
                    public void onCompleted() {
                        view.showSuccessTemplateView();
                    }

                    @Override
                    public void onError(Throwable e) {
                        PresenterErrorHandler.handleError(e, view);
                    }

                    @Override
                    public void onNext(Article article) {
                        view.setArticleDetail(article);
                        view.setMerchant(article.getMerchant());

                        if (article.getTitle() != null) {
                            view.setToolbarTitle(article.getTitle());
                        } else {
                            view.setToolbarTitle(StringUtils.EMPTY_STRING);
                        }
                        view.showCommentEdiText();
                    }
                });
        compositeSubscription.add(subscription);
    }

    void onAllViewPagerPagesInitialized() {
        getMessageDetail();
    }

    public void onDestroyView() {
        unsubscribe();
    }

    private void unsubscribe() {
        if (compositeSubscription != null
                && !compositeSubscription.isUnsubscribed()) {
            compositeSubscription.unsubscribe();
        }
    }

    void onTryUploadDataAgainClick() {
        getMessageDetail();
    }
}