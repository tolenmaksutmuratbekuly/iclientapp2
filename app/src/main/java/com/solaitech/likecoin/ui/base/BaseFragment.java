package com.solaitech.likecoin.ui.base;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;

import com.solaitech.likecoin.data.preferences.Preferences;
import com.solaitech.likecoin.data.preferences.PreferencesImpl;
import com.solaitech.likecoin.network.API;
import com.solaitech.likecoin.network.RetrofitServiceGenerator;

public class BaseFragment extends Fragment {

    protected API api;
    protected Preferences preferences;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        preferences = new PreferencesImpl(getContext());

        RetrofitServiceGenerator retrofitServiceGenerator = RetrofitServiceGenerator.getInstance(preferences);
        api = retrofitServiceGenerator.createService(API.class);
    }
}