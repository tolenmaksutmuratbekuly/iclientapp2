package com.solaitech.likecoin.ui.profile.edit_profile_image;

import android.app.IntentService;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.v4.content.LocalBroadcastManager;
import android.widget.Toast;

import java.io.File;

import com.solaitech.likecoin.BuildConfig;
import com.solaitech.likecoin.R;
import com.solaitech.likecoin.data.models.enums.BroadcastEventEnums;
import com.solaitech.likecoin.data.preferences.Preferences;
import com.solaitech.likecoin.data.preferences.PreferencesImpl;
import com.solaitech.likecoin.data.repository.user.UserRepositoryProvider;
import com.solaitech.likecoin.exceptions.APIException;
import com.solaitech.likecoin.exceptions.ConnectionTimeOutException;
import com.solaitech.likecoin.exceptions.NeedReAuthorizeException;
import com.solaitech.likecoin.exceptions.UnknownException;
import com.solaitech.likecoin.network.API;
import com.solaitech.likecoin.network.RetrofitServiceGenerator;
import com.solaitech.likecoin.network.error.RetrofitErrorHandler;
import com.solaitech.likecoin.utils.Logger;
import com.solaitech.likecoin.utils.StringUtils;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import rx.Subscriber;
import rx.functions.Action0;

public class UploadProfileImageIntentService extends IntentService {
    //    public static final String PHOTO_PATH = "photoPath";
    public static final String PHOTO_URI = "photoUri";

    public UploadProfileImageIntentService() {
        super("UploadProfileImageIntentService");
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        if (intent != null
                && intent.getExtras() != null) {
            Bundle bundle = intent.getExtras();
//            String photoPath = bundle.getString(PHOTO_PATH);
            Uri uri = bundle.getParcelable(PHOTO_URI);

            try {
//                File file = getFile(photoPath);
                File file = getFile(uri);
                uploadProfilePhoto(file);
            } catch (FileException e) {
                e.printStackTrace();
            }

        }
    }

//    private File getFile(String photoPath) throws FileException {
//        File file = FileUtils.compressFile(this, photoPath);
//
//        if (file == null) {
//            throw new FileException();
//        }
//
//        return file;
//    }

    private File getFile(Uri uri) throws FileException {
//        File file = FileUtils.compressFile(this, uri);
        File file = new File(uri.getPath());

        if (file == null) {
            throw new FileException();
        }

        return file;
    }

    private void uploadProfilePhoto(File file) throws FileException {
        UserRepositoryProvider.provideRepository(getAPI())
                .uploadAvatar(getParams(file))
                .doOnSubscribe(new Action0() {
                    @Override
                    public void call() {
                        showToastMessage(getString(R.string.your_request_is_being_processed));
                    }
                })
                .subscribe(new Subscriber<ResponseBody>() {
                    @Override
                    public void onCompleted() {
                        Logger.e(getClass(), "onCompleted");
                        showToastMessage(getString(R.string.profile_image_updated_successfully));
                        sendBroadcast();
                    }

                    @Override
                    public void onError(Throwable e) {
                        Logger.e(getClass(), "onError");
                        if (BuildConfig.IS_DEBUG) {
                            e.printStackTrace();
                        }
                        try {
                            RetrofitErrorHandler.handleException(e);
                        } catch (APIException e1) {
                            showToastMessage(StringUtils.replaceNull(e1.getErrorDescr()));
                        } catch (UnknownException e1) {
                            showToastMessage(getString(R.string.server_error));
                        } catch (ConnectionTimeOutException e1) {
                            showToastMessage(getString(R.string.connection_error));
                        } catch (NeedReAuthorizeException e1) {

                        }
                    }

                    @Override
                    public void onNext(ResponseBody responseBody) {
                        Logger.e(getClass(), "onNext");
                    }
                });
    }

    private API getAPI() {
        Preferences preferences = new PreferencesImpl(this);
        RetrofitServiceGenerator retrofitServiceGenerator = RetrofitServiceGenerator.getInstance(preferences);
        return retrofitServiceGenerator.createService(API.class);
    }

    private MultipartBody.Part getParams(File file) {
        // create RequestBody instance from file
        RequestBody requestFile =
                RequestBody.create(
                        MediaType.parse("image/*"), //MediaType.parse(getContentResolver().getType(fileUri)),
                        file
                );

        // MultipartBody.Part is used to send also the actual file name
        return  MultipartBody.Part.createFormData("file", file.getName(), requestFile);
    }

    private void showToastMessage(final String text) {
        new Handler(Looper.getMainLooper()).post(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(getApplicationContext(), text, Toast.LENGTH_SHORT).show();
            }
        });
    }

//    private void clearImages() {
//        ImageUtils.clearDiskCache(UploadProfileImageIntentService.this);
//        new Handler(Looper.getMainLooper()).post(new Runnable() {
//            @Override
//            public void run() {
//                ImageUtils.clearMemory(UploadProfileImageIntentService.this);
//            }
//        });
//    }

    private void sendBroadcast() {
        Intent broadcastIntent = new Intent(BroadcastEventEnums.PROFILE_IMAGE_UPDATED);
        LocalBroadcastManager.getInstance(this).sendBroadcast(broadcastIntent);
    }

    private class FileException extends Exception {

    }
}