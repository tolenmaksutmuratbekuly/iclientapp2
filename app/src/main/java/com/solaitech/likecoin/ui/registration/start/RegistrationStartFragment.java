package com.solaitech.likecoin.ui.registration.start;

import android.Manifest;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;

import com.redmadrobot.inputmask.MaskedTextChangedListener;
import com.redmadrobot.inputmask.helper.Mask;
import com.redmadrobot.inputmask.model.CaretString;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import com.solaitech.likecoin.R;
import com.solaitech.likecoin.data.PermissionRequestCodes;
import com.solaitech.likecoin.ui.base.loading.LoadingBaseFragment;
import com.solaitech.likecoin.ui.registration.complete.RegistrationCompleteDialog;
import com.solaitech.likecoin.utils.StringUtils;
import com.solaitech.likecoin.utils.animation.CustomAnimationUtils;
import com.solaitech.likecoin.utils.EditTextWatcher;
import com.solaitech.likecoin.utils.Logger;
import com.solaitech.likecoin.utils.PermissionUtils;

public class RegistrationStartFragment extends LoadingBaseFragment
        implements RegistrationStartView {

    private static final String PHONE_MASK = "+7 ([000]) [000]-[00]-[00]";

    private EditTextWatcher watcherPhoneNumber;
    private EditTextWatcher watcherEmail;
    private EditTextWatcher watcherName;
    private EditTextWatcher watcherSurname;

    @Bind(R.id.til_phone_number) TextInputLayout til_phone_number;
    @Bind(R.id.til_email) TextInputLayout til_email;
    @Bind(R.id.til_name) TextInputLayout til_name;
    @Bind(R.id.til_surname) TextInputLayout til_surname;
    @Bind(R.id.et_name) EditText et_name;
    @Bind(R.id.et_surname) EditText et_surname;
    @Bind(R.id.et_phone_number) EditText et_phone_number;
    @Bind(R.id.ll_rega_group) LinearLayout ll_rega_group;
    @Bind(R.id.ll_login_group) LinearLayout ll_login_group;
    @Bind(R.id.et_email) EditText et_email;
    @Bind(R.id.vg_container) ViewGroup vg_container;

    private RegistrationStartPresenter presenter;

    private static String[] PERMISSIONS_SMS = {
            Manifest.permission.RECEIVE_SMS
    };

    public static RegistrationStartFragment newInstance() {
        return new RegistrationStartFragment();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = super.onCreateView(inflater, container, savedInstanceState);
        ButterKnife.bind(this, v);
        configureViews();
        presenter = new RegistrationStartPresenter(this, api);
        presenter.init();
        return v;
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.frgm_registration_start;
    }

    private void configureViews() {
        et_phone_number.getBackground().mutate().setColorFilter(ContextCompat.getColor(getContext(), R.color.et_bottom_line), PorterDuff.Mode.SRC_ATOP);
        et_email.getBackground().mutate().setColorFilter(ContextCompat.getColor(getContext(), R.color.et_bottom_line), PorterDuff.Mode.SRC_ATOP);
        et_name.getBackground().mutate().setColorFilter(ContextCompat.getColor(getContext(), R.color.et_bottom_line), PorterDuff.Mode.SRC_ATOP);
        et_surname.getBackground().mutate().setColorFilter(ContextCompat.getColor(getContext(), R.color.et_bottom_line), PorterDuff.Mode.SRC_ATOP);

        CustomAnimationUtils.enableTransition(vg_container);
        final MaskedTextChangedListener listener = new MaskedTextChangedListener(
                PHONE_MASK,
                true,
                et_phone_number,
                null,
                new MaskedTextChangedListener.ValueListener() {
                    @Override
                    public void onTextChanged(boolean maskFilled, @NonNull final String extractedValue) {
                        Logger.e(getClass(), extractedValue);
                        Logger.e(getClass(), String.valueOf(maskFilled));
                    }
                }
        );

        et_phone_number.addTextChangedListener(listener);
        et_phone_number.setOnFocusChangeListener(listener);
        et_phone_number.setHint(listener.placeholder());

        watcherPhoneNumber = new EditTextWatcher(getContext(), til_phone_number);
        watcherEmail = new EditTextWatcher(getContext(), til_email);
        watcherName = new EditTextWatcher(getContext(), til_name);
        watcherSurname = new EditTextWatcher(getContext(), til_surname);

        et_phone_number.addTextChangedListener(watcherPhoneNumber);
        et_email.addTextChangedListener(watcherEmail);
        et_name.addTextChangedListener(watcherName);
        et_surname.addTextChangedListener(watcherSurname);
    }

    @OnClick(R.id.btn_start)
    void onStartClick() {
        presenter.onStartRegistrationClick();
    }

    @OnClick(R.id.btn_sign_up)
    void onSignUpClick() {
        presenter.onSignUpButtonClick();
    }

    @Override
    public String getPhoneNumber() {
        Mask mask = new Mask(PHONE_MASK);
        final String input = et_phone_number.getText().toString();
        Mask.Result result = mask.apply(
                new CaretString(
                        input,
                        input.length()
                ),
                true // you may consider disabling autocompletion for your case
        );

        //String output = result.getFormattedText().getString();

        StringBuilder sb = new StringBuilder();
        sb.append("7");
        sb.append(result.getExtractedValue());

        return sb.toString();
    }

    @Override
    public String getEmail() {
        return et_email.getText().toString();
    }

    @Override
    public String getName() {
        return et_name.getText().toString();
    }

    @Override
    public String getSurname() {
        return et_surname.getText().toString();
    }

    @Override
    public void showPhoneNumberEmptyError() {
        watcherPhoneNumber.showFieldError(getString(R.string.field_error));
    }

    @Override
    public void showPhoneNumberInvalidError() {
        watcherPhoneNumber.showFieldError(getString(R.string.phone_number_invalid_error));
    }

    @Override
    public void showEmailInvalidError() {
        watcherEmail.showFieldError(getString(R.string.email_invalid_error));
    }

    @Override
    public void showSurnameEmptyError() {
        watcherSurname.showFieldError(getString(R.string.field_error));
    }

    @Override
    public void showNameEmptyError() {
        watcherName.showFieldError(getString(R.string.field_error));
    }

    @Override
    protected void tryUploadDataAgain() {
        //do nothing
    }

    @Override
    public void openRegistrationCompleteDialog(String phoneNumber, String email, String name, String surname, boolean isRegistered) {
        RegistrationCompleteDialog dialog = RegistrationCompleteDialog.newInstance(phoneNumber, email, name, surname, isRegistered);
        dialog.setCancelable(false);
        dialog.show(getChildFragmentManager(), StringUtils.EMPTY_STRING);
    }

    @Override
    public void showRegistrationLayout() {
        ll_login_group.setVisibility(View.GONE);
        ll_rega_group.setVisibility(View.VISIBLE);
    }

    @Override
    public void onDestroyView() {
        presenter.onDestroyView();
        super.onDestroyView();
    }

    @Override
    public boolean isPermissionGrantedToSms() {
        return PermissionUtils.isPermissionGranted(getContext(), Manifest.permission.RECEIVE_SMS);
    }

    @Override
    public boolean shouldShowRequestPermissionRationaleToSms() {
        return ActivityCompat.shouldShowRequestPermissionRationale(getActivity(),
                Manifest.permission.RECEIVE_SMS);
    }

    @Override
    public void requestPermissionToSms() {
        requestPermissions(PERMISSIONS_SMS, PermissionRequestCodes.SMS);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {

        if (requestCode == PermissionRequestCodes.SMS) {
            if (PermissionUtils.verifyPermissions(grantResults)) {
                presenter.onPermissionGrantedToSms();
            } else {
                presenter.onPermissionNotGrantedToSms();
            }
        } else {
            super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }
}