package com.solaitech.likecoin.ui.profile.edit_profile_image;

import android.app.Dialog;
import android.view.View;

import com.solaitech.likecoin.R;
import com.solaitech.likecoin.ui.base.BaseBottomSheetDialogFragment;

public class EditProfileImageDialog extends BaseBottomSheetDialogFragment
        implements View.OnClickListener {

    private OnEditProfileImageTypeChooseListener onEditProfileImageTypeChooseListener;

    public static EditProfileImageDialog newInstance() {
        return new EditProfileImageDialog();
    }

    public void setOnEditProfileImageTypeChooseListener(OnEditProfileImageTypeChooseListener onEditProfileImageTypeChooseListener) {
        this.onEditProfileImageTypeChooseListener = onEditProfileImageTypeChooseListener;
    }

    @Override
    public void setupDialog(Dialog dialog, int style) {
        View v = View.inflate(getContext(), R.layout.dialog_edit_profile_image, null);
        dialog.setContentView(v);
        setBottomSheetCallback(v);
        initView(v);
    }

    private void initView(View v) {
        v.findViewById(R.id.tv_view_photo).setOnClickListener(this);
        v.findViewById(R.id.tv_select).setOnClickListener(this);
        v.findViewById(R.id.tv_make_a_photo).setOnClickListener(this);
        v.findViewById(R.id.tv_delete).setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.tv_view_photo:
                onEditProfileImageTypeChooseListener.onViewPhotoClick();
                dismiss();
                break;
            case R.id.tv_select:
                onEditProfileImageTypeChooseListener.onSelectProfileImageClick();
                dismiss();
                break;
            case R.id.tv_make_a_photo:
                onEditProfileImageTypeChooseListener.onMakePhotoClick();
                dismiss();
                break;
            case R.id.tv_delete:
                onEditProfileImageTypeChooseListener.onDeleteProfileImageClick();
                dismiss();
                break;
        }
    }

    public interface OnEditProfileImageTypeChooseListener {

        void onViewPhotoClick();

        void onSelectProfileImageClick();

        void onMakePhotoClick();

        void onDeleteProfileImageClick();

    }
}