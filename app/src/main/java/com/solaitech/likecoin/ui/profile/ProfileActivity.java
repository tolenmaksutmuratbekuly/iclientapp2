package com.solaitech.likecoin.ui.profile;

import android.Manifest;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.GradientDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.content.FileProvider;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.util.Pair;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.solaitech.likecoin.App;
import com.solaitech.likecoin.BuildConfig;
import com.solaitech.likecoin.R;
import com.solaitech.likecoin.data.Constants;
import com.solaitech.likecoin.data.PermissionRequestCodes;
import com.solaitech.likecoin.data.models.campaign.Merchant;
import com.solaitech.likecoin.data.models.enums.BroadcastEventEnums;
import com.solaitech.likecoin.data.models.enums.PartnerTypes;
import com.solaitech.likecoin.data.models.user.User;
import com.solaitech.likecoin.data.preferences.Preferences;
import com.solaitech.likecoin.data.preferences.PreferencesImpl;
import com.solaitech.likecoin.ui.base.BaseActivityFrgm;
import com.solaitech.likecoin.ui.coupons.detail.qr_code.ShowQrCodeDialog;
import com.solaitech.likecoin.ui.header.ProfileHeaderViewGroup;
import com.solaitech.likecoin.ui.profile.brix.BrixActivity;
import com.solaitech.likecoin.ui.profile.edit_profile.EditProfileDialog;
import com.solaitech.likecoin.ui.profile.edit_profile_image.DeleteProfileImageIntentService;
import com.solaitech.likecoin.ui.profile.edit_profile_image.EditProfileImageDialog;
import com.solaitech.likecoin.ui.profile.edit_profile_image.UploadProfileImageIntentService;
import com.solaitech.likecoin.ui.profile.edit_profile_image.UserInfoUpdate;
import com.solaitech.likecoin.ui.profile.level.LevelActivity;
import com.solaitech.likecoin.ui.profile.user_statement.UserStatementActivity;
import com.solaitech.likecoin.ui.registration.start.RegistrationStartActivity;
import com.solaitech.likecoin.utils.CustomColorUtils;
import com.solaitech.likecoin.utils.DialogUtils;
import com.solaitech.likecoin.utils.FileUtils;
import com.solaitech.likecoin.utils.ImageUtils;
import com.solaitech.likecoin.utils.PermissionUtils;
import com.solaitech.likecoin.utils.StringUtils;
import com.solaitech.likecoin.utils.ToastUtils;
import com.solaitech.likecoin.utils.animation.AvatarAnimation;
import com.yalantis.ucrop.UCrop;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Locale;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;

import static com.solaitech.likecoin.utils.StringUtils.PLUS_SIGN;
import static com.solaitech.likecoin.utils.StringUtils.length;

public class ProfileActivity extends BaseActivityFrgm implements ProfileView, UserInfoUpdate {
    private static final int REQUEST_TAKE_PICTURE = 1;
    private static final int REQUEST_CHOOSE_PICTURE = 2;
    private static final int REQUEST_PERMISSION_FOR_STORAGE = 3;

    private static final String SAMPLE_CROPPED_IMAGE_NAME = "SampleCropImage";

    private static String[] PERMISSIONS_STORAGE = {
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE
    };

    private Uri currentPhotoURI;
    @Bind(R.id.pb_indicator) ViewGroup pb_indicator;
    @Bind(R.id.v_profile) ProfileHeaderViewGroup v_profile;
    @Bind(R.id.iv_prof_img) CircleImageView iv_prof_img;
    @Bind(R.id.tv_name) TextView tv_name;
    @Bind(R.id.tv_level) TextView tv_level;
    @Bind(R.id.tv_phone) TextView tv_phone;
    @Bind(R.id.tv_email) TextView tv_email;
    @Bind(R.id.ll_ambassador) LinearLayout ll_ambassador;
    @Bind(R.id.img_expanded) ImageView img_expanded;
    @Bind(R.id.user_photo_container) LinearLayout user_photo_container;
    @Bind(R.id.tv_language) TextView tv_language;

    private ProfilePresenter presenter;
    private Preferences preferences;
    private DialogUtils dialogUtils;
    private AvatarAnimation avatarAnimation;

    private String selectedLang;
    private boolean needRestartActivity;

    public static Intent getIntent(Context context) {
        return new Intent(context, ProfileActivity.class);
    }

    public static Intent newIntentClearTop(Context packageContext) {
        Intent intent = new Intent(packageContext, ProfileActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
        return intent;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        ButterKnife.bind(this);
        dialogUtils = new DialogUtils();
        v_profile.setOnUpdateUserInfo(this);

        preferences = new PreferencesImpl(this);
        presenter = new ProfilePresenter(this, this, preferences, api);

        avatarAnimation = new AvatarAnimation(this, user_photo_container, img_expanded, iv_prof_img);
    }

    @OnClick(R.id.fab_edit_profile)
    void onProfileEditClick() {
        presenter.onEditProfileClick();
    }

    @OnClick(R.id.btn_show_qr)
    void onShowQrCode() {
        presenter.onShowQrCodeClick();
    }

    @OnClick(R.id.btn_logout)
    void onLogoutClick() {
        dialogUtils.setCallback(new DialogUtils.Callback() {
            @Override
            public void onPositiveClick() {
                presenter.onLogoutClick();
            }
        });
        dialogUtils.showPositiveNegative(this, R.string.label_settings_wantto_exit, R.string.yes, R.string.no);
    }

    @OnClick(R.id.iv_prof_img)
    void onAvatarChangeClick() {
        presenter.onEditProfileImageClick();
    }

    @OnClick(R.id.ll_lang)
    void onLanguageClick() {
        final Dialog dialog = new Dialog(this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_change_language);
        dialog.getWindow().setLayout(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        configureLanguageDialog(dialog);
        dialog.show();
    }

    @Override
    public void onBackPressed() {
        if (img_expanded.getVisibility() == View.VISIBLE) {
            avatarAnimation.closeImage();
        } else {
            super.onBackPressed();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        v_profile.onResume();
    }

    // Change language Dialog
    private void configureLanguageDialog(final Dialog dialog) {
        configureLanguage(dialog);
        selectedLang = StringUtils.EMPTY_STRING;
        RadioGroup radioGroup = dialog.findViewById(R.id.rg_lang);
        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                if (checkedId == R.id.rb_russian) {
                    selectedLang = Constants.KEY_LANG_RU;
                } else if (checkedId == R.id.rb_english) {
                    selectedLang = Constants.KEY_LANG_EN;
                }
            }
        });

        Button btn_cancel = dialog.findViewById(R.id.btn_cancel);
        btn_cancel.setOnClickListener(new View.OnClickListener() {
                                          @Override
                                          public void onClick(View view) {
                                              dialog.hide();
                                          }
                                      }
        );

        Button btn_ok = dialog.findViewById(R.id.btn_ok);
        btn_ok.setOnClickListener(new View.OnClickListener() {
                                      @Override
                                      public void onClick(View view) {
                                          if (selectedLang.length() > 0) {
                                              needRestartActivity = true;
                                              presenter.onChangeLanguageClick(selectedLang);
                                          }
                                          dialog.hide();
                                      }
                                  }
        );
    }

    private void configureLanguage(Dialog dialog) {
        RadioButton rb_russian = null;
        RadioButton rb_english = null;
        if (dialog != null) {
            rb_russian = dialog.findViewById(R.id.rb_russian);
            rb_english = dialog.findViewById(R.id.rb_english);
        }

        String lang = preferences.getLanguage();
        if (preferences.getLanguage().length() > 0) {
            if (lang.equals(Constants.KEY_LANG_EN)) {
                if (rb_english != null)
                    rb_english.setChecked(true);
                tv_language.setText(getString(R.string.lang_en));
            } else {
                if (rb_russian != null)
                    rb_russian.setChecked(true);

                tv_language.setText(getString(R.string.lang_ru));
            }
        } else {
            String currentLocale = Locale.getDefault().getLanguage();
            if (currentLocale.equals(Constants.KEY_LANG_EN)) {
                if (rb_english != null)
                    rb_english.setChecked(true);
                tv_language.setText(getString(R.string.lang_en));
            } else {
                if (rb_russian != null)
                    rb_russian.setChecked(true);
                tv_language.setText(getString(R.string.lang_ru));
            }
        }
    }

    ////////////////////////////////////////////////
    /// UserView                              ///
    ////////////////////////////////////////////////

    @Override
    public User getUser() {
        App app = (App) getApplicationContext();
        return app.getUser();
    }

    @Override
    public void openEditProfileDialog(User user) {
        EditProfileDialog dialog = EditProfileDialog.newInstance(user);
        dialog.show(getSupportFragmentManager(), "");
    }

    @Override
    public void openEditProfileImageDialog() {
        EditProfileImageDialog df = EditProfileImageDialog.newInstance();
        df.setOnEditProfileImageTypeChooseListener(new EditProfileImageDialog.OnEditProfileImageTypeChooseListener() {
            @Override
            public void onViewPhotoClick() {
                avatarAnimation.onOpenUserPhoto();
            }

            @Override
            public void onSelectProfileImageClick() {
                presenter.onSelectProfileImageClick();
            }

            @Override
            public void onMakePhotoClick() {
                presenter.onMakePhotoClick();
            }

            @Override
            public void onDeleteProfileImageClick() {
                presenter.onDeleteProfileImageClick();
            }
        });
        df.show(getSupportFragmentManager(), "");
    }

    @Override
    public void selectProfileImageFromIntent() {
        Intent photoLibraryIntent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        photoLibraryIntent.setType("image/*");
        startActivityForResult(photoLibraryIntent, REQUEST_CHOOSE_PICTURE);
    }

    @Override
    public void dispatchTakePictureIntent() {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        // Ensure that there's a camera activity to handle the intent
        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
            // Create the File where the photo should go
            File photoFile = null;
            try {
                photoFile = FileUtils.createImageFile(this);
                // Save a file: path for use with ACTION_VIEW intents
//                currentPhotoPath = photoFile.getAbsolutePath();
            } catch (IOException ex) {
                // Error occurred while creating the File
            }
            // Continue only if the File was successfully created
            if (photoFile != null) {
                currentPhotoURI = FileProvider.getUriForFile(this,
                        BuildConfig.FILE_PROVIDER,
                        photoFile);
                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, currentPhotoURI);
                startActivityForResult(takePictureIntent, REQUEST_TAKE_PICTURE);
            }
        }
    }

    @Override
    public void startServiceToDeleteProfileImage() {
        Intent intent = new Intent(this, DeleteProfileImageIntentService.class);
        startService(intent);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case REQUEST_TAKE_PICTURE: {
                if (resultCode == RESULT_OK) {
                    startCropActivity(currentPhotoURI);
                    currentPhotoURI = null;
                }
                break;
            }
            case REQUEST_CHOOSE_PICTURE: {
                if (data != null) {
                    Uri uri = data.getData();
                    startCropActivity(uri);
                }
                break;
            }
            case REQUEST_PERMISSION_FOR_STORAGE: {
                presenter.onActivityResultToStorage();
                break;
            }
            case UCrop.REQUEST_CROP: {
                if (data != null) {
                    Uri resultUri = UCrop.getOutput(data);
                    if (resultUri != null) {
                        presenter.onCropActivityResultOk(
                                resultUri
                        );
                    }
                }
                break;
            }
            case UCrop.RESULT_ERROR: {
                if (data != null) {
                    Throwable cropError = UCrop.getError(data);
                    if (cropError != null
                            && cropError.getMessage() != null) {
                        ToastUtils.show(this, cropError.getMessage());
                    }
                }
                break;
            }
        }
    }

    @Override
    public void startServiceToUploadProfileImage(Uri uri) {
        Bundle bundle = new Bundle();
        bundle.putParcelable(UploadProfileImageIntentService.PHOTO_URI, uri);

        Intent intent = new Intent(this, UploadProfileImageIntentService.class);
        intent.putExtras(bundle);
        startService(intent);
    }

//    @Override
//    public void galleryAddPic(String photoPath) {
//        Intent mediaScanIntent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
//        File f = new File(photoPath);
//        Uri contentUri = Uri.fromFile(f);
//        mediaScanIntent.setData(contentUri);
//        this.sendBroadcast(mediaScanIntent);
//    }

    @Override
    public boolean isPermissionGrantedToStorage() {
        return PermissionUtils.isPermissionGranted(this, Manifest.permission.WRITE_EXTERNAL_STORAGE);
    }

    @Override
    public boolean shouldShowRequestPermissionRationaleToStorage() {
        return ActivityCompat.shouldShowRequestPermissionRationale(this,
                Manifest.permission.WRITE_EXTERNAL_STORAGE);
    }

    @Override
    public void showRequestPermissionRationaleToStorage() {
        dialogUtils.setCallback(new DialogUtils.Callback() {
            @Override
            public void onPositiveClick() {
                presenter.onRationaleToStorageAllowed();
            }
        });
        dialogUtils.showPositiveNegative(this, R.string.rationale_storage, R.string.allow, R.string.reject);
    }

    @Override
    public void requestPermissionToStorage() {
        ActivityCompat.requestPermissions(
                this,
                PERMISSIONS_STORAGE,
                PermissionRequestCodes.STORAGE);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {

        if (requestCode == PermissionRequestCodes.STORAGE) {
            if (PermissionUtils.verifyPermissions(grantResults)) {
                presenter.onPermissionGrantedToStorage();
            } else {
                presenter.onPermissionNotGrantedToStorage();
            }
        } else {
            super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    @Override
    public void showRequestPermissionDialogToStorage() {
        dialogUtils.setCallback(new DialogUtils.Callback() {
            @Override
            public void onPositiveClick() {
                presenter.onDialogToStorageAllowed();
            }
        });
        dialogUtils.showPositiveNegative(this, R.string.rationale_storage, R.string.allow, R.string.reject);
    }

    @Override
    public void openRegistrationActivity() {
        startActivity(RegistrationStartActivity.newLogoutIntent(getApplicationContext()));
    }

    @Override
    public void showLoadingIndicator(boolean show) {
        pb_indicator.setVisibility(show ? View.VISIBLE : View.GONE);
    }

    @Override
    public void showPhoneNumber(String cellPhone) {
        tv_phone.setText(String.valueOf(PLUS_SIGN + cellPhone));
    }

    @Override
    public void showEmail(String email) {
        tv_email.setVisibility(length(email) > 0 ? View.VISIBLE : View.GONE);
        tv_email.setText(email);
    }

    @Override
    public void showUserLevel(int userLevel) {
        tv_level.setText(String.valueOf(userLevel));
        GradientDrawable drawable = (GradientDrawable) tv_level.getBackground();
        CustomColorUtils.setLevelColor(this, drawable, userLevel, null);
    }

    @Override
    public void showProfileImage(String avatarUrl) {
        ImageUtils.showAvatar(this, iv_prof_img, avatarUrl);
        ImageUtils.showAvatar(this, img_expanded, avatarUrl);
    }

    @Override
    public void showUserName(String name) {
        tv_name.setText(name);
    }

    @Override
    public void showSurname(String surname) {
        tv_name.append(" " + surname);
    }

    @Override
    public void setLanguage(String language) {
        preferences.setLanguage(language);
        configureLanguage(null);
    }

    @Override
    public void openSettingsActivity() {
        Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
        Uri uri = Uri.fromParts("package", getPackageName(), null);
        intent.setData(uri);
        startActivityForResult(intent, REQUEST_PERMISSION_FOR_STORAGE);
    }

    private void startCropActivity(@NonNull Uri uri) {
        String destinationFileName = SAMPLE_CROPPED_IMAGE_NAME;
        destinationFileName += ".jpg";
        UCrop uCrop = UCrop.of(uri, Uri.fromFile(new File(getCacheDir(), destinationFileName)));

        UCrop.Options options = new UCrop.Options();
        options.setCompressionFormat(Bitmap.CompressFormat.JPEG);
        options.withMaxResultSize(256, 256);
        options.setCompressionQuality(100);
        options.withAspectRatio(1, 1);
        options.setFreeStyleCropEnabled(false);
        options.setToolbarColor(CustomColorUtils.getColor(this, R.color.colorPrimary));
        options.setStatusBarColor(CustomColorUtils.getColor(this, R.color.colorPrimaryDark));
        options.setActiveWidgetColor(CustomColorUtils.getColor(this, R.color.colorPrimary));
        options.setToolbarTitle(getString(R.string.edit_photo));
        options.setHideBottomControls(false);
        options.setCircleDimmedLayer(true);
        uCrop.withOptions(options).start(this);
    }

    @Override
    public void setAmbassadorRoles(List<Merchant> ambassadorRoles) {
        ll_ambassador.setVisibility(View.VISIBLE);
        ll_ambassador.removeAllViews();
        LayoutInflater layoutInflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        for (int i = 0; i < ambassadorRoles.size(); i++) {
            if (i < PartnerTypes.ROLES_AMOUNT) {
                final Merchant merchant = ambassadorRoles.get(i);
                View child = layoutInflater.inflate(R.layout.v_ambassador, null);

                ImageView img_remove = child.findViewById(R.id.img_remove);
                img_remove.setVisibility(View.VISIBLE);
                img_remove.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        dialogUtils.setCallback(new DialogUtils.Callback() {
                            @Override
                            public void onPositiveClick() {
                                presenter.onRemoveClick(merchant.getId(), PartnerTypes.MERCHANT_ROLE_ID);
                            }
                        });
                        dialogUtils.showPositiveNegative(ProfileActivity.this, R.string.label_settings_wantto_delete, R.string.yes, R.string.no);
                    }
                });

                if (merchant.getLogo4AmbassadorUrl() != null) {
                    ImageUtils.showAmbassadorLogo(this, (ImageView) child.findViewById(R.id.iv_logo), merchant.getLogo4AmbassadorUrl());
                }

                if (merchant.getTitle() != null) {
                    ((TextView) child.findViewById(R.id.tv_title)).setText(merchant.getTitle());
                }
                ll_ambassador.addView(child);
            } else {
                break;
            }
        }
    }

    @Override
    public void hideAmbassadorRoles() {
        ll_ambassador.setVisibility(View.GONE);
        ll_ambassador.removeAllViews();
    }

    @Override
    public void sendBroadCast() {
        Intent broadcastIntent = new Intent(BroadcastEventEnums.PROFILE_UPDATED);
        LocalBroadcastManager.getInstance(this).sendBroadcast(broadcastIntent);
    }

    @Override
    public void showQrCode(String key) {
        ShowQrCodeDialog dialog = ShowQrCodeDialog.newInstance(key, true, getString(R.string.qr_code));
        dialog.show(getSupportFragmentManager(), "");
    }

    ////////////////////////////////////////////////
    /// UserInfoUpdate                           ///
    ////////////////////////////////////////////////

    @Override
    protected void onProfileImageUpdated() {
        super.onProfileImageUpdated();
        v_profile.onProfileImageUpdated();
    }

    @Override
    protected void onProfileUpdated() {
        super.onProfileUpdated();
        v_profile.onProfileUpdated();
    }

    @Override
    public void onUpdateUserInfo(User user) {
        presenter.setUserInfo(user);
        if (needRestartActivity) {
            needRestartActivity = false;
            startActivity(ProfileActivity.newIntentClearTop(this));
        }
    }

    @Override
    public void openProfileActivity() {

    }

    @Override
    public void openUserStatementActivity() {
        intentOpener(UserStatementActivity.getIntent(this));
    }

    @Override
    public void openLevelActivity() {
        intentOpener(LevelActivity.getIntent(this));
    }

    @Override
    public void openBrixActivity() {
        intentOpener(BrixActivity.getIntent(this));
    }

    @Override
    public void initToolbar(Toolbar toolbar) {
        toolbar.setNavigationIcon(R.drawable.ic_chevron_left_white_24dp);
        setSupportActionBar(toolbar);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                supportFinishAfterTransition();
            }
        });
    }

    private void intentOpener(Intent intent) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            Pair<View, String> p1 = Pair.create(findViewById(R.id.v_profile), getString(R.string.tn_profile));
            ActivityOptionsCompat options = ActivityOptionsCompat.makeSceneTransitionAnimation(this, p1);
            startActivity(intent, options.toBundle());
        } else {
            startActivity(intent);
        }
        finish();
    }
}