package com.solaitech.likecoin.ui.coupons.active;

import java.util.ArrayList;
import java.util.List;

import com.solaitech.likecoin.BuildConfig;
import com.solaitech.likecoin.data.models.coupons.Coupon;
import com.solaitech.likecoin.data.models.enums.CouponsStateIdEnums;
import com.solaitech.likecoin.data.repository.coupons.CouponsRepositoryProvider;
import com.solaitech.likecoin.network.API;
import com.solaitech.likecoin.ui.base.PresenterErrorHandler;
import rx.Subscriber;
import rx.Subscription;
import rx.functions.Action0;
import rx.subscriptions.CompositeSubscription;

class ActiveCouponsPresenter {

    private ActiveCouponsView view;
    private API api;
    private CompositeSubscription compositeSubscription = new CompositeSubscription();
    private Subscription searchSubscription;

    ActiveCouponsPresenter(ActiveCouponsView view, API api) {
        this.view = view;
        this.api = api;
    }

    public void init() {
        getCoupons();
    }

    private void getCoupons() {
        Subscription subscription = CouponsRepositoryProvider.provideRepository(api)
                .getCoupons(CouponsStateIdEnums.ACTIVE)
                .doOnSubscribe(new Action0() {
                    @Override
                    public void call() {
                        view.showProgressTemplateView();
                    }
                })
                .subscribe(new Subscriber<List<Coupon>>() {
                    @Override
                    public void onCompleted() {
                        view.showSuccessTemplateView();
                    }

                    @Override
                    public void onError(Throwable e) {
                        PresenterErrorHandler.handleError(e, view);
                    }

                    @Override
                    public void onNext(List<Coupon> coupons) {
                        view.updateAdapterData(coupons);
                    }
                });
        compositeSubscription.add(subscription);
    }

    public void onSwipeRefresh() {
        getCoupons();
    }

    public void onTryToUploadDataAgainClick() {
        getCoupons();
    }

    public void onBottomNavigationTabReselected() {
        getCoupons();
    }

    public void onItemClick(Object object) {
        if (object instanceof Coupon) {
            Coupon coupon = (Coupon) object;

            if (coupon.getId() != null) {
                view.openCouponDetailActivity(coupon.getId());
            }
        }
    }

    void onSearch(final String query) {
        if (searchSubscription != null
                && !searchSubscription.isUnsubscribed()) {
            searchSubscription.unsubscribe();
        }

        searchSubscription = CouponsRepositoryProvider
                .provideRepository(api)
                .search(view.getObjects(), query)
                .doOnSubscribe(new Action0() {
                    @Override
                    public void call() {
                        view.clearFilterObjects();
                    }
                })
                .subscribe(new Subscriber<Coupon>() {
                    @Override
                    public void onCompleted() {
                        //do nothing
                    }

                    @Override
                    public void onError(Throwable e) {
                        if (BuildConfig.IS_DEBUG) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onNext(Coupon coupon) {
                        view.onSearchObjectFound(coupon);
                    }
                });
        compositeSubscription.add(searchSubscription);
    }

    void onSearchShowAllListItems() {
        List<Object> objects = new ArrayList<>(view.getObjects());
        view.updateAdapterData(objects);
    }

    public void onDestroyView() {
        unsubscribe();
    }

    private void unsubscribe() {
        if (compositeSubscription != null
                && !compositeSubscription.isUnsubscribed()) {
            compositeSubscription.unsubscribe();
        }
    }

}