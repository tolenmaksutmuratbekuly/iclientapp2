package com.solaitech.likecoin.ui.news_feed.feedback;

import com.solaitech.likecoin.ui.base.interfaces.MVPBaseErrorView;

interface FeedbackView extends MVPBaseErrorView {
    void closeDialog();

}