package com.solaitech.likecoin.ui.campaign.detail.purchase_coupon;

import java.util.List;

import com.solaitech.likecoin.data.models.coupons.Coupon;

public interface OnCouponsPurchasedListener {

    void onCouponsPurchased(String couponsQuantity, String totalSum, List<Coupon> coupons);

}