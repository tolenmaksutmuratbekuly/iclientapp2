package com.solaitech.likecoin.ui.campaign.detail.purchase_coupon.count_view;

import android.content.Context;
import android.content.res.TypedArray;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.EditText;
import android.widget.LinearLayout;

import com.solaitech.likecoin.R;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnTextChanged;

public class PurchaseCountView extends LinearLayout {
    @Bind(R.id.et_count) EditText et_count;

    private int layoutId;
    private int maxCouponCount;
    private int level;
    private OnPurchaseCountChangedListener onPurchaseCountChangedListener;

    public PurchaseCountView(Context context) {
        super(context);
        inflate();
    }

    public PurchaseCountView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        TypedArray a = context.obtainStyledAttributes(attrs, R.styleable.PurchaseCountView, 0, 0);
        layoutId = a.getInt(R.styleable.PurchaseCountView_layout_id, -1);

        a.recycle();
        inflate();
    }

    public PurchaseCountView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        inflate();
    }

    public void setOnPurchaseCountChangedListener(OnPurchaseCountChangedListener onPurchaseCountChangedListener) {
        this.onPurchaseCountChangedListener = onPurchaseCountChangedListener;
    }

    private void inflate() {
        LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (layoutId == 0) {
            inflater.inflate(R.layout.v_purchase_count_sendlike, this);
        } else if (layoutId == 1) {
            inflater.inflate(R.layout.v_purchase_count, this);
        } else if (layoutId == 2) {
            inflater.inflate(R.layout.v_purchase_count_brix, this);
        }

        ButterKnife.bind(this);
        initView();
    }

    private void initView() {
        et_count.setText("1");
    }

    public void setMaxCouponCount(int maxCouponCount) {
        this.maxCouponCount = maxCouponCount;
    }

    public void setEachDayLikeAmount(int level) {
        this.level = level;
    }

    @OnClick(R.id.v_decrease)
    void onClickDecrease() {
        decrease();
    }

    @OnClick(R.id.v_increase)
    void onClickIncrease() {
        increase();
    }

    @OnTextChanged(R.id.et_count)
    protected void handleTextChange() {
        if (et_count.getText().toString().length() == 0) {
            et_count.setText(String.valueOf(1));
            et_count.setSelection(et_count.getText().toString().length());
        }

        if (layoutId == 2 && onPurchaseCountChangedListener != null) {
            onPurchaseCountChangedListener.onPurchaseCountChanged(getEnteredCount());
        }
    }

    private void decrease() {
        if (getEnteredCount() > 1) {
            setCount(getEnteredCount() - 1);
            onPurchaseCountChangedListener.onPurchaseCountChanged(getEnteredCount());
        }
    }

    private void increase() {
        if (layoutId == 0 || layoutId == 1) {
            if (getEnteredCount() < (layoutId == 0 ? level : maxCouponCount)) {
                setCount(getEnteredCount() + 1);
                onPurchaseCountChangedListener.onPurchaseCountChanged(getEnteredCount());
            }
        } else {
            setCount(getEnteredCount() + 1);
        }
    }

    public int getEnteredCount() {
        return Integer.parseInt(et_count.getText().toString());
    }

    public void setCount(int value) {
        et_count.setText(String.valueOf(value));
        et_count.setSelection(et_count.getText().toString().length());
    }
}