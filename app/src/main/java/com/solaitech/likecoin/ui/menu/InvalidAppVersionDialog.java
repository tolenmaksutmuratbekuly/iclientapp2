package com.solaitech.likecoin.ui.menu;

import android.app.Dialog;
import android.content.DialogInterface;
import android.view.KeyEvent;
import android.view.View;

import com.solaitech.likecoin.R;
import com.solaitech.likecoin.ui.base.BaseBottomSheetDialogFragment;
import com.solaitech.likecoin.utils.GooglePlayAppLicationUtils;

public class InvalidAppVersionDialog extends BaseBottomSheetDialogFragment
        implements View.OnClickListener {

    public static InvalidAppVersionDialog newInstance() {
        return new InvalidAppVersionDialog();
    }

    @Override
    public void setupDialog(Dialog dialog, int style) {
        View v = View.inflate(getContext(), R.layout.dialog_invalid_app_version, null);
        dialog.setCancelable(false);
        dialog.setCanceledOnTouchOutside(false);

        dialog.setOnKeyListener(new DialogInterface.OnKeyListener() {
            @Override
            public boolean onKey(DialogInterface dialog, int keyCode, KeyEvent event) {
                // Prevent dialog close on back press button
                return keyCode == KeyEvent.KEYCODE_BACK;
            }
        });

        dialog.setContentView(v);
        setBottomSheetCallback(v);
        initView(v);
    }

    private void initView(View v) {
        v.findViewById(R.id.btn_update_app).setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_update_app:
                GooglePlayAppLicationUtils.openAppDownloadLink(getContext());
//                getActivity().moveTaskToBack(true);
                getActivity().finish();
                break;
        }
    }

}