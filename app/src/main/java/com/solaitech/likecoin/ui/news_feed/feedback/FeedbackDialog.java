package com.solaitech.likecoin.ui.news_feed.feedback;

import android.app.Activity;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.view.ViewGroup;

import com.solaitech.likecoin.R;
import com.solaitech.likecoin.data.Constants;
import com.solaitech.likecoin.exceptions.APIException;
import com.solaitech.likecoin.exceptions.ConnectionTimeOutException;
import com.solaitech.likecoin.exceptions.NeedReAuthorizeException;
import com.solaitech.likecoin.exceptions.UnknownException;
import com.solaitech.likecoin.ui.base.BaseBottomSheetDialogFragment;
import com.solaitech.likecoin.ui.campaign.detail.purchase_coupon.count_view.OnPurchaseCountChangedListener;
import com.solaitech.likecoin.utils.animation.CustomAnimationUtils;

public class FeedbackDialog extends BaseBottomSheetDialogFragment
        implements FeedbackView, View.OnClickListener, OnPurchaseCountChangedListener {
    FeedbackPresenter presenter;

    public static FeedbackDialog newInstance() {
        return new FeedbackDialog();
    }

    @Override
    public void setupDialog(Dialog dialog, int style) {
        View v = View.inflate(getContext(), R.layout.dialog_feedback, null);
        dialog.setContentView(v);
        setBottomSheetCallback(v);
        initView(v);

        presenter = new FeedbackPresenter(this, api);
        presenter.init();
    }

    private void initView(View v) {
        ViewGroup vgContainer = (ViewGroup) v.findViewById(R.id.vg_container);
        CustomAnimationUtils.enableTransition(vgContainer);
        v.findViewById(R.id.btn_whatsapp).setOnClickListener(this);
        v.findViewById(R.id.btn_mail).setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btn_whatsapp:
                openWhatsapp();
                dismiss();
                break;
            case R.id.btn_mail:
                openEmail();
                dismiss();
                break;
        }
    }

    private void openWhatsapp() {
        if (!isWhatsappInstalled()) {
            return;
        }
        Uri uri = Uri.parse("smsto:" + Constants.FEEDBACK_WHATSAPP);
        Intent i = new Intent(Intent.ACTION_SENDTO, uri);
        i.setPackage("com.whatsapp");
        startActivity(i);
    }

    private void openEmail() {
        Intent emailIntent = new Intent(Intent.ACTION_SENDTO,
                Uri.fromParts("mailto", Constants.FEEDBACK_MAIL, null));
        startActivity(Intent.createChooser(emailIntent, getString(R.string.send_mail)));
    }

    @Override
    public void closeDialog() {
        getTargetFragment().onActivityResult(getTargetRequestCode(), Activity.RESULT_OK, getActivity().getIntent());
        dismiss();
    }

    private boolean isWhatsappInstalled() {
        try {
            PackageManager pm = getActivity().getPackageManager();
            pm.getPackageInfo("com.whatsapp", 0);
            return true;
        } catch (PackageManager.NameNotFoundException e) {
            return false;
        }
    }

    @Override
    public void onDismiss(DialogInterface dialog) {
        super.onDismiss(dialog);
    }

    @Override
    public void handleAPIException(APIException e) {
        //do nothing
    }

    @Override
    public void handleConnectionTimeOutException(ConnectionTimeOutException e) {
        //do nothing
    }

    @Override
    public void handleUnknownException(UnknownException e) {
        //do nothing
    }

    @Override
    public void handleNeedReAuthorizeException(NeedReAuthorizeException e) {

    }

    @Override
    public void showErrorDialog(String message) {
        new AlertDialog.Builder(getContext())
                .setMessage(message)
                .setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                })
                .create()
                .show();
    }

    @Override
    public String getUnknownExceptionMessage() {
        return getString(R.string.server_error);
    }

    @Override
    public String getConnectionTimeOutExceptionMessage() {
        return getString(R.string.connection_error);
    }

    @Override
    public void onPurchaseCountChanged(int count) {

    }
}