package com.solaitech.likecoin.ui.coupons.active;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import com.solaitech.likecoin.R;
import com.solaitech.likecoin.data.models.coupons.Coupon;
import com.solaitech.likecoin.data.models.enums.CouponTypeEnums;
import com.solaitech.likecoin.ui.base.recycler_view.RecyclerViewBaseFragment;
import com.solaitech.likecoin.ui.coupons.CouponsCallback;
import com.solaitech.likecoin.ui.coupons.detail.CouponDetailActivity;
import com.solaitech.likecoin.ui.menu.Search;

public class ActiveCouponsFragment extends RecyclerViewBaseFragment
        implements ActiveCouponsView, CouponsCallback, Search {

    private ActiveCouponsPresenter presenter;

    public static ActiveCouponsFragment newInstance() {
        return new ActiveCouponsFragment();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = super.onCreateView(inflater, container, savedInstanceState);
        initView(v);

        presenter = new ActiveCouponsPresenter(this, api);
        presenter.init();
        return v;
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.template_recycler_view_with_swipe_refresh_layout;
    }

    private void initView(View v) {
        initRecyclerView(v);
        initSwipeRefreshLayout(v);
    }

    @Override
    protected RecyclerView.LayoutManager getLayoutManager() {
        return getLinearLayoutManager();
    }

    @Override
    protected RecyclerView.Adapter getAdapter() {
        adapter = new ActiveCouponsAdapter(getContext(), objectsFilter);
        ((ActiveCouponsAdapter) adapter).setOnRecyclerViewItemClickListener(this);
        return adapter;
    }

    @Override
    protected void tryUploadDataAgain() {
        presenter.onTryToUploadDataAgainClick();
    }

    @Override
    protected void onSwipeRefresh() {
        super.onSwipeRefresh();
        presenter.onSwipeRefresh();
    }

    public void scrollToPosition(int position) {
        ((LinearLayoutManager) recyclerView.getLayoutManager()).scrollToPositionWithOffset(position, 0);
    }

    @Override
    public void updateAdapterData(List<? extends Object> coupons) {
        objects.clear();
        objects.addAll(coupons);

        objectsFilter.clear();
        objectsFilter.addAll(objects);

        adapter.notifyDataSetChanged();
    }

    @Override
    public void onItemClick(Object object) {
        super.onItemClick(object);
        presenter.onItemClick(object);
    }

    @Override
    public void openCouponDetailActivity(String couponId) {
        Intent intent = CouponDetailActivity.getIntent(getContext(), couponId, CouponTypeEnums.ACTIVE);
        startActivity(intent);
    }

    @Override
    public void onBottomNavigationTabReselected() {
        presenter.onBottomNavigationTabReselected();
        scrollToPosition(0);
    }

    @Override
    public void doSearch(String text) {
        presenter.onSearch(text);
    }

    @Override
    public void showAllListItems() {
        presenter.onSearchShowAllListItems();
    }

    @Override
    public List<Object> getObjects() {
        return objects;
    }

    @Override
    public void clearFilterObjects() {
        objectsFilter.clear();
        adapter.notifyDataSetChanged();
    }

    @Override
    public void onSearchObjectFound(Coupon coupon) {
        objectsFilter.add(coupon);
        adapter.notifyItemInserted(objectsFilter.size());
    }

    @Override
    public void onDestroyView() {
        presenter.onDestroyView();
        super.onDestroyView();
    }

}