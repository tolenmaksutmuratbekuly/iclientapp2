package com.solaitech.likecoin.ui.profile;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;

import com.solaitech.likecoin.BuildConfig;
import com.solaitech.likecoin.R;
import com.solaitech.likecoin.data.models.user.User;
import com.solaitech.likecoin.data.models.user.UserProfileQR;
import com.solaitech.likecoin.data.preferences.Preferences;
import com.solaitech.likecoin.data.repository.user.UserRepositoryProvider;
import com.solaitech.likecoin.exceptions.APIException;
import com.solaitech.likecoin.exceptions.ConnectionTimeOutException;
import com.solaitech.likecoin.exceptions.NeedReAuthorizeException;
import com.solaitech.likecoin.exceptions.UnknownException;
import com.solaitech.likecoin.network.API;
import com.solaitech.likecoin.network.error.RetrofitErrorHandler;
import com.solaitech.likecoin.ui.profile.edit_profile.UpdateProfileIntentService;
import com.solaitech.likecoin.utils.Logger;
import com.solaitech.likecoin.utils.StringUtils;
import com.solaitech.likecoin.utils.ToastUtils;

import okhttp3.ResponseBody;
import rx.Subscriber;
import rx.functions.Action0;

class ProfilePresenter {
    private Context context;
    private ProfileView view;
    private Preferences preferences;
    private static final int TOKEN_TYPE_USER_DEVICE = 3;
    private API api;

    ProfilePresenter(final Context context, ProfileView view, Preferences preferences, API api) {
        this.context = context;
        this.view = view;
        this.preferences = preferences;
        this.api = api;

        setUserInfo(view.getUser());
    }

    void setUserInfo(User user) {
        if (user == null)
            return;

        if (user.getAmbassadorRoles() != null && user.getAmbassadorRoles().size() > 0) {
            view.setAmbassadorRoles(user.getAmbassadorRoles());
        } else {
            view.hideAmbassadorRoles();
        }

        if (user.getUserLevel() != null && user.getUserLevel().getLevel() != null) {
            view.showUserLevel(user.getUserLevel().getLevel());
        } else {
            view.showUserLevel(0);
        }

        if (user.getAvatarUrl() != null) {
            view.showProfileImage(user.getAvatarUrl());
        }

        if (user.getName() != null) {
            view.showUserName(user.getName());
        } else {
            view.showUserName(StringUtils.EMPTY_STRING);
        }

        if (user.getSurname() != null) {
            view.showSurname(user.getSurname());
        } else {
            view.showSurname(StringUtils.EMPTY_STRING);
        }

        if (user.getCellPhone() != null) {
            view.showPhoneNumber(user.getCellPhone());
        } else {
            view.showPhoneNumber(StringUtils.EMPTY_STRING);
        }

        if (user.getEmail() != null) {
            view.showEmail(user.getEmail());
        } else {
            view.showEmail(StringUtils.EMPTY_STRING);
        }

        if (user.getLanguage() != null) {
            view.setLanguage(user.getLanguage());
        }
    }

    void onEditProfileClick() {
        view.openEditProfileDialog(view.getUser());
    }

    void onChangeLanguageClick(String language) {
        User user = view.getUser();
        if (user != null) {
            Bundle bundle = new Bundle();
            if (user.getName() != null)
                bundle.putString(UpdateProfileIntentService.NAME, user.getName());

            if (user.getSurname() != null)
                bundle.putString(UpdateProfileIntentService.SURNAME, user.getSurname());

            if (user.getEmail() != null)
                bundle.putString(UpdateProfileIntentService.EMAIL, user.getEmail());

            bundle.putString(UpdateProfileIntentService.LANGUAGE, language);

            Intent intent = new Intent(context, UpdateProfileIntentService.class);
            intent.putExtras(bundle);
            context.startService(intent);
        }
    }

    void onShowQrCodeClick() {
        UserRepositoryProvider.provideRepository(api)
                .getUserProfileToken()
                .doOnSubscribe(new Action0() {
                    @Override
                    public void call() {
                        view.showLoadingIndicator(true);
                    }
                })
                .doOnTerminate(new Action0() {
                    @Override
                    public void call() {
                        view.showLoadingIndicator(false);
                    }
                })
                .subscribe(new Subscriber<UserProfileQR>() {
                    @Override
                    public void onCompleted() {
                        Logger.e(getClass(), "onCompleted");
                    }

                    @Override
                    public void onError(Throwable e) {
                        Logger.e(getClass(), "onError=" + e.getMessage());
                        if (BuildConfig.IS_DEBUG) {
                            e.printStackTrace();
                        }
                        try {
                            RetrofitErrorHandler.handleException(e);
                        } catch (APIException e1) {
                            ToastUtils.showOnHandler(context, StringUtils.replaceNull(e1.getErrorDescr()));
                        } catch (UnknownException e1) {
                            ToastUtils.showOnHandler(context, context.getString(R.string.server_error));
                        } catch (ConnectionTimeOutException e1) {
                            ToastUtils.showOnHandler(context, context.getString(R.string.connection_error));
                        } catch (NeedReAuthorizeException e1) {
                            e1.printStackTrace();
                        }
                    }

                    @Override
                    public void onNext(UserProfileQR responseBody) {
                        Logger.e(getClass(), "onNext");
                        if (StringUtils.isStringOk(responseBody.getKey())) {
                            view.showQrCode(responseBody.getKey());
                        }
                    }
                });
    }

    void onLogoutClick() {
        UserRepositoryProvider.provideRepository(api)
                .deleteUserDeviceToken(TOKEN_TYPE_USER_DEVICE, preferences.getUserDeviceToken())
                .doOnSubscribe(new Action0() {
                    @Override
                    public void call() {
                        view.showLoadingIndicator(true);
                    }
                })
                .doOnTerminate(new Action0() {
                    @Override
                    public void call() {
                        view.showLoadingIndicator(false);
                    }
                })
                .subscribe(new Subscriber<ResponseBody>() {
                    @Override
                    public void onCompleted() {
                        Logger.e(getClass(), "onCompleted");
                    }

                    @Override
                    public void onError(Throwable e) {
                        Logger.e(getClass(), "onError=" + e.getMessage());
                        if (BuildConfig.IS_DEBUG) {
                            e.printStackTrace();
                        }
                        try {
                            RetrofitErrorHandler.handleException(e);
                        } catch (APIException e1) {
                            ToastUtils.showOnHandler(context, StringUtils.replaceNull(e1.getErrorDescr()));
                        } catch (UnknownException e1) {
                            ToastUtils.showOnHandler(context, context.getString(R.string.server_error));
                        } catch (ConnectionTimeOutException e1) {
                            ToastUtils.showOnHandler(context, context.getString(R.string.connection_error));
                        } catch (NeedReAuthorizeException e1) {
                            e1.printStackTrace();
                        }
                    }

                    @Override
                    public void onNext(ResponseBody responseBody) {
                        Logger.e(getClass(), "onNext");
                        preferences.clearAllPreferences();
                        view.openRegistrationActivity();
                    }
                });
    }

    void onRemoveClick(String merchantId, int roleId) {
        UserRepositoryProvider.provideRepository(api)
                .deleteUserRole(merchantId, roleId)
                .doOnSubscribe(new Action0() {
                    @Override
                    public void call() {
                        view.showLoadingIndicator(true);
                    }
                })
                .doOnTerminate(new Action0() {
                    @Override
                    public void call() {
                        view.showLoadingIndicator(false);
                    }
                })
                .subscribe(new Subscriber<ResponseBody>() {
                    @Override
                    public void onCompleted() {
                        Logger.e(getClass(), "onCompleted");
                    }

                    @Override
                    public void onError(Throwable e) {
                        Logger.e(getClass(), "onError=" + e.getMessage());
                        if (BuildConfig.IS_DEBUG) {
                            e.printStackTrace();
                        }
                        try {
                            RetrofitErrorHandler.handleException(e);
                        } catch (APIException e1) {
                            ToastUtils.showOnHandler(context, StringUtils.replaceNull(e1.getErrorDescr()));
                        } catch (UnknownException e1) {
                            ToastUtils.showOnHandler(context, context.getString(R.string.server_error));
                        } catch (ConnectionTimeOutException e1) {
                            ToastUtils.showOnHandler(context, context.getString(R.string.connection_error));
                        } catch (NeedReAuthorizeException e1) {
                            e1.printStackTrace();
                        }
                    }

                    @Override
                    public void onNext(ResponseBody responseBody) {
                        view.sendBroadCast();
                    }
                });
    }

    void onEditProfileImageClick() {
        view.openEditProfileImageDialog();
    }

    void onMakePhotoClick() {
        view.dispatchTakePictureIntent();
    }

    void onDeleteProfileImageClick() {
        view.startServiceToDeleteProfileImage();
    }

    void onCropActivityResultOk(Uri uri) {
        if (uri != null) {
            view.startServiceToUploadProfileImage(uri);
        }
    }

    void onSelectProfileImageClick() {
        if (view.isPermissionGrantedToStorage()) {
            view.selectProfileImageFromIntent();
        } else {
            if (view.shouldShowRequestPermissionRationaleToStorage()) {
                view.showRequestPermissionRationaleToStorage();
            } else {
                view.requestPermissionToStorage();
            }
        }
    }

    void onRationaleToStorageAllowed() {
        view.requestPermissionToStorage();
    }

    void onPermissionGrantedToStorage() {
        view.selectProfileImageFromIntent();
    }

    void onPermissionNotGrantedToStorage() {
        if (view.shouldShowRequestPermissionRationaleToStorage()) {
            view.showRequestPermissionRationaleToStorage();
        } else {
            view.showRequestPermissionDialogToStorage();
        }
    }

    void onDialogToStorageAllowed() {
        view.openSettingsActivity();
    }

    void onActivityResultToStorage() {
        if (view.isPermissionGrantedToStorage()) {
            view.selectProfileImageFromIntent();
        }
    }
}

/*
https://github.com/wg/scrypt
https://github.com/web3j/web3j
https://stackoverflow.com/questions/22730620/bad-apk-when-using-bitcoinj-from-maven-central
https://github.com/web3j/web3j/issues/157
https://github.com/wg/scrypt/tree/master/src/android/resources/lib/arm5

https://github.com/web3j/web3j/issues/193

    packagingOptions {
        exclude 'META-INF/DEPENDENCIES.txt'
        exclude 'META-INF/LICENSE.txt'
        exclude 'META-INF/NOTICE.txt'
        exclude 'META-INF/NOTICE'
        exclude 'META-INF/LICENSE'
        exclude 'META-INF/DEPENDENCIES'
        exclude 'META-INF/notice.txt'
        exclude 'META-INF/license.txt'
        exclude 'META-INF/dependencies.txt'
        exclude 'META-INF/LGPL2.1'
    }


    compile 'org.web3j:core-android:2.2.1'

    import geth module in project

 */