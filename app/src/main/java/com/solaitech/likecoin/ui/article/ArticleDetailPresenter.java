package com.solaitech.likecoin.ui.article;

import com.solaitech.likecoin.data.models.article.Article;
import com.solaitech.likecoin.data.params.BuyArticleParams;
import com.solaitech.likecoin.data.repository.article.ArticleRepositoryProvider;
import com.solaitech.likecoin.data.repository.like.LikesRepositoryProvider;
import com.solaitech.likecoin.network.API;
import com.solaitech.likecoin.ui.base.PresenterErrorHandler;
import com.solaitech.likecoin.utils.StringUtils;

import okhttp3.ResponseBody;
import rx.Subscriber;
import rx.Subscription;
import rx.functions.Action0;
import rx.subscriptions.CompositeSubscription;

class ArticleDetailPresenter {
    private ArticleDetailView view;
    private API api;
    private CompositeSubscription compositeSubscription = new CompositeSubscription();

    ArticleDetailPresenter(ArticleDetailView view, API api) {
        this.view = view;
        this.api = api;
    }

    public void init() {
    }

    private void getArticleById() {
        Subscription subscription = ArticleRepositoryProvider
                .provideRepository(api)
                .getArticleById(view.getArticleId())
                .doOnSubscribe(new Action0() {
                    @Override
                    public void call() {
                        if (view.isAppBarLayoutExpanded()) {
                            view.hideAppBarLayout();
                        }
                        view.hideBuyArticleView();
                        view.showProgressTemplateView();
                    }
                })
                .subscribe(new Subscriber<Article>() {
                    @Override
                    public void onCompleted() {
                        view.showAppBarLayout();
                        view.showSuccessTemplateView();
                    }

                    @Override
                    public void onError(Throwable e) {
                        PresenterErrorHandler.handleError(e, view);
                    }

                    @Override
                    public void onNext(Article article) {
                        view.showImageLarge(article.getImgUrl());
                        view.setArticleDetail(article);
                        view.setMerchant(article.getMerchant());

                        if (article.getTitle() != null) {
                            view.setToolbarTitle(article.getTitle());
                        } else {
                            view.setToolbarTitle(StringUtils.EMPTY_STRING);
                        }

                        if (article.getOwned() != null && !article.getOwned() && article.getPrice() != null) {
                            view.setBuyState(false);
                            view.showBuyArticleView();
                            view.showArticlePrice(String.valueOf(article.getPrice()));
                        } else {
                            view.setBuyState(true);
                            view.hideBuyArticleView();
                        }
                        view.showCommentEdiText();
                    }
                });
        compositeSubscription.add(subscription);
    }

    void onAllViewPagerPagesInitialized() {
        getArticleById();
    }

    public void onDestroyView() {
        unsubscribe();
    }

    private void unsubscribe() {
        if (compositeSubscription != null
                && !compositeSubscription.isUnsubscribed()) {
            compositeSubscription.unsubscribe();
        }
    }

    void onBuyArticleClick() {
        if (view.getArticleId() != null) {
            buyArticle(new BuyArticleParams(view.getArticleId()));
        }
    }

    void onTryUploadDataAgainClick() {
        getArticleById();
    }

    private void buyArticle(BuyArticleParams params) {
        Subscription subscription = LikesRepositoryProvider
                .provideRepository(api)
                .buyArticle(params)
                .doOnSubscribe(new Action0() {
                    @Override
                    public void call() {
                        view.showProgressDialog();
                    }
                })
                .doOnTerminate(new Action0() {
                    @Override
                    public void call() {
                        view.dismissProgressDialog();
                    }
                })
                .subscribe(new Subscriber<ResponseBody>() {
                    @Override
                    public void onCompleted() {
                        view.showSuccessToastMessage();
                        view.sendBroadcast();
                    }

                    @Override
                    public void onError(Throwable e) {
                        PresenterErrorHandler.handleError2(e, view);
                    }

                    @Override
                    public void onNext(ResponseBody responseBody) {
                        view.setBuyState(true);
                        view.showBuyArticleView();
                    }
                });

        compositeSubscription.add(subscription);
    }
}