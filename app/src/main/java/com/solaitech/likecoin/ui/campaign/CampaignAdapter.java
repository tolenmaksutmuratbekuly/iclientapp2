package com.solaitech.likecoin.ui.campaign;

import android.content.Context;
import android.graphics.drawable.GradientDrawable;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import com.solaitech.likecoin.R;
import com.solaitech.likecoin.data.models.campaign.Campaign;
import com.solaitech.likecoin.ui.base.recycler_view.RecyclerViewBaseAdapter;
import com.solaitech.likecoin.utils.CustomColorUtils;
import com.solaitech.likecoin.utils.DateTimeUtils;
import com.solaitech.likecoin.utils.ImageUtils;

class CampaignAdapter extends RecyclerViewBaseAdapter {
    private static final int VIEW_TYPE_CAMPAIGN = 1;
    static final int VIEW_TYPE_FIRST_ELEMENT_EMPTY_SPACE = 2;

    private List<Object> objects;

    CampaignAdapter(Context context, List<Object> objects) {
        super(context);
        this.objects = objects;
    }

    @Override
    public MainViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        switch (viewType) {
            case VIEW_TYPE_CAMPAIGN:
                return new CampaignViewHolder(inflate(parent, R.layout.adapter_promotions));
            case VIEW_TYPE_FIRST_ELEMENT_EMPTY_SPACE:
                return new FirstElementEmptySpaceViewHolder(inflate(parent, R.layout.adapter_first_element_empty_space)
                );
            default:
                throw incorrectOnCreateViewHolder();
        }
    }

    class CampaignViewHolder extends MainViewHolder {
        @Bind(R.id.iv) ImageView iv;
        @Bind(R.id.tv_price) TextView tv_price;
        @Bind(R.id.tv_title) TextView tv_title;
        @Bind(R.id.tv_merchant_title) TextView tv_merchant_title;
        @Bind(R.id.tv_description) TextView tv_description;
        @Bind(R.id.tv_date) TextView tv_date;
        @Bind(R.id.tv_level_title) TextView tv_level_title;
        @Bind(R.id.tv_level) TextView tv_level;
        @Bind(R.id.vg_price_container) ViewGroup vg_price_container;

        Campaign bindedCampaign;
        CampaignViewHolder(View v) {
            super(v);
            ButterKnife.bind(this, v);
        }

        void bind(Campaign campaign) {
            bindedCampaign = campaign;
            if (campaign == null) {
                return;
            }

            if (campaign.isActive()) {
                vg_price_container.setVisibility(View.VISIBLE);

                if (campaign.getCouponPrice() != null) {
                    tv_price.setText(String.valueOf(campaign.getCouponPrice()));
                } else {
                    tv_price.setText("-");
                }
            } else {
                vg_price_container.setVisibility(View.GONE);
            }

            if (campaign.getTitle() != null) {
                tv_title.setText(campaign.getTitle());
            } else {
                tv_title.setText("-");
            }

            if (campaign.getMerchant() != null
                    && campaign.getMerchant().getTitle() != null) {
                tv_merchant_title.setText(campaign.getMerchant().getTitle());
            } else {
                tv_merchant_title.setText("-");
            }

            if (campaign.getShortDescr() != null) {
                tv_description.setText(campaign.getShortDescr());
            } else {
                tv_description.setText("-");
            }

            if (campaign.getCouponStartDt() != null) {
                tv_date.setText(DateTimeUtils.newsFeedDate(campaign.getCouponStartDt(), getContext()));
            }

            if (campaign.getMinUserlevel() != null) {
                int level = campaign.getMinUserlevel();
                tv_level.setText(String.valueOf(level));
                GradientDrawable drawable = (GradientDrawable) tv_level.getBackground();
                CustomColorUtils.setLevelColor(getContext(), drawable, level, tv_level_title);
            }
            ImageUtils.showCampaignImgSmall(getContext(), iv, campaign.getSmallImgUrl());
        }

        @OnClick(R.id.vg_container)
        void onItemViewClick() {
            onItemClick(bindedCampaign);
        }
    }

    @Override
    public void onBindViewHolder(MainViewHolder holder, int position) {
        switch (holder.getItemViewType()) {
            case VIEW_TYPE_CAMPAIGN:
                ((CampaignViewHolder) holder).bind((Campaign) objects.get(position));
                break;
        }
    }

    @Override
    public int getItemViewType(int position) {
        if (objects.get(position) instanceof Campaign) {
            return VIEW_TYPE_CAMPAIGN;
        } else if (objects.get(position) instanceof Integer) {
            return (Integer) objects.get(position);
        }

        throw incorrectGetItemViewType();
    }

    @Override
    public int getItemCount() {
        return objects.size();
    }

}