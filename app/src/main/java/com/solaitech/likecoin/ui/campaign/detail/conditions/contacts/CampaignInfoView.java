package com.solaitech.likecoin.ui.campaign.detail.conditions.contacts;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.solaitech.likecoin.R;
import com.solaitech.likecoin.utils.EmailUtils;
import com.solaitech.likecoin.utils.chrome_customtabs.ChromeCustomTabs;

public class CampaignInfoView extends LinearLayout implements View.OnClickListener {
    private TextView tv_info;
    private View vLine;
    private int viewType;

    public CampaignInfoView(Context context) {
        super(context);
        inflate();
    }

    public CampaignInfoView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        inflate();
    }

    public CampaignInfoView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        inflate();
    }

    private void inflate() {
        LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflater.inflate(R.layout.v_campaign_info, this);

        initView();
    }

    private void initView() {
        tv_info = findViewById(R.id.tv_info);
        tv_info.setOnClickListener(this);
        vLine = findViewById(R.id.v_line);
        vLine.setVisibility(GONE);
    }

    public void showInfoView(String info, int viewType) {
        this.viewType = viewType;
        tv_info.setText(info);
    }

    public void hideLine() {
        vLine.setVisibility(GONE);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.tv_info:
                if (viewType == CampaignInfoListView.VIEW_CONTACTS ||
                        viewType == CampaignInfoListView.VIEW_EMAILS) {
                    makeCall(tv_info.getText().toString());
                } else if (viewType == CampaignInfoListView.VIEW_LINKS) {
                    String link = tv_info.getText().toString();
                    new ChromeCustomTabs().loadWebSite(getContext(), link);
                }
                break;
        }
    }

    private void makeCall(String phone) {
        if (EmailUtils.isEmailValid(phone)) {
            final Intent emailIntent = new Intent(android.content.Intent.ACTION_SEND);
            emailIntent.setType("plain/text");
            emailIntent.putExtra(android.content.Intent.EXTRA_EMAIL, new String[]{phone});
            getContext().startActivity(Intent.createChooser(emailIntent, getContext().getString(R.string.send)));
        } else {
            Intent callIntent = new Intent(Intent.ACTION_DIAL);
            callIntent.setData(Uri.parse("tel:" + phone));
            getContext().startActivity(callIntent);
        }
    }
}