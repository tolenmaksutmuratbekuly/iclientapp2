package com.solaitech.likecoin.ui.contacts.send_like;

import com.solaitech.likecoin.data.models.news_feed.NewsOwner;
import com.solaitech.likecoin.data.models.user.User;
import com.solaitech.likecoin.ui.base.interfaces.MVPBaseErrorView;

interface SendLikeView extends MVPBaseErrorView {
    User getDstUser();

    NewsOwner getOwner();

    boolean isUserProfile();

    void showProgressDialog();

    void dismissProgressDialog();

    void showSuccessToastMessage();

    void closeDialog();
}