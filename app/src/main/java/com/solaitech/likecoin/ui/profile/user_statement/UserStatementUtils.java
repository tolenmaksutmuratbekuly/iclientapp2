package com.solaitech.likecoin.ui.profile.user_statement;

import android.content.Context;
import android.text.SpannableStringBuilder;
import android.text.Spanned;

import java.math.BigDecimal;

import com.solaitech.likecoin.R;
import com.solaitech.likecoin.data.models.user_statement.Partner;
import com.solaitech.likecoin.utils.CenteredImageSpan;
import com.solaitech.likecoin.utils.StringUtils;

class UserStatementUtils {
    private static final int TYPE_OUTGOING = 1;
    private static final int TYPE_INCOMING = 2;
    private static final int TYPE_UNKNOWN = 3;

    static int getOperationType(BigDecimal amount) {
        if (amount != null) {
            if (amount.intValue() >= 0) {
                return TYPE_INCOMING;
            } else {
                return TYPE_OUTGOING;
            }
        }
        return TYPE_UNKNOWN;
    }

    static CharSequence getOperationText(int operationType, Context context, BigDecimal amount, String description) {
        switch (operationType) {
            case TYPE_OUTGOING:
                return getOutgoingOperationText(context, getAmount(amount), description);
            case TYPE_INCOMING:
                return getIncomingOperationText(context, getAmount(amount), description);
            default:
                return StringUtils.EMPTY_STRING;
        }
    }

    static String getFullName(Partner partner) {
        String fullName = StringUtils.EMPTY_STRING;
        if (partner.getName() != null) {
            fullName = partner.getName();
        }

        if (partner.getSurname() != null) {
            fullName += " " + partner.getSurname();
        }

        return fullName;
    }

    private static CharSequence getOutgoingOperationText(Context context, String amount, String description) {
        SpannableStringBuilder spannableStringBuilder = new SpannableStringBuilder();
        spannableStringBuilder.append(context.getString(R.string.outgoing_user_statement, amount));

        spannableStringBuilder.append(" ");
        CenteredImageSpan centeredImageSpan = new CenteredImageSpan(context, R.drawable.ic_like_blue);
        spannableStringBuilder.setSpan(centeredImageSpan, spannableStringBuilder.length() - 1, spannableStringBuilder.length(), Spanned.SPAN_INCLUSIVE_EXCLUSIVE);

        spannableStringBuilder.append(description.length() > 0 ? "\n\"" + description + "\"" : StringUtils.EMPTY_STRING);
        return spannableStringBuilder;
    }

    private static CharSequence getIncomingOperationText(Context context, String amount, String description) {
        SpannableStringBuilder spannableStringBuilder = new SpannableStringBuilder();
        spannableStringBuilder.append(context.getString(R.string.incoming_user_statement, amount));

        spannableStringBuilder.append(" ");
        CenteredImageSpan centeredImageSpan = new CenteredImageSpan(context, R.drawable.ic_like_yellow);
        spannableStringBuilder.setSpan(centeredImageSpan, spannableStringBuilder.length() - 1, spannableStringBuilder.length(), Spanned.SPAN_INCLUSIVE_EXCLUSIVE);
        
        spannableStringBuilder.append(description.length() > 0 ? "\n\"" + description + "\"" : StringUtils.EMPTY_STRING);
        return spannableStringBuilder;
    }

    private static String getAmount(BigDecimal amount) {
        return String.valueOf(Math.abs(amount.intValue()));
    }

    static int getOperationViewBgResId(int operationType) {
        switch (operationType) {
            case TYPE_OUTGOING:
                return R.drawable.shape_outgoing_user_statement;
            case TYPE_INCOMING:
                return R.drawable.shape_incoming_user_statement;
            default:
                return R.drawable.shape_unknown_user_statement;
        }
    }

    static int getOperationImgResId(int operationType) {
        switch (operationType) {
            case TYPE_OUTGOING:
                return R.drawable.ic_arrow_right_white;
            case TYPE_INCOMING:
                return R.drawable.ic_arrow_left_white;
            default:
                return R.drawable.shape_unknown_user_statement;
        }
    }
}