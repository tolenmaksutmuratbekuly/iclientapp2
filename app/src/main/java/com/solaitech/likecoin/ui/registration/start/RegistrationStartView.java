package com.solaitech.likecoin.ui.registration.start;

import com.solaitech.likecoin.ui.base.interfaces.MVPBaseView;

interface RegistrationStartView extends MVPBaseView {

    boolean isPermissionGrantedToSms();

    boolean shouldShowRequestPermissionRationaleToSms();

    void requestPermissionToSms();

    String getPhoneNumber();

    String getEmail();

    String getName();

    String getSurname();

    void showPhoneNumberEmptyError();

    void showPhoneNumberInvalidError();

    void showEmailInvalidError();

    void showNameEmptyError();

    void showSurnameEmptyError();

    void openRegistrationCompleteDialog(String phoneNumber, String email, String name, String surname, boolean isRegistered);

    void showRegistrationLayout();
}