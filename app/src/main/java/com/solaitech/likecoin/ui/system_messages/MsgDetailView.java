package com.solaitech.likecoin.ui.system_messages;

import com.solaitech.likecoin.data.models.article.Article;
import com.solaitech.likecoin.data.models.campaign.Merchant;
import com.solaitech.likecoin.ui.base.interfaces.MVPBaseView;

interface MsgDetailView extends MVPBaseView {

    String getArticleId();

    void setArticleDetail(Article articleDetail);

    void setMerchant(Merchant merchant);

    void setToolbarTitle(String title);

    void showCommentEdiText();

    void showSuccessToastMessage();

    void sendBroadcast();
}