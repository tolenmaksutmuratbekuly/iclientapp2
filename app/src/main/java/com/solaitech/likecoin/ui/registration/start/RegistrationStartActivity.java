package com.solaitech.likecoin.ui.registration.start;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;

import com.solaitech.likecoin.R;
import com.solaitech.likecoin.ui.base.BaseActivityFrgm;

public class RegistrationStartActivity extends BaseActivityFrgm {
    public static Intent newLogoutIntent(Context applicationContext) {
        return new Intent(applicationContext, RegistrationStartActivity.class).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
    }

    public static Intent getIntent(Context context) {
        return new Intent(context, RegistrationStartActivity.class);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration_start);
        if (savedInstanceState == null) {
            replaceFragment(RegistrationStartFragment.newInstance());
        }
    }
}