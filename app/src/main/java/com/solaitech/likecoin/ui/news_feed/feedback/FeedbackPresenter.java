package com.solaitech.likecoin.ui.news_feed.feedback;

import com.solaitech.likecoin.network.API;
import rx.subscriptions.CompositeSubscription;

class FeedbackPresenter {
    private FeedbackView view;
    private API api;
    private CompositeSubscription compositeSubscription = new CompositeSubscription();

    FeedbackPresenter(FeedbackView view, API api) {
        this.view = view;
        this.api = api;
    }

    public void init() {

    }
}