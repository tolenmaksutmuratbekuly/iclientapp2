package com.solaitech.likecoin.ui.intro;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.widget.Button;

import com.solaitech.likecoin.R;
import com.solaitech.likecoin.ui.registration.start.RegistrationStartActivity;
import com.solaitech.likecoin.utils.support.FragmentArrayAdapter;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.OnPageChange;
import me.relex.circleindicator.CircleIndicator;

public class TutorialActivity extends AppCompatActivity {
    FragmentArrayAdapter pagerAdapter;
    @Bind(R.id.pager) ViewPager pager;
    @Bind(R.id.pager_indicator) CircleIndicator pagerIndicator;
    @Bind(R.id.btn_continue) Button btn_continue;

    public static Intent getIntent(Context context) {
        return new Intent(context, TutorialActivity.class);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_tutorial);
        ButterKnife.bind(this);
        configureViewPager();
    }

    void configureViewPager() {
        pagerAdapter = new FragmentArrayAdapter(getSupportFragmentManager());
        pagerAdapter.addFragment(FrgmTutorials.newInstance(R.layout.frgm_tutor1));
        pagerAdapter.addFragment(FrgmTutorials.newInstance(R.layout.frgm_tutor2));
        pagerAdapter.addFragment(FrgmTutorials.newInstance(R.layout.frgm_tutor3));
        pagerAdapter.addFragment(FrgmTutorials.newInstance(R.layout.frgm_tutor4));
        pager.setAdapter(pagerAdapter);
        pagerIndicator.setViewPager(pager);
    }

    @OnPageChange(value = R.id.pager, callback = OnPageChange.Callback.PAGE_SELECTED)
    void onPageSelected(int position) {
        if (position == pagerAdapter.getCount() - 1) {
            btn_continue.setText(R.string.action_start);
        } else {
            btn_continue.setText(R.string.action_continue);
        }
    }

    @OnClick(R.id.btn_continue)
    void onContinueClick() {
        if (pager.getCurrentItem() < pagerAdapter.getCount() - 1) {
            pager.setCurrentItem(pager.getCurrentItem() + 1);
        } else {
            startActivity(RegistrationStartActivity.getIntent(this));
            finish();
        }
    }
}