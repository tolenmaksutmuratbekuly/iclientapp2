package com.solaitech.likecoin.ui.base.loading;

import android.animation.Animator;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;
import android.view.View;

import com.solaitech.likecoin.R;
import com.solaitech.likecoin.exceptions.APIException;
import com.solaitech.likecoin.exceptions.ConnectionTimeOutException;
import com.solaitech.likecoin.exceptions.NeedReAuthorizeException;
import com.solaitech.likecoin.exceptions.UnknownException;
import com.solaitech.likecoin.ui.base.interfaces.MVPBaseView;
import com.solaitech.likecoin.ui.base.loading.views.TemplateFailView;
import com.solaitech.likecoin.ui.base.loading.views.TemplateProgressView;

public class LoadingBase implements MVPBaseView {
    private Context context;
    private MVPBaseView view;

    private View vSuccess;
    private TemplateFailView vFail;
    private TemplateProgressView vProgress;

    private ProgressDialog progressDialog;

    public LoadingBase(
            Context context,
            MVPBaseView view,
            View vSuccess, TemplateFailView vFail, TemplateProgressView vProgress) {
        this.context = context;
        this.view = view;
        this.vSuccess = vSuccess;
        this.vFail = vFail;
        this.vProgress = vProgress;
    }

    @Override
    public void showProgressTemplateView() {
        setProgressVisibility(true);

        setFailVisibility(false);
        setSuccessVisibility(false);
    }

    @Override
    public void showProgressTemplateView(String title) {
        vProgress.setMessage(title);
        showProgressTemplateView();
    }

    @Override
    public void showSuccessTemplateView() {
        setSuccessVisibility(true);

        setProgressVisibility(false);
        setFailVisibility(false);
    }

    @Override
    public void showFailTemplateView() {
        setFailVisibility(true);

        setProgressVisibility(false);
        setSuccessVisibility(false);
    }

    @Override
    public void showFailTemplateView(String message) {
        vFail.setMessage(message);
        showFailTemplateView();
    }

    private void setProgressVisibility(boolean isVisible) {
        if (isVisible) {
            vProgress.setVisibility(View.VISIBLE);
        } else {
            vProgress.setVisibility(View.GONE);
        }
    }

    private void setSuccessVisibility(boolean isVisible) {
        if (isVisible) {
            vSuccess.setVisibility(View.VISIBLE);
            vSuccess.setAlpha(0.0f);
            vSuccess.animate().alpha(1.0f).setDuration(1000).setListener(new Animator.AnimatorListener() {
                @Override
                public void onAnimationStart(Animator animator) {

                }

                @Override
                public void onAnimationEnd(Animator animator) {
                    if (vSuccess.getVisibility() != View.VISIBLE) {
                        vSuccess.setVisibility(View.VISIBLE);
                    }
                    vSuccess.animate().setListener(null);
                }

                @Override
                public void onAnimationCancel(Animator animator) {

                }

                @Override
                public void onAnimationRepeat(Animator animator) {

                }
            });
        } else {
            vSuccess.setVisibility(View.INVISIBLE);
        }
    }

    private void setFailVisibility(boolean isVisible) {
        vSuccess.animate().setListener(null);
        if (isVisible) {
            vFail.setVisibility(View.VISIBLE);
        } else {
            vFail.setVisibility(View.GONE);
        }
    }

    @Override
    public void handleAPIException(APIException e) {
        view.showFailTemplateView(e.getErrorDescr());
    }

    @Override
    public void handleConnectionTimeOutException(ConnectionTimeOutException e) {
        view.showFailTemplateView(getConnectionTimeOutExceptionMessage());
    }

    @Override
    public void handleUnknownException(UnknownException e) {
        view.showFailTemplateView(getUnknownExceptionMessage());
    }

    @Override
    public void handleNeedReAuthorizeException(NeedReAuthorizeException e) {

    }

    @Override
    public String getUnknownExceptionMessage() {
        return getString(R.string.server_error);
    }

    @Override
    public String getConnectionTimeOutExceptionMessage() {
        return getString(R.string.connection_error);
    }

    @Override
    public void showErrorDialog(String message) {
        new AlertDialog.Builder(context)
                .setMessage(message)
                .setPositiveButton(getString(R.string.ok), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                })
                .create()
                .show();
    }

    @Override
    public void showProgressDialog() {
        if (progressDialog == null
                || !progressDialog.isShowing()) {
            progressDialog = ProgressDialog.show(context, "", getString(R.string.loading), true, false);
        }
    }

    @Override
    public void dismissProgressDialog() {
        if (progressDialog != null && progressDialog.isShowing()) {
            progressDialog.dismiss();
        }
    }

    private String getString(int id) {
        return context.getString(id);
    }

}