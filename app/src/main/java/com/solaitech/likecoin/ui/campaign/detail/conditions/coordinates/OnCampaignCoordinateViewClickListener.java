package com.solaitech.likecoin.ui.campaign.detail.conditions.coordinates;

import com.solaitech.likecoin.data.models.campaign.Coordinate;

public interface OnCampaignCoordinateViewClickListener {

    void onCampaignCoordinateViewClick(Coordinate coordinate);

}