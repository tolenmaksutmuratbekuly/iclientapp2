package com.solaitech.likecoin.ui.dialogs;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.widget.DatePicker;

import java.util.Calendar;
import java.util.Date;

public class CustomDatePickerDialog extends DialogFragment implements DatePickerDialog.OnDateSetListener {

    private Date currentDate;
    private Date minDate;
    private Date maxDate;
    private Callback callback;

    public static CustomDatePickerDialog newInstance(Date currentDate, Date minDate, Date maxDate) {
        CustomDatePickerDialog d = new CustomDatePickerDialog();
        d.currentDate = currentDate;
        d.minDate = minDate;
        d.maxDate = maxDate;
        return d;
    }

    public void setCallback(Callback callback) {
        this.callback = callback;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        final Calendar calendar = Calendar.getInstance();

        if (currentDate != null) {
            calendar.setTime(currentDate);
        }

        int yy = calendar.get(Calendar.YEAR);
        int mm = calendar.get(Calendar.MONTH);
        int dd = calendar.get(Calendar.DAY_OF_MONTH);

        DatePickerDialog datePickerDialog = new DatePickerDialog(getActivity(), this, yy, mm, dd);

        if (minDate != null) {
            datePickerDialog.getDatePicker().setMinDate(minDate.getTime());
        }

        if (maxDate != null) {
            Calendar cal = Calendar.getInstance();
            cal.setTime(maxDate);
//            cal.add(Calendar.DATE, 1);
            datePickerDialog.getDatePicker().setMaxDate(cal.getTimeInMillis());
        }

        return datePickerDialog;
    }

    @Override
    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.YEAR, year);
        calendar.set(Calendar.MONTH, monthOfYear);
        calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);

        Date date = calendar.getTime();

        Bundle bundle = new Bundle();
        bundle.putSerializable("date", date);

        if (callback != null) {
            callback.onDateSet(bundle);
        }
        dismiss();
    }

    public interface Callback {
        void onDateSet(Bundle bundle);
    }
}