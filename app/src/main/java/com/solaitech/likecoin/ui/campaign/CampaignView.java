package com.solaitech.likecoin.ui.campaign;

import java.util.List;

import com.solaitech.likecoin.data.models.campaign.Campaign;
import com.solaitech.likecoin.ui.base.interfaces.MVPBaseView;

interface CampaignView extends MVPBaseView {

    void openCampaignDetailActivity(String id, boolean openCommentsTab);

    void updateAdapterData(List<? extends Object> campaigns);

    List<Object> getObjects();

    void clearFilterObjects();

    void onSearchObjectFound(Campaign campaign);
}
