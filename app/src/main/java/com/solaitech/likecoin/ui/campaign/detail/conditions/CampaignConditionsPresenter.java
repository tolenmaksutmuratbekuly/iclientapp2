package com.solaitech.likecoin.ui.campaign.detail.conditions;

import java.util.List;

import com.solaitech.likecoin.data.models.campaign.CampaignDetail;
import com.solaitech.likecoin.data.models.campaign.Coordinate;
import com.solaitech.likecoin.utils.DateTimeUtils;
import com.solaitech.likecoin.utils.HtmlSourceUtils;
import com.solaitech.likecoin.utils.StringUtils;

class CampaignConditionsPresenter {
    private CampaignConditionsView view;
    CampaignConditionsPresenter(CampaignConditionsView view) {
        this.view = view;
    }

    public void init() {
        if (view.isPermissionGrantedToLocation()) {
            view.hideAllowAccessToLocationButton();
        } else {
            view.showAllowAccessToLocationButton();
        }
    }

    void onGoogleApiClientConnected() {
        if (view.isPermissionGrantedToLocation()) {
            view.initLastLocation();
        }
    }

    void onCampaignDetailSet() {
        CampaignDetail campaign = view.getCampaignDetail();

        if (campaign != null) {

            if (StringUtils.isStringOk(campaign.getTitle())) {
                view.showTitle(campaign.getTitle());
            } else {
                view.hideTitle();
            }

            if (StringUtils.isStringOk(campaign.getShortDescr())) {
                view.showShortDescription(HtmlSourceUtils.styleTextForWebView(campaign.getShortDescr()));
            } else {
                view.hideShortDescription();
            }

            if (StringUtils.isStringOk(campaign.getFullDescr())) {
                view.showFullDescription(HtmlSourceUtils.styleTextForWebView(campaign.getFullDescr()));
            } else {
                view.hideFullDescription();
            }

            if (campaign.getCouponStartDt() != null && campaign.getExpiryDate() != null) {
                view.showDate(
                        DateTimeUtils.getCampaignConditionsDate(view.getContextFromPage(), campaign.getCouponStartDt()),
                        DateTimeUtils.getCampaignConditionsDate(view.getContextFromPage(), campaign.getExpiryDate()));
            } else {
                view.hideValidUntil();
            }

            if (campaign.getContacts() != null
                    && !campaign.getContacts().isEmpty()) {
                view.showContacts(campaign.getContacts());
            } else {
                view.hideContacts();
            }

            if (campaign.getUrlList() != null
                    && !campaign.getUrlList().isEmpty()) {
                view.showLinks(campaign.getUrlList());
            } else {
                view.hideLinks();
            }

            if (campaign.getCoordinates() != null
                    && !campaign.getCoordinates().isEmpty()) {
                view.setCoordinates(campaign.getCoordinates());
                view.showCoordinates(campaign.getCoordinates());
                view.displayMarkers(campaign.getCoordinates());

                Coordinate coordinate = campaign.getCoordinates().get(0);
                if (coordinate.getLatitude() != 0 && coordinate.getLongitude() != 0) {
                    view.moveCamera(coordinate.getLatitude(), coordinate.getLongitude());
                }
            } else {
                view.setCoordinates(null);
                view.hideCoordinates();
                view.clearMarkers();
            }

        } else {
            view.hideTitle();
            view.hideShortDescription();
            view.hideFullDescription();
            view.hideValidUntil();
            view.hideContacts();
            view.hideLinks();

            view.setCoordinates(null);
            view.hideCoordinates();
            view.clearMarkers();
        }
    }

    void onMapReady() {
        List<Coordinate> coordinates = view.getCoordinates();
        view.clearMarkers();

        if (coordinates != null
                && !coordinates.isEmpty()) {
            view.displayMarkers(coordinates);

            Coordinate coordinate = coordinates.get(0);
            view.moveCamera(coordinate.getLatitude(), coordinate.getLongitude());
        }
    }

    void onAllowAccessToLocationButtonClick() {
        if (view.isPermissionGrantedToLocation()) {
            onPermissionGrantedToLocation();
        } else {
//            if (view.shouldShowRequestPermissionRationaleToLocation()) {
//                view.showRequestPermissionRationaleToLocation();
//            } else {
            view.requestPermissionToLocation();
//            }
        }
    }

    void onRationaleToLocationAllowed() {
        view.requestPermissionToLocation();
        view.hideAllowAccessToLocationButton();
    }

    void onPermissionGrantedToLocation() {
        view.setMyLocationEnabled();
    }

    void onPermissionNotGrantedToLocation() {
        if (view.shouldShowRequestPermissionRationaleToLocation()) {
            view.showRequestPermissionRationaleToLocation();
        } else {
            view.showRequestPermissioDialogToLocation();
        }
    }

    void onDialogToLocationAllowed() {
        view.openSettingsActivity();
    }

    void onActivityResultToLocation() {
        if (view.isPermissionGrantedToLocation()) {
            view.setMyLocationEnabled();
            view.hideAllowAccessToLocationButton();
        }
    }

}