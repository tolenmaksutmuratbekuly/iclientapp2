package com.solaitech.likecoin.ui.campaign.detail.company;

import com.solaitech.likecoin.data.models.campaign.Merchant;

import java.util.List;

interface CampaignCompanyView {

    Merchant getMerchant();

    void showTitle(String title);

    void showShortDescription(String description);

    void showFullDescription(String description);

    void showLinks(List<String> links);

    void hideLinks();

    void hideTitle();

    void hideShortDescription();

    void hideFullDescription();

    void showContacts(List<String> contacts);

    void hideContacts();

    void showEmails(List<String> emails);

    void hideEmails();
}