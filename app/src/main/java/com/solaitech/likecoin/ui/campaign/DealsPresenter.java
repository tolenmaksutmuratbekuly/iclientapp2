package com.solaitech.likecoin.ui.campaign;

import com.solaitech.likecoin.data.models.campaign.Campaign;
import com.solaitech.likecoin.data.models.campaign.Tag;
import com.solaitech.likecoin.data.repository.campaign.CampaignRepositoryProvider;
import com.solaitech.likecoin.network.API;
import com.solaitech.likecoin.ui.base.PresenterErrorHandler;

import java.util.ArrayList;
import java.util.List;

import rx.Subscriber;
import rx.Subscription;
import rx.functions.Action0;
import rx.subscriptions.CompositeSubscription;

class DealsPresenter {
    private DealsView view;
    private API api;
    private CompositeSubscription compositeSubscription = new CompositeSubscription();

    DealsPresenter(DealsView view, API api) {
        this.view = view;
        this.api = api;
        getTags();
    }

    void getTags() {
        Subscription subscription = CampaignRepositoryProvider
                .provideRepository(api)
                .getTags()
                .doOnSubscribe(new Action0() {
                    @Override
                    public void call() {
                        view.showProgressTemplateView();
                    }
                })
                .subscribe(new Subscriber<List<Tag>>() {
                    @Override
                    public void onCompleted() {
                        view.showSuccessTemplateView();
                    }

                    @Override
                    public void onError(Throwable e) {
                        PresenterErrorHandler.handleError(e, view);
                    }

                    @Override
                    public void onNext(List<Tag> tagList) {
                        if (tagList != null && tagList.size() > 0) {
                            for (Tag tag : tagList) {
                                if (tag != null) {
                                    if (tag.getCampaigns() != null && tag.getCampaigns().size() > 0) {
                                        view.addPromotionByTag(tag);
                                    }
                                }
                            }

                            List<Campaign> campaigns = new ArrayList<>();
                            view.addAllPromotions(campaigns);
                            view.notifyAdapter();
                        }
                    }
                });
        compositeSubscription.add(subscription);
    }

    public void onTryToUploadDataAgainClick() {
        getTags();
    }

    public void onBottomNavigationTabReselected() {
        getTags();
    }

    public void onDestroyView() {
        unsubscribe();
    }

    private void unsubscribe() {
        if (compositeSubscription != null
                && !compositeSubscription.isUnsubscribed()) {
            compositeSubscription.unsubscribe();
        }
    }
}