package com.solaitech.likecoin.ui.profile.brix;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.http.SslError;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.util.Pair;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.SslErrorHandler;
import android.webkit.WebChromeClient;
import android.webkit.WebResourceRequest;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.solaitech.likecoin.App;
import com.solaitech.likecoin.R;
import com.solaitech.likecoin.data.models.user.User;
import com.solaitech.likecoin.ui.base.BaseActivityFrgm;
import com.solaitech.likecoin.ui.header.ProfileHeaderViewGroup;
import com.solaitech.likecoin.ui.menu.MainActivity;
import com.solaitech.likecoin.ui.profile.ProfileActivity;
import com.solaitech.likecoin.ui.profile.edit_profile_image.UserInfoUpdate;
import com.solaitech.likecoin.ui.profile.level.LevelActivity;
import com.solaitech.likecoin.ui.profile.user_statement.UserStatementActivity;
import com.solaitech.likecoin.utils.StringUtils;
import com.solaitech.likecoin.utils.ToastUtils;

import java.util.Collection;
import java.util.Map;

import butterknife.Bind;
import butterknife.ButterKnife;

import static com.solaitech.likecoin.BuildConfig.ERROR_LINK;
import static com.solaitech.likecoin.BuildConfig.FAILURE_BACK_LINK;
import static com.solaitech.likecoin.BuildConfig.PAYMENT_LOGON_LINK;

public class PaymentActivity extends BaseActivityFrgm implements PaymentView, UserInfoUpdate {
    @Bind(R.id.v_profile) ProfileHeaderViewGroup v_profile;
    @Bind(R.id.pb_indicator) ViewGroup pb_indicator;
    @Bind(R.id.wv_payment) WebView wv_payment;

    private PaymentPresenter presenter;
    private String hash;
    private String URL;

    public static Intent getIntent(Context context, String hash) {
        Bundle bundle = new Bundle();
        bundle.putString("hash", hash);
        Intent intent = new Intent(context, PaymentActivity.class);
        intent.putExtras(bundle);
        return intent;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment);
        ButterKnife.bind(this);
        handleIntent(getIntent());
        v_profile.setOnUpdateUserInfo(this);
        presenter = new PaymentPresenter(this, preferences, hash);
        initViews();
    }

    private void handleIntent(Intent intent) {
        if (intent != null && intent.getExtras() != null) {
            Bundle bundle = intent.getExtras();
            hash = bundle.getString("hash");
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        v_profile.onResume();
    }

    @Override
    protected void onProfileImageUpdated() {
        super.onProfileImageUpdated();
        v_profile.onProfileImageUpdated();
    }

    @Override
    protected void onProfileUpdated() {
        super.onProfileUpdated();
        v_profile.onProfileUpdated();
    }

    private void initViews() {
        wv_payment.getSettings().setJavaScriptEnabled(true);
        wv_payment.getSettings().setCacheMode(WebSettings.LOAD_NO_CACHE);
        wv_payment.getSettings().setAppCacheEnabled(false);
        wv_payment.getSettings().setBuiltInZoomControls(true);
        wv_payment.getSettings().setDisplayZoomControls(false);
        wv_payment.getSettings().setLoadWithOverviewMode(true);
        wv_payment.getSettings().setUseWideViewPort(true);
        float scale = 100 * wv_payment.getScaleX();
        wv_payment.setInitialScale((int) scale);
        wv_payment.setWebChromeClient(new WebChromeClient());
        wv_payment.clearCache(true);
        wv_payment.getSettings().setUserAgentString("Mozilla/5.0 (Linux; U; Android 2.2; en-us; Nexus One Build/FRF91) AppleWebKit/533.1 (KHTML, like Gecko) Version/4.0 Mobile Safari/533.1");
        wv_payment.setWebViewClient(new WebViewClient() {
            @Override
            public void onReceivedSslError(WebView view, SslErrorHandler handler, SslError error) {
                pb_indicator.setVisibility(View.GONE);
                handler.cancel();
            }

            @TargetApi(Build.VERSION_CODES.N)
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, WebResourceRequest request) {
                URL = request.getUrl().toString();
                overrideURL(URL);
                return super.shouldOverrideUrlLoading(view, request);
            }

            @SuppressWarnings("deprecation")
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                URL = url;
                overrideURL(url);
                return false;
            }

            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                super.onPageStarted(view, url, favicon);
                pb_indicator.setVisibility(View.VISIBLE);
            }

            @Override
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);
                pb_indicator.setVisibility(View.GONE);
                wv_payment.loadUrl("javascript:window.HtmlViewer.showHTML" +
                        "('<html>'+document.getElementsByTagName('html')[0].innerHTML+'</html>');");
            }
        });
    }

    void overrideURL(String URL) {
        if (URL.contains(FAILURE_BACK_LINK)) {
            finish();
        } else if (URL.contains(ERROR_LINK)) {
            ToastUtils.show(this, getString(R.string.error_occurred));
            finish();
        }
    }

    private void clientPost(WebView webView, String url, Collection<Map.Entry<String, String>> postData) {
        StringBuilder sb = new StringBuilder();
        sb.append("<html><head></head>");
        sb.append("<body onload='form1.submit()'>");
        sb.append(String.format("<form id='form1' action='%s' method='%s'>", url, "post"));
        for (Map.Entry<String, String> item : postData) {
            sb.append(String.format("<input name='%s' type='hidden' value='%s' />", item.getKey(), item.getValue()));
        }
        sb.append("</form></body></html>");

        webView.loadData(sb.toString(), "text/html", "UTF-8");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    ////////////////////////////////////////////////
    /// PaymentView                              ///
    ////////////////////////////////////////////////

    @Override
    public User getUser() {
        App app = (App) getApplicationContext();
        return app.getUser();
    }

    @Override
    public void showLoadingIndicator(boolean show) {
        pb_indicator.setVisibility(show ? View.VISIBLE : View.GONE);
    }

    @Override
    public void postDataInWebView(Collection<Map.Entry<String, String>> postData) {
        clientPost(wv_payment, PAYMENT_LOGON_LINK, postData);
    }

    ////////////////////////////////////////////////
    /// UserInfoUpdate                           ///
    ////////////////////////////////////////////////

    @Override
    public void onUpdateUserInfo(User user) {
    }

    @Override
    public void openProfileActivity() {
        intentOpener(ProfileActivity.getIntent(this));
    }

    @Override
    public void openUserStatementActivity() {
        intentOpener(UserStatementActivity.getIntent(this));
    }

    @Override
    public void openLevelActivity() {
        intentOpener(LevelActivity.getIntent(this));
    }

    @Override
    public void openBrixActivity() {
        intentOpener(BrixActivity.getIntent(this));
    }

    private void intentOpener(Intent intent) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            Pair<View, String> p1 = Pair.create(findViewById(R.id.v_profile), getString(R.string.tn_profile));
            ActivityOptionsCompat options = ActivityOptionsCompat.makeSceneTransitionAnimation(this, p1);
            startActivity(intent, options.toBundle());
        } else {
            startActivity(intent);
        }
        finish();
    }

    @Override
    public void initToolbar(Toolbar toolbar) {
        toolbar.setTitle(StringUtils.EMPTY_STRING);
        toolbar.setNavigationIcon(R.drawable.ic_chevron_left_white_24dp);
        setSupportActionBar(toolbar);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goBack();
            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        goBack();
    }

    private void goBack() {
        if (isTaskRoot()) {
            startActivity(MainActivity.getIntent(this));
        } else {
            supportFinishAfterTransition();
        }
    }
}