package com.solaitech.likecoin.ui.base.loading.views;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.solaitech.likecoin.R;

public class TemplateProgressView extends LinearLayout {

    private ProgressBar progressBar;
    private TextView tvMessage;

    public TemplateProgressView(Context context) {
        super(context);
        inflate();
    }

    public TemplateProgressView(Context context, AttributeSet attrs) {
        super(context, attrs);
        inflate();
    }

    public TemplateProgressView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        inflate();
    }

    private void inflate() {
        LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflater.inflate(R.layout.v_template_progress, this);

        initView();
    }

    private void initView() {
        progressBar = (ProgressBar) findViewById(R.id.progress_bar);
        tvMessage = (TextView) findViewById(R.id.tv_message);

        tvMessage.setVisibility(GONE);
    }

    public void setMessage(String message) {
        tvMessage.setText(message);
    }

    public void setMessageTextColor(int color) {
        tvMessage.setTextColor(color);
    }

}