package com.solaitech.likecoin.ui.campaign;

import com.solaitech.likecoin.BuildConfig;
import com.solaitech.likecoin.data.models.campaign.Campaign;
import com.solaitech.likecoin.data.repository.campaign.CampaignRepositoryProvider;
import com.solaitech.likecoin.network.API;
import com.solaitech.likecoin.ui.base.PresenterErrorHandler;
import com.solaitech.likecoin.utils.StringUtils;

import java.util.ArrayList;
import java.util.List;

import rx.Subscriber;
import rx.Subscription;
import rx.functions.Action0;
import rx.subscriptions.CompositeSubscription;

class CampaignPresenter {
    private CampaignView view;
    private API api;
    private CompositeSubscription compositeSubscription = new CompositeSubscription();

    private Subscription searchSubscription;

    CampaignPresenter(CampaignView view, API api) {
        this.view = view;
        this.api = api;
    }

    private void getCampaigns(String tagId) {
        Subscription subscription = CampaignRepositoryProvider
                .provideRepository(api)
                .getCampaigns(StringUtils.EMPTY_STRING, false, tagId)
                .doOnSubscribe(new Action0() {
                    @Override
                    public void call() {
                        view.showProgressTemplateView();
                    }
                })
                .subscribe(new Subscriber<List<Campaign>>() {
                    @Override
                    public void onCompleted() {
                        view.showSuccessTemplateView();
                    }

                    @Override
                    public void onError(Throwable e) {
                        PresenterErrorHandler.handleError(e, view);
                    }

                    @Override
                    public void onNext(List<Campaign> campaigns) {
                        view.updateAdapterData(campaigns);
                    }
                });
        compositeSubscription.add(subscription);
    }

    public void onSwipeRefresh(String tagId) {
        getCampaigns(tagId);
    }

    public void onTryToUploadDataAgainClick(String tagId) {
        getCampaigns(tagId);
    }

    public void onItemClick(Object object) {
        if (object instanceof Campaign) {
            Campaign campaign = (Campaign) object;
            view.openCampaignDetailActivity(campaign.getId(), false);
        }
    }

    void onSearch(final String query) {
        if (searchSubscription != null
                && !searchSubscription.isUnsubscribed()) {
            searchSubscription.unsubscribe();
        }

        searchSubscription = CampaignRepositoryProvider
                .provideRepository(api)
                .search(view.getObjects(), query)
                .doOnSubscribe(new Action0() {
                    @Override
                    public void call() {
                        view.clearFilterObjects();
                    }
                })
                .subscribe(new Subscriber<Campaign>() {
                    @Override
                    public void onCompleted() {
                        //do nothing
                    }

                    @Override
                    public void onError(Throwable e) {
                        if (BuildConfig.IS_DEBUG) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onNext(Campaign campaign) {
                        view.onSearchObjectFound(campaign);
                    }
                });
        compositeSubscription.add(searchSubscription);
    }

    void onSearchShowAllListItems() {
        List<Object> objects = new ArrayList<>(view.getObjects());
        view.updateAdapterData(objects);
    }

    public void onDestroyView() {
        unsubscribe();
    }

    private void unsubscribe() {
        if (compositeSubscription != null
                && !compositeSubscription.isUnsubscribed()) {
            compositeSubscription.unsubscribe();
        }
    }
}