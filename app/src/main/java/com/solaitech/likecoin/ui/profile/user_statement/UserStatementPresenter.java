package com.solaitech.likecoin.ui.profile.user_statement;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import com.solaitech.likecoin.data.models.user_statement.UserStatement;
import com.solaitech.likecoin.data.repository.user.UserRepositoryProvider;
import com.solaitech.likecoin.network.API;
import com.solaitech.likecoin.ui.base.PresenterErrorHandler;
import com.solaitech.likecoin.utils.DateTimeUtils;
import com.solaitech.likecoin.utils.Logger;
import rx.Subscriber;
import rx.Subscription;
import rx.functions.Action0;
import rx.subscriptions.CompositeSubscription;

class UserStatementPresenter {
    private UserStatementView view;
    private API api;

    private CompositeSubscription compositeSubscription = new CompositeSubscription();

    UserStatementPresenter(UserStatementView view, API api) {
        this.view = view;
        this.api = api;
    }

    public void init() {
        Calendar calendar = Calendar.getInstance();
        Date endDate = calendar.getTime();

        calendar.add(Calendar.DATE, -7);
        Date startDate = calendar.getTime();

        view.setStartDate(startDate);
        view.showStartDate(DateTimeUtils.convertDateToString(startDate));

        view.setEndDate(endDate);
        view.showEndDate(DateTimeUtils.convertDateToString(endDate));

        getUserStatement();
    }

    private void getUserStatement() {
        Subscription subscription = UserRepositoryProvider.provideRepository(api)
                .getUserStatement(getStartDateParam(), getEndDateParam())
                .doOnSubscribe(new Action0() {
                    @Override
                    public void call() {
                        view.showProgressTemplateView();
                    }
                })
                .subscribe(new Subscriber<List<UserStatement>>() {
                    @Override
                    public void onCompleted() {
                        view.showSuccessTemplateView();
                    }

                    @Override
                    public void onError(Throwable e) {
                        PresenterErrorHandler.handleError(e, view);
                    }

                    @Override
                    public void onNext(List<UserStatement> userStatements) {
                        Logger.e(getClass(), "onNext");
                        view.showUserStatement(userStatements);
                    }
                });
        compositeSubscription.add(subscription);
    }

    private String getStartDateParam() {
        return DateTimeUtils.convertUserStatementDateParamToString(view.getStartDate());
    }

    private String getEndDateParam() {
        return DateTimeUtils.convertUserStatementDateParamToString(view.getEndDate());
    }

    public void onTryToUploadDataAgainClick() {
        getUserStatement();
    }

    public void onSwipeRefresh() {
        getUserStatement();
    }

    public void onDestroyView() {
        unsubscribe();
    }

    private void unsubscribe() {
        if (compositeSubscription != null
                && !compositeSubscription.isUnsubscribed()) {
            compositeSubscription.unsubscribe();
        }
    }

    /**
     * PERIOD
     */
    void onChooseStartDateClick() {
        view.chooseStartDate(view.getStartDate(), view.getEndDate());
    }

    void onChooseEndDateClick() {
        view.chooseEndDate(view.getEndDate(), view.getStartDate());
    }

    void onStartDateChosen(Date date) {
        view.setStartDate(date);
        view.showStartDate(DateTimeUtils.convertDateToString(date));
        getUserStatement();
    }

    void onEndDateChosen(Date date) {
        view.setEndDate(date);
        view.showEndDate(DateTimeUtils.convertDateToString(date));
        getUserStatement();
    }
}