package com.solaitech.likecoin.ui.help;

import android.content.Context;

import com.solaitech.likecoin.data.Constants;
import com.solaitech.likecoin.utils.ContactsUtils;

class HelpPresenter {

    private HelpView view;
    private Context context;

    HelpPresenter(HelpView view, Context context) {
        this.view = view;
        this.context = context;
    }

    void onPermissionGrantedContacts() {
        if (view.isPermissionGrantedContacts()) {
            if (!ContactsUtils.isContactExists(context, Constants.FEEDBACK_WHATSAPP)) {
                ContactsUtils.addAsContactAutomatic(context);
            }
        } else {
            view.requestContactsPermissions();
        }
    }
}