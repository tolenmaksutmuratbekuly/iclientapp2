package com.solaitech.likecoin.ui.campaign.detail.conditions.coordinates;

import android.content.Context;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.solaitech.likecoin.R;
import com.solaitech.likecoin.data.models.campaign.Coordinate;
import com.solaitech.likecoin.utils.StringUtils;

public class CampaignCoordinateView extends LinearLayout
        implements View.OnClickListener {

    private TextView tvCoordinate;
    private View vLine;

    private Coordinate coordinate;

    private OnCampaignCoordinateViewClickListener onCampaignCoordinateViewClickListener;

    public CampaignCoordinateView(Context context) {
        super(context);
        inflate();
    }

    public CampaignCoordinateView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        inflate();
    }

    public CampaignCoordinateView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        inflate();
    }

    public void setOnCampaignCoordinateViewClickListener(OnCampaignCoordinateViewClickListener onCampaignCoordinateViewClickListener) {
        this.onCampaignCoordinateViewClickListener = onCampaignCoordinateViewClickListener;
    }

    private void inflate() {
        LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflater.inflate(R.layout.v_campaign_coordinate, this);

        initView();
    }

    private void initView() {
        tvCoordinate = (TextView) findViewById(R.id.tv_coordinate);
        tvCoordinate.setOnClickListener(this);

        vLine = findViewById(R.id.v_line);
        vLine.setVisibility(GONE);
    }

    public void showCoordinate(Coordinate coordinate) {
        this.coordinate = coordinate;

        if (StringUtils.isStringOk(coordinate.getAddress())) {
            tvCoordinate.setText(coordinate.getAddress());
        } else {
            tvCoordinate.setText("-");
        }
    }

    public void hideLine() {
        vLine.setVisibility(GONE);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.tv_coordinate:
                onCampaignCoordinateViewClickListener.onCampaignCoordinateViewClick(coordinate);
                break;
        }
    }

}