package com.solaitech.likecoin.ui.base;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;

import com.solaitech.likecoin.R;
import com.solaitech.likecoin.data.models.enums.BroadcastEventEnums;
import com.solaitech.likecoin.data.models.push_notification.PushNotification;
import com.solaitech.likecoin.data.preferences.Preferences;
import com.solaitech.likecoin.data.preferences.PreferencesImpl;
import com.solaitech.likecoin.ui.push_notification.PushNotificationHandler;
import com.solaitech.likecoin.utils.LanguageUtils;
import com.solaitech.likecoin.utils.Logger;
import com.solaitech.likecoin.utils.StringUtils;

import org.json.JSONException;
import org.json.JSONObject;

public class BaseBroadCastActivity extends AppCompatActivity {
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Preferences preferences = new PreferencesImpl(this);
        LanguageUtils langUtil = new LanguageUtils(preferences, this);
        langUtil.fixLocale(langUtil.getLocale());
    }

    @Override
    protected void onStart() {
        super.onStart();
        Logger.e(getClass(), "onStart");
        LocalBroadcastManager
                .getInstance(this)
                .registerReceiver(
                        profileImageUpdatedBroadcastReceiver,
                        new IntentFilter(BroadcastEventEnums.PROFILE_IMAGE_UPDATED)
                );
        LocalBroadcastManager
                .getInstance(this)
                .registerReceiver(
                        profileUpdatedBroadcastReceiver,
                        new IntentFilter(BroadcastEventEnums.PROFILE_UPDATED)
                );
        LocalBroadcastManager
                .getInstance(this)
                .registerReceiver(
                        pushNotificationBroadcastReceiver,
                        new IntentFilter(BroadcastEventEnums.PUSH_NOTIFICATION)
                );
    }

    @Override
    protected void onStop() {
        Logger.e(getClass(), "onStop");
        LocalBroadcastManager.getInstance(this).unregisterReceiver(profileImageUpdatedBroadcastReceiver);
        LocalBroadcastManager.getInstance(this).unregisterReceiver(profileUpdatedBroadcastReceiver);
        LocalBroadcastManager.getInstance(this).unregisterReceiver(pushNotificationBroadcastReceiver);
        super.onStop();
    }

    private BroadcastReceiver profileImageUpdatedBroadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            onProfileImageUpdated();
        }
    };

    private BroadcastReceiver profileUpdatedBroadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            onProfileUpdated();
        }
    };

    private BroadcastReceiver pushNotificationBroadcastReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent != null
                    && intent.getExtras() != null) {
                Bundle bundle = intent.getExtras();
                final PushNotification pushNotification = bundle.getParcelable("pushNotification");
                AlertDialog.Builder builder = new AlertDialog.Builder(BaseBroadCastActivity.this);

//                LayoutInflater layoutInflater = LayoutInflater.from(context);
//                View v = layoutInflater.inflate(R.layout.dialog_push_notification_open_document, null);
//                builder.setView(v);
//
//                TextView tvNotificationBody = (TextView) v.findViewById(R.id.tv_notification_body);
//                tvNotificationBody.setText(
//                        StringUtils.replaceNull(notification.getNotificationBody())
//                );

                builder.setTitle(StringUtils.replaceNull(pushNotification.getTitle()));
                builder.setMessage(StringUtils.replaceNull(pushNotification.getBody()));
                builder.setPositiveButton(context.getString(R.string.open), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        PushNotificationHandler handler = new PushNotificationHandler(BaseBroadCastActivity.this);
                        try {
                            handler.process(new JSONObject(pushNotification.getData()));
                        } catch (JSONException e) {
                            e.printStackTrace();
                            handler.process(null);
                        }
                        dialog.dismiss();
                    }
                });
                builder.setNegativeButton(context.getString(R.string.cancellation), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });


                builder.create().show();
            }
        }
    };

    protected void onProfileImageUpdated() {
        //override in activities
    }

    protected void onProfileUpdated() {
        //override in activities
    }
}
