package com.solaitech.likecoin.ui.help;

import android.Manifest;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.DrawableRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.annotation.StringRes;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import com.solaitech.likecoin.R;
import com.solaitech.likecoin.data.PermissionRequestCodes;
import com.solaitech.likecoin.ui.base.loading.LoadingBaseFragment;
import com.solaitech.likecoin.ui.menu.MainActivity;
import com.solaitech.likecoin.ui.news_feed.feedback.FeedbackDialog;
import com.solaitech.likecoin.utils.PermissionUtils;

public class HelpFragment extends LoadingBaseFragment implements HelpView {
    private static String[] PERMISSIONS_CONTACT = {Manifest.permission.WRITE_CONTACTS, Manifest.permission.READ_CONTACTS};

    private HelpPresenter presenter;
    public static HelpFragment newInstance() {
        return new HelpFragment();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = super.onCreateView(inflater, container, savedInstanceState);
        initViews(v);
        presenter = new HelpPresenter(this, getContext());
        return v;
    }

    RowViewHolder inc_about;
    RowViewHolder inc_faq;
    RowViewHolder inc_help;
    RowViewHolder inc_contacts;
    private void initViews(View v) {
        ButterKnife.bind(this, v);
        inc_about = new RowViewHolder(v.findViewById(R.id.inc_about));
        inc_about.bindHolder(R.drawable.ic_help_info, R.string.menu_about);

        inc_faq = new RowViewHolder(v.findViewById(R.id.inc_faq));
        inc_faq.bindHolder(R.drawable.ic_help_faq, R.string.menu_faq);

        inc_help = new RowViewHolder(v.findViewById(R.id.inc_help));
        inc_help.bindHolder(R.drawable.ic_help_sos, R.string.menu_help);

        inc_contacts = new RowViewHolder(v.findViewById(R.id.inc_contacts));
        inc_contacts.bindHolder(R.drawable.ic_help_contacts, R.string.menu_contacts);
    }

    class RowViewHolder {
        ViewGroup rowView;
        @Bind(R.id.img_info) ImageView img_info;
        @Bind(R.id.tv_title) TextView tv_title;

        RowViewHolder(View view) {
            ButterKnife.bind(this, view);
            rowView = (ViewGroup) view;
        }

        void bindHolder(@DrawableRes int imgResource, @StringRes int titleRes) {
            if (titleRes == 0 || imgResource == 0) {
                rowView.setVisibility(View.GONE);
            } else {
                rowView.setVisibility(View.VISIBLE);
                img_info.setImageResource(imgResource);
                tv_title.setText(titleRes);
            }
        }

        @OnClick(R.id.ll_help_item)
        void onClickItem() {
            if (rowView.getId() == R.id.inc_about) {
                startActivity(AboutActivity.getIntent(getContext()));
            } else if (rowView.getId() == R.id.inc_faq) {
                startActivity(FAQActivity.getIntent(getContext()));
            } else if (rowView.getId() == R.id.inc_help) {
                presenter.onPermissionGrantedContacts();
                FeedbackDialog dialog = FeedbackDialog.newInstance();
                dialog.show(getFragmentManager(), "");
            } else if (rowView.getId() == R.id.inc_contacts) {
                startActivity(CompanyDetailActivity.getIntent(getContext()));
            }
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ((MainActivity) getActivity()).hideHeaderProgress();
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.frgm_help;
    }

    @Override
    protected void tryUploadDataAgain() {

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        if (requestCode == PermissionRequestCodes.REQUEST_CONTACTS) {
            if (PermissionUtils.verifyPermissions(grantResults)) {
                presenter.onPermissionGrantedContacts();
            }
        } else {
            super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    ////////////////////////////////////////////////////////////////////////////
    // HelpView implementation
    ///////////////////////////////////////////////////////////////////////////

    @Override
    public boolean isPermissionGrantedContacts() {
        return (PermissionUtils.isPermissionGranted(getContext(), Manifest.permission.WRITE_CONTACTS) &&
                PermissionUtils.isPermissionGranted(getContext(), Manifest.permission.READ_CONTACTS));
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    public void requestContactsPermissions() {
        requestPermissions(PERMISSIONS_CONTACT, PermissionRequestCodes.REQUEST_CONTACTS);
    }
}