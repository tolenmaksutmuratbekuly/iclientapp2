package com.solaitech.likecoin.ui.coupons.detail.qr_code;

import android.graphics.Bitmap;

interface ShowQrCodeView {

    void showPromotionalCode(String code);

    void showProgressBar();

    void hideProgressBar();

    void showErrorView();

    void hideErrorView();

    String getKey();

    int getBlackColor();

    int getWhiteColor();

    void showQrCode(Bitmap bitmap);

    int getDisplayWidth();

    int getDisplayHeight();

}