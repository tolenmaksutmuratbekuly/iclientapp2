package com.solaitech.likecoin.ui.push_notification;

import android.content.Context;
import android.content.Intent;
import android.support.v4.app.TaskStackBuilder;

import com.solaitech.likecoin.data.models.enums.NewsSourceEntityRefTypeEnums;
import com.solaitech.likecoin.data.models.enums.PushNotificationEnums;
import com.solaitech.likecoin.data.models.push_notification.PushNotificationData;
import com.solaitech.likecoin.network.GsonProvider;
import com.solaitech.likecoin.ui.campaign.detail.CampaignDetailActivity;
import com.solaitech.likecoin.ui.menu.MainActivity;
import com.solaitech.likecoin.ui.profile.level.LevelActivity;
import com.solaitech.likecoin.ui.profile.user_statement.UserStatementActivity;

import org.json.JSONObject;

public class PushNotificationHandler {
    private Context context;

    public PushNotificationHandler(Context context) {
        this.context = context;
    }

    public void process(JSONObject data) {
        try {
            processResult(data);
        } catch (Exception e) {
            e.printStackTrace();
            openMainActivity(null);
        }
    }

    private void processResult(JSONObject data) throws Exception {
        if (data == null) {
            throw new Exception();
        }
        PushNotificationData pushNotificationData = GsonProvider.gson.fromJson(data.toString(), PushNotificationData.class);
        checkResult(pushNotificationData);

        switch (pushNotificationData.getTypeId()) {
            case PushNotificationEnums.PUSH_NOTIFICATION_TYPE_CAMPAIGN:
                if (pushNotificationData.getSourceEntityRef().getType().getId().equals(NewsSourceEntityRefTypeEnums.CAMPAIGN)) {
                    openCampaignDetailActivity(pushNotificationData.getSourceEntityRef().getId(), false);
                }
                break;
            case PushNotificationEnums.PUSH_NOTIFICATION_TYPE_CAMPAIGN_LIST:
                openMainActivity(PushNotificationEnums.PUSH_NOTIFICATION_TYPE_CAMPAIGN_LIST);
                break;
            case PushNotificationEnums.PUSH_NOTIFICATION_TYPE_COMMENT_REPLY:
                if (pushNotificationData.getSourceEntityRef().getType().getId().equals(NewsSourceEntityRefTypeEnums.CAMPAIGN)) {
                    openCampaignDetailActivity(pushNotificationData.getSourceEntityRef().getId(), true);
                }
                break;
            case PushNotificationEnums.PUSH_NOTIFICATION_TYPE_NEXT_LEVEL:
                openLevelActivity();
                break;
            default:
                openMainActivity(null);
                break;
        }

//        switch (pushNotificationData.getSourceEntityRef().getType().getId()) {
//            case NewsSourceEntityRefTypeEnums.SYS_MESSAGE:
//                openMainActivity();
//                break;
//            case NewsSourceEntityRefTypeEnums.TRANSACTION:
//                openUserStateActivity();
//                break;
//            case NewsSourceEntityRefTypeEnums.CAMPAIGN:
//                openCampaignDetailActivity(pushNotificationData.getSourceEntityRef().getId());
//                break;
//            case NewsSourceEntityRefTypeEnums.COMMENT:
//                openMainActivity();
//                break;
//        }
    }

    private void checkResult(PushNotificationData pushNotificationData) throws Exception {
        if (pushNotificationData != null
                && pushNotificationData.getTypeId() != null
                && pushNotificationData.getSourceEntityRef() != null
                && pushNotificationData.getSourceEntityRef().getType() != null
                && pushNotificationData.getSourceEntityRef().getType().getId() != null) {

            if (pushNotificationData.getSourceEntityRef().getType().getId().equalsIgnoreCase(NewsSourceEntityRefTypeEnums.CAMPAIGN)
                    && pushNotificationData.getSourceEntityRef().getId() == null) {
                throw new Exception();
            }

            return;
        }
        throw new Exception();
    }

    private void openMainActivity(String tabSelect) {
        Intent intent = MainActivity.getIntent(context);
        if (tabSelect != null) {
            intent.putExtra("tabSelect", tabSelect);
        }
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(intent);
    }

    private void openUserStateActivity() {
        Intent intent = UserStatementActivity.getIntent(context);
        openIntentWithParentStack(intent);
    }

    private void openCampaignDetailActivity(String id, boolean openCommentsTab) {
        Intent intent = CampaignDetailActivity.getIntent(context, id, openCommentsTab);
        openIntentWithParentStack(intent);
    }

    private void openLevelActivity() {
        Intent intent = LevelActivity.getIntent(context);
        openIntentWithParentStack(intent);
    }

    private void openIntentWithParentStack(Intent intent) {
        intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT | Intent.FLAG_ACTIVITY_NEW_TASK);
        TaskStackBuilder.create(context)
                .addNextIntentWithParentStack(intent)
                .startActivities();
    }

}