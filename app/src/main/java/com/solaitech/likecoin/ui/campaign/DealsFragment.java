package com.solaitech.likecoin.ui.campaign;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.solaitech.likecoin.R;
import com.solaitech.likecoin.data.models.campaign.Campaign;
import com.solaitech.likecoin.data.models.campaign.Tag;
import com.solaitech.likecoin.ui.base.loading.LoadingBaseFragment;
import com.solaitech.likecoin.ui.menu.BottomNavigationTabFragment;
import com.solaitech.likecoin.utils.StringUtils;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

public class DealsFragment extends LoadingBaseFragment implements BottomNavigationTabFragment, DealsView {
    private DealsPresenter presenter;

    @Bind(R.id.tab_layout) TabLayout tab_layout;
    @Bind(R.id.view_pager) ViewPager view_pager;
    private ViewPagerAdapter adapter;

    public static DealsFragment newInstance() {
        return new DealsFragment();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = super.onCreateView(inflater, container, savedInstanceState);
        ButterKnife.bind(this, v);
        configureView();
        presenter = new DealsPresenter(this, api);
        return v;
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.frgm_deals;
    }

    @Override
    public void onDestroyView() {
        presenter.onDestroyView();
        super.onDestroyView();
    }

    private void configureView() {
        adapter = new ViewPagerAdapter(getChildFragmentManager());
        adapter.clear();
        tab_layout.setupWithViewPager(view_pager);
        view_pager.setAdapter(adapter);
    }

    @Override
    protected void tryUploadDataAgain() {
        presenter.getTags();
    }

    @Override
    public void onBottomNavigationTabReselected() {
        for (Fragment fragment : adapter.getFragmentsList()) {
            if (fragment instanceof BottomNavigationTabFragment) {
                ((BottomNavigationTabFragment) fragment).onBottomNavigationTabReselected();
            }
        }
    }

    ////////////////////////////////////
    /// DealsView
    //////////////////////////////////
    @SuppressWarnings("unchecked")
    @Override
    public void addAllPromotions(final List<Campaign> campaigns) {
        CampaignFragment fragment = CampaignFragment.newInstance(StringUtils.EMPTY_STRING, campaigns);
        fragment.setCallback(new CampaignFragment.Callback() {
            @Override
            public void setNewCampaignList(List<Object> campaignList) {
                campaigns.clear();
                campaigns.addAll((List<Campaign>) (Object) campaignList);
            }
        });
        adapter.addFrag(fragment, getString(R.string.all_promo), 0);
    }

    @SuppressWarnings("unchecked")
    @Override
    public void addPromotionByTag(final Tag tag) {
        CampaignFragment fragment = CampaignFragment.newInstance(tag.getId(), tag.getCampaigns());
        fragment.setCallback(new CampaignFragment.Callback() {
            @Override
            public void setNewCampaignList(List<Object> campaignList) {
                tag.setCampaigns((List<Campaign>) (Object) campaignList);
            }
        });
        adapter.addFrag(fragment, tag.getName());
    }

    @Override
    public void notifyAdapter() {
        adapter.notifyDataSetChanged();
    }
}