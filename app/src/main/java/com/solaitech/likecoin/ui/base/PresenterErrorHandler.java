package com.solaitech.likecoin.ui.base;


import com.solaitech.likecoin.exceptions.APIException;
import com.solaitech.likecoin.exceptions.ConnectionTimeOutException;
import com.solaitech.likecoin.exceptions.NeedReAuthorizeException;
import com.solaitech.likecoin.exceptions.UnknownException;
import com.solaitech.likecoin.network.error.RetrofitErrorHandler;
import com.solaitech.likecoin.ui.base.interfaces.MVPBaseErrorView;

//Методы с одинаковыми названиями (разные номера) отличаются тем, что некоторые exception по разному обрабатываются:
// handle...Exception, showErrorDialog и showErrorView
public class PresenterErrorHandler {

    public static void handleError(Throwable e, MVPBaseErrorView view) {
        e.printStackTrace();
        try {
            RetrofitErrorHandler.handleException(e);
        } catch (NeedReAuthorizeException e1) {
            view.handleNeedReAuthorizeException(e1);
        } catch (APIException e1) {
            view.handleAPIException(e1);
        } catch (UnknownException e1) {
            view.handleUnknownException(e1);
        } catch (ConnectionTimeOutException e1) {
            view.handleConnectionTimeOutException(e1);
        }
    }

    public static void handleError2(Throwable e, MVPBaseErrorView view) {
        e.printStackTrace();
        try {
            RetrofitErrorHandler.handleException(e);
        } catch (NeedReAuthorizeException e1) {
            view.handleNeedReAuthorizeException(e1);
        } catch (APIException e1) {
            view.showErrorDialog(e1.getErrorDescr());
        } catch (UnknownException e1) {
            view.showErrorDialog(view.getUnknownExceptionMessage());
        } catch (ConnectionTimeOutException e1) {
            view.showErrorDialog(view.getConnectionTimeOutExceptionMessage());
        }
    }
}