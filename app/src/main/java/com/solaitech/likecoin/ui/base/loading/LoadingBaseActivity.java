package com.solaitech.likecoin.ui.base.loading;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.ViewGroup;

import com.solaitech.likecoin.R;
import com.solaitech.likecoin.data.preferences.Preferences;
import com.solaitech.likecoin.data.preferences.PreferencesImpl;
import com.solaitech.likecoin.exceptions.APIException;
import com.solaitech.likecoin.exceptions.ConnectionTimeOutException;
import com.solaitech.likecoin.exceptions.NeedReAuthorizeException;
import com.solaitech.likecoin.exceptions.UnknownException;
import com.solaitech.likecoin.ui.base.BaseActivityFrgm;
import com.solaitech.likecoin.ui.base.interfaces.MVPBaseView;
import com.solaitech.likecoin.ui.base.loading.views.TemplateFailView;
import com.solaitech.likecoin.ui.base.loading.views.TemplateProgressView;
import com.solaitech.likecoin.ui.registration.start.RegistrationStartActivity;
import com.solaitech.likecoin.utils.animation.CustomAnimationUtils;

public abstract class LoadingBaseActivity extends BaseActivityFrgm
        implements MVPBaseView, TemplateFailView.OnTryAgainClickListener {

    private ViewGroup vgSuccess;
    private LoadingBase loadingBase;
    private Preferences preferencesLogOut;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        preferencesLogOut = new PreferencesImpl(this);
    }

    protected void initContainerView(int viewId) {
        ViewGroup vgContent = findViewById(viewId);
        CustomAnimationUtils.enableTransition(vgContent);
    }

    protected void initLoadingView() {
        vgSuccess = findViewById(R.id.vg_success);

        TemplateProgressView vProgress = findViewById(R.id.v_template_progress);

        TemplateFailView vFail = findViewById(R.id.v_template_fail);
        vFail.setOnTryAgainClickListener(this);

        loadingBase = new LoadingBase(this, this, vgSuccess, vFail, vProgress);

        showSuccessTemplateView();
    }

//    protected View inflateSuccessView(int resourceId) {
//        LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
//        return inflater.inflate(resourceId, vgSuccess);
//    }

    @Override
    public void showProgressTemplateView() {
        loadingBase.showProgressTemplateView();
    }

    @Override
    public void showProgressTemplateView(String title) {
        loadingBase.showProgressTemplateView(title);
    }

    @Override
    public void showSuccessTemplateView() {
        loadingBase.showSuccessTemplateView();
    }

    @Override
    public void showFailTemplateView() {
        loadingBase.showFailTemplateView();
    }

    @Override
    public void showFailTemplateView(String message) {
        loadingBase.showFailTemplateView(message);
    }

    @Override
    public void onTryAgainClick() {
        tryUploadDataAgain();
    }

    protected abstract void tryUploadDataAgain();

    @Override
    public void showProgressDialog() {
        loadingBase.showProgressDialog();
    }

    @Override
    public void dismissProgressDialog() {
        loadingBase.dismissProgressDialog();
    }

    @Override
    public void handleAPIException(APIException e) {
        loadingBase.handleAPIException(e);
    }

    @Override
    public void handleConnectionTimeOutException(ConnectionTimeOutException e) {
        loadingBase.handleConnectionTimeOutException(e);
    }

    @Override
    public void handleUnknownException(UnknownException e) {
        loadingBase.handleUnknownException(e);
    }

    @Override
    public void showErrorDialog(String message) {
        loadingBase.showErrorDialog(message);
    }

    @Override
    public String getUnknownExceptionMessage() {
        return loadingBase.getUnknownExceptionMessage();
    }

    @Override
    public String getConnectionTimeOutExceptionMessage() {
        return loadingBase.getConnectionTimeOutExceptionMessage();
    }

    @Override
    public void handleNeedReAuthorizeException(NeedReAuthorizeException e) {
        preferencesLogOut.clearAllPreferences();
        startActivity(RegistrationStartActivity.newLogoutIntent(this));
    }
}