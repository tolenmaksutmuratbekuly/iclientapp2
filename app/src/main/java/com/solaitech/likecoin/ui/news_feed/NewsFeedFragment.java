package com.solaitech.likecoin.ui.news_feed;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.solaitech.likecoin.App;
import com.solaitech.likecoin.R;
import com.solaitech.likecoin.data.models.coupons.Coupon;
import com.solaitech.likecoin.data.models.enums.BroadcastEventEnums;
import com.solaitech.likecoin.data.models.news_feed.NewsFeed;
import com.solaitech.likecoin.data.models.news_feed.NewsOwner;
import com.solaitech.likecoin.paginate.LoadingListItemSpanLookup;
import com.solaitech.likecoin.paginate.Paginate;
import com.solaitech.likecoin.ui.article.ArticleDetailActivity;
import com.solaitech.likecoin.ui.base.recycler_view.RecyclerViewBaseFragment;
import com.solaitech.likecoin.ui.campaign.detail.CampaignDetailActivity;
import com.solaitech.likecoin.ui.campaign.detail.purchase_coupon.OnCouponsPurchasedListener;
import com.solaitech.likecoin.ui.campaign.detail.purchase_coupon.PurchaseCouponDialog;
import com.solaitech.likecoin.ui.campaign.detail.purchase_coupon.success.PurchasedCouponsInfoDialog;
import com.solaitech.likecoin.ui.contacts.send_like.SendLikeDialog;
import com.solaitech.likecoin.ui.menu.BottomNavigationTabFragment;
import com.solaitech.likecoin.ui.menu.Search;
import com.solaitech.likecoin.ui.profile.ProfileActivity;
import com.solaitech.likecoin.ui.profile.brix.BrixActivity;
import com.solaitech.likecoin.ui.profile.user_statement.UserStatementActivity;
import com.solaitech.likecoin.ui.system_messages.MsgDetailActivity;
import com.solaitech.likecoin.ui.user_page.UserActivity;
import com.solaitech.likecoin.utils.ToastUtils;
import com.solaitech.likecoin.utils.animation.FlippingAnimationCallback;

import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.solaitech.likecoin.utils.StringUtils.EMPTY_STRING;

public class NewsFeedFragment extends RecyclerViewBaseFragment
        implements NewsFeedView, BottomNavigationTabFragment, Search {

    private NewsFeedPresenter presenter;
    final int SEND_LIKE_REQUEST_CODE = 1;
    private String lastNewsId = "";

    // PAGINATION
    private Paginate paginate;

    private boolean loading = false;
    private boolean hasLoadedAllItems = false;
    private String baseId = "";

    private boolean loadingUp = false;
    private boolean hasLoadedAllItemsUp = false;
    private String baseIdUp = "";

    private ProgressDialog progressDialog;
    private FlippingAnimationCallback coinFlippingCallback;
    private NewsFeedAdapter.FavoriteCallback favoriteCallback;
    private NewsFeedAdapter.ArticleCallback articleCallback;
    private App app;

    @Bind(R.id.img_anim_coin) ImageView img_anim_coin;
    @Bind(R.id.ll_participate_in_ico) LinearLayout ll_participate_in_ico;

    public static NewsFeedFragment newInstance() {
        return new NewsFeedFragment();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        app = (App) getContext().getApplicationContext();
        lastNewsId = preferences.getNewsFeedLastVisibleItemId();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = super.onCreateView(inflater, container, savedInstanceState);
        ButterKnife.bind(this, v);
        initView(v);
        presenter = new NewsFeedPresenter(this, api);
        presenter.init();
        return v;
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.frgm_news_feed;
    }

    private void initView(View v) {
        initRecyclerView(v);
        DefaultItemAnimator animator = new DefaultItemAnimator() {
            @Override
            public boolean canReuseUpdatedViewHolder(@NonNull RecyclerView.ViewHolder viewHolder) {
                return true;
            }
        };
        recyclerView.setItemAnimator(animator);
        initSwipeRefreshLayout(v);
    }

    @Override
    protected void tryUploadDataAgain() {
        presenter.onTryToUploadDataAgainClick();
    }

    @Override
    public void onBottomNavigationTabReselected() {
        presenter.onBottomNavigationTabReselected();
        scrollToPosition(0);
    }

    @OnClick(R.id.ll_participate_in_ico)
    void onParticipateIcoClick() {
        startActivity(BrixActivity.getIntent(getContext()));
    }

    ////////////////////////////////////
    /// Search
    //////////////////////////////////

    @Override
    public void doSearch(String text) {
        presenter.onSearch(text);
    }

    @Override
    public void showAllListItems() {
        presenter.onSearchShowAllListItems();
    }

    @Override
    public void disableSwipeToRefresh() {
        swipeRefreshLayout.setEnabled(false);
    }

    @Override
    public void enableSwipeToRefresh() {
        swipeRefreshLayout.setEnabled(true);
    }

    @Override
    public List<Object> getObjects() {
        return objects;
    }

    @Override
    public void clearFilterObjects() {
        objectsFilter.clear();
        adapter.notifyDataSetChanged();
    }

    @Override
    public void onSearchObjectFound(NewsFeed newsFeed) {
        if (objectsFilter.isEmpty()) {
            objectsFilter.add(NewsFeedAdapter.VIEW_TYPE_FIRST_ELEMENT_EMPTY_SPACE);
        }
        objectsFilter.add(newsFeed);
        adapter.notifyItemInserted(objectsFilter.size());
    }

    @Override
    public void openCampaignDetailActivity(String id, boolean openCommentsTab) {
        Intent intent = CampaignDetailActivity.getIntent(getContext(), id, openCommentsTab);
        startActivity(intent);
    }

    @Override
    public void openUserStatementActivity() {
        Intent intentProfile = UserStatementActivity.getIntent(getContext());
        startActivity(intentProfile);
    }

    @Override
    public void openArticleDetailActivity(String id, boolean openCommentsTab) {
        Intent intent = ArticleDetailActivity.getIntent(getContext(), id, openCommentsTab);
        startActivity(intent);
    }

    @Override
    public void openMsgDetailActivity(String id, String type, boolean openCommentsTab) {
        Intent intent = MsgDetailActivity.getIntent(getContext(), type, id, openCommentsTab);
        startActivity(intent);
    }

    /**
     * ON DESTROY VIEW
     */
    @Override
    public void onDestroyView() {
        presenter.onDestroyView();
        super.onDestroyView();
    }

    @Override
    public int getFirstVisibleItemPosition() {
        if (recyclerView != null
                && recyclerView.getLayoutManager() instanceof LinearLayoutManager) {
            return ((LinearLayoutManager) recyclerView.getLayoutManager()).findFirstVisibleItemPosition();
        }

        return -1;
    }

    @Override
    public List<Object> getFilterObjects() {
        return objectsFilter;
    }

    @Override
    public void saveNewsFeedLastVisibleItemId(String newsFeedId) {
        if (newsFeedId != null
                && preferences != null) {
            preferences.setNewsFeedLastVisibleItemId(newsFeedId);
        }
    }

    @Override
    public void showSuccessToastMessage() {
        Toast.makeText(getContext(), getString(R.string.like_sending_success), Toast.LENGTH_SHORT).show();
        if (coinFlippingCallback != null) {
            coinFlippingCallback.show();
        }
    }

    @Override
    public void onLiked4EntitySuccess(NewsFeed newsFeed, int position) {
        if (coinFlippingCallback != null) {
            coinFlippingCallback.like4EntitySuccess(newsFeed, position);
        }
    }

    @Override
    public void onFavoriteState(NewsFeed newsFeed) {
        if (favoriteCallback != null) {
            favoriteCallback.onFavoriteState(newsFeed);
        }
    }

    @Override
    public void onArticleBought() {
        if (articleCallback != null) {
            articleCallback.onArticleBoughtState();
        }
    }

    @Override
    public String getLocalUserId() {
        return app.getUser() != null ? app.getUser().getId() : null;
    }

    @Override
    public void openUserActivity(String userId, boolean isUser) {
        Intent intent = isUser ? UserActivity.getIntent(getContext(), userId, EMPTY_STRING, true)
                : ProfileActivity.getIntent(getContext());
        startActivity(intent);
    }

    @Override
    public void showIcoContainer(boolean show) {
        ll_participate_in_ico.setVisibility(!show ? View.VISIBLE : View.GONE);
    }

    @Override
    public void sendBroadcast() {
        Intent broadcastIntent = new Intent(BroadcastEventEnums.PROFILE_UPDATED);
        LocalBroadcastManager.getInstance(getContext()).sendBroadcast(broadcastIntent);
    }

    @Override
    public void showPbDialog() {
        if (progressDialog == null
                || !progressDialog.isShowing()) {
            progressDialog = ProgressDialog.show(getContext(), "", getString(R.string.loading), true, false);
        }
    }

    @Override
    public void dismissPbDialog() {
        if (progressDialog != null && progressDialog.isShowing()) {
            progressDialog.dismiss();
        }
    }

    /////////////////////////////////
    /// RecyclerView              //
    ///////////////////////////////
    @Override
    protected void onSwipeRefresh() {
        super.onSwipeRefresh();
        presenter.onSwipeRefresh();
    }

    @Override
    protected RecyclerView.LayoutManager getLayoutManager() {
        return getLinearLayoutManager();
    }

    @Override
    protected RecyclerView.Adapter getAdapter() {
        adapter = new NewsFeedAdapter(getContext(), objectsFilter);
        ((NewsFeedAdapter) adapter).setCallback(new NewsFeedAdapter.Callback() {
            @Override
            public void onSendLikeClick(NewsOwner owner, boolean isOneLike, FlippingAnimationCallback callback) {
                coinFlippingCallback = callback;
                if (isOneLike) {
                    presenter.onSendButtonClick(owner);
                } else {
                    SendLikeDialog dialog = SendLikeDialog.newInstance(app.getUser(), owner, img_anim_coin);
                    dialog.setTargetFragment(NewsFeedFragment.this, SEND_LIKE_REQUEST_CODE);
                    dialog.show(getActivity().getSupportFragmentManager(), "");
                }
            }

            @Override
            public void onLike4Entity(String entityId, String entityTypeId, int position, FlippingAnimationCallback listener) {
                coinFlippingCallback = listener;
                presenter.onLike4Entity(entityId, entityTypeId, position);
            }

            @Override
            public void onBuyClick(String campaignId, int couponPrice, int maxCouponsCount) {
                PurchaseCouponDialog dialog = PurchaseCouponDialog.newInstance(campaignId, couponPrice, maxCouponsCount);
                dialog.setOnCouponsPurchasedListener(new OnCouponsPurchasedListener() {
                    @Override
                    public void onCouponsPurchased(String couponsQuantity, String totalSum, List<Coupon> coupons) {
                        PurchasedCouponsInfoDialog dialog = PurchasedCouponsInfoDialog.newInstance(couponsQuantity, totalSum, coupons);
                        dialog.show(getFragmentManager(), "");
                    }
                });
                dialog.show(getFragmentManager(), "");
            }

            @Override
            public void onArticleBuyClick(String articleId, NewsFeedAdapter.ArticleCallback callback) {
                articleCallback = callback;
                presenter.onArticleBuyClick(articleId);
            }

            @Override
            public void onCommentsClick(NewsFeed newsFeed) {
                presenter.openDetailPage(newsFeed, true);
            }

            @Override
            public void onFavoriteClick(String newsFeedId, int isFavorite, NewsFeedAdapter.FavoriteCallback callback) {
                favoriteCallback = callback;
                presenter.onFavoritesClick(newsFeedId, isFavorite);
            }

            @Override
            public void avatarClick(String userId) {
                presenter.onAvatarClick(userId);
            }

            @Override
            public String localUserId() {
                return getLocalUserId();
            }
        });

        ((NewsFeedAdapter) adapter).setOnRecyclerViewItemClickListener(this);
        return adapter;
    }

    // QUERY PARAMS
    @Override
    public String getNewsFeedLastVisibleItemId() {
        return lastNewsId;
    }

    @Override
    public void setNewsFeedLastVisibleItemId(String lastNewsId) {
        this.lastNewsId = lastNewsId;
    }

    // ADAPTER OBJECTS
    @Override
    public void updateAdapterData(List<? extends Object> newsFeeds) {
        objects.clear();
        objects.addAll(newsFeeds);

        objectsFilter.clear();
        if (!newsFeeds.isEmpty()) {
            objectsFilter.add(NewsFeedAdapter.VIEW_TYPE_FIRST_ELEMENT_EMPTY_SPACE);
        }
        objectsFilter.addAll(objects);
        adapter.notifyDataSetChanged();
    }

    @Override
    public void addAdapterData(List<NewsFeed> newsFeeds) {
        objects.addAll(newsFeeds);
        objectsFilter.addAll(newsFeeds);
        adapter.notifyDataSetChanged();
    }

    @Override
    public void addAdapterDataUp(List<NewsFeed> newsFeeds) {
        objects.addAll(0, newsFeeds);
        objectsFilter.addAll(0, newsFeeds);
        adapter.notifyDataSetChanged();
    }

    @Override
    public void scrollToPosition(int position) {
        ((LinearLayoutManager) recyclerView.getLayoutManager()).scrollToPositionWithOffset(position, 0);
    }

    @Override
    public void showToastQueryError() {
        ToastUtils.show(getContext(), getString(R.string.paginate_error_occured));
    }

    /**
     * PAGINATE
     */
    @Override
    public void setupPagination() {
        if (paginate == null) {
            paginate = Paginate.with(recyclerView, callbacks)
                    .setLoadingTriggerThreshold(0)
                    .addLoadingListItem(true)
                    .setLoadingListItemSpanSizeLookup(new LoadingListItemSpanLookup() {
                        @Override
                        public int getSpanSize() {
                            return 1;
                        }
                    })
                    .build();

            paginate.setHasMoreDataToLoad(!hasLoadedAllItems);
            paginate.setHasMoreDataToLoadUp(!hasLoadedAllItemsUp);
        }
    }

    private Paginate.Callbacks callbacks = new Paginate.Callbacks() {
        @Override
        public void onLoadMore() {
            // Load next page of data (e.g. network or database)
            presenter.onLoadMore();
        }

        @Override
        public boolean isLoading() {
            // Indicate whether new page loading is in progress or not
            return loading;
        }

        @Override
        public boolean hasLoadedAllItems() {
            // Indicate whether all data (pages) are loaded or not
            return hasLoadedAllItems;
        }

        @Override
        public void onLoadMoreUp() {
            presenter.onLoadMoreUp();
        }

        @Override
        public boolean isLoadingUp() {
            return loadingUp;
        }

        @Override
        public boolean hasLoadedAllItemsUp() {
            return hasLoadedAllItemsUp;
        }
    };

    @Override
    public void unbindPagination() {
        if (paginate != null) {
            paginate.unbind();
            paginate = null;
        }
    }

    /**
     * PAGINATE VALUES
     */
    @Override
    public void setIsLoading(boolean loading) {
        this.loading = loading;
    }

    @Override
    public void setHasLoadedAllItems(boolean hasLoadedAllItems) {
        this.hasLoadedAllItems = hasLoadedAllItems;
    }

    @Override
    public boolean hasLoadedAllItems() {
        return hasLoadedAllItems;
    }

    @Override
    public void setBaseId(String baseId) {
        this.baseId = baseId;
    }

    @Override
    public String getBaseId() {
        return baseId;
    }

    @Override
    public void setHasMoreDataToLoad(boolean value) {
        if (paginate != null) {
            paginate.setHasMoreDataToLoad(value);
        }
    }

    @Override
    public void setIsLoadingUp(boolean loading) {
        this.loadingUp = loading;
    }

    @Override
    public void setHasLoadedAllItemsUp(boolean hasLoadedAllItems) {
        this.hasLoadedAllItemsUp = hasLoadedAllItems;
        ((NewsFeedAdapter) adapter).setHasLoadedAllItemsUp(hasLoadedAllItems);
    }

    @Override
    public boolean hasLoadedAllItemsUp() {
        return hasLoadedAllItemsUp;
    }

    @Override
    public void setBaseIdUp(String baseIdUp) {
        this.baseIdUp = baseIdUp;
    }

    @Override
    public String getBaseIdUp() {
        return baseIdUp;
    }

    @Override
    public void setHasMoreDataToLoadUp(boolean value) {
        if (paginate != null) {
            paginate.setHasMoreDataToLoadUp(value);
        }
    }

    /**
     * ON RECYCLER VIEW ITEM CLICK
     */
    @Override
    public void onItemClick(Object object) {
        super.onItemClick(object);
        presenter.onItemClick(object);
    }
}