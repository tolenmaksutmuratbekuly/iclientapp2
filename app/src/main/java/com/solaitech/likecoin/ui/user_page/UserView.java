package com.solaitech.likecoin.ui.user_page;

import com.solaitech.likecoin.data.models.campaign.Merchant;
import com.solaitech.likecoin.data.models.user.User;

import java.util.List;

public interface UserView {
    void showLoadingIndicator(boolean show);

    void setUser(User user);

    void showUserLevel(int userLevel);

    void showProfileImage(String avatarUrl);

    void showUserName(String name);

    void showSurname(String surname);

    void showShortDescription(String description);

    void showUrl(String url);

    void showError(String error);

    String getUnknownExceptionMessage();

    String getConnectionTimeOutExceptionMessage();

    void setAmbassadorRoles(List<Merchant> ambassadorRoles);
}