package com.solaitech.likecoin.ui.coupons.detail;

import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;

import com.solaitech.likecoin.data.models.coupons.CouponDetail;
import com.solaitech.likecoin.data.models.enums.CouponTypeEnums;
import com.solaitech.likecoin.data.models.enums.CouponsStateIdEnums;
import com.solaitech.likecoin.data.params.QrBitmapParams;
import com.solaitech.likecoin.data.repository.coupons.CouponsRepositoryProvider;
import com.solaitech.likecoin.data.repository.qr_code.QrCodeRepositoryProvider;
import com.solaitech.likecoin.network.API;
import com.solaitech.likecoin.ui.base.PresenterErrorHandler;
import com.solaitech.likecoin.utils.FileUtils;
import com.solaitech.likecoin.utils.pdf.PDFileFormSave;

import java.io.File;

import rx.Subscriber;
import rx.Subscription;
import rx.functions.Action0;
import rx.subscriptions.CompositeSubscription;

//import org.web3j.crypto.CipherException;

class CouponDetailPresenter {
    private CouponDetailView view;
    private API api;
    private CompositeSubscription compositeSubscription = new CompositeSubscription();
    private Bitmap qrCodeBitmap;

    CouponDetailPresenter(CouponDetailView view, API api) {
        this.view = view;
        this.api = api;
    }

    public void init() {
        if (view.getCouponType() == CouponTypeEnums.USED) {
            view.hideShowCouponView();
        }
    }

    private void getCouponById() {
        Subscription subscription = CouponsRepositoryProvider
                .provideRepository(api)
                .getCouponById(view.getCouponId())
                .doOnSubscribe(new Action0() {
                    @Override
                    public void call() {
                        if (view.isAppBarLayoutExpanded()) {
                            view.hideAppBarLayout();
                        }
                        if (view.getCouponType() == CouponTypeEnums.ACTIVE) {
                            view.hideShowCouponView();
                        }
                        view.showProgressTemplateView();
                    }
                })
                .subscribe(new Subscriber<CouponDetail>() {
                    @Override
                    public void onCompleted() {
                        view.showAppBarLayout();
                        view.showSuccessTemplateView();

                        if (view.getCouponType() == CouponTypeEnums.ACTIVE) {
                            view.showShowCouponView();
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        PresenterErrorHandler.handleError(e, view);
                    }

                    @Override
                    public void onNext(CouponDetail couponDetail) {
                        if (couponDetail.getCampaign() != null) {
                            view.showImageLarge(couponDetail.getCampaign().getLargeImgUrl());
                        }

                        view.setCouponDetail(couponDetail);

                        if (couponDetail.getCampaign() != null
                                && couponDetail.getCampaign().getMerchant() != null
                                && couponDetail.getCampaign().getMerchant().getTitle() != null) {
                            view.setToolbarTitle(couponDetail.getCampaign().getMerchant().getTitle());
                        } else {
                            view.setToolbarTitle("");
                        }

                        // Load QR Code
                        if (couponDetail.getCampaign() != null && couponDetail.getCampaign().getTypeId() != null) {
                            if (couponDetail.getCampaign().getTypeId() != CouponsStateIdEnums.CAMPAIGN_TYPE_PROMOCODE_DEFAULT) {
                                getQrCode(couponDetail.getKey());
                            }
                        }
                    }
                });
        compositeSubscription.add(subscription);
    }

    void onFragmentViewCreated() {
        getCouponById();
    }

    public void onDestroyView() {
        unsubscribe();
    }

    private void unsubscribe() {
        if (compositeSubscription != null
                && !compositeSubscription.isUnsubscribed()) {
            compositeSubscription.unsubscribe();
        }
    }

    void onShowCouponButtonClick() {
        CouponDetail couponDetail = view.getCouponDetail();
        if (couponDetail != null) {
            boolean showQrCode = false;
            if (couponDetail.getCampaign() != null && couponDetail.getCampaign().getTypeId() != null) {
                if (couponDetail.getCampaign().getTypeId() != CouponsStateIdEnums.CAMPAIGN_TYPE_PROMOCODE_DEFAULT) {
                    showQrCode = true;
                }
            }

            view.openPromocodeDialog(couponDetail.getKey(), showQrCode);
        }
    }

    void onTryUploadDataAgainClick() {
        getCouponById();
    }

    void onRationaleToStorageAllowed() {
        view.requestPermissionToStorage();
    }

    void onDialogToStorageAllowed() {
        view.openSettingsActivity();
    }

    void onPermissionGrantedToStorage() {
        FileUtils.createFolder(view.getAppContext());
//        generateNewWalletFile();
        CouponDetail couponDetail = view.getCouponDetail();
        String pdfFilePath = null;
        if (couponDetail != null) {
            if (couponDetail.getCampaign() != null && couponDetail.getCampaign().getTypeId() != null) {
                if (couponDetail.getCampaign().getTypeId() != CouponsStateIdEnums.CAMPAIGN_TYPE_PROMOCODE_DEFAULT) {
                    if (qrCodeBitmap != null) {
                        pdfFilePath = new PDFileFormSave(view.getAppContext(), couponDetail.getKey(), couponDetail.getCampaign(), view.getMapSnapshot(), qrCodeBitmap).createPDFile();
                    } else {
                        view.qrCodeGeneratedError();
                    }
                } else {
                    pdfFilePath = new PDFileFormSave(view.getAppContext(), couponDetail.getKey(), couponDetail.getCampaign(), view.getMapSnapshot(), qrCodeBitmap).createPDFile();
                }
            }
        }

        if (pdfFilePath != null) {
            Intent intentShareFile = new Intent(Intent.ACTION_SEND);
            File fileWithinMyDir = new File(pdfFilePath);

            if (fileWithinMyDir.exists()) {
                intentShareFile.setType("application/pdf");
                intentShareFile.putExtra(Intent.EXTRA_STREAM, Uri.parse("file://" + pdfFilePath));
                view.shareCoupon(intentShareFile);
            }
        }
    }

//    private void generateNewWalletFile() {
//        try {
//            String walletAddress;
//            // Create new key
//            File file = new File(Environment.getExternalStorageDirectory().toString() + view.getAppContext().getString(R.string.root_directory));
//
//            walletAddress = OwnWalletUtils.generateNewWalletFile("Password", file, true);
//            // Privatekey passed
////            ECKeyPair keys = ECKeyPair.create(Hex.decode(privatekey));
////            walletAddress = OwnWalletUtils.generateWalletFile("Password", keys, new File(this.getFilesDir(), ""), true);
//            Log.d("alksdjalsd", "salkdjasdklasd walletAddress=" + walletAddress);
//        } catch (CipherException e) {
//            e.printStackTrace();
//        } catch (IOException e) {
//            e.printStackTrace();
//        } catch (InvalidAlgorithmParameterException e) {
//            e.printStackTrace();
//        } catch (NoSuchAlgorithmException e) {
//            e.printStackTrace();
//        } catch (NoSuchProviderException e) {
//            e.printStackTrace();
//        }
//    }

    void onPermissionNotGrantedToStorage() {
        if (view.shouldShowRequestPermissionRationaleToStorage()) {
            view.showRequestPermissionRationaleToStorage();
        } else {
            view.showRequestPermissionDialogToStorage();
        }
    }

    void onActivityResultToStorage() {
        if (view.isPermissionGrantedToStorage()) {
            onPermissionGrantedToStorage();
        }
    }

    void onShareButtonClick() {
        if (view.isPermissionGrantedToStorage()) {
            onPermissionGrantedToStorage();
        } else {
            if (view.shouldShowRequestPermissionRationaleToStorage()) {
                view.showRequestPermissionRationaleToStorage();
            } else {
                view.requestPermissionToStorage();
            }
        }
    }

    private void getQrCode(String key) {
        Subscription subscription = QrCodeRepositoryProvider
                .provideRepository()
                .getQrBitmap(getParams(key))
                .subscribe(new Subscriber<Bitmap>() {
                    @Override
                    public void onCompleted() {
                        //do nothing
                    }

                    @Override
                    public void onError(Throwable e) {
                        e.printStackTrace();
                    }

                    @Override
                    public void onNext(Bitmap bitmap) {
                        qrCodeBitmap = bitmap;
                    }
                });
        compositeSubscription.add(subscription);
    }

    private QrBitmapParams getParams(String key) {
        return new QrBitmapParams(
                key, view.getBlackColor(), view.getWhiteColor(), view.getDisplayWidth(), view.getDisplayHeight()
        );
    }
}