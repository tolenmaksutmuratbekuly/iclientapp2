package com.solaitech.likecoin.ui.contacts;

import android.Manifest;
import android.content.ContentResolver;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ShareCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.solaitech.likecoin.App;
import com.solaitech.likecoin.R;
import com.solaitech.likecoin.data.PermissionRequestCodes;
import com.solaitech.likecoin.data.models.enums.BroadcastEventEnums;
import com.solaitech.likecoin.data.models.user.User;
import com.solaitech.likecoin.ui.base.recycler_view.RecyclerViewBaseFragment;
import com.solaitech.likecoin.ui.contacts.send_like.SendLikeDialog;
import com.solaitech.likecoin.ui.menu.BottomNavigationTabFragment;
import com.solaitech.likecoin.ui.menu.MainActivity;
import com.solaitech.likecoin.ui.profile.ProfileActivity;
import com.solaitech.likecoin.ui.user_page.UserActivity;
import com.solaitech.likecoin.utils.PermissionUtils;

import java.util.List;

import static com.solaitech.likecoin.utils.StringUtils.EMPTY_STRING;

public class ContactsFragment extends RecyclerViewBaseFragment implements ContactsView, BottomNavigationTabFragment {

    private static String[] PERMISSIONS_CONTACT = {
            Manifest.permission.READ_CONTACTS
    };
    private ContactsPresenter presenter;
    final int SEND_LIKE_REQUEST_CODE = 1;
    private String localUserId;
    private ImageView img_anim_coin;
    private App app;

    public static ContactsFragment newInstance() {
        return new ContactsFragment();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        app = (App) getContext().getApplicationContext();
        localUserId = app.getUser() != null ? app.getUser().getId() : null;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = super.onCreateView(inflater, container, savedInstanceState);
        initView(v);
        presenter = new ContactsPresenter(this, api);
        presenter.init();
        return v;
    }

    @Override
    protected int getLayoutResourceId() {
        return R.layout.frgm_contacts;
    }

    private void initView(View v) {
        img_anim_coin = v.findViewById(R.id.img_anim_coin);
        initRecyclerView(v);
        initSwipeRefreshLayout(v);
    }

    @Override
    public boolean isPermissionGrantedToReadContacts() {
        return PermissionUtils.isPermissionGranted(getContext(), Manifest.permission.READ_CONTACTS);
    }

    /**
     * Requests the Contacts permissions.
     * If the permission has been denied previously, a SnackBar will prompt the user to grant the
     * permission, otherwise it is requested directly.
     */
    @Override
    public void requestContactsPermissions() {
        requestPermissions(PERMISSIONS_CONTACT, PermissionRequestCodes.REQUEST_CONTACTS);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == PermissionRequestCodes.REQUEST_CONTACTS) {
            if (PermissionUtils.verifyPermissions(grantResults)) {
                presenter.onPermissionGrantedToReadContacts();
            }
        } else {
            super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    @Override
    protected void tryUploadDataAgain() {
        presenter.onTryToUploadDataAgainClick();
    }

    @Override
    protected void onSwipeRefresh() {
        super.onSwipeRefresh();
        presenter.onSwipeRefresh();
    }

    @Override
    public void onItemClick(Object object) {
        super.onItemClick(object);
        presenter.onItemClick(object);
    }

    @Override
    public View getFABBtn() {
        if (isAdded())
            return ((MainActivity) getActivity()).getFABBtn();

        return null;
    }

    @Override
    public void showHeaderProgress() {
        if (isAdded())
            ((MainActivity) getActivity()).showHeaderProgress();

    }

    @Override
    public void hideHeaderProgress() {
        if (isAdded())
            ((MainActivity) getActivity()).hideHeaderProgress();
    }

    @Override
    public void saveRegisteredUsers(List<User> registeredUsers) {
        if (isAdded())
            ((App) getContext().getApplicationContext()).setRegisteredUsers(registeredUsers);
    }

    @Override
    public List<User> getRegisteredUsers() {
        if (isAdded())
            return ((App) getContext().getApplicationContext()).getRegisteredUsers();
        return null;
    }

    @Override
    public void shareApp(String text) {
        ShareCompat.IntentBuilder
                .from(getActivity())
                .setText(text)
                .setType("text/plain")
                .setChooserTitle(getString(R.string.invite_friend))
                .startChooser();
    }

    @Override
    public void sendLike(User distUser) {
        SendLikeDialog dialog = SendLikeDialog.newInstance(app.getUser(), distUser, false, img_anim_coin);
        dialog.setTargetFragment(this, SEND_LIKE_REQUEST_CODE);
        dialog.show(getActivity().getSupportFragmentManager(), "");
    }

    public void sendBroadcast() {
        Intent broadcastIntent = new Intent(BroadcastEventEnums.PROFILE_UPDATED);
        LocalBroadcastManager.getInstance(getContext()).sendBroadcast(broadcastIntent);
    }

    @Override
    public void setContactsUpdated(boolean isUpdated) {
        ((App) getActivity().getApplication()).setContactsUpdated(isUpdated);
    }

    @Override
    public boolean isContactsUpdated() {
        return ((App) getActivity().getApplication()).isContactsUpdated();
    }

    @Override
    public void showAccessDisclaimer() {
        this.objects.clear();
        this.objects.add(ContactsAdapter.VIEW_TYPE_FIRST_ELEMENT_EMPTY_SPACE);
        this.objects.add(ContactsAdapter.VIEW_TYPE_NO_ACCESS);

        adapter.notifyDataSetChanged();
    }

    @Override
    public ContentResolver getContentResolver() {
        return getContext().getContentResolver();
    }

    @Override
    public void updateAdapterData(List<User> registeredUsers) {
        this.objects.clear();
        if (!registeredUsers.isEmpty()) {
            this.objects.add(ContactsAdapter.VIEW_TYPE_FIRST_ELEMENT_EMPTY_SPACE);
        }
        this.objects.addAll(registeredUsers);

        if (!registeredUsers.isEmpty()) {
            this.objects.add(ContactsAdapter.VIEW_TYPE_FIRST_ELEMENT_EMPTY_SPACE);
            this.objects.add(ContactsAdapter.VIEW_TYPE_FIRST_ELEMENT_EMPTY_SPACE);
            this.objects.add(ContactsAdapter.VIEW_TYPE_FIRST_ELEMENT_EMPTY_SPACE);
            this.objects.add(ContactsAdapter.VIEW_TYPE_FIRST_ELEMENT_EMPTY_SPACE);
        }

        adapter.notifyDataSetChanged();
    }

    @Override
    protected RecyclerView.LayoutManager getLayoutManager() {
        return getLinearLayoutManager();
    }

    @Override
    protected RecyclerView.Adapter getAdapter() {
        adapter = new ContactsAdapter(getContext(), objects);
        ((ContactsAdapter) adapter).setCallback(new ContactsAdapter.Callback() {
            @Override
            public void avatarClick(String userId) {
                if (localUserId != null && !userId.equals(localUserId)) {
                    Intent intent = UserActivity.getIntent(getContext(), userId, EMPTY_STRING, true);
                    startActivity(intent);
                } else {
                    Intent intent = ProfileActivity.getIntent(getContext());
                    startActivity(intent);
                }
            }
        });
        ((ContactsAdapter) adapter).setOnRecyclerViewItemClickListener(this);
        return adapter;
    }

    public void scrollToPosition(int position) {
        ((LinearLayoutManager) recyclerView.getLayoutManager()).scrollToPositionWithOffset(position, 0);
    }

    @Override
    public void onBottomNavigationTabReselected() {
        presenter.onBottomNavigationTabReselected();
        scrollToPosition(0);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ((MainActivity) getActivity()).hideHeaderProgress();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
//        presenter.onTryToUploadDataAgainClick();
    }
}