package com.solaitech.likecoin;

import android.app.Application;

import com.crashlytics.android.Crashlytics;
import com.facebook.stetho.Stetho;
import com.onesignal.OneSignal;
import com.solaitech.likecoin.data.models.user.User;
import com.solaitech.likecoin.data.preferences.Preferences;
import com.solaitech.likecoin.data.preferences.PreferencesImpl;
import com.solaitech.likecoin.ui.push_notification.NotificationOpenedHandler;
import com.solaitech.likecoin.ui.push_notification.NotificationReceivedHandler;
import com.solaitech.likecoin.utils.LanguageUtils;
import com.solaitech.likecoin.utils.stetho.StethoCustomConfigBuilder;

import java.util.ArrayList;
import java.util.List;

import io.fabric.sdk.android.Fabric;

////////////////////////////////////////////////
/// UserStatementView                        ///
////////////////////////////////////////////////

public class App extends Application {

    private Preferences preferences;
    private boolean isContactsUpdated; // флаг, о синхронизации контактов. Чтобы не отправлять тяжелые запросы при каждом открытии раздела "Контакты".


    private User user;

    private List<User> registeredUsers = new ArrayList<>();

    public boolean isContactsUpdated() {
        return isContactsUpdated;
    }

    public void setContactsUpdated(boolean contactsUpdated) {
        isContactsUpdated = contactsUpdated;
    }

    @Override
    public void onCreate() {

        super.onCreate();
        preferences = new PreferencesImpl(this);

        Fabric.with(this, new Crashlytics());
        setupOneSignal();
        if (BuildConfig.IS_DEBUG) {
            setupStetho();
        }

        LanguageUtils langUtil = new LanguageUtils(preferences, this);
        langUtil.fixLocale(langUtil.getLocale());
    }

    private void setupOneSignal() {
        OneSignal.startInit(this)
                .inFocusDisplaying(OneSignal.OSInFocusDisplayOption.None)
                .unsubscribeWhenNotificationsAreDisabled(true)
                .setNotificationReceivedHandler(new NotificationReceivedHandler())
                .setNotificationOpenedHandler(new NotificationOpenedHandler(this, preferences))
                .init();
    }

    private void setupStetho() {
        Stetho.Initializer initializer = new StethoCustomConfigBuilder(this)
                .viewHierarchyInspectorEnabled(false)
                .build();
        Stetho.initialize(initializer);
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public List<User> getRegisteredUsers() {
        return registeredUsers;
    }

    public void setRegisteredUsers(List<User> registeredUsers) {
        this.registeredUsers.clear();
        this.registeredUsers.addAll(registeredUsers);
    }
}
//errorCode
//        if (errorCode != 200) {
//        } else if (errorCode == 200 && body == EMPTY) {
//
//        } else if (errorCode == 200 && object == EMPTY) {
//
//        }