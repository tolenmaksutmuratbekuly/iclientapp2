package com.solaitech.likecoin.config;

import android.os.Build;

import static com.solaitech.likecoin.BuildConfig.APP_HEADER_VERSION;
import static com.solaitech.likecoin.BuildConfig.VERSION_NAME;

public class LikeCoinConfig {
    public static final String BASIC_AUTH_HEADER = APP_HEADER_VERSION;
    public static final String APP_VERSION = VERSION_NAME;
    public static final String ANDROID_SDK_VERSION = Build.VERSION.RELEASE;
    public static final String APP_TYPE = "client";

    public static String getDeviceName() {
        String manufacturer = Build.MANUFACTURER;
        String model = Build.MODEL;
        if (model.startsWith(manufacturer)) {
            return capitalize(model);
        } else {
            return capitalize(manufacturer) + " " + model;
        }
    }

    private static String capitalize(String s) {
        if (s == null || s.length() == 0) {
            return "";
        }
        char first = s.charAt(0);
        if (Character.isUpperCase(first)) {
            return s;
        } else {
            return Character.toUpperCase(first) + s.substring(1);
        }
    }
}