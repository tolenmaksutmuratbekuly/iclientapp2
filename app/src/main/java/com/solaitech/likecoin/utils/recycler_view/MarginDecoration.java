package com.solaitech.likecoin.utils.recycler_view;

import android.content.Context;
import android.graphics.Rect;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.solaitech.likecoin.R;

/**
 * Created by computer on 12/10/15.
 */
public class MarginDecoration extends RecyclerView.ItemDecoration {

    private int marginLeft;
    private int marginTop;
    private int marginRight;
    private int marginBottom;

    public MarginDecoration(Context context) {
        marginLeft = context.getResources().getDimensionPixelSize(R.dimen.dp_0);
        marginTop = context.getResources().getDimensionPixelSize(R.dimen.dp_0);
        marginRight = context.getResources().getDimensionPixelSize(R.dimen.dp_0);
        marginBottom = context.getResources().getDimensionPixelSize(R.dimen.dp_0);
    }

    public MarginDecoration(int left, int top, int right, int bottom) {
        marginLeft = left;
        marginTop = top;
        marginRight = right;
        marginBottom = bottom;
    }

    @Override
    public void getItemOffsets(
            Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
        outRect.set(marginLeft, marginTop, marginRight, marginBottom);
    }

}