package com.solaitech.likecoin.utils;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;

public class BigDecimalUtils {
    private static DecimalFormat decimalFormat;

    public static DecimalFormat getDecimalFormat() {

        if (decimalFormat == null) {

            DecimalFormatSymbols decimalFormatSymbols = new DecimalFormatSymbols();
            decimalFormatSymbols.setGroupingSeparator(',');
            decimalFormatSymbols.setDecimalSeparator('.');
            decimalFormat = new DecimalFormat("###,##0.00", decimalFormatSymbols);
        }

        return decimalFormat;
    }

    public static String format(BigDecimal bigDecimal) {
        if (bigDecimal == null) {
            return "";
        }
        return getDecimalFormat().format(bigDecimal);
    }
}