package com.solaitech.likecoin.utils;


import android.content.Context;
import android.content.res.Configuration;

import com.solaitech.likecoin.data.Constants;
import com.solaitech.likecoin.data.preferences.Preferences;

import java.util.Locale;

public class LanguageUtils {
    private Preferences preferences;
    private Context context;
    public LanguageUtils(Preferences preferences, Context context) {
        this.preferences = preferences;
        this.context = context;
    }

    public Locale getLocale() {
        String savedLang = preferences.getLanguage();
        if (savedLang.length() > 0) {
            return new Locale(savedLang);
        } else {
            String lang = Locale.getDefault().getLanguage();
            if (lang.equals(Constants.KEY_LANG_RU)) {
                preferences.setLanguage(Constants.KEY_LANG_RU);
            } else if (lang.equals(Constants.KEY_LANG_EN)) {
                preferences.setLanguage(Constants.KEY_LANG_EN);
            } else {
                preferences.setLanguage(Constants.KEY_LANG_RU);
            }

            if (DeviceUtils.isAboveNougat()) {
                return context.getResources().getConfiguration().getLocales().get(0);
            } else {
                return context.getResources().getConfiguration().locale;
            }
        }
    }

    public void fixLocale(Locale locale) {
        Configuration configuration = context.getResources().getConfiguration();
        Locale.setDefault(locale);
        if (DeviceUtils.isJellyBeanMR1OrAbove()) {
            configuration.setLocale(locale);
        } else {
            configuration.locale = locale;
        }
        context.getResources().updateConfiguration(configuration, context.getResources().getDisplayMetrics());
    }
}
