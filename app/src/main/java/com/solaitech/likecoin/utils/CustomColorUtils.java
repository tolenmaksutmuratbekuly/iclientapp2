package com.solaitech.likecoin.utils;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.graphics.drawable.ShapeDrawable;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.widget.TextView;

import com.solaitech.likecoin.R;

public class CustomColorUtils {

    public static int getColor(Context context, int colorResourceId) {
        return ContextCompat.getColor(context, colorResourceId);
    }

    public static int[] getColorsForSwipeRefreshLayout(Context context) {
        int[] colors = {
                ContextCompat.getColor(context, R.color.colorPrimary),
                ContextCompat.getColor(context, R.color.colorPrimaryDark),
                ContextCompat.getColor(context, R.color.colorAccent)
        };

        return colors;
    }

    public static void changeRoundedShapeBackgroundColor(View v, int color) {
        Drawable background = v.getBackground();

        if (background instanceof ShapeDrawable) {
            ((ShapeDrawable) background).getPaint().setColor(color);
        } else if (background instanceof GradientDrawable) {
            ((GradientDrawable) background).setColor(color);
        } else if (background instanceof ColorDrawable) {
            ((ColorDrawable) background).setColor(color);
        } else {
            v.setBackgroundColor(color);
        }
    }

    public static void setLevelColor(Context context, GradientDrawable drawable, int level, TextView tv_level_title) {
        int color = R.color.level_progress_default;
        TypedArray ta = context.getResources().obtainTypedArray(R.array.levels);
        drawable.setColor(ContextCompat.getColor(context, color));
        if (tv_level_title != null) {
            tv_level_title.setTextColor(ContextCompat.getColor(context, color));
        }
        for (int i = 0; i < ta.length(); i++) {
            if (level - 1 == i) {
                color = ta.getColor(i, 0);
                drawable.setColor(color);
                if (tv_level_title != null) {
                    tv_level_title.setTextColor(color);
                }
            }
        }
        ta.recycle();
    }

    public static int getLevelColor(Context context, int level) {
        int color = ContextCompat.getColor(context, R.color.level_progress_default);
        TypedArray ta = context.getResources().obtainTypedArray(R.array.levels);
        for (int i = 0; i < ta.length(); i++) {
            if (level - 1 == i) {
                color = ta.getColor(i, 0);
            }
        }
        ta.recycle();
        return color;
    }
}
