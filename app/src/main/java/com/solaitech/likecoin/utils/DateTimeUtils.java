package com.solaitech.likecoin.utils;

import android.content.Context;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import com.solaitech.likecoin.R;

public class DateTimeUtils {

    private static final String DEFAULT_PATTERN = "dd.MM.yyyy";
    private static final String USER_STATEMENT_TIME_PATTERN = "HH:mm";
    private static final String USER_STATEMENT_DATE_PATTERN = "dd.MM.yy";
    private static final String USER_CONTACT_UPDATE_DATE_PATTERN = "yyyy-MM-dd HH:mm:ss";
    private static final String USER_STATEMENT_DATE_PARAM_PATTERN = "yyyy-MM-dd";

    public static String convertDateToString(Date date) {
        return convertDateToString(date, DEFAULT_PATTERN);
    }

    public static String convertUserStatementTimeToString(Date date) {
        return convertDateToString(date, USER_STATEMENT_TIME_PATTERN);
    }

    public static String convertUserStatementDateToString(Date date) {
        return convertDateToString(date, USER_STATEMENT_DATE_PATTERN);
    }

    public static Date convertLongToDate(long timestamp) {
        Date date = new Date();
        date.setTime(timestamp);
        return date;
    }

    public static String convertUserContactUpdateTimestampToString(long timestamp) {
        return convertDateToString(convertLongToDate(timestamp), USER_CONTACT_UPDATE_DATE_PATTERN);
    }

    public static String convertUserStatementDateParamToString(Date date) {
        return convertDateToString(date, USER_STATEMENT_DATE_PARAM_PATTERN);
    }

    public static String convertDateToString(Date date, String pattern) {
        try {
            SimpleDateFormat sdf = new SimpleDateFormat(pattern);
            return sdf.format(date);
        } catch (Exception e) {
            return "-";
        }
    }

    public static Date convertStringToDate(String dateInString, String pattern) {
        SimpleDateFormat sdfFrom = new SimpleDateFormat(pattern);
        Date date = null;
        try {
            date = sdfFrom.parse(dateInString);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return date;
    }

    /**
     * @param dt1
     * @param dt2
     * @return if dt1 more than dt2 => 1, if less => -1, if equals => 0
     */
    public static int compareContactDates(Date dt1, Date dt2) {
        long t1 = dt1.getTime();
        long t2 = dt2.getTime();

        if (t1 - t2 > 1000)
            return 1;
        else if (t1 - t2 < -1000)
            return -1;

        return 0;
    }

    public static String convertMillisToTimer(long seconds) {
        Date date = new Date(seconds * 1000);
        SimpleDateFormat sdf = new SimpleDateFormat("mm:ss");
        return sdf.format(date);
    }

    public static String newsFeedDate(Date origDate, Context context) {
        long origTmstmp = origDate.getTime();

        Calendar origCalendar = Calendar.getInstance();
        origCalendar.setTime(origDate);
        int origDay = origCalendar.get(Calendar.DAY_OF_MONTH);
        int origMonth = origCalendar.get(Calendar.MONTH);
        int origYear = origCalendar.get(Calendar.YEAR);
        int origHours = origCalendar.get(Calendar.HOUR_OF_DAY);
        int origMinutes = origCalendar.get(Calendar.MINUTE);
        origCalendar.get(Calendar.SECOND); //number of seconds

        long difference = 0;
        long mDate = System.currentTimeMillis();
        Date currDate = new Date();
        Calendar currCalendar = Calendar.getInstance();
        currCalendar.setTime(currDate);
        int currDay = currCalendar.get(Calendar.DAY_OF_MONTH);
        int currYear = currCalendar.get(Calendar.YEAR);

        //Yesterday
        currCalendar.add(Calendar.DATE, -1);
        Date yesterdayDate = currCalendar.getTime();
        Calendar yesterdayCalendar = Calendar.getInstance();
        yesterdayCalendar.setTime(yesterdayDate);
        int yesterdayDay = currCalendar.get(Calendar.DAY_OF_MONTH);
        int yesterdayYear = currCalendar.get(Calendar.YEAR);

        difference = mDate - origTmstmp;
        final long seconds = difference / 1000;
        final long minutes = seconds / 60;
        final long hours = minutes / 60;
        final long days = hours / 24;
        final long months = days / 31;
        final long years = days / 365;

        if (seconds < 86400 && origDay == currDay
                && origYear == currYear) {
            return (origHours < 10 ? "0" : "") + origHours +
                    ":" +
                    (origMinutes < 10 ? "0" : "") + origMinutes;
        }
//        else if (seconds < 172800 && origDay == yesterdayDay
//                && origYear == yesterdayYear) {
//            return context.getString(R.string.label_yesterday) + ", " +
//                    (origHours < 10 ? "0" : "") + origHours +
//                    ":" +
//                    (origMinutes < 10 ? "0" : "") + origMinutes;
//        }
        else if (origYear != currYear) {
            return getDateStr(origDay, origMonth, origYear, origHours, origMinutes, true, context);
        } else {
            return getDateStr(origDay, origMonth, origYear, origHours, origMinutes, false, context);
        }
    }

    private static String getDateStr(int day, int month, int year, int hours, int minutes, boolean isContainYear, Context context) {
        String arrMonth[] = context.getResources().getStringArray(R.array.months_array_short);
        if (isContainYear)
            // Longer date with year
            return day + " " + arrMonth[month] + ". " + year + ", " +
                    (hours < 10 ? "0" : "") + hours +
                    ":" +
                    (minutes < 10 ? "0" : "") + minutes;

        else
            // Longer date without year
            return day + " " + arrMonth[month] + ". " +
                    (hours < 10 ? "0" : "") + hours +
                    ":" +
                    (minutes < 10 ? "0" : "") + minutes;

    }

    public static String getCampaignConditionsDate(Context context, Date origDate) {
        String result = StringUtils.EMPTY_STRING;
        if (origDate != null) {
            String arrMonth[] = context.getResources().getStringArray(R.array.months_array_long);
            Calendar origCalendar = Calendar.getInstance();
            origCalendar.setTime(origDate);
            int origDay = origCalendar.get(Calendar.DAY_OF_MONTH);
            int origMonth = origCalendar.get(Calendar.MONTH);
            int origYear = origCalendar.get(Calendar.YEAR);

            Date currDate = new Date();
            Calendar currCalendar = Calendar.getInstance();
            currCalendar.setTime(currDate);
            int currYear = currCalendar.get(Calendar.YEAR);

            if (origYear == currYear) {
                result = origDay + " " + arrMonth[origMonth];
            } else {
                result = origDay + " " + arrMonth[origMonth] + ", " + origYear;
            }
        }
        return result;
    }
}