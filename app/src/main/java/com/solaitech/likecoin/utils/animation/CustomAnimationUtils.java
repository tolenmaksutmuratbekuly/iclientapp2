package com.solaitech.likecoin.utils.animation;

import android.animation.LayoutTransition;
import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;

import com.plattysoft.leonids.ParticleSystem;
import com.solaitech.likecoin.R;

import de.hdodenhof.circleimageview.CircleImageView;

public class CustomAnimationUtils {

    @TargetApi(android.os.Build.VERSION_CODES.JELLY_BEAN)
    public static void enableTransition(ViewGroup v) {

        int currentapiVersion = android.os.Build.VERSION.SDK_INT;
        if (currentapiVersion >= android.os.Build.VERSION_CODES.JELLY_BEAN) {

            LayoutTransition transition = v.getLayoutTransition();
            if (transition != null) {
                transition.enableTransitionType(LayoutTransition.CHANGING);
            }
        }
    }

    public static void flippingCoinAnimation(final Context context, ImageView img_coin, final CircleImageView iv_avatar, final AnimationCallback callback) {
        final FlipAnimation flipAnimation = FlipAnimation.create().with(img_coin);
        flipAnimation.setDuration(650);
        flipAnimation.setRepeatCount(4);
        flipAnimation.start();

        Animation animation = AnimationUtils.loadAnimation(context, R.anim.flipping);
        animation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                flipAnimation.stopAnimation();
                animation.cancel();
                new ParticleSystem((Activity) context, 10, R.drawable.star_pink, 600)
                        .setSpeedRange(0.1f, 0.20f)
                        .oneShot(iv_avatar, 10);
                if (callback != null)
                    callback.onAnimationFinish();
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        img_coin.startAnimation(animation);
    }
}