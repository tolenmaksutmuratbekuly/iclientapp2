package com.solaitech.likecoin.utils;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;

import com.solaitech.likecoin.BuildConfig;

public class GooglePlayAppLicationUtils {

    private static final String APP_ID = BuildConfig.APPLICATION_ID;

    public static void openAppDownloadLink(Context context) {
        try {
            context.startActivity(new Intent(Intent.ACTION_VIEW,
                    Uri.parse("market://details?id=" + APP_ID)));
        } catch (android.content.ActivityNotFoundException anfe) {
            context.startActivity(new Intent(Intent.ACTION_VIEW,
                    Uri.parse("http://play.google.com/store/apps/details?id=" + APP_ID)));
        }
    }

}