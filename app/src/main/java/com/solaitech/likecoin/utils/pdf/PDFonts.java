package com.solaitech.likecoin.utils.pdf;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Font;
import com.itextpdf.text.pdf.BaseFont;

import java.io.IOException;

public class PDFonts {
    public static Font fontBlack12;
    public static Font fontBlackBold12;
    public static Font fontBlack14;
    public static Font fontBlackBold14;
    public static Font fontBlackBold16;
    public static Font fontBlackBold19;

    public void initFonts() {
        try {
            BaseFont baseFont = BaseFont.createFont("assets/timcyr.ttf", BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
            fontBlack12 = new Font(baseFont, 12, Font.NORMAL, BaseColor.BLACK);
            fontBlackBold12 = new Font(baseFont, 12, Font.BOLD, BaseColor.BLACK);
            fontBlack14 = new Font(baseFont, 14, Font.NORMAL, BaseColor.BLACK);
            fontBlackBold14 = new Font(baseFont, 14, Font.BOLD, BaseColor.BLACK);
            fontBlackBold16 = new Font(baseFont, 16, Font.BOLD, BaseColor.BLACK);
            fontBlackBold19 = new Font(baseFont, 19, Font.BOLD, BaseColor.BLACK);

        } catch (DocumentException | IOException e) {
            e.printStackTrace();
        }
    }
}
