package com.solaitech.likecoin.utils.pdf;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.Environment;

import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.pdf.PdfWriter;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;

import com.solaitech.likecoin.R;
import com.solaitech.likecoin.data.models.campaign.CampaignDetail;
import com.solaitech.likecoin.ui.coupons.detail.pdf.CouponPDF;
import com.solaitech.likecoin.utils.StringUtils;

public class PDFileFormSave {
    private Context context;
    private String key;
    private Object object;
    private Bitmap mapSnapshot;
    private Bitmap qrCodeBitmap;
    public PDFileFormSave(Context context, String key, Object object, Bitmap mapSnapshot, Bitmap qrCodeBitmap) {
        this.context = context;
        this.key = key;
        this.object = object;
        this.mapSnapshot = mapSnapshot;
        this.qrCodeBitmap = qrCodeBitmap;
        new PDFonts().initFonts();
    }

    public String createPDFile() {
        File file = null;
        try {
            if (object instanceof CampaignDetail) {
                CampaignDetail campaignDetail = (CampaignDetail) object;
                String merchantTitle = StringUtils.EMPTY_STRING;
                if (campaignDetail.getMerchant() != null && campaignDetail.getMerchant().getTitle() != null) {
                    merchantTitle = campaignDetail.getMerchant().getTitle();
                }

                Long tsLong = System.currentTimeMillis();
                String timeStamp = tsLong.toString();
                String fileTemp = String.format("%s_%s.pdf", merchantTitle, timeStamp);
                file = new File(Environment.getExternalStorageDirectory().toString() + context.getString(R.string.root_directory) + context.getResources().getString(R.string.root_directory_coupons), fileTemp);
                Document document = new Document();
                PdfWriter.getInstance(document, new FileOutputStream(file));
                document.open();
                addMetaData(document);

                new CouponPDF(context, key, (CampaignDetail)object, document, mapSnapshot, qrCodeBitmap).addPage();
                document.newPage();

                document.close();
            }
        } catch (DocumentException | FileNotFoundException e) {
            e.printStackTrace();
        }

        return file != null ? file.getAbsolutePath() : null;
    }

    // Add metadata to the PDF which can be viewed in your Adobe Reader under File -> Properties
    private void addMetaData(Document document) {
        document.addTitle(context.getString(R.string.app_name));
//		document.addSubject("Using iText");
//	    document.addKeywords("Java, PDF, iText");
//	    document.addAuthor("Lars Vogel");
//	    document.addCreator("Lars Vogel");
    }
}
