package com.solaitech.likecoin.utils;

import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;

public class DialogUtils {
    private Callback callback;
    public DialogUtils() {}

    public void setCallback(Callback callback) {
        this.callback = callback;
    }

    public void showPositiveNegative(Context context, int message, int positive, int negative) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setMessage(context.getString(message));
        builder.setPositiveButton(context.getString(positive), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                if (callback != null) {
                    callback.onPositiveClick();
                }
            }
        });

        builder.setNegativeButton(context.getString(negative), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        AlertDialog alert = builder.create();
        alert.show();
    }

    public interface Callback {
        void onPositiveClick();
    }
}
