package com.solaitech.likecoin.utils;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Handler;
import android.os.SystemClock;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;

import static android.content.Context.INPUT_METHOD_SERVICE;
import static android.view.inputmethod.InputMethodManager.HIDE_NOT_ALWAYS;

public class KeyboardUtils {

//    public static void setupUI(final Context context, final View view) {
//
//        //Set up touch listener for non-text box views to hide keyboard.
//        if (!(view instanceof EditText)) {
//
//            view.setOnTouchListener(new View.OnTouchListener() {
//
//                @Override
//                public boolean onTouch(View v, MotionEvent event) {
//                    hideKeyboard(context, view);
//                    return false;
//                }
//
//            });
//        }
//
//        if (view instanceof ViewGroup) {
//
//            for (int i = 0; i < ((ViewGroup) view).getChildCount(); i++) {
//
//                View innerView = ((ViewGroup) view).getChildAt(i);
//
//                setupUI(context, innerView);
//            }
//        }
//    }

    @SuppressLint("Recycle")
    public static void showKeyBoard(final Context context, final View et) {
        showKeyBoard(context, et, 100);
    }

    public static void showKeyBoard(final Context context, final View et, long delayInMs) {
        (new Handler()).postDelayed(new Runnable() {

            public void run() {
                InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.showSoftInput(et, InputMethodManager.SHOW_IMPLICIT);
                et.dispatchTouchEvent(MotionEvent.obtain(SystemClock.uptimeMillis(), SystemClock.uptimeMillis(), MotionEvent.ACTION_DOWN, 0, 0, 0));
                et.dispatchTouchEvent(MotionEvent.obtain(SystemClock.uptimeMillis(), SystemClock.uptimeMillis(), MotionEvent.ACTION_UP, 0, 0, 0));

                try {
                    ((EditText) et).setSelection(((EditText) et).getText().toString().length());
                } catch (ClassCastException e) {

                } catch (Exception e) {
                }

            }
        }, delayInMs);
    }

    public static void hideSoftKeyboard(View view) {
        Context context = view.getContext();
        InputMethodManager inputMethodManager = (InputMethodManager) context.getSystemService(INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(view.getApplicationWindowToken(), HIDE_NOT_ALWAYS);
    }
}
