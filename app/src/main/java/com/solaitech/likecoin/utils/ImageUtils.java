package com.solaitech.likecoin.utils;

import android.content.Context;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.solaitech.likecoin.R;
import com.solaitech.likecoin.data.models.user_statement.Partner;

import de.hdodenhof.circleimageview.CircleImageView;

public class ImageUtils {

    public static void showAvatar(Context context, ImageView view, String url) {
        Glide.with(context.getApplicationContext())
                .load(url)
                .error(R.drawable.img_placeholder_rounded)
                .placeholder(R.drawable.img_placeholder_rounded)
                .dontAnimate()
                .diskCacheStrategy(DiskCacheStrategy.SOURCE)
//                .bitmapTransform(new CropCircleTransformation(context))
                .into(view);
    }

    public static void showMerchantLogo(Context context, ImageView view, String url) {
        Glide.with(context.getApplicationContext())
                .load(url)
                .error(R.drawable.img_placeholder_rounded)
                .placeholder(R.drawable.img_placeholder_rounded)
                .crossFade()
                .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                .into(view);
    }

    public static void showUserStatementProfileImage(Context context, CircleImageView view, Partner partner) {
        String imageUrl = "";
        if (partner.getImgUrl() != null) {
            imageUrl = partner.getImgUrl();
        }
        showAvatar(context, view, imageUrl);
    }

    public static void showCampaignImgSmall(Context context, ImageView view, String url) {
        Glide.with(context.getApplicationContext())
                .load(url)
                .error(R.drawable.img_placeholder)
                .placeholder(R.drawable.img_placeholder)
                .crossFade()
                .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                .into(view);
    }

    public static void showCampaignImgLarge(Context context, ImageView view, String url) {
        //1080x450
        int bgWidth = 1080;
        int bgHeight = 450;

        int displayWidth = DeviceUtils.getDisplayWidth(context);
        int height = DeviceUtils.getDisplayWidth(context)
                * bgHeight
                / bgWidth;

//        view.getLayoutParams().width = displayWidth;
        view.getLayoutParams().height = height;

        Glide.with(context.getApplicationContext())
                .load(url)
                .error(R.color.colorPrimary)
                .crossFade()
                .override(displayWidth, height)
                .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                .into(view);
    }

    public static void showNewsFeedImg(Context context, ImageView view, String url, int placeholder) {
        Glide.with(context.getApplicationContext())
                .load(url)
                .error(placeholder)
                .placeholder(placeholder)
                .crossFade()
                .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                .into(view);
    }

    public static void showAmbassadorLogo(Context context, ImageView view, String url) {
        Glide.with(context.getApplicationContext())
                .load(url)
                .error(R.drawable.img_placeholder)
                .placeholder(R.drawable.img_placeholder)
                .into(view);
    }

//    public static void clearDiskCache(Context context) {
//        Glide.get(context).clearDiskCache(); //must be called on a background thread.
//    }
//
//    public static void clearMemory(Context context) {
//        Glide.get(context).clearMemory(); //must be called on the main thread
//    }

}