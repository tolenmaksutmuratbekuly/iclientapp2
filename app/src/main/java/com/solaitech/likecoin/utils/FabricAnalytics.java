package com.solaitech.likecoin.utils;

import com.crashlytics.android.answers.Answers;
import com.crashlytics.android.answers.CustomEvent;
import com.crashlytics.android.answers.SignUpEvent;

public class FabricAnalytics {
    private static final String EVENT_BACK_BUTTON = "Pressed BACK button";
    private static final String EVENT_HOME_BUTTON = "Pressed HOME button";

    public static void signUpEvent() {
        Answers.getInstance().logSignUp(new SignUpEvent());
    }

    public static void eventBackButtonPressed() {
        Answers.getInstance().logCustom(new CustomEvent(EVENT_BACK_BUTTON));
    }

    public static void eventHomeButtonPressed() {
        Answers.getInstance().logCustom(new CustomEvent(EVENT_HOME_BUTTON));
    }
}
