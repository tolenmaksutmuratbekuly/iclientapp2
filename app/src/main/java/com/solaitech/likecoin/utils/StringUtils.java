package com.solaitech.likecoin.utils;

import android.content.Context;

public class StringUtils {
    public static final String EMPTY_STRING = "";
    public static final String PLUS_SIGN = "+";

    public static boolean isStringOk(String text) {
        return text != null && text.trim().length() > 0;
    }

    public static int length(String text) {
        if (text != null) {
            return text.trim().length();
        } else {
            return 0;
        }
    }

    public static String replaceNull(String text) {
        if (isStringOk(text)) {
            return text;
        } else {
            return "";
        }
    }

    public static boolean isStringNotNull(String text) {
        return text != null;
    }

//    public static SpannableString getExpandText(Context context) {
//        SpannableString ssExpand = new SpannableString(context.getString(R.string.expand));
//        ssExpand.setSpan(new UnderlineSpan(), 0, context.getString(R.string.expand).length(), 0);
//        return ssExpand;
//    }
//
//    public static SpannableString getRollUpText(Context context) {
//        SpannableString ssRollUp = new SpannableString(context.getString(R.string.roll_up));
//        ssRollUp.setSpan(new UnderlineSpan(), 0, context.getString(R.string.roll_up).length(), 0);
//        return ssRollUp;
//    }

    private static int getTextSize(Context context, int resourceId) {
        return context.getResources().getDimensionPixelSize(resourceId);
    }

    public static boolean isTextContainSearchQuery(String text, String searchQuery) {
        return replaceNull(text).toLowerCase().contains(searchQuery.toLowerCase());
    }

    private static char[] c = new char[]{'k', 'm', 'b', 't'};

    public static String coolFormat(double n, int iteration) {
        double d = ((long) n / 100) / 10.0;
        boolean isRound = (d * 10) % 10 == 0;//true if the decimal part is equal to 0 (then it's trimmed anyway)
        return (d < 1000 ? //this determines the class, i.e. 'k', 'm' etc
                ((d > 99.9 || isRound || (!isRound && d > 9.99) ? //this decides whether to trim the decimals
                        (int) d * 10 / 10 : d + "" // (int) d * 10 / 10 drops the decimal
                ) + "" + c[iteration])
                : coolFormat(d, iteration + 1));

    }
}