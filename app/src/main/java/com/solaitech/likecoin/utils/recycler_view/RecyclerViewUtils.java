package com.solaitech.likecoin.utils.recycler_view;

import android.support.v7.widget.RecyclerView;
import android.view.ViewGroup;

/**
 * Created by computer on 7/20/16.
 */
public class RecyclerViewUtils {

    public static RecyclerView.LayoutParams getRecyclerViewParams() {
        RecyclerView.LayoutParams lp = new RecyclerView.LayoutParams(
                ViewGroup.LayoutParams.MATCH_PARENT, //width
                ViewGroup.LayoutParams.WRAP_CONTENT);//height
        return lp;
    }

}