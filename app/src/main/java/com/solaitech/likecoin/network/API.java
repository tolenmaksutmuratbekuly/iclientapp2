package com.solaitech.likecoin.network;

import com.solaitech.likecoin.data.models.app_version_check.AppVersionChecking;
import com.solaitech.likecoin.data.models.article.Article;
import com.solaitech.likecoin.data.models.brix.BrixRate;
import com.solaitech.likecoin.data.models.campaign.Campaign;
import com.solaitech.likecoin.data.models.campaign.CampaignDetail;
import com.solaitech.likecoin.data.models.campaign.Comments;
import com.solaitech.likecoin.data.models.campaign.Merchant;
import com.solaitech.likecoin.data.models.campaign.Tag;
import com.solaitech.likecoin.data.models.common_info.CompanyDetails;
import com.solaitech.likecoin.data.models.common_info.IcoCondition;
import com.solaitech.likecoin.data.models.contacts.InvitationText;
import com.solaitech.likecoin.data.models.contacts.UserContact;
import com.solaitech.likecoin.data.models.coupons.Coupon;
import com.solaitech.likecoin.data.models.coupons.CouponDetail;
import com.solaitech.likecoin.data.models.news_feed.NewsFeed;
import com.solaitech.likecoin.data.models.registration.RegistrationComplete;
import com.solaitech.likecoin.data.models.registration.RegistrationStart;
import com.solaitech.likecoin.data.models.user.User;
import com.solaitech.likecoin.data.models.user.UserById;
import com.solaitech.likecoin.data.models.user.UserProfileQR;
import com.solaitech.likecoin.data.models.user_statement.UserStatement;
import com.solaitech.likecoin.data.params.BuyArticleParams;
import com.solaitech.likecoin.data.params.CommentParams;
import com.solaitech.likecoin.data.params.CouponPurchaseParams;
import com.solaitech.likecoin.data.params.FavoriteParams;
import com.solaitech.likecoin.data.params.Like4EntityParams;
import com.solaitech.likecoin.data.params.PushNotificationAddDeviceParams;
import com.solaitech.likecoin.data.params.RegistrationCompleteParams;
import com.solaitech.likecoin.data.params.RegistrationStartParams;
import com.solaitech.likecoin.data.params.UpdateUserInfoParams;
import com.solaitech.likecoin.data.params.brix.BuyBrixParams;
import com.solaitech.likecoin.data.params.contacts.ContactAddingParams;

import java.util.List;

import okhttp3.MultipartBody;
import okhttp3.ResponseBody;
import retrofit2.http.Body;
import retrofit2.http.DELETE;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Part;
import retrofit2.http.Path;
import retrofit2.http.Query;
import rx.Observable;

public interface API {

    /**
     * REGISTRATION
     */
    @POST("user/registrationStart")
    Observable<RegistrationStart> registrationStart(@Body RegistrationStartParams params);

    @POST("user/registrationComplete")
    Observable<RegistrationComplete> registrationComplete(@Header("userRegistrationToken") String userRegistrationToken,
                                                          @Body RegistrationCompleteParams params);

    /**
     * USER
     */
    @GET("user/")
    Observable<User> getUser();

    @GET("user/{userId}")
    Observable<User> getUser(@Path("userId") String userId);

    @GET("merchant/{merchantId}")
    Observable<Merchant> getMerchant(@Path("merchantId") String merchantId);

    @PUT("user/")
    Observable<User> updateUserInfo(@Body UpdateUserInfoParams params);

    @Multipart
    @POST("user/avatar")
    Observable<ResponseBody> uploadAvatar(@Part MultipartBody.Part file);

    @DELETE("user/avatar")
    Observable<ResponseBody> deleteUserAvatar();

    @DELETE("token/{tokenType}/{tokenKey}")
    Observable<ResponseBody> deleteUserDeviceToken(@Path("tokenType") int tokenType, @Path("tokenKey") String tokenKey);

    @DELETE("userRole/{merchantId}/{roleId}")
    Observable<ResponseBody> deleteUserRole(@Path("merchantId") String merchantId, @Path("roleId") int roleId);

    @GET("userStatement/")
    Observable<List<UserStatement>> getUserStatement(@Query("startDate") String startDate,
                                                     @Query("endDate") String endDate);

    @GET("user/{id}")
    Observable<UserById> getUserById(@Path("id") String userId);

    @POST("user/getUserProfileToken/")
    Observable<UserProfileQR> getUserProfileToken();


    /**
     * CAMPAIGN
     */
    @GET("campaign")
    Observable<List<Campaign>> getCampaigns(@Query("merchantId") String merchantId,
                                            @Query("includeInactive") Boolean includeInactive,
                                            @Query("tagId") String tagId);

    @GET("tag/campaign")
    Observable<List<Tag>> getTags();

    @GET("campaign/{campaignId}")
    Observable<CampaignDetail> getCampaignById(@Path("campaignId") String campaignId);

    @GET("comment")
    Observable<List<Comments>> getComments(@Query("objectEntityId") String objectEntityId,
                                           @Query("objectEntityTypeId") String objectEntityTypeId,
                                           @Query("baseId") String baseId,
                                           @Query("rowCount") int rowCount,
                                           @Query("direction") String direction);

    @POST("comment")
    Observable<Comments> postComment(@Body CommentParams params);

    /**
     * ARTICLE
     */
    @GET("article/{articleId}")
    Observable<Article> getArticleById(@Path("articleId") String articleId);

    /**
     * COUPONS
     */
    @GET("coupon")
    Observable<List<Coupon>> getCoupons(@Query("stateId") String stateId);

    @GET("coupon/{couponId}")
    Observable<CouponDetail> getCouponById(@Path("couponId") String couponId);

    @POST("coupon/purchase")
    Observable<List<Coupon>> couponPurchase(@Body CouponPurchaseParams params);

    /**
     * NEWS
     */
    @GET("newsFeed")
    Observable<List<NewsFeed>> getNewsFeed(@Query("baseId") String baseId,
                                           @Query("rowCount") int rowCount,
                                           @Query("direction") String direction);

    /**
     * CONTACTS
     */
    @GET("userContact/")
    Observable<List<UserContact>> getUserContacts(@Query("onlyRegistered") boolean onlyRegistered);

    @POST("userContact/")
    Observable<ResponseBody> addUserContacts(@Body List<ContactAddingParams> params);

    @DELETE("userContact/")
    Observable<ResponseBody> deleteUserContacts(@Body List<String> params);

    @POST("like/like4User")
    Observable<ResponseBody> sendLike(@Body Object params);

    @POST("articleUser")
    Observable<ResponseBody> buyArticle(@Body BuyArticleParams params);

    @POST("newsFeed/{newsFeedId}/favorite")
    Observable<NewsFeed> favorite(@Path("newsFeedId") String newsFeedId, @Body FavoriteParams params);

    @POST("like/like4Entity")
    Observable<NewsFeed> like4EntityNews(@Body Like4EntityParams params);

    @POST("like/like4Entity")
    Observable<Comments> like4EntityComment(@Body Like4EntityParams params);

    @GET("user/inviteText")
    Observable<InvitationText> getInvitationText();

    /**
     * APP VERSION CHECK
     */
    @GET("commonInfo/checkAppVersion")
    Observable<AppVersionChecking> appVersionChecking(@Query("os") String os,
                                                      @Query("appVersion") String appVersion,
                                                      @Query("appType") String appType);

    @GET("commonInfo/contacts")
    Observable<CompanyDetails> getCompanyDetails();

    @GET("commonInfo/icoCondition")
    Observable<IcoCondition> getIcoCondition();

    /**
     * PUSH NOTIFICATION
     */
    //Добавление девайса для пуш уведомлений ( POST /lc/ )
    @POST("user/addDevice")
    Observable<ResponseBody> addDevice(@Body PushNotificationAddDeviceParams params);

    /**
     * Brix
     */

    @POST("localBrix/purchase")
    Observable<String> brixPurchase(@Body BuyBrixParams params);

    @GET("localBrix/rate")
    Observable<BrixRate> brixRate();
}