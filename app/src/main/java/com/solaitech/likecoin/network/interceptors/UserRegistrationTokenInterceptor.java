package com.solaitech.likecoin.network.interceptors;

import com.solaitech.likecoin.config.LikeCoinConfig;
import com.solaitech.likecoin.data.preferences.Preferences;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

public class UserRegistrationTokenInterceptor implements Interceptor {

    private Preferences preferences;

    public UserRegistrationTokenInterceptor(Preferences preferences) {
        this.preferences = preferences;
    }

    @Override
    public Response intercept(Interceptor.Chain chain) throws IOException {
        Request originalRequest = chain.request();
        Request.Builder newRequestBuilder = originalRequest.newBuilder();
        if (!preferences.getUserDeviceToken().isEmpty()) {
            newRequestBuilder.header("userDeviceToken",
                    preferences.getUserDeviceToken());
        }
        newRequestBuilder.header("User-Agent",
                LikeCoinConfig.APP_TYPE + "/"
                        + LikeCoinConfig.APP_VERSION + " ("
                        + LikeCoinConfig.getDeviceName()
                        + ") android ("
                        + LikeCoinConfig.ANDROID_SDK_VERSION + ")");
        newRequestBuilder.header("Accept", LikeCoinConfig.BASIC_AUTH_HEADER);

        newRequestBuilder.method(originalRequest.method(), originalRequest.body());
        Request newRequest = newRequestBuilder.build();
        return chain.proceed(newRequest);
    }
}