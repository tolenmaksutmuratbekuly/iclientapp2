package com.solaitech.likecoin.network.error;

import com.solaitech.likecoin.exceptions.APIException;
import com.solaitech.likecoin.exceptions.ConnectionTimeOutException;
import com.solaitech.likecoin.exceptions.NeedReAuthorizeException;
import com.solaitech.likecoin.exceptions.UnknownException;
import com.solaitech.likecoin.network.GsonProvider;
import com.solaitech.likecoin.utils.StringUtils;

import java.io.IOException;

import retrofit2.Response;
import retrofit2.adapter.rxjava.HttpException;

import static com.solaitech.likecoin.network.error.APIError.ERROR_CODE_NEED_RE_AUTHORIZE;

public class RetrofitErrorHandler {

    public static void handleException(Throwable e)
            throws
            APIException,
            UnknownException,
            ConnectionTimeOutException,
            NeedReAuthorizeException {

        if (e instanceof NeedReAuthorizeException) {
            throw new NeedReAuthorizeException();
        } else if (e instanceof HttpException) {
            handleHttpException(e);
        } else if (e instanceof IOException) {
            throw new ConnectionTimeOutException();
        } else {
            throw new UnknownException();
        }
    }

    private static void handleHttpException(Throwable e)
            throws
            APIException,
            UnknownException,
            NeedReAuthorizeException {

        HttpException exception = (HttpException) e;
        Response response = exception.response();

        if (response != null) {
            APIError apiError;

            try {
                apiError = parseError(response);
            } catch (Exception e1) {
                e1.printStackTrace();
                throw new UnknownException();
            }

            if (apiError != null) {
                String errorCode = apiError.getErrorCode();
                String errorMessage = StringUtils.replaceNull(apiError.getErrorDescr());

                if (errorCode.equals(ERROR_CODE_NEED_RE_AUTHORIZE)) {
                    throw new NeedReAuthorizeException();
                }
                throw new APIException(errorMessage);
            }
        }

        throw new UnknownException();
        //handle JsonParseException, occured when html page returned instead of JSON
    }

    private static APIError parseError(Response<?> response) throws IOException {
        return GsonProvider.gson.fromJson(response.errorBody().string(), APIError.class);
    }
}