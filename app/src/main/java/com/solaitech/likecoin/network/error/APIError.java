package com.solaitech.likecoin.network.error;

public class APIError {
    public static final String ERROR_CODE_NEED_RE_AUTHORIZE = "-1000";
    private String errorCode;
    private String errorDescr;

    public String getErrorCode() {
        return errorCode;
    }

    public String getErrorDescr() {
        return errorDescr;
    }

}