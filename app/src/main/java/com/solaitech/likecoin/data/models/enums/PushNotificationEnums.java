package com.solaitech.likecoin.data.models.enums;

public class PushNotificationEnums {
    public static final String SYS_MESSAGE = "sys_message";
    public static final String TRANSACTION = "transaction";
    public static final String CAMPAIGN = "campaign";
    public static final String COMMENT = "comment";

    public static final String PUSH_NOTIFICATION_TYPE_GENERIC = "0";
    public static final String PUSH_NOTIFICATION_P2P_INPUT = "1"; // display false
    public static final String PUSH_NOTIFICATION_TYPE_NEXT_LEVEL = "2";
    public static final String PUSH_NOTIFICATION_TYPE_CAMPAIGN = "3";
    public static final String PUSH_NOTIFICATION_TYPE_CAMPAIGN_LIST = "4";
    public static final String PUSH_NOTIFICATION_TYPE_COMMENT_REPLY = "5";
    public static final String PUSH_NOTIFICATION_TYPE_USED_COUPON = "6";

}