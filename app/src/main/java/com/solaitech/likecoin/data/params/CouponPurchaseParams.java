package com.solaitech.likecoin.data.params;

public class CouponPurchaseParams {

    private String campaignId;
    private String couponsCount;

    public CouponPurchaseParams(String campaignId, String couponsCount) {
        this.campaignId = campaignId;
        this.couponsCount = couponsCount;
    }

}