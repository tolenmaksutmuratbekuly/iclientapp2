package com.solaitech.likecoin.data.models.campaign;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.List;

public class MerchantContacts implements Parcelable {
    private List<String> whatsApp;
    private List<String> mobile;
    private List<String> landline;
    private List<String> fax;
    private List<String> email;

    protected MerchantContacts(Parcel in) {
        whatsApp = in.createStringArrayList();
        mobile = in.createStringArrayList();
        landline = in.createStringArrayList();
        fax = in.createStringArrayList();
        email = in.createStringArrayList();
    }

    public static final Creator<MerchantContacts> CREATOR = new Creator<MerchantContacts>() {
        @Override
        public MerchantContacts createFromParcel(Parcel in) {
            return new MerchantContacts(in);
        }

        @Override
        public MerchantContacts[] newArray(int size) {
            return new MerchantContacts[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeStringList(whatsApp);
        parcel.writeStringList(mobile);
        parcel.writeStringList(landline);
        parcel.writeStringList(fax);
        parcel.writeStringList(email);
    }

    public List<String> getWhatsApp() {
        return whatsApp;
    }

    public List<String> getMobile() {
        return mobile;
    }

    public List<String> getLandline() {
        return landline;
    }

    public List<String> getFax() {
        return fax;
    }

    public List<String> getEmail() {
        return email;
    }
}
