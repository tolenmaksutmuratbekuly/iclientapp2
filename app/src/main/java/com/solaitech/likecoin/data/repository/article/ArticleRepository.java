package com.solaitech.likecoin.data.repository.article;

import com.solaitech.likecoin.data.models.article.Article;
import com.solaitech.likecoin.data.models.campaign.Campaign;
import com.solaitech.likecoin.data.models.campaign.CampaignDetail;
import com.solaitech.likecoin.data.models.campaign.Tag;
import com.solaitech.likecoin.data.models.user_statement.UserStatement;
import com.solaitech.likecoin.network.API;

import java.util.List;

import rx.Observable;

public interface ArticleRepository {

    void setAPI(API api);

    Observable<Article> getArticleById(String articleId);
}