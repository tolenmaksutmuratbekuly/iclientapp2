package com.solaitech.likecoin.data.models.common_info;

import java.util.List;

public class CompanyDetails {
    private List<String> emails;
    private List<String> addresses;
    private List<String> mobileNumbers;
    private List<String> landlineNumbers;
    private List<String> whatsAppNumbers;
    private String companyName;

    public List<String> getEmails() {
        return emails;
    }

    public List<String> getAddresses() {
        return addresses;
    }

    public List<String> getMobileNumbers() {
        return mobileNumbers;
    }

    public List<String> getLandlineNumbers() {
        return landlineNumbers;
    }

    public List<String> getWhatsAppNumbers() {
        return whatsAppNumbers;
    }

    public String getCompanyName() {
        return companyName;
    }
}
