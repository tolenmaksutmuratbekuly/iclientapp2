package com.solaitech.likecoin.data.models.campaign;

import android.os.Parcel;
import android.os.Parcelable;

import com.solaitech.likecoin.data.models.enums.CampaignStateIdEnums;

import java.util.Date;

public class Campaign implements Parcelable {

    private String id;
    private String title;
    private String stateId;
    private String shortDescr;
    private Merchant merchant;
    private Integer couponPrice;
    private Date expiryDate;
    private Date couponStartDt;
    private Date couponEndDt;
    private String largeImgUrl;
    private String smallImgUrl;
    private Integer minUserlevel;

    protected Campaign(Parcel in) {
        id = in.readString();
        title = in.readString();
        stateId = in.readString();
        shortDescr = in.readString();
        merchant = in.readParcelable(Merchant.class.getClassLoader());
        couponPrice = in.readInt();
        expiryDate = (java.util.Date) in.readSerializable();
        couponStartDt = (java.util.Date) in.readSerializable();
        couponEndDt = (java.util.Date) in.readSerializable();
        largeImgUrl = in.readString();
        smallImgUrl = in.readString();
        minUserlevel = in.readInt();
    }

    public static final Creator<Campaign> CREATOR = new Creator<Campaign>() {
        @Override
        public Campaign createFromParcel(Parcel in) {
            return new Campaign(in);
        }

        @Override
        public Campaign[] newArray(int size) {
            return new Campaign[size];
        }
    };

    public String getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public String getStateId() {
        return stateId;
    }

    public String getShortDescr() {
        return shortDescr;
    }

    public Merchant getMerchant() {
        return merchant;
    }

    public Integer getCouponPrice() {
        return couponPrice;
    }

    public Date getExpiryDate() {
        return expiryDate;
    }

    public Date getCouponStartDt() {
        return couponStartDt;
    }

    public Date getCouponEndDt() {
        return couponEndDt;
    }

    public Integer getMinUserlevel() {
        return minUserlevel;
    }

    public String getLargeImgUrl() {
        return largeImgUrl;
    }

    public String getSmallImgUrl() {
        return smallImgUrl;
    }

    public boolean isActive() {
        if (stateId != null
                && stateId.equalsIgnoreCase(CampaignStateIdEnums.ACTIVE)) {
            return true;
        }

        return false;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int flags) {
        parcel.writeString(id);
        parcel.writeString(title);
        parcel.writeString(stateId);
        parcel.writeString(shortDescr);
        parcel.writeParcelable(merchant, flags);
        parcel.writeInt(couponPrice);
        parcel.writeSerializable(expiryDate);
        parcel.writeSerializable(couponStartDt);
        parcel.writeSerializable(couponEndDt);
        parcel.writeString(largeImgUrl);
        parcel.writeString(smallImgUrl);
        parcel.writeInt(minUserlevel);
    }
}