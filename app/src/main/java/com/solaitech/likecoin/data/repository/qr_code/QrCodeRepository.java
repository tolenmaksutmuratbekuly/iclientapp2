package com.solaitech.likecoin.data.repository.qr_code;

import android.graphics.Bitmap;

import com.solaitech.likecoin.data.params.QrBitmapParams;
import rx.Observable;

public interface QrCodeRepository {
    Observable<Bitmap> getQrBitmap(QrBitmapParams params);
}