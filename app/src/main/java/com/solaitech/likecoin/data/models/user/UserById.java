package com.solaitech.likecoin.data.models.user;

import java.util.Date;

public class UserById extends User {

    private Date regDT;
    private String lastNewsFeedId;
    private String likesBalance;

    public Date getRegDT() {
        return regDT;
    }

    public String getLastNewsFeedId() {
        return lastNewsFeedId;
    }

    public String getLikesBalance() {
        return likesBalance;
    }

}