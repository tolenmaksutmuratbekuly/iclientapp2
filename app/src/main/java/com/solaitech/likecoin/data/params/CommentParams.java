package com.solaitech.likecoin.data.params;


public class CommentParams {
    private String objectEntityId;
    private String objectEntityTypeId;
    private String message;

    public CommentParams(String objectEntityId, String objectEntityTypeId, String message) {
        this.objectEntityId = objectEntityId;
        this.objectEntityTypeId = objectEntityTypeId;
        this.message = message;
    }
}
