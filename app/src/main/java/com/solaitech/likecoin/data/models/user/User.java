package com.solaitech.likecoin.data.models.user;

import com.solaitech.likecoin.data.models.campaign.Merchant;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class User {
    private String id;
    private String name;
    private String surname;
    private String cellPhone;
    private String email;
    private Date nextCoinsRechargeDT;
    private String coinsBalance;
    private String language;
    private UserLevel userLevel;
    private LocalBrixAccount localBrixAccount;

    //for contacts
    private boolean beenLiked;

    private List<Merchant> ambassadorRoles;

    public boolean isBeenLiked() {
        return beenLiked;
    }

    private List<String> merchants = new ArrayList<>();
    private String avatarUrl;

    public String getId() {
        return id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public String getSurname() {
        return surname;
    }

    public String getCellPhone() {
        return cellPhone;
    }

    public String getEmail() {
        return email;
    }

    public Date getNextCoinsRechargeDT() {
        return nextCoinsRechargeDT;
    }

    public String getCoinsBalance() {
        return coinsBalance;
    }

    public int getCoinsBalanceInt() {
        try {
            return Integer.parseInt(coinsBalance);
        } catch (Exception e) {
            return 0;
        }
    }

    public UserLevel getUserLevel() {
        return userLevel;
    }

    public List<String> getMerchants() {
        return merchants;
    }

    public String getAvatarUrl() {
        return avatarUrl;
    }

    public String getLanguage() {
        return language;
    }

    public List<Merchant> getAmbassadorRoles() {
        return ambassadorRoles;
    }

    public LocalBrixAccount getLocalBrixAccount() {
        return localBrixAccount;
    }
}