package com.solaitech.likecoin.data.models.enums;

public class CouponsStateIdEnums {

    public static final String ACTIVE = "1";
    public static final String USED = "2";

    public final static int CAMPAIGN_TYPE_PROMOCODE_GENERATED = 1;
    public final static int CAMPAIGN_TYPE_PROMOCODE_DEFAULT = 2;
    public final static int CAMPAIGN_TYPE_PROMOCODE_POOL = 3;
}