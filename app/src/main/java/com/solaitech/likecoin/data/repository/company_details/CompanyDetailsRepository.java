package com.solaitech.likecoin.data.repository.company_details;

import com.solaitech.likecoin.data.models.common_info.CompanyDetails;
import com.solaitech.likecoin.data.models.common_info.IcoCondition;
import com.solaitech.likecoin.network.API;

import rx.Observable;

public interface CompanyDetailsRepository {

    void setAPI(API api);

    Observable<CompanyDetails> getCompanyDetails();

    Observable<IcoCondition> getIcoCondition();
}
