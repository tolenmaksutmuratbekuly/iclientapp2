package com.solaitech.likecoin.data.models.news_feed;

public class NewsOwner {

    private String id;
    private String name;
    private String surname;
    private NewsOwnerCustomData customData;
    private String logoUrl;
    private String avatarUrl;

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getSurname() {
        return surname;
    }

    public NewsOwnerCustomData getCustomData() {
        return customData;
    }

    public String getLogoUrl() {
        return logoUrl;
    }

    public String getAvatarUrl() {
        return avatarUrl;
    }

    //owner.customData - это данные, характерные только для определенного типа владельца. (у юзера есть список брендов, у мерчанта - нет).
    public boolean isUser() {
        return customData != null && customData.getMerchants() != null;
    }
}