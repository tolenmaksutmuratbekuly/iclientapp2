package com.solaitech.likecoin.data.models.news_feed;

public class NewsCustomData {
    private Integer takenCouponsCount;
    private Integer maxCouponsCount;
    private Integer couponPrice;
    private Integer likes;
    private Integer level;
    private Integer articlePrice;

    public Integer getTakenCouponsCount() {
        return takenCouponsCount;
    }

    public Integer getMaxCouponsCount() {
        return maxCouponsCount;
    }

    public Integer getCouponPrice() {
        return couponPrice;
    }

    public Integer getLikes() {
        return likes;
    }

    public Integer getLevel() {
        return level;
    }

    public Integer getArticlePrice() {
        return articlePrice;
    }
}