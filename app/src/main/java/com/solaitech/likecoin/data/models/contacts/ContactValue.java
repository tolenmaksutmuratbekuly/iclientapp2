package com.solaitech.likecoin.data.models.contacts;

import com.solaitech.likecoin.data.models.user.User;

public class ContactValue {

    String id;
    User refUser;
    String value;

    public String getId() {
        return id;
    }

    public User getRefUser() {
        return refUser;
    }

    public String getValue() {
        return value;
    }
}
