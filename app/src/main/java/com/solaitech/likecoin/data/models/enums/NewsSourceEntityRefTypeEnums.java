package com.solaitech.likecoin.data.models.enums;

public class NewsSourceEntityRefTypeEnums {
    public static final String SYS_MESSAGE = "0";
    public static final String TRANSACTION = "1";
    public static final String CAMPAIGN = "2";
    public static final String COMMENT = "3";
    public static final String USER = "4";
    public static final String MERCHANT = "5";
    public static final String CAMPAIGN_MODERATION = "6";
    public static final String MERCHANT_MESSAGE = "7";
    public static final String COMMENT_REPLY = "8";
    public static final String NEWSFEED = "9";
    public static final String COUPON = "10";
    public static final String ARTICLE = "11";

    public static final int NEWSFEEDTYPEID_SYSMESSAGE = 0;
    public static final int NEWSFEEDTYPEID_NEWLEVEL = 1;
    public static final int NEWSFEEDTYPEID_CAMPAIGN = 2;
    public static final int NEWSFEEDTYPEID_CAMPAIGNLIST = 3;
    public static final int NEWSFEEDTYPEID_TRANSACTION_P2P = 4;
    public static final int NEWSFEEDTYPEID_COMMENT = 5;
    public static final int NEWSFEEDTYPEID_ANSWER_COMMENT = 6;
    public static final int NEWSFEEDTYPEID_MERCHANT_MESSAGE = 7;
    public static final int NEWSFEEDTYPEID_ARTICLE = 8;
}