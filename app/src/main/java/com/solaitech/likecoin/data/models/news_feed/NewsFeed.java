package com.solaitech.likecoin.data.models.news_feed;

import android.support.annotation.NonNull;

import java.util.Date;

public class NewsFeed implements Comparable<NewsFeed> {
    private String id;
    private Date createDT;
    private String title;
    private String descr;
    private String imgUrl;
    private Boolean isOwned;
    private LikesCounter likesCounter;
    private NewsOwner owner;
    private NewsCustomData customData;
    private NewsSourceEntityRef sourceEntityRef;
    private Integer typeId;
    private RelatedFavorite relatedFavorite;

    public String getId() {
        return id;
    }

    public Date getCreateDT() {
        return createDT;
    }

    public String getTitle() {
        return title;
    }

    public String getDescr() {
        return descr;
    }

    public String getImgUrl() {
        return imgUrl;
    }

    public LikesCounter getLikesCounter() {
        return likesCounter;
    }

    public NewsOwner getOwner() {
        return owner;
    }

    public NewsCustomData getCustomData() {
        return customData;
    }

    public NewsSourceEntityRef getSourceEntityRef() {
        return sourceEntityRef;
    }

    public Integer getTypeId() {
        return typeId;
    }

    public void setOwned(Boolean owned) {
        isOwned = owned;
    }

    public Boolean getOwned() {
        return isOwned;
    }

    public RelatedFavorite getRelatedFavorite() {
        return relatedFavorite;
    }

    @Override
    public int compareTo(@NonNull NewsFeed newsFeed) {
        return Integer.valueOf(newsFeed.getId()).compareTo(Integer.valueOf(this.id));
    }
}