package com.solaitech.likecoin.data.models.news_feed;

public class NewsSourceEntityRef {

    private String id;
    private NewsSourceEntityRefType type;

    public String getId() {
        return id;
    }

    public NewsSourceEntityRefType getType() {
        return type;
    }

}