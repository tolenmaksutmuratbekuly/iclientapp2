package com.solaitech.likecoin.data.models.news_feed;

public class LikesCounter {
    private Integer totalLikes;
    private boolean beenLiked;

    public Integer getTotalLikes() {
        return totalLikes;
    }

    public boolean isBeenLiked() {
        return beenLiked;
    }

    public void setBeenLiked(boolean beenLiked) {
        this.beenLiked = beenLiked;
    }
}
