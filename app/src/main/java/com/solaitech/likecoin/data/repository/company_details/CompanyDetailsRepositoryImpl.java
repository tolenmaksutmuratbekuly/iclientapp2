package com.solaitech.likecoin.data.repository.company_details;

import com.solaitech.likecoin.data.models.common_info.CompanyDetails;
import com.solaitech.likecoin.data.models.common_info.IcoCondition;
import com.solaitech.likecoin.network.API;
import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

class CompanyDetailsRepositoryImpl implements CompanyDetailsRepository {
    private API api;
    CompanyDetailsRepositoryImpl() {
        //do nothing
    }

    @Override
    public void setAPI(API api) {
        this.api = api;
    }

    @Override
    public Observable<CompanyDetails> getCompanyDetails() {
        return api
                .getCompanyDetails()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }
    @Override
    public Observable<IcoCondition> getIcoCondition() {
        return api
                .getIcoCondition()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }
}