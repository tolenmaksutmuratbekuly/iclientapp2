package com.solaitech.likecoin.data.models.brix;

import java.util.Date;

public class BrixRate {
    private String id;
    private Double buyRate;
    private Double sellRate;
    private Date date;
    private String typeId;

    public String getId() {
        return id;
    }

    public Double getBuyRate() {
        return buyRate;
    }

    public Double getSellRate() {
        return sellRate;
    }

    public Date getDate() {
        return date;
    }

    public String getTypeId() {
        return typeId;
    }
}
