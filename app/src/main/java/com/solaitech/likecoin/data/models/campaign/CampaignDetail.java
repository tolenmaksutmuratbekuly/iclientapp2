package com.solaitech.likecoin.data.models.campaign;

import android.os.Parcel;

import java.util.ArrayList;
import java.util.List;

public class CampaignDetail extends Campaign {

    private String fullDescr;
    private List<String> contacts = new ArrayList<>();
    private List<String> urlList = new ArrayList<>();
    private List<Coordinate> coordinates = new ArrayList<>();
    private int maxCouponCount;
    private int takenCouponsCount;
    private List<CampaignTag> tags = new ArrayList<>();
    private Integer typeId;

    protected CampaignDetail(Parcel in) {
        super(in);
    }

    public String getFullDescr() {
        return fullDescr;
    }

    public List<String> getContacts() {
        return contacts;
    }

    public List<String> getUrlList() {
        return urlList;
    }

    public List<Coordinate> getCoordinates() {
        return coordinates;
    }

    public int getMaxCouponCount() {
        return maxCouponCount;
    }

    public int getTakenCouponsCount() {
        return takenCouponsCount;
    }

    public List<CampaignTag> getTags() {
        return tags;
    }

    public Integer getTypeId() {
        return typeId;
    }
}