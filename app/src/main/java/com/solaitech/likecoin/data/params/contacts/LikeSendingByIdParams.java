package com.solaitech.likecoin.data.params.contacts;

public class LikeSendingByIdParams {
    private String destinationUserId;
    private int amount;
    private String descr;

    public LikeSendingByIdParams(String destinationUserId, int amount, String descr) {
        this.destinationUserId = destinationUserId;
        this.amount = amount;
        this.descr = descr;
    }
}