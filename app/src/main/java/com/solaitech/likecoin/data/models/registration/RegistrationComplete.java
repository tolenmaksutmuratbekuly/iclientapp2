package com.solaitech.likecoin.data.models.registration;

import java.util.Date;

public class RegistrationComplete {

    private String id;
    private String key;
    private String type;
    private String userId;
    private Date regDT;

    public String getId() {
        return id;
    }

    public String getKey() {
        return key;
    }

    public String getType() {
        return type;
    }

    public String getUserId() {
        return userId;
    }

    public Date getRegDT() {
        return regDT;
    }

}