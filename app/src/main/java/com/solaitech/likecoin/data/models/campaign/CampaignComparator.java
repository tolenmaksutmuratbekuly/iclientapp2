package com.solaitech.likecoin.data.models.campaign;

import java.util.Comparator;

public class CampaignComparator implements Comparator<Campaign> {
    @Override
    public int compare(Campaign campaign1, Campaign campaign2) {
        return campaign1.getId().compareTo(campaign2.getId());
    }
}
