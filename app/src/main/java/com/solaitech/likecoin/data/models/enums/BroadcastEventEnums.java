package com.solaitech.likecoin.data.models.enums;

import com.solaitech.likecoin.BuildConfig;

public class BroadcastEventEnums {
    public static final String PROFILE_IMAGE_UPDATED = BuildConfig.APPLICATION_ID + ".profile_image_updated";
    public static final String PROFILE_UPDATED = BuildConfig.APPLICATION_ID + ".profile_updated";
    public static final String PUSH_NOTIFICATION = BuildConfig.APPLICATION_ID + ".push_notification";
}