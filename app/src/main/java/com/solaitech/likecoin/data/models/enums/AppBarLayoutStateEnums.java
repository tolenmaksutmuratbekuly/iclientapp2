package com.solaitech.likecoin.data.models.enums;

public class AppBarLayoutStateEnums {
    public static final int EXPANDED = 1;
    public static final int COLLAPSED = 2;
    public static final int IN_BETWEEN = 3;

}