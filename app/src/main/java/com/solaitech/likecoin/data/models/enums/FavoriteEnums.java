package com.solaitech.likecoin.data.models.enums;

public class FavoriteEnums {
    public static final int REMOVE_FOREVER = -1;
    public static final int REMOVE_FROM_FAVORITES = 0;
    public static final int ADD_TO_FAVORITES = 1;

    /*
    -1 = не очень нравится
    0 = безразлично
    1 = нравится
    */
}
