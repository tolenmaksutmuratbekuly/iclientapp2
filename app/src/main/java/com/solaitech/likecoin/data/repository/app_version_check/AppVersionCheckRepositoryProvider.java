package com.solaitech.likecoin.data.repository.app_version_check;

import android.support.annotation.NonNull;

import com.solaitech.likecoin.network.API;

/**
 * Created by sultanbek on 22.11.16.
 */

public class AppVersionCheckRepositoryProvider {

    private static AppVersionCheckRepository repository;

    @NonNull
    public static AppVersionCheckRepository provideRepository(API api) {
        if (repository == null) {
            repository = new AppVersionCheckRepositoryImpl();
        }
        repository.setAPI(api);
        return repository;
    }

    public static void setRepository(AppVersionCheckRepository repo) {
        repository = repo;
    }

}