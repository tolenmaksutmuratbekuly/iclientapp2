package com.solaitech.likecoin.data.repository.user;

import android.support.annotation.NonNull;

import com.solaitech.likecoin.network.API;

public class UserRepositoryProvider {

    private static UserRepository repository;

    private UserRepositoryProvider() {
        //do nothing
    }

    @NonNull
    public static UserRepository provideRepository(API api) {
        if (repository == null) {
            repository = new UserRepositoryImpl();
        }
        repository.setAPI(api);
        return repository;
    }

    public static void setRepository(UserRepository repo) {
        repository = repo;
    }

}