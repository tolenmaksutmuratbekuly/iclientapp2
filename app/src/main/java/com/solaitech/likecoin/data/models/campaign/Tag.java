package com.solaitech.likecoin.data.models.campaign;

import java.util.List;

public class Tag {
    private String id;
    private String name;
    private List<Campaign> campaigns;

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setCampaigns(List<Campaign> campaigns) {
        this.campaigns = campaigns;
    }

    public List<Campaign> getCampaigns() {
        return campaigns;
    }
}
