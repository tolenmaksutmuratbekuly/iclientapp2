package com.solaitech.likecoin.data.repository.qr_code;

import android.support.annotation.NonNull;

/**
 * Created by sultanbek on 18.04.17.
 */

public class QrCodeRepositoryProvider {

    private static QrCodeRepository repository;

    @NonNull
    public static QrCodeRepository provideRepository() {
        if (repository == null) {
            repository = new QrCodeRepositoryImpl();
        }
        return repository;
    }

    public static void setRepository(QrCodeRepository repo) {
        repository = repo;
    }

}