package com.solaitech.likecoin.data.models.coupons;

import java.util.Date;

import com.solaitech.likecoin.data.models.campaign.CampaignDetail;

public class CouponDetail {

    private String id;
    private String key;
    private String stateId;
    private CampaignDetail campaign;
    private Date regDT;

    public String getId() {
        return id;
    }

    public String getKey() {
        return key;
    }

    public String getStateId() {
        return stateId;
    }

    public CampaignDetail getCampaign() {
        return campaign;
    }

    public Date getRegDT() {
        return regDT;
    }

}