package com.solaitech.likecoin.data.repository.user;

import com.solaitech.likecoin.data.models.campaign.Merchant;
import com.solaitech.likecoin.data.models.user.User;
import com.solaitech.likecoin.data.models.user.UserById;
import com.solaitech.likecoin.data.models.user.UserProfileQR;
import com.solaitech.likecoin.data.models.user_statement.UserStatement;
import com.solaitech.likecoin.data.params.PushNotificationAddDeviceParams;
import com.solaitech.likecoin.data.params.UpdateUserInfoParams;
import com.solaitech.likecoin.network.API;

import java.util.List;

import okhttp3.MultipartBody;
import okhttp3.ResponseBody;
import rx.Observable;

public interface UserRepository {

    void setAPI(API api);

    Observable<User> getUser();

    Observable<User> getUser(String userId);

    Observable<Merchant> getMerchant(String merchantId);

    Observable<User> updateUserInfo(UpdateUserInfoParams params);

    Observable<ResponseBody> uploadAvatar(MultipartBody.Part file);

    Observable<List<UserStatement>> getUserStatement(String startDate,
                                                     String endDate);

    Observable<ResponseBody> deleteUserAvatar();

    Observable<UserById> getUserById(String userId);

    Observable<ResponseBody> deleteUserDeviceToken(int tokenType, String tokenKey);

    Observable<ResponseBody> deleteUserRole(String merchantId, int roleId);

    Observable<ResponseBody> addDevice(PushNotificationAddDeviceParams params);

    Observable<UserProfileQR> getUserProfileToken();
}