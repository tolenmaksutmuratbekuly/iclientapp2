package com.solaitech.likecoin.data.repository.campaign;

import java.util.List;

import com.solaitech.likecoin.data.models.campaign.Campaign;
import com.solaitech.likecoin.data.models.campaign.CampaignDetail;
import com.solaitech.likecoin.data.models.campaign.Tag;
import com.solaitech.likecoin.data.models.user_statement.UserStatement;
import com.solaitech.likecoin.network.API;
import com.solaitech.likecoin.utils.StringUtils;
import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Func1;
import rx.schedulers.Schedulers;

class CampaignRepositoryImpl implements CampaignRepository {
    private API api;

    @Override
    public void setAPI(API api) {
        this.api = api;
    }

    @Override
    public Observable<List<Campaign>> getCampaigns(String merchantId, Boolean includeInactive, String tagId) {
        return api
                .getCampaigns(merchantId, includeInactive, tagId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    @Override
    public Observable<List<Tag>> getTags() {
        return api
                .getTags()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    @Override
    public Observable<CampaignDetail> getCampaignById(String campaignId) {
        return api
                .getCampaignById(campaignId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    @Override
    public Observable<Campaign> search(List<Object> objects, final String searchQuery) {
        return Observable.from(objects)
                .ofType(Campaign.class)
                .filter(new Func1<Campaign, Boolean>() {
                    @Override
                    public Boolean call(Campaign campaign) {
                        return (
                                StringUtils.isTextContainSearchQuery(campaign.getTitle(), searchQuery)
                                        || StringUtils.isTextContainSearchQuery(campaign.getShortDescr(), searchQuery)
                        );
                    }
                })
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    @Override
    public Observable<List<UserStatement>> getUserStatement(String startDate, String endDate) {
        return null;
    }
}