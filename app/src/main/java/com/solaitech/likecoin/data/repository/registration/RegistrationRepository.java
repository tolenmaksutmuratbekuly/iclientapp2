package com.solaitech.likecoin.data.repository.registration;

import com.solaitech.likecoin.data.models.registration.RegistrationComplete;
import com.solaitech.likecoin.data.models.registration.RegistrationStart;
import com.solaitech.likecoin.data.params.RegistrationCompleteParams;
import com.solaitech.likecoin.data.params.RegistrationStartParams;
import com.solaitech.likecoin.network.API;
import rx.Observable;

public interface RegistrationRepository {

    void setAPI(API api);

    Observable<RegistrationStart> registrationStart(RegistrationStartParams params);

    Observable<RegistrationComplete> registrationComplete(String userRegistrationToken,
                                                          RegistrationCompleteParams params);

}
