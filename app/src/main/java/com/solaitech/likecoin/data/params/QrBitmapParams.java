package com.solaitech.likecoin.data.params;

/**
 * Created by sultanbek on 19.04.17.
 */

public class QrBitmapParams {

    private String content;
    private int blackColor;
    private int whiteColor;
    private int displayWidth;
    private int displayHeight;

    public QrBitmapParams(String content, int blackColor, int whiteColor, int displayWidth, int displayHeight) {
        this.content = content;
        this.blackColor = blackColor;
        this.whiteColor = whiteColor;
        this.displayWidth = displayWidth;
        this.displayHeight = displayHeight;
    }

    public String getContent() {
        return content;
    }

    public int getBlackColor() {
        return blackColor;
    }

    public int getWhiteColor() {
        return whiteColor;
    }

    public int getDisplayWidth() {
        return displayWidth;
    }

    public int getDisplayHeight() {
        return displayHeight;
    }

}