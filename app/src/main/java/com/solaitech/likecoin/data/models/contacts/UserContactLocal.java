package com.solaitech.likecoin.data.models.contacts;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import com.solaitech.likecoin.utils.DateTimeUtils;

public class UserContactLocal {

    private String id;
    private String name;
    private List<Phone> phones;
    private long lastUpdatedTimestamp;
    private boolean isChecked;

    public UserContactLocal(String id, String name, long lastUpdatedTimestamp, List<Phone> phones) {
        this.id = id;
        this.name = name;
        this.lastUpdatedTimestamp = lastUpdatedTimestamp;
        this.phones = phones;
    }

    public boolean isChecked() {
        return isChecked;
    }

    public void setChecked(boolean checked) {
        isChecked = checked;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public List<Phone> getPhones() {
        return phones;
    }

    public List<String> getPhoneNumberList() {
        List<String> list = new ArrayList<>();
        for (Phone phone : getPhones()) {
            list.add(phone.getNumber());
        }
        return list;
    }

    public Date getLastUpdatedDate() {
        return DateTimeUtils.convertLongToDate(lastUpdatedTimestamp);
    }

    public String getLastUpdatedDateString() {
        return DateTimeUtils.convertUserContactUpdateTimestampToString(lastUpdatedTimestamp);
    }

}