package com.solaitech.likecoin.data.models.user;

public class LocalBrixAccount {
    private String id;
    private String userId;
    private Double balance;
    private Double blockedBalance;

    public String getId() {
        return id;
    }

    public String getUserId() {
        return userId;
    }

    public int getBalance() {
        if (balance != null) {
            return balance.intValue();
        } else {
            return 0;
        }
    }

    public Double getBlockedBalance() {
        return blockedBalance;
    }
}
