package com.solaitech.likecoin.data.params;

public class Like4EntityParams {
    private String entityId;
    private String entityTypeId;

    public Like4EntityParams(String entityId, String entityTypeId) {
        this.entityId = entityId;
        this.entityTypeId = entityTypeId;
    }
}
