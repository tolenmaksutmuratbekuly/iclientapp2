package com.solaitech.likecoin.data.repository.campaign;

import java.util.List;

import com.solaitech.likecoin.data.models.campaign.Campaign;
import com.solaitech.likecoin.data.models.campaign.CampaignDetail;
import com.solaitech.likecoin.data.models.campaign.Tag;
import com.solaitech.likecoin.data.models.user_statement.UserStatement;
import com.solaitech.likecoin.network.API;
import rx.Observable;

public interface CampaignRepository {

    void setAPI(API api);

    Observable<List<Campaign>> getCampaigns(String merchantId,
                                            Boolean includeInactive,
                                            String tagId);

    Observable<List<Tag>> getTags();

    Observable<CampaignDetail> getCampaignById(String campaignId);

    Observable<Campaign> search(List<Object> objects, String searchQuery);


    Observable<List<UserStatement>> getUserStatement(String startDate,
                                                     String endDate);

}