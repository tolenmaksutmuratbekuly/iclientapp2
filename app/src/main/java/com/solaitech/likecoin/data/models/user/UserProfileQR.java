package com.solaitech.likecoin.data.models.user;

import java.util.Date;

public class UserProfileQR {
    private String id;
    private String key;
    private Integer type;
    private String userId;
    private Date regDT;


    public String getId() {
        return id;
    }

    public String getKey() {
        return key;
    }

    public Integer getType() {
        return type;
    }

    public String getUserId() {
        return userId;
    }

    public Date getRegDT() {
        return regDT;
    }
}
