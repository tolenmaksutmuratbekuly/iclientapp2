package com.solaitech.likecoin.data.params;

public class UpdateUserInfoParams {

    private String name;
    private String surname;
    private String email;
    private String language;

    public UpdateUserInfoParams(String name, String surname, String email, String language) {
        this.name = name;
        this.surname = surname;
        this.email = email;
        this.language = language;
    }
}