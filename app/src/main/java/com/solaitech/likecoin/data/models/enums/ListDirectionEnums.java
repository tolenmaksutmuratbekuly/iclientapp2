package com.solaitech.likecoin.data.models.enums;

public class ListDirectionEnums {
    public static final String ASCENDING_DESCENDING = "0";
    public static final String ASCENDING = "1";
    public static final String DESCENDING = "-1";
}