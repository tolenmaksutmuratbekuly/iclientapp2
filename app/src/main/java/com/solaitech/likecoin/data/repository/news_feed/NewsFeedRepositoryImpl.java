package com.solaitech.likecoin.data.repository.news_feed;

import java.util.List;

import com.solaitech.likecoin.data.models.news_feed.NewsFeed;
import com.solaitech.likecoin.network.API;
import com.solaitech.likecoin.utils.StringUtils;
import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Func1;
import rx.schedulers.Schedulers;

public class NewsFeedRepositoryImpl implements NewsFeedRepository {

    private API api;

    @Override
    public void setAPI(API api) {
        this.api = api;
    }

    @Override
    public Observable<List<NewsFeed>> getNewsFeed(String baseId, int rowCount, String direction) {
        return api
                .getNewsFeed(baseId, rowCount, direction)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    @Override
    public Observable<NewsFeed> search(List<Object> objects, final String searchQuery) {
        return Observable.from(objects)
                .ofType(NewsFeed.class)
                .filter(new Func1<NewsFeed, Boolean>() {
                    @Override
                    public Boolean call(NewsFeed newsFeed) {
                        return (
                                StringUtils.isTextContainSearchQuery(newsFeed.getTitle(), searchQuery)
                                        || StringUtils.isTextContainSearchQuery(newsFeed.getDescr(), searchQuery)
                        );
                    }
                })
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

}