package com.solaitech.likecoin.data.repository.article;

import android.support.annotation.NonNull;

import com.solaitech.likecoin.network.API;

public class ArticleRepositoryProvider {
    private static ArticleRepository repository;

    @NonNull
    public static ArticleRepository provideRepository(API api) {
        if (repository == null) {
            repository = new ArticleRepositoryImpl();
        }
        repository.setAPI(api);
        return repository;
    }

    public static void setRepository(ArticleRepository repo) {
        repository = repo;
    }
}