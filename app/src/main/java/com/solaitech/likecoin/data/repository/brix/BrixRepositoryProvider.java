package com.solaitech.likecoin.data.repository.brix;

import android.support.annotation.NonNull;

import com.solaitech.likecoin.data.repository.user.UserRepository;
import com.solaitech.likecoin.data.repository.user.UserRepositoryImpl;
import com.solaitech.likecoin.network.API;

public class BrixRepositoryProvider {

    private static BrixRepository repository;

    private BrixRepositoryProvider() {
        //do nothing
    }

    @NonNull
    public static BrixRepository provideRepository(API api) {
        if (repository == null) {
            repository = new BrixRepositoryImpl();
        }
        repository.setAPI(api);
        return repository;
    }

    public static void setRepository(BrixRepository repo) {
        repository = repo;
    }

}