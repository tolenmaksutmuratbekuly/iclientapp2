package com.solaitech.likecoin.data.repository.brix;

import com.solaitech.likecoin.data.models.brix.BrixRate;
import com.solaitech.likecoin.data.models.brix.PurchaseBrix;
import com.solaitech.likecoin.data.params.brix.BuyBrixParams;
import com.solaitech.likecoin.network.API;

import okhttp3.ResponseBody;
import retrofit2.Response;
import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class BrixRepositoryImpl implements BrixRepository {

    private API api;

    @Override
    public void setAPI(API api) {
        this.api = api;
    }

    @Override
    public Observable<String> purchaseBrix(BuyBrixParams params) {
        return api
                .brixPurchase(params)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    @Override
    public Observable<BrixRate> brixRate() {
        return api
                .brixRate()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }
}