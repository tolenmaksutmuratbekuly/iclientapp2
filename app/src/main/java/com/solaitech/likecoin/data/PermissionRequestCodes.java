package com.solaitech.likecoin.data;

public class PermissionRequestCodes {

    public static final int REQUEST_CONTACTS = 1;
    public static final int SMS = 2;
    public static final int STORAGE = 3;
    public static final int LOCATION = 4;

}