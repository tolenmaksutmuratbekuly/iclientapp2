package com.solaitech.likecoin.data.models.coupons;

import com.solaitech.likecoin.data.models.campaign.Campaign;

public class Coupon {

    private String id;
    private String key;
    private String stateId;
    private Campaign campaign;

    public String getId() {
        return id;
    }

    public String getKey() {
        return key;
    }

    public String getStateId() {
        return stateId;
    }

    public Campaign getCampaign() {
        return campaign;
    }

}