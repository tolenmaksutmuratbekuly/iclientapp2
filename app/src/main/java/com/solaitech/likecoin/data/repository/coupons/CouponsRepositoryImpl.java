package com.solaitech.likecoin.data.repository.coupons;

import com.solaitech.likecoin.data.models.coupons.Coupon;
import com.solaitech.likecoin.data.models.coupons.CouponDetail;
import com.solaitech.likecoin.data.params.CouponPurchaseParams;
import com.solaitech.likecoin.network.API;
import com.solaitech.likecoin.utils.StringUtils;

import java.util.List;

import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Func1;
import rx.schedulers.Schedulers;

public class CouponsRepositoryImpl implements CouponsRepository {

    private API api;

    public CouponsRepositoryImpl() {
        //do nothing
    }

    @Override
    public void setAPI(API api) {
        this.api = api;
    }

    @Override
    public Observable<List<Coupon>> getCoupons(String stateId) {
        return api
                .getCoupons(stateId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    @Override
    public Observable<CouponDetail> getCouponById(String couponId) {
        return api
                .getCouponById(couponId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    @Override
    public Observable<List<Coupon>> couponPurchase(CouponPurchaseParams params) {
        return api
                .couponPurchase(params)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    @Override
    public Observable<Coupon> search(List<Object> objects, final String searchQuery) {
        return Observable.from(objects)
                .ofType(Coupon.class)
                .filter(new Func1<Coupon, Boolean>() {
                    @Override
                    public Boolean call(Coupon coupon) {
                        return (coupon.getCampaign() != null
                                && (StringUtils.isTextContainSearchQuery(coupon.getCampaign().getTitle(), searchQuery)
                                || StringUtils.isTextContainSearchQuery(coupon.getCampaign().getShortDescr(), searchQuery))
                        );
                    }
                })
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

}