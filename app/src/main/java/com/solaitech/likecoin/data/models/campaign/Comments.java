package com.solaitech.likecoin.data.models.campaign;

import android.support.annotation.NonNull;

import com.solaitech.likecoin.data.models.news_feed.LikesCounter;
import com.solaitech.likecoin.data.models.news_feed.NewsOwner;
import com.solaitech.likecoin.data.models.news_feed.NewsSourceEntityRef;

import java.util.Date;

public class Comments implements Comparable<Comments> {
    private String id;
    private Date regDT;
    private String message;
    private String replyMessage;
    private Date replyRegDT;
    private LikesCounter likesCounter;
    private LikesCounter replyLikesCounter;
    private NewsOwner owner;
    private NewsOwner replyOwner;
    private NewsSourceEntityRef objectEntityRef;

    public String getId() {
        return id;
    }

    public Date getRegDT() {
        return regDT;
    }

    public String getMessage() {
        return message;
    }

    public String getReplyMessage() {
        return replyMessage;
    }

    public Date getReplyRegDT() {
        return replyRegDT;
    }

    public NewsOwner getOwner() {
        return owner;
    }

    public NewsOwner getReplyOwner() {
        return replyOwner;
    }

    public NewsSourceEntityRef getObjectEntityRef() {
        return objectEntityRef;
    }

    public LikesCounter getLikesCounter() {
        return likesCounter;
    }

    public LikesCounter getReplyLikesCounter() {
        return replyLikesCounter;
    }

    @Override
    public int compareTo(@NonNull Comments comments) {
        return Integer.valueOf(comments.getId()).compareTo(Integer.valueOf(this.id));
    }
}
