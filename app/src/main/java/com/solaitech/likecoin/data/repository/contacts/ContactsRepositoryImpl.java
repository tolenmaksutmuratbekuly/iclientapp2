package com.solaitech.likecoin.data.repository.contacts;

import android.content.ContentResolver;
import android.database.Cursor;
import android.provider.ContactsContract;

import java.util.ArrayList;
import java.util.List;

import com.solaitech.likecoin.data.models.contacts.InvitationText;
import com.solaitech.likecoin.data.models.contacts.Phone;
import com.solaitech.likecoin.data.models.contacts.UserContact;
import com.solaitech.likecoin.data.models.contacts.UserContactLocal;
import com.solaitech.likecoin.data.params.contacts.ContactAddingParams;
import com.solaitech.likecoin.network.API;
import okhttp3.ResponseBody;
import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Func0;
import rx.schedulers.Schedulers;


class ContactsRepositoryImpl implements ContactsRepository {
    private API api;
    private ContentResolver contentResolver;

    @Override
    public void setAPI(API api) {
        this.api = api;
    }

    @Override
    public void setContentResolver(ContentResolver contentResolver) {
        this.contentResolver = contentResolver;
    }

    @Override
    public Observable<List<UserContactLocal>> getLocalContacts() {
        return Observable.defer(
                new Func0<Observable<List<UserContactLocal>>>() {
                    @Override
                    public Observable<List<UserContactLocal>> call() {
                        return Observable.just(getUserLocalContacts());
                    }
                })
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    @Override
    public Observable<List<UserContact>> getContacts(boolean isOnlyRegistered) {
        return api
                .getUserContacts(isOnlyRegistered)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    @Override
    public Observable<ResponseBody> addContacts(List<ContactAddingParams> userContacts) {
        return api
                .addUserContacts(userContacts)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }


    @Override
    public Observable<ResponseBody> deleteUserContacts(List<String> ids) {
        return api
                .deleteUserContacts(ids)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    @Override
    public Observable<InvitationText> getInvitationText() {
        return api
                .getInvitationText()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    private List<UserContactLocal> getUserLocalContacts() {
        List<UserContactLocal> userContactLocals = new ArrayList<>();

        Cursor cursor = getContactsCursor();

        if (cursor.getCount() > 0) {

            while (cursor.moveToNext()) {

                String id = getId(cursor);
                String name = getDisplayName(cursor);
                long time = getLastUpdatedTimestamp(cursor);
                if (hasPhoneNumbers(cursor)) {
                    List<Phone> phones = getPhoneNumbers(id);
                    userContactLocals.add(new UserContactLocal(id, name, time, phones));
                }
            }
        }

        return userContactLocals;
    }

    private Cursor getContactsCursor() {
        return contentResolver.query(ContactsContract.Contacts.CONTENT_URI,
                null, null, null, null);
    }

    private String getId(Cursor cursor) {
        return cursor.getString(
                cursor.getColumnIndex(ContactsContract.Contacts._ID));
    }

    private String getDisplayName(Cursor cursor) {
        return cursor.getString(
                cursor.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));
    }

    private long getLastUpdatedTimestamp(Cursor cursor) {
        return cursor.getLong(
                cursor.getColumnIndex(ContactsContract.Contacts.CONTACT_LAST_UPDATED_TIMESTAMP));
    }

    private boolean hasPhoneNumbers(Cursor cursor) {
        return
                Integer.parseInt(
                        cursor.getString(
                                cursor.getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER))
                ) > 0;
    }

    public List<Phone> getPhoneNumbers(String id) {

        List<Phone> phones = new ArrayList<>();

        Cursor cursor = getPhonesCursor(id);

        while (cursor.moveToNext()) {
            phones.add(new Phone(
                    getPhoneNumber(cursor),
                    getPhoneType(cursor)
            ));

        }
        cursor.close();

        return phones;
    }

    private Cursor getPhonesCursor(String id) {
        return contentResolver.query(
                ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
                null,
                ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = ?",
                new String[]{id}, null);
    }

    private String getPhoneNumber(Cursor cursor) {
        return cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
    }

    private String getPhoneType(Cursor cursor) {
        return cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.TYPE));
    }

}