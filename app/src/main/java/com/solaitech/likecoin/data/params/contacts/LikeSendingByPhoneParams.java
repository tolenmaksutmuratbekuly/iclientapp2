package com.solaitech.likecoin.data.params.contacts;

public class LikeSendingByPhoneParams {
    private String destinationUserCellPhone;
    private int amount;
    private String descr;

    public LikeSendingByPhoneParams(String destinationUserCellPhone, int amount, String descr) {
        this.destinationUserCellPhone = destinationUserCellPhone;
        this.amount = amount;
        this.descr = descr;
    }
}