package com.solaitech.likecoin.data.params.brix;

public class BuyBrixParams {
    private int brixAmount;
    private int epayAmount;
    private int brixRate;

    public BuyBrixParams(int brixAmount, double epayAmount, double brixRate) {
        epayAmount = epayAmount * 100;
        this.brixAmount = brixAmount;
        this.epayAmount = (int) epayAmount;
        this.brixRate = (int) brixRate;
    }
}
