package com.solaitech.likecoin.data.models.contacts;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class UserContact {
    String extId;
    Date updateDT;
    String name;
    List<ContactValue> values = new ArrayList<>();

    public UserContact(String extId, String name, Date updateDT, List<ContactValue> values) {
        this.extId = extId;
        this.name = name;
        this.updateDT = updateDT;
        this.values = values;
    }

    public UserContact() {
    }

    public String getName() {
        return name;
    }

    public List<ContactValue> getValues() {
        return values;
    }

    public String getExtId() {
        return extId;
    }

    public Date getUpdateDT() {
        return updateDT;
    }
}
