package com.solaitech.likecoin.data.models.user_statement;

import java.math.BigDecimal;
import java.util.Date;

import com.solaitech.likecoin.utils.StringUtils;

public class UserStatement {

    private String id;
    private String descr;
    private Partner partner;
    private Date regDT;
    private BigDecimal amount;
    
    public String getId() {
        return id;
    }

    public Partner getPartner() {
        return partner;
    }

    public Date getRegDT() {
        return regDT;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public String getDescr() {
        return descr != null ? descr : StringUtils.EMPTY_STRING;
    }
}