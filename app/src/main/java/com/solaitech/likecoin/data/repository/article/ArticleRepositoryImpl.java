package com.solaitech.likecoin.data.repository.article;

import com.solaitech.likecoin.data.models.article.Article;
import com.solaitech.likecoin.data.models.campaign.Campaign;
import com.solaitech.likecoin.data.models.campaign.CampaignDetail;
import com.solaitech.likecoin.data.models.campaign.Tag;
import com.solaitech.likecoin.data.models.user_statement.UserStatement;
import com.solaitech.likecoin.data.repository.campaign.CampaignRepository;
import com.solaitech.likecoin.network.API;
import com.solaitech.likecoin.utils.StringUtils;

import java.util.List;

import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Func1;
import rx.schedulers.Schedulers;

class ArticleRepositoryImpl implements ArticleRepository {
    private API api;

    @Override
    public void setAPI(API api) {
        this.api = api;
    }

    @Override
    public Observable<Article> getArticleById(String articleId) {
        return api
                .getArticleById(articleId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }
}