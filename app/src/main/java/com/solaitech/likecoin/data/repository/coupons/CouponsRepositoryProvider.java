package com.solaitech.likecoin.data.repository.coupons;

import android.support.annotation.NonNull;

import com.solaitech.likecoin.network.API;

/**
 * Created by sultanbek on 22.11.16.
 */

public class CouponsRepositoryProvider {

    private static CouponsRepository repository;

    private CouponsRepositoryProvider() {
        //do nothing
    }

    @NonNull
    public static CouponsRepository provideRepository(API api) {
        if (repository == null) {
            repository = new CouponsRepositoryImpl();
        }
        repository.setAPI(api);
        return repository;
    }

    public static void setRepository(CouponsRepository repo) {
        repository = repo;
    }

}