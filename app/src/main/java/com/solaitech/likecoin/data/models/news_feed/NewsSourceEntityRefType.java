package com.solaitech.likecoin.data.models.news_feed;

public class NewsSourceEntityRefType {

    private String id;
    private String descr;

    public String getId() {
        return id;
    }

    public String getDescr() {
        return descr;
    }

}