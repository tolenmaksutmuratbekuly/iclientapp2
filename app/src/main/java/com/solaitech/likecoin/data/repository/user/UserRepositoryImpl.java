package com.solaitech.likecoin.data.repository.user;

import com.solaitech.likecoin.data.models.campaign.Merchant;
import com.solaitech.likecoin.data.models.user.User;
import com.solaitech.likecoin.data.models.user.UserById;
import com.solaitech.likecoin.data.models.user.UserProfileQR;
import com.solaitech.likecoin.data.models.user_statement.UserStatement;
import com.solaitech.likecoin.data.params.PushNotificationAddDeviceParams;
import com.solaitech.likecoin.data.params.UpdateUserInfoParams;
import com.solaitech.likecoin.network.API;

import java.util.List;

import okhttp3.MultipartBody;
import okhttp3.ResponseBody;
import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class UserRepositoryImpl implements UserRepository {

    private API api;

    @Override
    public void setAPI(API api) {
        this.api = api;
    }

    @Override
    public Observable<User> getUser() {
        return api
                .getUser()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    @Override
    public Observable<User> getUser(String userId) {
        return api
                .getUser(userId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    @Override
    public Observable<Merchant> getMerchant(String merchantId) {
        return api
                .getMerchant(merchantId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());

    }

    @Override
    public Observable<User> updateUserInfo(UpdateUserInfoParams params) {
        return api
                .updateUserInfo(params);
//                .subscribeOn(Schedulers.io())
//                .observeOn(AndroidSchedulers.mainThread());
    }

    @Override
    public Observable<ResponseBody> uploadAvatar(MultipartBody.Part file) {
        return api
                .uploadAvatar(file);
//                .subscribeOn(Schedulers.io())
//                .observeOn(AndroidSchedulers.mainThread());
    }

    @Override
    public Observable<List<UserStatement>> getUserStatement(String startDate, String endDate) {
        return api
                .getUserStatement(startDate, endDate)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    @Override
    public Observable<ResponseBody> deleteUserAvatar() {
        return api
                .deleteUserAvatar();
//                .subscribeOn(Schedulers.io())
//                .observeOn(AndroidSchedulers.mainThread());
    }

    @Override
    public Observable<UserById> getUserById(String userId) {
        return api
                .getUserById(userId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    @Override
    public Observable<ResponseBody> deleteUserDeviceToken(int tokenType, String tokenKey) {
        return api.deleteUserDeviceToken(tokenType, tokenKey)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    @Override
    public Observable<ResponseBody> deleteUserRole(String merchantId, int roleId) {
        return api.deleteUserRole(merchantId, roleId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    @Override
    public Observable<ResponseBody> addDevice(PushNotificationAddDeviceParams params) {
        return api
                .addDevice(params)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    @Override
    public Observable<UserProfileQR> getUserProfileToken() {
        return api.getUserProfileToken()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }
}