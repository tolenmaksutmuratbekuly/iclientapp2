package com.solaitech.likecoin.data.params;

public class RegistrationCompleteParams {
    private String cellPhone;
    private String email;
    private String name;
    private String surname;
    private String language;

    public RegistrationCompleteParams(String cellPhone, String email, String name, String surname, String language) {
        this.cellPhone = cellPhone;
        this.email = email;
        this.name = name;
        this.surname = surname;
        this.language = language;
    }
}