package com.solaitech.likecoin.data;

public class Constants {
    public static final String APP_URL = "https://play.google.com/store/apps/details?id=kz.crystalspring.likecoin";
    public static final String LC_LANDING_PAGE_URL = "http://likecoin.kz/";
    public static final String WHATSAPP_CONTACT = "LikeCoin";
    public static final String FEEDBACK_WHATSAPP = "+77075251080";
    public static final String FEEDBACK_MAIL = "info@solaitech.com";
    public static final String BRIX_PUBLIC_OFFER_EN = "https://lctest.solaitech.com/data/docs/publicOfferBrix_en.pdf";
    public static final String BRIX_PUBLIC_OFFER_KK = "https://lctest.solaitech.com/data/docs/publicOfferBrix_kk.pdf";
    public static final String BRIX_PUBLIC_OFFER_RU = "https://lctest.solaitech.com/data/docs/publicOfferBrix_ru.pdf";

    public static final String TAG = Constants.class.getName();

    public static final String KEY_LANG_RU = "ru";
    public static final String KEY_LANG_EN = "en";

    public static final String KEY_LANG_EPAY_RU = "rus";
    public static final String KEY_LANG_EPAY_EN = "eng";

}
