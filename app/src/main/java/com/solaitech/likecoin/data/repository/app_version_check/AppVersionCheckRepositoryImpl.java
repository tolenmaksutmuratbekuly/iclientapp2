package com.solaitech.likecoin.data.repository.app_version_check;

import com.solaitech.likecoin.data.models.app_version_check.AppVersionChecking;
import com.solaitech.likecoin.network.API;
import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class AppVersionCheckRepositoryImpl implements AppVersionCheckRepository {

    private API api;
    @Override
    public void setAPI(API api) {
        this.api = api;
    }

    @Override
    public Observable<AppVersionChecking> appVersionChecking(String os, String appVersion, String appType) {
        return api
                .appVersionChecking(os, appVersion, appType)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

}