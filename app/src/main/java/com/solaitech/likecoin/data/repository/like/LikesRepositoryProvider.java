package com.solaitech.likecoin.data.repository.like;

import android.support.annotation.NonNull;

import com.solaitech.likecoin.network.API;

public class LikesRepositoryProvider {

    private static LikesRepository repository;

    private LikesRepositoryProvider() {
        //do nothing
    }

    @NonNull
    public static LikesRepository provideRepository(API api) {
        if (repository == null) {
            repository = new LikesRepositoryImpl();
        }
        repository.setAPI(api);
        return repository;
    }

    public static void setRepository(LikesRepository repo) {
        repository = repo;
    }

}