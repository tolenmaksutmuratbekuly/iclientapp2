package com.solaitech.likecoin.data.models.push_notification;

import android.os.Parcel;
import android.os.Parcelable;

public class PushNotificationSourceEntityRef implements Parcelable {

    private String id;
    private PushNotificationSourceEntityRefType type;

    public String getId() {
        return id;
    }

    public PushNotificationSourceEntityRefType getType() {
        return type;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.id);
        dest.writeParcelable(this.type, flags);
    }

    public PushNotificationSourceEntityRef() {
    }

    protected PushNotificationSourceEntityRef(Parcel in) {
        this.id = in.readString();
        this.type = in.readParcelable(PushNotificationSourceEntityRefType.class.getClassLoader());
    }

    public static final Creator<PushNotificationSourceEntityRef> CREATOR = new Creator<PushNotificationSourceEntityRef>() {
        @Override
        public PushNotificationSourceEntityRef createFromParcel(Parcel source) {
            return new PushNotificationSourceEntityRef(source);
        }

        @Override
        public PushNotificationSourceEntityRef[] newArray(int size) {
            return new PushNotificationSourceEntityRef[size];
        }
    };

}