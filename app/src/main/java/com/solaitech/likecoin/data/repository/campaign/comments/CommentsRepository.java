package com.solaitech.likecoin.data.repository.campaign.comments;


import java.util.List;

import com.solaitech.likecoin.data.models.campaign.Comments;
import com.solaitech.likecoin.data.params.CommentParams;
import com.solaitech.likecoin.network.API;
import rx.Observable;

public interface CommentsRepository {

    void setAPI(API api);

    Observable<List<Comments>> getComments(String objectEntityId,
                                           String objectEntityTypeId,
                                           String baseId,
                                           int rowCount,
                                           String direction);

    Observable<Comments> postComment(CommentParams params);
}
