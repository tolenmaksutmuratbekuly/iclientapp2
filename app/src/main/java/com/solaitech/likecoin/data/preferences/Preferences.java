package com.solaitech.likecoin.data.preferences;

import java.util.Date;

public interface Preferences {

    void setRegistrationCompleted(boolean registered);

    boolean isRegistrationCompleted();

    void setUserDeviceToken(String userDeviceToken);

    String getUserDeviceToken();

    void setRegistrationCompleteId(String id);

    void setRegistrationCompleteType(String type);

    void setRegistrationCompleteUserId(String userId);

    void setRegistrationCompleteRegDT(Date regDT);

    String getRegistrationCompleteId();

    String getRegistrationCompleteType();

    String getRegistrationCompleteUserId();

    long getRegistrationCompleteRegDT();

    void removePreference(String preferenceKey);

    void clearAllPreferences();

    //last news id
    void setNewsFeedLastVisibleItemId(String newsId);

    String getNewsFeedLastVisibleItemId();

    void setLanguage(String language);

    String getLanguage();
}