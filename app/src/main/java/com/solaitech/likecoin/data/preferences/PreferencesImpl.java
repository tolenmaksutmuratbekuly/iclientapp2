package com.solaitech.likecoin.data.preferences;

import android.content.Context;
import android.content.SharedPreferences;

import com.solaitech.likecoin.utils.StringUtils;

import java.util.Date;

public class PreferencesImpl implements Preferences {

    public static final String IS_REGISTRATION_COMPLETED = "isRegistrationCompleted";
    private static final String USER_DEVICE_TOKEN = "userDeviceToken";
    private static final String REGISTRATION_COMPLETE_ID = "registrationCompleteId";
    private static final String REGISTRATION_COMPLETE_TYPE = "registrationCompleteType";
    private static final String REGISTRATION_COMPLETE_USER_ID = "registrationCompleteUserId";
    private static final String REGISTRATION_COMPLETE_REG_DT = "registrationCompleteRegDT";

    private static final String NEWS_FEED_LAST_VISIBLE_ITEM_ID = "newsFeedLastVisibleItemId";
    private static final String LOCALE_LANG = "locale_language";

    private SharedPreferences shPr;
    private SharedPreferences.Editor editor;

    public PreferencesImpl(Context ctx) {
        shPr = ctx.getSharedPreferences("LikeCoinPreferences", Context.MODE_PRIVATE);
        editor = shPr.edit();
    }

    @Override
    public void setRegistrationCompleted(boolean registered) {
        editor.putBoolean(IS_REGISTRATION_COMPLETED, registered);
        editor.commit();
    }

    @Override
    public boolean isRegistrationCompleted() {
        return shPr.getBoolean(IS_REGISTRATION_COMPLETED, false);
    }

    @Override
    public void setUserDeviceToken(String userDeviceToken) {
        editor.putString(USER_DEVICE_TOKEN, userDeviceToken);
        editor.commit();
    }

    @Override
    public String getUserDeviceToken() {
        return shPr.getString(USER_DEVICE_TOKEN, StringUtils.EMPTY_STRING);
    }

    @Override
    public void setRegistrationCompleteId(String id) {
        editor.putString(REGISTRATION_COMPLETE_ID, id);
        editor.commit();
    }

    @Override
    public void setRegistrationCompleteType(String type) {
        editor.putString(REGISTRATION_COMPLETE_TYPE, type);
        editor.commit();
    }

    @Override
    public void setRegistrationCompleteUserId(String userId) {
        editor.putString(REGISTRATION_COMPLETE_USER_ID, userId);
        editor.commit();
    }

    @Override
    public void setRegistrationCompleteRegDT(Date regDT) {
        editor.putLong(REGISTRATION_COMPLETE_REG_DT, regDT.getTime());
        editor.commit();
    }

    @Override
    public String getRegistrationCompleteId() {
        return shPr.getString(REGISTRATION_COMPLETE_ID, StringUtils.EMPTY_STRING);
    }

    @Override
    public String getRegistrationCompleteType() {
        return shPr.getString(REGISTRATION_COMPLETE_TYPE, StringUtils.EMPTY_STRING);
    }

    @Override
    public String getRegistrationCompleteUserId() {
        return shPr.getString(REGISTRATION_COMPLETE_USER_ID, StringUtils.EMPTY_STRING);
    }

    @Override
    public long getRegistrationCompleteRegDT() {
        return shPr.getLong(REGISTRATION_COMPLETE_REG_DT, 0);
    }

    @Override
    public void removePreference(String preferenceKey) {
        editor.remove(preferenceKey).apply();
    }

    @Override
    public void clearAllPreferences() {
        editor.clear().commit();
    }

    @Override
    public void setNewsFeedLastVisibleItemId(String newsId) {
        editor.putString(NEWS_FEED_LAST_VISIBLE_ITEM_ID, newsId);
        editor.commit();
    }

    @Override
    public String getNewsFeedLastVisibleItemId() {
        return shPr.getString(NEWS_FEED_LAST_VISIBLE_ITEM_ID, StringUtils.EMPTY_STRING);
    }

    @Override
    public void setLanguage(String language) {
        editor.putString(LOCALE_LANG, language);
        editor.commit();
    }

    @Override
    public String getLanguage() {
        return shPr.getString(LOCALE_LANG, StringUtils.EMPTY_STRING);
    }
}