package com.solaitech.likecoin.data.models.push_notification;

import android.os.Parcel;
import android.os.Parcelable;

public class PushNotificationData implements Parcelable {

    private String typeId;
    private boolean display;

    private PushNotificationSourceEntityRef sourceEntityRef;

    public PushNotificationSourceEntityRef getSourceEntityRef() {
        return sourceEntityRef;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeParcelable(this.sourceEntityRef, flags);
    }

    public PushNotificationData() {
    }

    protected PushNotificationData(Parcel in) {
        this.sourceEntityRef = in.readParcelable(PushNotificationSourceEntityRef.class.getClassLoader());
    }

    public static final Creator<PushNotificationData> CREATOR = new Creator<PushNotificationData>() {
        @Override
        public PushNotificationData createFromParcel(Parcel source) {
            return new PushNotificationData(source);
        }

        @Override
        public PushNotificationData[] newArray(int size) {
            return new PushNotificationData[size];
        }
    };

    public String getTypeId() {
        return typeId;
    }

    public boolean isDisplay() {
        return display;
    }
}