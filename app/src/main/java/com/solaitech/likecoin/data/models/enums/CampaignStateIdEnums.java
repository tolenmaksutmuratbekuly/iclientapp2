package com.solaitech.likecoin.data.models.enums;

public class CampaignStateIdEnums {

    public static final String NOT_ACTIVE_YET = "0";
    public static final String ACTIVE = "1";
    public static final String EXPIRED = "2";
    public static final String SUSPENDED = "3";

//0 - еще не активный
//1 - активный
//2 - заэкспайрен
//3 - приостановлен

}