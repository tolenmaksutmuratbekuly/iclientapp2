package com.solaitech.likecoin.data.params;

public class BuyArticleParams {
    private String articleId;

    public BuyArticleParams(String articleId) {
        this.articleId = articleId;
    }
}
