package com.solaitech.likecoin.data.models.user;
public class UserLevel {

    private Integer likesCount;
    private Integer level;
    private Integer nextLevelLikesCount;
    private Integer nextLevelProgress;

    public Integer getLikesCount() {
        return likesCount;
    }

    public Integer getLevel() {
        return level;
    }

    public Integer getNextLevelLikesCount() {
        return nextLevelLikesCount;
    }

    public Integer getNextLevelProgress() {
        return nextLevelProgress;
    }

}