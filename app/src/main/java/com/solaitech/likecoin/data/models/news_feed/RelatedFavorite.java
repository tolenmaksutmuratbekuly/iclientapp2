package com.solaitech.likecoin.data.models.news_feed;

public class RelatedFavorite {
    String id;
    String entityTypeId;
    String entityId;
    Integer isFavorite;

    public String getId() {
        return id;
    }

    public String getEntityTypeId() {
        return entityTypeId;
    }

    public String getEntityId() {
        return entityId;
    }

    public Integer getIsFavorite() {
        return isFavorite;
    }

    public void setIsFavorite(Integer isFavorite) {
        this.isFavorite = isFavorite;
    }
}
