package com.solaitech.likecoin.data.repository.registration;

import com.solaitech.likecoin.data.models.registration.RegistrationComplete;
import com.solaitech.likecoin.data.models.registration.RegistrationStart;
import com.solaitech.likecoin.data.params.RegistrationCompleteParams;
import com.solaitech.likecoin.data.params.RegistrationStartParams;
import com.solaitech.likecoin.network.API;
import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class RegistrationRepositoryImpl implements RegistrationRepository {

    private API api;

    public RegistrationRepositoryImpl() {
        //do nothing
    }

    @Override
    public void setAPI(API api) {
        this.api = api;
    }

    @Override
    public Observable<RegistrationStart> registrationStart(RegistrationStartParams params) {
        return api
                .registrationStart(params)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    @Override
    public Observable<RegistrationComplete> registrationComplete(String userRegistrationToken, RegistrationCompleteParams params) {
        return api
                .registrationComplete(userRegistrationToken, params)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }
}