package com.solaitech.likecoin.data.repository.news_feed;

import android.support.annotation.NonNull;

import com.solaitech.likecoin.network.API;


public class NewsFeedRepositoryProvider {

    private static NewsFeedRepository repository;

    private NewsFeedRepositoryProvider() {
        //do nothing
    }

    @NonNull
    public static NewsFeedRepository provideRepository(API api) {
        if (repository == null) {
            repository = new NewsFeedRepositoryImpl();
        }
        repository.setAPI(api);
        return repository;
    }

    public static void setRepository(NewsFeedRepository repo) {
        repository = repo;
    }

}