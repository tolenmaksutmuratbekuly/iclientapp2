package com.solaitech.likecoin.data.models.contacts;

/**
 * Created by sultanbek on 10.04.17.
 */

public class Phone {

    private String number;
    private String type;

    public Phone(String number, String type) {
        this.number = number;
        this.type = type;
    }

    public String getNumber() {
        return number;
    }

    public String getType() {
        return type;
    }

    @Override
    public String toString() {
        return getNumber();
    }

}