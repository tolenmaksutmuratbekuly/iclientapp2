package com.solaitech.likecoin.data.repository.app_version_check;

import com.solaitech.likecoin.data.models.app_version_check.AppVersionChecking;
import com.solaitech.likecoin.network.API;
import rx.Observable;

public interface AppVersionCheckRepository {

    void setAPI(API api);

    Observable<AppVersionChecking> appVersionChecking(String os,
                                                      String appVersion,
                                                      String appType);

}