package com.solaitech.likecoin.data.repository.qr_code;

import android.graphics.Bitmap;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.MultiFormatWriter;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;

import com.solaitech.likecoin.data.params.QrBitmapParams;
import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Func0;
import rx.schedulers.Schedulers;

class QrCodeRepositoryImpl implements QrCodeRepository {

    private final static int QR_CODE_HEIGHT = 500;
    private final static int QR_CODE_WIDTH = 500;

    @Override
    public Observable<Bitmap> getQrBitmap(final QrBitmapParams params) {
        return Observable.defer(
                new Func0<Observable<Bitmap>>() {
                    @Override
                    public Observable<Bitmap> call() {
                        try {
                            Bitmap bitmap = textToImageEncode(params);
                            return Observable.just(bitmap);
                        } catch (WriterException e) {
                            e.printStackTrace();
                            return Observable.error(new Throwable());
                        }
                    }
                })
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    private Bitmap textToImageEncode(QrBitmapParams params) throws WriterException {
        BitMatrix bitMatrix;
        try {
            bitMatrix = new MultiFormatWriter().encode(
                    params.getContent(),
                    BarcodeFormat.DATA_MATRIX.QR_CODE,
                    params.getDisplayWidth(), params.getDisplayWidth(), null
            );

        } catch (IllegalArgumentException Illegalargumentexception) {

            return null;
        }

        int bitMatrixWidth = bitMatrix.getWidth();
        int bitMatrixHeight = bitMatrix.getHeight();

        int[] pixels = new int[bitMatrixWidth * bitMatrixHeight];

        for (int y = 0; y < bitMatrixHeight; y++) {
            int offset = y * bitMatrixWidth;

            for (int x = 0; x < bitMatrixWidth; x++) {
                pixels[offset + x] = bitMatrix.get(x, y) ? params.getBlackColor() : params.getWhiteColor();
            }
        }
        Bitmap bitmap = Bitmap.createBitmap(bitMatrixWidth, bitMatrixHeight, Bitmap.Config.ARGB_4444);
        //java.lang.IllegalArgumentException: abs(stride) must be >= width zxing
        //bitmap.setPixels(pixels, 0, 500, 0, 0, bitMatrixWidth, bitMatrixHeight);
        bitmap.setPixels(pixels, 0, bitMatrixWidth, 0, 0, bitMatrixWidth, bitMatrixHeight);
        return bitmap;
    }

}