package com.solaitech.likecoin.data.models.enums;

public class PartnerTypes {
    public static final String USER = "user";
    public static final String MERCHANT = "merchant";

    public static final int ROLES_AMOUNT = 3;
    public static final int MERCHANT_ROLE_ID = 4;
}