package com.solaitech.likecoin.data.params;

public class PushNotificationAddDeviceParams {

    private String platformId = "1"; //android
    private String deviceId; //playerId

    public PushNotificationAddDeviceParams(String deviceId) {
        this.deviceId = deviceId;
    }

}