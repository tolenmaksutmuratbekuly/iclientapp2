package com.solaitech.likecoin.data.params;

public class FavoriteParams {
    private int isFavorite;

    public FavoriteParams(int isFavorite) {
        this.isFavorite = isFavorite;
    }
}
