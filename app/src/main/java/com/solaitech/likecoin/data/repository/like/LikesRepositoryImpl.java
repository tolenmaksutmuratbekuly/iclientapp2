package com.solaitech.likecoin.data.repository.like;

import com.solaitech.likecoin.data.models.campaign.Comments;
import com.solaitech.likecoin.data.models.news_feed.NewsFeed;
import com.solaitech.likecoin.data.params.BuyArticleParams;
import com.solaitech.likecoin.data.params.FavoriteParams;
import com.solaitech.likecoin.data.params.Like4EntityParams;
import com.solaitech.likecoin.network.API;
import okhttp3.ResponseBody;
import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class LikesRepositoryImpl implements LikesRepository {

    private API api;

    public LikesRepositoryImpl() {
        //do nothing
    }

    @Override
    public void setAPI(API api) {
        this.api = api;
    }

    @Override
    public Observable<ResponseBody> sendLike(Object params) {
        return api
                .sendLike(params)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    @Override
    public Observable<ResponseBody> buyArticle(BuyArticleParams params) {
        return api
                .buyArticle(params)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    @Override
    public Observable<NewsFeed> favoriteState(String newsFeedId, FavoriteParams params) {
        return api
                .favorite(newsFeedId, params)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    @Override
    public Observable<NewsFeed> like4EntityNews(Like4EntityParams params) {
        return api
                .like4EntityNews(params)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    @Override
    public Observable<Comments> like4EntityComment(Like4EntityParams params) {
        return api
                .like4EntityComment(params)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }
}