package com.solaitech.likecoin.data.repository.contacts;

import android.content.ContentResolver;
import android.support.annotation.NonNull;

import com.solaitech.likecoin.network.API;

public class ContactsRepositoryProvider {

    private static ContactsRepository repository;

    private ContactsRepositoryProvider() {
        //do nothing
    }

    @NonNull
    public static ContactsRepository provideRepository(ContentResolver contentResolver) {
        if (repository == null) {
            repository = new ContactsRepositoryImpl();
        }
        repository.setContentResolver(contentResolver);
        return repository;
    }

    @NonNull
    public static ContactsRepository provideRepository(API api) {
        if (repository == null) {
            repository = new ContactsRepositoryImpl();
        }
        repository.setAPI(api);
        return repository;
    }

    public static void setRepository(ContactsRepository repo) {
        repository = repo;
    }

}