package com.solaitech.likecoin.data.models.article;

import com.solaitech.likecoin.data.models.campaign.Merchant;

import java.util.Date;

public class Article {
    private String id;
    private Date regDT;
    private String stateId;
    private Integer price;
    private String title;
    private String content;
    private Merchant merchant;
    private String merchantId;
    private String shortDescr;
    private String imgUrl;
    private Boolean isOwned;

    public String getId() {
        return id;
    }

    public Date getRegDT() {
        return regDT;
    }

    public String getStateId() {
        return stateId;
    }

    public Integer getPrice() {
        return price;
    }

    public String getTitle() {
        return title;
    }

    public String getContent() {
        return content;
    }

    public Merchant getMerchant() {
        return merchant;
    }

    public String getMerchantId() {
        return merchantId;
    }

    public String getShortDescr() {
        return shortDescr;
    }

    public void setOwned(Boolean owned) {
        isOwned = owned;
    }

    public Boolean getOwned() {
        return isOwned;
    }

    public String getImgUrl() {
        return imgUrl;
    }
}
