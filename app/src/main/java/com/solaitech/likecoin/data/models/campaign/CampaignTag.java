package com.solaitech.likecoin.data.models.campaign;

public class CampaignTag {

    private String id;
    private String name;
    private String parentId;

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getParentId() {
        return parentId;
    }

}