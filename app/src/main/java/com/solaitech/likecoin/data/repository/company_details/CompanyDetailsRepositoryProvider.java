package com.solaitech.likecoin.data.repository.company_details;

import android.support.annotation.NonNull;
import com.solaitech.likecoin.network.API;

public class CompanyDetailsRepositoryProvider {
    private static CompanyDetailsRepository repository;

    private CompanyDetailsRepositoryProvider() {
        //do nothing
    }

    @NonNull
    public static CompanyDetailsRepository provideRepository(API api) {
        if (repository == null) {
            repository = new CompanyDetailsRepositoryImpl();
        }
        repository.setAPI(api);
        return repository;
    }

    public static void setRepository(CompanyDetailsRepository repo) {
        repository = repo;
    }

}