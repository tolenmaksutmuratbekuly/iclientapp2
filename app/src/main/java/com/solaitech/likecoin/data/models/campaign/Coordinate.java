package com.solaitech.likecoin.data.models.campaign;

import com.google.android.gms.maps.model.LatLng;

public class Coordinate {
    private String address;
    private double latitude;
    private double longitude;

    public String getAddress() {
        return address;
    }

    public double getLatitude() {
        return latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public LatLng getLatLng() {
        return new LatLng(getLatitude(), getLongitude());
    }

}