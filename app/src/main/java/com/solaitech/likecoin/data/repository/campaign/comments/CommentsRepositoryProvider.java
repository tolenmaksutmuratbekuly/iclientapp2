package com.solaitech.likecoin.data.repository.campaign.comments;

import android.support.annotation.NonNull;

import com.solaitech.likecoin.network.API;

public class CommentsRepositoryProvider {
    private static CommentsRepository repository;

    private CommentsRepositoryProvider() {
        //do nothing
    }

    @NonNull
    public static CommentsRepository provideRepository(API api) {
        if (repository == null) {
            repository = new CommentsRepositoryImpl();
        }
        repository.setAPI(api);
        return repository;
    }

    public static void setRepository(CommentsRepository repo) {
        repository = repo;
    }
}