package com.solaitech.likecoin.data.models.campaign;

import android.os.Parcel;
import android.os.Parcelable;

public class Merchant implements Parcelable {
    private String id;
    private String title;
    private String shortDescr;
    private String fullDescr;
    private String url;
    private String logoUrl;
    private String logo4AmbassadorUrl;
    private MerchantContacts contacts;

    protected Merchant(Parcel in) {
        id = in.readString();
        title = in.readString();
        shortDescr = in.readString();
        fullDescr = in.readString();
        url = in.readString();
        logoUrl = in.readString();
        logo4AmbassadorUrl = in.readString();
        contacts = in.readParcelable(MerchantContacts.class.getClassLoader());
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(id);
        dest.writeString(title);
        dest.writeString(shortDescr);
        dest.writeString(fullDescr);
        dest.writeString(url);
        dest.writeString(logoUrl);
        dest.writeString(logo4AmbassadorUrl);
        dest.writeParcelable(contacts, flags);
    }

    public static final Creator<Merchant> CREATOR = new Creator<Merchant>() {
        @Override
        public Merchant createFromParcel(Parcel in) {
            return new Merchant(in);
        }

        @Override
        public Merchant[] newArray(int size) {
            return new Merchant[size];
        }
    };

    public String getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public String getShortDescr() {
        return shortDescr;
    }

    public String getFullDescr() {
        return fullDescr;
    }

    public String getUrl() {
        return url;
    }

    public String getLogoUrl() {
        return logoUrl;
    }

    public String getLogo4AmbassadorUrl() {
        return logo4AmbassadorUrl;
    }

    public MerchantContacts getContacts() {
        return contacts;
    }

    @Override
    public int describeContents() {
        return 0;
    }

}