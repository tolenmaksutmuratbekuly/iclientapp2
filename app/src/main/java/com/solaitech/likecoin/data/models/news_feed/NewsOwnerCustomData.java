package com.solaitech.likecoin.data.models.news_feed;

import java.util.ArrayList;
import java.util.List;

import com.solaitech.likecoin.data.models.campaign.Merchant;

public class NewsOwnerCustomData {

    private List<Merchant> merchants = new ArrayList<>();

    private Integer userLevel;

    private Merchant employerMerchant;

    public List<Merchant> getMerchants() {
        return merchants;
    }

    public Integer getUserLevel() {
        return userLevel;
    }

    public Merchant getEmployerMerchant() {
        return employerMerchant;
    }
}