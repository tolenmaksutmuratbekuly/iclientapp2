package com.solaitech.likecoin.data.repository.campaign.comments;

import java.util.List;

import com.solaitech.likecoin.data.models.campaign.Comments;
import com.solaitech.likecoin.data.params.CommentParams;
import com.solaitech.likecoin.network.API;
import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

class CommentsRepositoryImpl implements CommentsRepository {

    private API api;
    @Override
    public void setAPI(API api) {
        this.api = api;
    }

    @Override
    public Observable<List<Comments>> getComments(String objectEntityId, String objectEntityTypeId, String baseId, int rowCount, String direction) {
        return api
                .getComments(objectEntityId, objectEntityTypeId, baseId, rowCount, direction)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }

    @Override
    public Observable<Comments> postComment(CommentParams params) {
        return api
                .postComment(params)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }
}