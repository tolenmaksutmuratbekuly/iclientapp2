package com.solaitech.likecoin.data.repository.coupons;

import java.util.List;

import com.solaitech.likecoin.data.models.coupons.Coupon;
import com.solaitech.likecoin.data.models.coupons.CouponDetail;
import com.solaitech.likecoin.data.params.CouponPurchaseParams;
import com.solaitech.likecoin.network.API;
import rx.Observable;

public interface CouponsRepository {

    void setAPI(API api);

    Observable<List<Coupon>> getCoupons(String stateId);

    Observable<CouponDetail> getCouponById(String couponId);

    Observable<List<Coupon>> couponPurchase(CouponPurchaseParams params);

    Observable<Coupon> search(List<Object> objects, String searchQuery);

}
