package com.solaitech.likecoin.data.models.user_statement;

public class Partner {
    private String type;
    private String id;
    private String name;
    private String surname;
    private String imgUrl;
    private Integer userLevel;

    public String getType() {
        return type;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getSurname() {
        return surname;
    }

    public String getImgUrl() {
        return imgUrl;
    }

    public Integer getUserLevel() {
        return userLevel;
    }
}