package com.solaitech.likecoin.data.repository.registration;

import android.support.annotation.NonNull;

import com.solaitech.likecoin.network.API;

/**
 * Created by sultanbek on 22.11.16.
 */

public class RegistrationRepositoryProvider {

    private static RegistrationRepository repository;

    private RegistrationRepositoryProvider() {
        //do nothing
    }

    @NonNull
    public static RegistrationRepository provideRepository(API api) {
        if (repository == null) {
            repository = new RegistrationRepositoryImpl();
        }
        repository.setAPI(api);
        return repository;
    }

    public static void setRepository(RegistrationRepository repo) {
        repository = repo;
    }

}