package com.solaitech.likecoin.data.repository.contacts;

import android.content.ContentResolver;

import java.util.List;

import com.solaitech.likecoin.data.models.contacts.InvitationText;
import com.solaitech.likecoin.data.models.contacts.UserContact;
import com.solaitech.likecoin.data.models.contacts.UserContactLocal;
import com.solaitech.likecoin.data.params.contacts.ContactAddingParams;
import com.solaitech.likecoin.network.API;
import okhttp3.ResponseBody;
import rx.Observable;

public interface ContactsRepository {

    void setAPI(API api);

    void setContentResolver(ContentResolver contentResolver);

    Observable<List<UserContactLocal>> getLocalContacts();

    Observable<List<UserContact>> getContacts(boolean isOnlyRegistered);

    Observable<ResponseBody> addContacts(List<ContactAddingParams> userContacts);

    Observable<ResponseBody> deleteUserContacts(List<String> ids);

    Observable<InvitationText> getInvitationText();

}