package com.solaitech.likecoin.data.repository.brix;

import com.solaitech.likecoin.data.models.brix.BrixRate;
import com.solaitech.likecoin.data.models.brix.PurchaseBrix;
import com.solaitech.likecoin.data.params.brix.BuyBrixParams;
import com.solaitech.likecoin.network.API;

import okhttp3.ResponseBody;
import retrofit2.Response;
import rx.Observable;

public interface BrixRepository {

    void setAPI(API api);

    Observable<String> purchaseBrix(BuyBrixParams params);

    Observable<BrixRate> brixRate();
}