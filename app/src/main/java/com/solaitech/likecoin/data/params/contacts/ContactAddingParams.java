package com.solaitech.likecoin.data.params.contacts;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by amnazhatdinov on 4/18/17.
 */

public class ContactAddingParams {

    private String extId;
    private String updateDT;
    private String name;
    private List<String> values = new ArrayList<>();

    public ContactAddingParams(String extId, String name, String updateDT, List<String> values) {
        this.extId = extId;
        this.updateDT = updateDT;
        this.name = name;
        this.values = values;
    }

    @Override
    public String toString() {
        return extId + " : " + updateDT + " : " + name + " : " + values.toString() + " ::: ";
    }

}