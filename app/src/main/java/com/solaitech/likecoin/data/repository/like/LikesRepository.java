package com.solaitech.likecoin.data.repository.like;

import com.solaitech.likecoin.data.models.campaign.Comments;
import com.solaitech.likecoin.data.models.news_feed.NewsFeed;
import com.solaitech.likecoin.data.params.BuyArticleParams;
import com.solaitech.likecoin.data.params.FavoriteParams;
import com.solaitech.likecoin.data.params.Like4EntityParams;
import com.solaitech.likecoin.network.API;

import okhttp3.ResponseBody;
import rx.Observable;

public interface LikesRepository {

    void setAPI(API api);

    Observable<ResponseBody> sendLike(Object params);

    Observable<ResponseBody> buyArticle(BuyArticleParams params);

    Observable<NewsFeed> favoriteState(String newsFeedId, FavoriteParams params);

    Observable<NewsFeed> like4EntityNews(Like4EntityParams params);

    Observable<Comments> like4EntityComment (Like4EntityParams params);
}
