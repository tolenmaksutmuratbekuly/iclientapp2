package com.solaitech.likecoin.data.models.push_notification;

import android.os.Parcel;
import android.os.Parcelable;

public class PushNotificationSourceEntityRefType implements Parcelable {

    private String id;
    private String descr;

    public String getId() {
        return id;
    }

    public String getDescr() {
        return descr;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.id);
        dest.writeString(this.descr);
    }

    public PushNotificationSourceEntityRefType() {
    }

    protected PushNotificationSourceEntityRefType(Parcel in) {
        this.id = in.readString();
        this.descr = in.readString();
    }

    public static final Creator<PushNotificationSourceEntityRefType> CREATOR = new Creator<PushNotificationSourceEntityRefType>() {
        @Override
        public PushNotificationSourceEntityRefType createFromParcel(Parcel source) {
            return new PushNotificationSourceEntityRefType(source);
        }

        @Override
        public PushNotificationSourceEntityRefType[] newArray(int size) {
            return new PushNotificationSourceEntityRefType[size];
        }
    };

}