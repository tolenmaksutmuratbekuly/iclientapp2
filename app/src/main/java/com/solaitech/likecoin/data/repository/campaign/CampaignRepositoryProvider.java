package com.solaitech.likecoin.data.repository.campaign;

import android.support.annotation.NonNull;

import com.solaitech.likecoin.network.API;

public class CampaignRepositoryProvider {
    private static CampaignRepository repository;

    @NonNull
    public static CampaignRepository provideRepository(API api) {
        if (repository == null) {
            repository = new CampaignRepositoryImpl();
        }
        repository.setAPI(api);
        return repository;
    }

    public static void setRepository(CampaignRepository repo) {
        repository = repo;
    }
}