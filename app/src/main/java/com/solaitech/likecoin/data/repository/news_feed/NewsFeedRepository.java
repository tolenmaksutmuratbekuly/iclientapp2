package com.solaitech.likecoin.data.repository.news_feed;

import java.util.List;

import com.solaitech.likecoin.data.models.news_feed.NewsFeed;
import com.solaitech.likecoin.network.API;
import rx.Observable;

public interface NewsFeedRepository {

    void setAPI(API api);

    Observable<List<NewsFeed>> getNewsFeed(String baseId,
                                           int rowCount,
                                           String direction);

    Observable<NewsFeed> search(List<Object> objects, String searchQuery);

}