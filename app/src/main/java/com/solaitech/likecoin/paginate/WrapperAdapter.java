package com.solaitech.likecoin.paginate;

import android.support.v7.widget.RecyclerView;
import android.view.ViewGroup;

class WrapperAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private static final int ITEM_VIEW_TYPE_LOADING = Integer.MAX_VALUE - 50; // Magic
    private static final int ITEM_VIEW_TYPE_LOADING_UP = Integer.MAX_VALUE - 49; // Magic

    private final RecyclerView.Adapter wrappedAdapter;
    private final LoadingListItemCreator loadingListItemCreator;
    private boolean displayLoadingRow = true;
    private boolean displayLoadingRowUp = true;

    public WrapperAdapter(RecyclerView.Adapter adapter, LoadingListItemCreator creator) {
        this.wrappedAdapter = adapter;
        this.loadingListItemCreator = creator;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == ITEM_VIEW_TYPE_LOADING) {
            return loadingListItemCreator.onCreateViewHolder(parent, viewType);
        } else if (viewType == ITEM_VIEW_TYPE_LOADING_UP) {
            return loadingListItemCreator.onCreateViewHolder(parent, viewType);
        } else {
            return wrappedAdapter.onCreateViewHolder(parent, viewType);
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (isLoadingRow(position)) {
            loadingListItemCreator.onBindViewHolder(holder, position);
        } else if (isLoadingRowUp(position)) {
            loadingListItemCreator.onBindViewHolder(holder, position);
        } else {
            if (displayLoadingRowUp) {
                wrappedAdapter.onBindViewHolder(holder, position - 1);
            } else {
                wrappedAdapter.onBindViewHolder(holder, position);
            }
        }
    }

    @Override
    public int getItemCount() {
        int count = 0;

        if (displayLoadingRow) {
            count++;
        }

        if (displayLoadingRowUp) {
            count++;
        }

        return wrappedAdapter.getItemCount() + count;
    }

    @Override
    public int getItemViewType(int position) {
        if (isLoadingRow(position)) {
            return ITEM_VIEW_TYPE_LOADING;
        } else if (isLoadingRowUp(position)) {
            return ITEM_VIEW_TYPE_LOADING_UP;
        } else {
            if (displayLoadingRowUp) {
                return wrappedAdapter.getItemViewType(position - 1);
            } else {
                return wrappedAdapter.getItemViewType(position);
            }
        }
    }

    @Override
    public long getItemId(int position) {
        if (isLoadingRow(position)) {
            return RecyclerView.NO_ID;
        } else if (isLoadingRowUp(position)) {
            return RecyclerView.NO_ID;
        } else {
            if (displayLoadingRowUp) {
                return wrappedAdapter.getItemId(position - 1);
            } else {
                return wrappedAdapter.getItemId(position);
            }
        }
    }

    @Override
    public void setHasStableIds(boolean hasStableIds) {
        super.setHasStableIds(hasStableIds);
        wrappedAdapter.setHasStableIds(hasStableIds);
    }

    public RecyclerView.Adapter getWrappedAdapter() {
        return wrappedAdapter;
    }

    boolean isDisplayLoadingRow() {
        return displayLoadingRow;
    }

    boolean isDisplayLoadingRowUp() {
        return displayLoadingRowUp;
    }

    void displayLoadingRow(boolean displayLoadingRow) {
        if (this.displayLoadingRow != displayLoadingRow) {
            this.displayLoadingRow = displayLoadingRow;
            notifyDataSetChanged();
        }
    }

    void displayLoadingRowUp(boolean displayLoadingRowUp) {
        if (this.displayLoadingRowUp != displayLoadingRowUp) {
            this.displayLoadingRowUp = displayLoadingRowUp;
            notifyDataSetChanged();
        }
    }

    boolean isLoadingRow(int position) {
        return displayLoadingRow && position == getLoadingRowPosition();
    }

    private int getLoadingRowPosition() {
        return displayLoadingRow ? getItemCount() - 1 : -1;
    }

    boolean isLoadingRowUp(int position) {
        return displayLoadingRowUp && position == getLoadingRowPositionUp();
    }

    private int getLoadingRowPositionUp() {
        return displayLoadingRowUp ? 0 : -1;
    }

}