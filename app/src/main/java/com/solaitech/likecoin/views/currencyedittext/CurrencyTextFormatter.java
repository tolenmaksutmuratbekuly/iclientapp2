package com.solaitech.likecoin.views.currencyedittext;


public final class CurrencyTextFormatter {

    private CurrencyTextFormatter() {
    }

    public static String formatText(String val, Integer decimalDigits) {
        //special case for the start of a negative number
        if (val.equals("-")) return val;

        int currencyDecimalDigits = 0;
        if (decimalDigits != null) {
            currencyDecimalDigits = decimalDigits;
        }

        //retain information about the negativity of the value before stripping all non-digits
        boolean isNegative = false;
        if (val.contains("-")) {
            isNegative = true;
        }

        //strip all non-digits so the formatter always has a 'clean slate' of numbers to work with
        val = val.replaceAll("[^\\d]", "");
        //if there's nothing left, that means we were handed an empty string. Also, cap the raw input so the formatter doesn't break.
        if (!val.equals("")) {
            //if we're given a value that's smaller than our decimal location, pad the value.
            if (val.length() <= currencyDecimalDigits) {
                String formatString = "%" + currencyDecimalDigits + "s";
                val = String.format(formatString, val).replace(' ', '0');
            }

            //place the decimal in the proper location to construct a double which we will give the formatter.
            //This is NOT the decimal separator for the currency value, but for the double which drives it.
            String preparedVal = new StringBuilder(val).insert(val.length() - currencyDecimalDigits, '.').toString();

            //Convert the string into a double, which will be passed into the currency formatter
            double newTextValue = Double.valueOf(preparedVal);

            //reapply the negativity
            newTextValue *= isNegative ? -1 : 1;

            //finally, do the actual formatting
            val = String.valueOf(newTextValue);
        } else {
            throw new IllegalArgumentException("Invalid amount of digits found (either zero or too many) in argument val");
        }
        return val;
    }

}
