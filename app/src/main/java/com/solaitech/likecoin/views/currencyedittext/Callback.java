package com.solaitech.likecoin.views.currencyedittext;

import android.text.Editable;

public interface Callback {
    void afterTextChanged(String editable);
}
