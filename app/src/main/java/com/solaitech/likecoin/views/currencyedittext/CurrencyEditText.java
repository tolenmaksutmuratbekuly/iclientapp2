package com.solaitech.likecoin.views.currencyedittext;

import android.content.Context;
import android.text.InputType;
import android.util.AttributeSet;
import android.widget.EditText;

@SuppressWarnings("unused")
public class CurrencyEditText extends EditText {
    private long rawValue = 0L;

    private CurrencyTextWatcher textWatcher;
    private int decimalDigits = 0;

    private Callback callback;

    /*
    PUBLIC METHODS
     */
    public CurrencyEditText(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
        processAttributes(context, attrs);
    }

    public void setCallback(Callback callback) {
        this.callback = callback;
        if (textWatcher != null) {
            textWatcher.setCallback(callback);
        }
    }

    /**
     * Returns whether or not negative values have been allowed for this CurrencyEditText field
     */
    public boolean areNegativeValuesAllowed(){
        return false;
    }

    /**
     * Retrieve the raw value that was input by the user in their currencies lowest denomination (e.g. pennies).
     *
     * IMPORTANT: Remember that the location of the decimal varies by currentCurrency/Locale. This method
     *  returns the raw given value, and does not account for locality of the user. It is up to the
     *  calling application to handle that level of conversion.
     *  For example, if the text of the field is $13.37, this method will return a long with a
     *  value of 1337, as penny is the lowest denomination for USD. It will be up to the calling
     *  application to know that it needs to handle this value as pennies and not some other denomination.
     *
     * @return The raw value that was input by the user, in the lowest denomination of that users
     *  deviceLocale.
     */
    public long getRawValue() {
        return rawValue;
    }

    /**
     * Sets the value to be formatted and displayed in the CurrencyEditText view.
     *
     * @param value - The value to be converted, represented in the target currencies lowest denomination (e.g. pennies).
     */
    public void setValue(long value){
        String formattedText = format(value);
        setText(formattedText);
    }

    /**
     * Convenience method to get the current Hint back as a string rather than a CharSequence
     */
    public String getHintString() {
        CharSequence result = super.getHint();
        if (result == null) return null;
        return super.getHint().toString();
    }

    /**
     * Returns the number of decimal digits this CurrencyEditText instance is currently configured
     * to use. This value will be based on the current Currency object unless the value
     * was overwritten by setDecimalDigits().
     */
    public int getDecimalDigits(){
        return decimalDigits;
    }

    /**
     * Sets the number of decimal digits the currencyTextFormatter will use, overriding
     * the number of digits specified by the current currency. Note, however,
     * that calls to setCurrency() and configureViewForLocale() will override this value.
     *
     * Note that this method will also invoke the formatter to update the current view if the current
     * value is not null/empty.
     *
     * @param digits The number of digits to be shown following the decimal in the formatted text.
     *               Value must be between 0 and 340 (inclusive).
     * @throws IllegalArgumentException If provided value does not fall within the range (0, 340) inclusive.
     */
    public void setDecimalDigits(int digits){
        if(digits < 0 || digits > 340){
            throw new IllegalArgumentException("Decimal Digit value must be between 0 and 340");
        }
        decimalDigits = digits;

        refreshView();
    }

    /**
     * Pass in a value to have it formatted using the same rules used during data entry.
     * @param rawVal A long which represents the value you'd like formatted. It is expected that this value will be in the same format returned by the getRawValue() method (i.e. a series of digits, such as
     *            "1000" to represent "$10.00").
     * @return A deviceLocale-formatted string of the passed in value, represented as currentCurrency.
     */
    public String formatCurrency(long rawVal){
        return format(rawVal);
    }

    /*
    PRIVATE HELPER METHODS
     */

    private void refreshView(){
        setText(format(getRawValue()));
    }

    private String format(long val){
        return CurrencyTextFormatter.formatText(String.valueOf(val), decimalDigits);
    }

    private void init(){
        this.setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_FLAG_DECIMAL | InputType.TYPE_NUMBER_FLAG_SIGNED);
        initCurrencyTextWatcher();
    }

    private void initCurrencyTextWatcher(){
        if(textWatcher != null){
            this.removeTextChangedListener(textWatcher);
        }
        textWatcher = new CurrencyTextWatcher(this, callback);
        this.addTextChangedListener(textWatcher);
    }

    private void processAttributes(Context context, AttributeSet attrs){
        this.setDecimalDigits(2);
    }

    protected void setRawValue(long value) {
        rawValue = value;
    }
}
